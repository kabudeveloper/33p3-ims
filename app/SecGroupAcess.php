<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SecGroupAcess extends Model
{
    protected $table = 'sec_groupaccessrights';

    public $timestamps = false;
}
