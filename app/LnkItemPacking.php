<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LnkItemPacking extends Model
{
    protected $table = 'lnk_itempacking';
    protected $fillable = ['_packaging','_um','umvalue','innerquantity','description','_level'];
    public $timestamps = false;

    public function jopacking()
    {
        return $this->hasMany('App\JoDetail','packing');
    }


}
