<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadTime extends Model
{
    protected $table ='mst_leadtimes';
    public $timestamps = false;
}
