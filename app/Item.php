<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'mst_items';

    public $timestamps = false;
    protected $fillable = ['code','name','_class','_subclass','storagelocation','buying_um','production_um'];

    public function vendor(){
        return $this->belongsToMany('App\Vendor','lnk_vendoritems','_item','_vendor')->withPivot('cost', 'isactive');
    }

    public function category(){
        return $this->belongsTo('App\Category','_category');
    }
    
    public function lnkitem(){
        return $this->hasOne('App\LnkItemStocking','_item');
    }

    public function jodetails()
    {
        return $this->hasMany('App\JoDetail','_item');
    }

    public function itemname(){
        return $this->getAttribute('name');
    }

    public function uomconvertion(){
        return $this->belongsTo('App\Uomconvertion','_uomconvertion');
    }

}
