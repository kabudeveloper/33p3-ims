<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'sec_positions';
    public $timestamps = false;
    protected $fillable = ['description'];

    public function user(){
        return $this->hasMany('App\User','_position');
    }
}
