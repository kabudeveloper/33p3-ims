<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uomconvertion extends Model
{
    protected $table = 'mst_uomconvertions';

    public $timestamps = false;

    protected $fillable = ['buying_uom','production_uom','convertedvalues'];

    public function items()
    {
        return $this->hasMany('App\Item','_uomconvertion');
    }
}
