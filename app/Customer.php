<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'mst_customers';

    public $timestamps = false;
    protected $fillable = ['code','name','billing_address','delivery_address','tin','zipcode','_paymentterm','_currency'];

    public function paymentterm(){
        return $this->belongsTo('App\PaymentTerm','_paymentterm');
    }

    public function salesorder(){
        return $this->hasMany('App\SalesOrder','_customer');
    }

    public function contact(){
        return $this->hasMany('App\Contact','_mstlnk');
    }

}
