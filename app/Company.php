<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'mst_companies';

    public $timestamps = false;
    protected $fillable = ['code','glcode','name','address','tin','zipcode','email','telephone','fax','_currency'];

    public function branch(){
        return $this->hasMany('App\BranchCompany','_company');
    }
}
