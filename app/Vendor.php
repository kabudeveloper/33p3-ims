<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    protected $table = 'mst_vendors';

    public $timestamps = false;

    protected $fillable = ['code','name','address','tin','zipcode','_paymentterm','_type','deliveryleadtime'];


    public function item(){
        return $this->belongsToMany('App\Item','lnk_vendoritems','_vendor','_item')->withPivot('cost', 'isactive');
    }
}
