<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillOfMaterial extends Model
{
    //
    protected $table = 'lnk_itembom';

    public $timestamps = false;

    protected $fillable = ['packing','quantity'];

   
}
