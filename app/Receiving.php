<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receiving extends Model
{
    protected $table = 'trn_receiving';
    public $timestamps = false;
    protected $fillable = ['tdate','tmode','_branch','_vendor','ponumber','trrnumber', 'invoicenumber', 
    	'invoicedate', 'status', 'notes'

    ];


    public $primaryKey = 'tnumber';

    
}
