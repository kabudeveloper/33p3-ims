<?php

/*\DB::listen(function($query){

    \Log::info($query->sql);
});*/


Route::auth();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'DashBoardController@index');

    Route::get('contact/delcutcontact/{d}', 'ContactController@destroy');
    Route::get('rawmaterial/getcategory', 'RawMaterialController@getcategory');
    Route::get('rawmaterial/getclss', 'RawMaterialController@getclss');
    Route::get('rawmaterial/getsubclass', 'RawMaterialController@getsubclass');
    Route::get('rawmaterial/geteditcategory', 'RawMaterialController@geteditcategory');
    Route::get('finishedproduct/getcategory', 'FinishedProductController@getcategory');
    Route::get('finishedproduct/getclss', 'FinishedProductController@getclss');
    Route::get('finishedproduct/getsubclass', 'FinishedProductController@getsubclass');
    Route::get('finishedproduct/geteditcategory', 'FinishedProductController@geteditcategory');
    Route::get('supplies/getcategory', 'SuppliesController@getcategory');
    Route::get('supplies/getclss', 'SuppliesController@getclss');
    Route::get('supplies/getsubclass', 'SuppliesController@getsubclass');
    Route::get('supplies/geteditcategory', 'SuppliesController@geteditcategory');
    Route::get('pullcosting', 'FinishedProductController@pullcosting');
    Route::get('getselectedrmotherprod', 'OtherProductController@getselectedrmotherprod');
    Route::get('getpaymentterms', 'JobOrderController@getpaymentterms');
    Route::get('joborderdetail/{tnumber}/item/{jodetitem}/edit', 'JobOrderDetailsController@edit');
    Route::get('canceljoborder', 'JobOrderController@canceljoborder');
    Route::get('paymenttermsget', 'PurchasingController@paymenttermsget');
    Route::get('purchaseorderdetail/{tnumber}/item/{jodetitem}/edit', 'PurchaseOrderDetailsController@edit');
    Route::get('cancelpo', 'PurchasingController@cancelpo');
    Route::get('getpaymenttermstextfield', 'SalesOrderController@getpaymenttermstextfield');

    Route::get('getpurchaseorder', 'ReceivingController@getpurchaseorder');
    Route::get('receivingpodetail', 'ReceivingController@receivingpodetail');
    Route::get('cancelrr', 'ReceivingController@cancelrr');
    // Route::get('updatebal', ['as'=>'updatebal','uses'=>'ReceivingController@updatebal']);


    Route::get('production/{tnumber}/joborder', 'ProductionController@joborder');
    Route::get('production/{tnumber}/joborderbom', 'ProductionController@joborderbom');

    Route::get('getbranch', 'MaterialRequestController@getbranch');
    Route::get('materialrequest/{tnumber}/view', 'MaterialRequestController@view');
    Route::get('company/{tnumber}/view', 'CompaniesController@view');
    Route::get('branch/{tnumber}/view', 'BranchCompanyController@view');
    Route::get('location/{tnumber}/view', 'LocationController@view');
    Route::get('customer/{tnumber}/view', 'CustomerController@view');
    Route::get('paymentterm/{tnumber}/view', 'PaymentTermController@view');
    Route::get('vendors/{tnumber}/view', 'VendorController@view');
    Route::get('currency/{tnumber}/view', 'CurrencyController@view');
    Route::get('category/{tnumber}/view', 'CategoryController@view');
    Route::get('clss/{tnumber}/view', 'ClassController@view');
    Route::get('subclss/{tnumber}/view', 'SubClassController@view');
    Route::get('box/{tnumber}/view', 'BoxController@view');
    Route::get('packaging/{tnumber}/view', 'PackagingController@view');
    Route::get('unitofmeasure/{tnumber}/view', 'UnitOfMeasureController@view');
    Route::get('customer/{tnumber}/contact/{jodetitem}/view', 'ContactController@view');
    Route::get('vendors/{tnumber}/contact/{jodetitem}/view', 'VendorContactController@view');
    Route::get('rawmaterial/{tnumber}/view', 'RawMaterialController@view');
    Route::get('rawmaterial/{tnumber}/venditem/{jodetitem}/view', 'VendorItemController@view');
    Route::get('rawmaterial/{tnumber}/itempacking/{jodetitem}/view', 'ItemPackingController@view');
    Route::get('finishedproduct/{tnumber}/view', 'FinishedProductController@view');
    Route::get('finishedproduct/{tnumber}/otherproduct/{jodetitem}/view', 'OtherProductController@view');
    Route::get('finishedproduct/{tnumber}/bom/{jodetitem}/view', 'BillOfMaterialController@view');
    Route::get('finishedproduct/{tnumber}/finishedpacking/{jodetitem}/view', 'FinishedPackingController@view');
    Route::get('supplies/{tnumber}/view', 'SuppliesController@view');
    Route::get('supplies/{tnumber}/suppliesvendor/{jodetitem}/view', 'SuppliesVendorController@view');
    Route::get('supplies/{tnumber}/suppliesitempacking/{jodetitem}/view', 'SuppliesItemPackingController@view');

    Route::POST('purchasebalance/{id}/createpo','PurchaseBalanceController@createpo');
    Route::get('getaccess', 'UserGroupController@getaccess');
    Route::get('getaccesscreate', 'UserGroupController@getaccesscreate');
    Route::get('updateaccess', 'UserGroupController@updateaccess');
    Route::DELETE('contact/delcustcontact/{custcon}', 'ContactController@destroy');
    Route::PUT('customer/{custid}/custconupdate/{custcon}', 'ContactController@update');
    Route::PUT('vendors/{custid}/vendconupdate/{custcon}', 'VendorContactController@update');
    Route::DELETE('vendors/delvendcontact/{custcon}', '@destroy');
    Route::get('verifycode','VendorController@verifycode');
    Route::get('vendoritem-all-vendor','VendorController@vendorall');
    Route::POST('vendoritem-search','VendorController@vendorsearch');
    Route::POST('vendorsearchid','VendorController@vendorsearchid');
    Route::get('verifycoderawmat','ItemsController@verifycoderawmat');
    Route::get('findrawmaterial','ItemsController@findrawmaterial');
    Route::POST('rawmaterial-search','ItemsController@rawmaterialsearch');
    Route::POST('rawmaterialsearchid','ItemsController@rawmaterialsearchid');
    Route::get('otherproductverifycode','OtherProductController@otherproductverifycode');
    Route::post('otherproductsearch','OtherProductController@otherproductsearch');
    Route::get('finished-otherproduct','OtherProductController@otherproductall');
    Route::POST('otherproductsearchid','OtherProductController@otherproductsearchid');
    Route::POST('costingsave','FinishedProductController@costingsave');
    Route::POST('costingupdate','FinishedProductController@costingupdate');
    Route::POST('costchanges','RawMaterialController@costchanges');
    Route::get('getcostchanges','RawMaterialController@getcostchanges');
    Route::POST('suppliescostchanges','SuppliesController@suppliescostchanges');
    Route::get('suppliesgetcostchanges','SuppliesController@suppliesgetcostchanges');
    Route::get('getselectedrm', 'BillOfMaterialController@getselectedrm');
    Route::get('getdroppacking','BillOfMaterialController@getdroppacking');
    Route::PUT('resetpassword','UserSecurityController@resetpassword');
    Route::get('getselecteditempacking', 'JobOrderDetailsController@getselecteditempacking');
    Route::DELETE('joborderdetail/{tn}/jodetail/{item}', 'JobOrderDetailsController@destroy');
    Route::get('getjodsellingprice', 'JobOrderDetailsController@getjodsellingprice');
    Route::get('accessroles', 'UserGroupController@accessroles');
    Route::get('getposellingprice', 'PurchaseOrderDetailsController@getposellingprice');
    Route::DELETE('purchaseorderdetail/{tn}/podetail/{item}', 'PurchaseOrderDetailsController@destroy');
    Route::get('getrmcost', 'SalesOrderDetailController@getrmcost');
    Route::get('updateaup', 'SalesOrderDetailController@updateaup');
    Route::DELETE('salesorderdetail/{tn}/sod/{item}', 'SalesOrderDetailController@destroy');
    Route::get('salesorderdetail/{sodtnumber}/sodetail/{soditem}/edit', 'SalesOrderDetailController@edit');
    Route::get('getcurrencycustomer', 'SalesOrderController@currencycustomer');
    Route::get('getcurrencies', 'SalesOrderController@getcurrencies');
    Route::get('convertcurrency', 'SalesOrderController@convertcurrency');
    Route::get('currencyexcept', 'SalesOrderController@currencyexcept');
    Route::DELETE('currencyconvertion/{bcon}/convertion/{tarcon}', 'CurrencyconvertionController@destroy');
    Route::get('currencyconvertion/{cur1}', ['as'=>'createconvertions' ,'uses'=> 'CurrencyconvertionController@create']);
    Route::get('getcurrenciesrate', 'SalesOrderController@getcurrenciesrate');
    Route::get('getselectedrmformr', 'MaterialRequestDetailController@getselectedrmformr');


    Route::resource('company', 'CompaniesController');
    Route::resource('branch', 'BranchCompanyController');
    Route::resource('paymentterm', 'PaymentTermController');
    Route::resource('unitofmeasure', 'UnitOfMeasureController');
    Route::resource('customer', 'CustomerController');
    Route::resource('location', 'LocationController');
    Route::resource('box', 'BoxController');
    Route::resource('vendortype', 'VendorTypeController');
    Route::resource('vendors', 'VendorController');
    Route::resource('packaging', 'PackagingController');
    Route::resource('category', 'CategoryController');
    Route::resource('clss', 'ClassController');
    Route::resource('subclss', 'SubClassController');
    Route::resource('item', 'ItemsController');
    Route::resource('customer.contact', 'ContactController');
    Route::resource('vendors.contact', 'VendorContactController');
    Route::resource('rawmaterial', 'RawMaterialController');
    Route::resource('finishedproduct', 'FinishedProductController');
    Route::resource('rawmaterial.venditem', 'VendorItemController');
    Route::resource('rawmaterial.itempacking', 'ItemPackingController');
    Route::resource('finishedproduct.otherproduct', 'OtherProductController');
    Route::resource('finishedproduct.bom', 'BillOfMaterialController');
    Route::resource('finishedproduct.finishedpacking', 'FinishedPackingController');
    Route::resource('supplies', 'SuppliesController');
    Route::resource('supplies.suppliesvendor', 'SuppliesVendorController');
    Route::resource('supplies.suppliesitempacking', 'SuppliesItemPackingController');
    Route::resource('usercontrol', 'UserControlController');
    Route::resource('securityuser', 'UserSecurityController');
    Route::resource('usergroup', 'UserGroupController');
    Route::resource('joborder', 'JobOrderController');
    Route::resource('joborderdetail', 'JobOrderDetailsController');
    Route::resource('purchasing', 'PurchasingController');
    Route::resource('joborder.joborderbom', 'JobOrderBomController');
    Route::resource('purchaseorderdetail', 'PurchaseOrderDetailsController');
    Route::resource('purchasebalance', 'PurchaseBalanceController');
    Route::resource('poinstruction', 'PoInstructionController');
    Route::resource('salesorder', 'SalesOrderController');
    Route::resource('salesorderdetail', 'SalesOrderDetailController');
    Route::resource('positions', 'PositionController');
    Route::resource('currency', 'CurrencyController');
    Route::resource('currencyconvertion', 'CurrencyconvertionController');

    Route::resource('receiving', 'ReceivingController');
    Route::resource('production', 'ProductionController');
    Route::resource('materialrequest', 'MaterialRequestController');
    Route::resource('materialrequestdetail', 'MaterialRequestDetailController');

    Route::resource('unitofmeasureconvertion', 'UomconvertionController');
    Route::resource('processs', 'ProcessController');


    Route::get('dashboard/customer', 'DatatablesController@customer');
    Route::get('dashboard/company', 'DatatablesController@company');
    Route::get('dashboard/branch', 'DatatablesController@branch');
    Route::get('dashboard/paymentterm', 'DatatablesController@paymentterm');
    Route::get('dashboard/unitofmeasure', 'DatatablesController@unitofmeasure');
    Route::get('dashboard/locat', 'DatatablesController@locat');
    Route::get('dashboard/box', 'DatatablesController@box');
    Route::get('dashboard/vendortype', 'DatatablesController@vendortype');
    Route::get('dashboard/vendors', 'DatatablesController@vendors');
    Route::get('dashboard/packaging', 'DatatablesController@packaging');
    Route::get('dashboard/category', 'DatatablesController@category');
    Route::get('dashboard/clss', 'DatatablesController@clss');
    Route::get('dashboard/subclss', 'DatatablesController@subclss');
    Route::get('dashboard/item', 'DatatablesController@item');
    Route::get('customer/{d}/custcontact', 'DatatablesController@custcontact');
    Route::get('vendors/{d}/vendorcontact', 'DatatablesController@vendorcontact');
    Route::get('dashboard/rawmaterial', 'DatatablesController@rawmaterial');
    Route::get('dashboard/finishedproduct', 'DatatablesController@finishedproduct');
    Route::get('rawmaterial/{rid}/rawmatvenditem', 'DatatablesController@rawmatvenditem');
    Route::get('rawmaterial/{rid}/rawmatitempackaging', 'DatatablesController@rawmatitempackaging');
    Route::get('finishedproduct/{rid}/otherproducts', 'DatatablesController@otherproducts');
    Route::get('finishedproduct/{rid}/fpbom', 'DatatablesController@bom');
    Route::get('finishedproduct/{rid}/fppack', 'DatatablesController@finishedpacking');
    Route::get('process/getprocess', 'DatatablesController@process');
    Route::get('dashboard/supplies', 'DatatablesController@supplies');
    Route::get('supplies/{rid}/suppliesvend', 'DatatablesController@suppliesvend');
    Route::get('supplies/{rid}/suppliesitempack', 'DatatablesController@suppliesitempack');
    Route::get('dashboard/securityuser', 'DatatablesController@securityuser');
    Route::get('dashboard/usergroup', 'DatatablesController@usergroup');
    Route::get('dashboard/joborderlist', 'DatatablesController@joborderlist');
    Route::get('jocust', 'DatatablesController@jocust');
    Route::get('jodetailfinishprod', 'DatatablesController@jodetailfinishprod');
    Route::get('joborder/{item}/jodetaillist', 'DatatablesController@jodetaillist');
    Route::get('dashboard/purchaseorderlist', 'DatatablesController@purchaseorderlist');
    Route::get('povendor', 'DatatablesController@povendor');
    Route::get('purchasing/{item}/podetaillist', 'DatatablesController@podetaillist');
    Route::get('purchasebalance/{item}/pobaldetaillist', 'DatatablesController@pobaldetaillist');
    Route::get('getpurchasedetail/{vendor}', 'DatatablesController@podetailfinishprod');
    Route::get('purchasebalance/{id}/view', 'PurchaseBalanceController@show');
    Route::get('purchasing/{item}/poinstructions/{poid}', 'DatatablesController@poinstructions');
    Route::get('joborder/{tn}/joborderbomgetbom', 'DatatablesController@getbom');
    Route::get('jobomlookup', 'DatatablesController@jobomlookup');
    Route::get('dashboard/salesoderlist', 'DatatablesController@salesoderlist');
    Route::get('salesorder/{so}/sodetaillist', 'DatatablesController@sodetaillist');
    Route::get('dashboard/positions', 'DatatablesController@positions');
    Route::get('dashboard/currency', 'DatatablesController@currency');
    Route::get('conlist/{conid}', 'DatatablesController@currencylist');
    Route::get('getprocesses', 'DatatablesController@getprocesses');
    Route::get('dashboard/purchaseorderbalancelist', 'DatatablesController@purchaseorderbalancelist'); 

    Route::get('receiving/{item}/receivingpodetail', 'DatatablesController@receivingpodetail');
    Route::get('production/{item}/prodjobomlist', 'DatatablesController@prodjobomlist');
    Route::get('mrjolist', 'DatatablesController@mrjolist');
    Route::get('solist', 'DatatablesController@solist');
    Route::get('materialrequest/{item}/mrdetaillist', 'DatatablesController@mrdetaillist');
    Route::get('getmrdetail/{tnumber}', 'DatatablesController@mrdetailitems');

    Route::get('dashboard/uomconvertion', 'DatatablesController@uomconvertion');

    Route::get('dashboard', ['as'=>'dashboard' ,'uses'=> 'DashBoardController@index']);
    Route::get('customer', ['as'=>'customer' ,'uses'=> 'CustomerController@index']);
    Route::get('company', ['as'=>'company' ,'uses'=> 'CompaniesController@index']);
    Route::get('company/create', ['as'=>'create' ,'uses'=> 'CompaniesController@create']);
    Route::get('company/{id}', ['as'=>'update' ,'uses'=> 'CompaniesController@update']);
    Route::get('branch', ['as'=>'branch' ,'uses'=> 'BranchCompanyController@index']);
    Route::get('paymentterm', ['as'=>'paymentterm' ,'uses'=> 'PaymentTermController@index']);
    Route::get('unitofmeasure', ['as'=>'unitofmeasure' ,'uses'=> 'UnitOfMeasureController@index']);
    Route::get('customer', ['as'=>'customer' ,'uses'=> 'CustomerController@index']);
    Route::get('location', ['as'=>'location' ,'uses'=> 'LocationController@index']);
    Route::get('box', ['as'=>'box' ,'uses'=> 'BoxController@index']);
    Route::get('vendortype', ['as'=>'vendortype' ,'uses'=> 'VendorTypeController@index']);
    Route::get('vendors', ['as'=>'vendors' ,'uses'=> 'VendorController@index']);
    Route::get('packaging', ['as'=>'packaging' ,'uses'=> 'PackagingController@index']);
    Route::get('category', ['as'=>'category' ,'uses'=> 'CategoryController@index']);
    Route::get('clss', ['as'=>'clss' ,'uses'=> 'ClassController@index']);
    Route::get('subclss', ['as'=>'subclss' ,'uses'=> 'SubClassController@index']);
    Route::get('item', ['as'=>'item' ,'uses'=> 'ItemsController@index']);
    Route::get('contact', ['as'=>'contact' ,'uses'=> 'ContactController@index']);
    Route::get('rawmaterial', ['as'=>'rawmaterial' ,'uses'=> 'RawMaterialController@index']);
    Route::get('finishedproduct', ['as'=>'finishedproduct' ,'uses'=> 'FinishedProductController@index']);
    Route::get('supplies', ['as'=>'supplies' ,'uses'=> 'SuppliesController@index']);
    Route::get('usercontrol', ['as'=>'usercontrol' ,'uses'=> 'UserControlController@index']);
    Route::get('securityuser', ['as'=>'securityuser' ,'uses'=> 'UserSecurityController@index']);
    Route::get('usergroup', ['as'=>'usergroup' ,'uses'=> 'UserGroupController@index']);
    Route::get('joborder', ['as'=>'joborder' ,'uses'=> 'JobOrderController@index']);
    Route::get('joborderdetail', ['as'=>'joborderdetail' ,'uses'=> 'JobOrderDetailsController@index']);
    Route::get('submitjoborder', ['as'=>'submitjoborder' ,'uses'=> 'JobOrderController@submitjo']);
    Route::get('approvejoborder', ['as'=>'approvejoborder' ,'uses'=> 'JobOrderController@approvejo']);
    Route::get('rollbackjoborder', ['as'=>'rollbackjoborder' ,'uses'=> 'JobOrderController@rollbackjo']);
    Route::get('printjo', ['as'=>'printjo' ,'uses'=> 'JobOrderController@printjo']);
    Route::get('printjomgmt', ['as'=>'printmanagement' ,'uses'=> 'JobOrderController@printjomgmt']);
    Route::get('purchase', ['as'=>'purchase' ,'uses'=> 'PurchasingController@index']);
    Route::get('purchasebalance', ['as'=>'purchasebalance' ,'uses'=> 'PurchaseBalanceController@index']);
    Route::get('purchasing/create', ['as'=>'create' ,'uses'=> 'PurchasingController@create']);
    Route::get('purchaseorderdetail', ['as'=>'purchaseorderdetail' ,'uses'=> 'PurchaseOrderDetailsController@index']);
    Route::get('poinstruction', ['as'=>'poinstruction' ,'uses'=> 'PoInstructionController@index']);
    Route::get('purchasing/create', ['as'=>'create' ,'uses'=> 'PurchasingController@create']);
    Route::get('purchase/create', ['as'=>'create' ,'uses'=> 'PurchasingController@create']);
    Route::get('salesorder', ['as'=>'salesorder' ,'uses'=> 'SalesOrderController@index']);
    Route::get('approvesalesorder', ['as'=>'approvesalesorder' ,'uses'=> 'SalesOrderController@approvesalesorder']);
    Route::get('cancelsalesorder', ['as'=>'cancelsalesorder' ,'uses'=> 'SalesOrderController@cancelsalesorder']);
    Route::get('submitpo', ['as'=>'submitpo' ,'uses'=> 'PurchasingController@submitpo']);
    Route::get('approvepo', ['as'=>'approvepo' ,'uses'=> 'PurchasingController@approvepo']);
    Route::get('receivepo', ['as'=>'receivepo' ,'uses'=> 'PurchasingController@receivepo']);
    Route::get('rollbackpo', ['as'=>'rollbackpo' ,'uses'=> 'PurchasingController@rollbackpo']);
    Route::get('printpo', ['as'=>'printpo' ,'uses'=> 'PurchasingController@printpo']);
    Route::get('createtrr', ['as'=>'createtrr' ,'uses'=> 'PurchasingController@createtrr']);
    Route::get('printquotation', ['as'=>'printquotation' ,'uses'=> 'SalesOrderController@printsalesorderquotation']);
    Route::get('rollbacksalesorder', ['as'=>'rollbacksalesorder' ,'uses'=> 'SalesOrderController@rollbackso']);
    Route::get('submitsalesorder', ['as'=>'submitsalesorder' ,'uses'=> 'SalesOrderController@submitso']);
   
    
    //production routes
    Route::get('production', ['as'=>'production' ,'uses'=> 'ProductionController@index']);
    Route::get('dashboard/productionlist', 'DatatablesController@productionlist');
    Route::get('onproduction', ['as'=>'onproduction' ,'uses'=> 'ProductionController@onproduction']);
    Route::get('onhold', ['as'=>'onhold' ,'uses'=> 'ProductionController@onhold']);
    Route::get('complete', ['as'=>'complete' ,'uses'=> 'ProductionController@complete']);
    //production route end
    
    Route::get('positions', ['as'=>'positions' ,'uses'=> 'PositionController@index']);
    Route::get('currency', ['as'=>'currency' ,'uses'=> 'CurrencyController@index']);
    Route::get('genjoborder', ['as'=>'generatejoborder' ,'uses'=> 'SalesOrderController@genjoborder']);
    Route::get('printsalesorder', ['as'=>'printsalesorder' ,'uses'=> 'SalesOrderController@printsalesorder']);

    //receiving route
    Route::get('receiving', ['as'=>'receiving' ,'uses'=> 'ReceivingController@index']);
    Route::get('receiving/create', ['as'=>'create' ,'uses'=> 'ReceivingController@create']);
    Route::get('dashboard/receivinglist', 'DatatablesController@receivinglist');
    Route::get('receivingpolookup', 'DatatablesController@receivingpolookup');
    Route::get('submitrr', ['as'=>'submitrr' ,'uses'=> 'ReceivingController@submitrr']);
    Route::get('approverr', ['as'=>'approverr' ,'uses'=> 'ReceivingController@approverr']);
    Route::get('rollbackrr', ['as'=>'rollbackrr' ,'uses'=> 'ReceivingController@rollbackrr']);
    Route::get('createrr', ['as'=>'createrr' ,'uses'=> 'ReceivingController@createrr']);
    //production route end

    //material request route
    Route::get('materialrequest', ['as'=>'materialrequest' ,'uses'=> 'MaterialRequestController@index']);
    Route::get('materialrequest/create', ['as'=>'create' ,'uses'=> 'MaterialRequestController@create']);
    Route::get('dashboard/mrlist', 'DatatablesController@mrlist');
    Route::get('submitmr', ['as'=>'submitmr' ,'uses'=> 'MaterialRequestController@submitmr']);
    Route::get('approvemr', ['as'=>'approvemr' ,'uses'=> 'MaterialRequestController@approvemr']);
    Route::get('servemr', ['as'=>'servemr' ,'uses'=> 'MaterialRequestController@servemr']);
    Route::get('rollbackmr', ['as'=>'rollbackmr' ,'uses'=> 'MaterialRequestController@rollbackmr']);
    //material request route end

    Route::get('unitofmeasureconvertion', ['as'=>'unitofmeasureconvertion' ,'uses'=> 'UomconvertionController@index']);
    Route::post('updatecurrency', ['as'=>'updatecurr' ,'uses'=> 'CurrencyconvertionController@updatecurrency']);
    Route::get('vendorcontact', ['as'=>'vendorcontact' ,'uses'=> 'VendorContactController@index']);
    Route::get('currency', ['as'=>'currency' ,'uses'=> 'CurrencyController@index']);
    Route::get('vendoritem', ['as'=>'vendoritem' ,'uses'=> 'VendorItemController@index']);
    Route::get('supplies', ['as'=>'supplies' ,'uses'=> 'SuppliesController@index']);
    Route::post('store', 'SalesOrderController@store')->name('store');

    //process maintenance controller
    Route::get('process', ['as'=>'process' ,'uses'=> 'ProcessController@index']);
    Route::get('process/create', ['as'=>'create' ,'uses'=> 'ProcessController@create']);
    Route::get('process/getcategory', ['as'=>'getcategory', 'uses'=>'ProcessController@getcategory']);
    Route::post('process/createProcess', 'ProcessController@store');
    Route::get('process/{id}/view', 'ProcessController@view');
    Route::get('process/{id}/edit', 'ProcessController@edit');
    Route::post('process/{id}/delete', 'ProcessController@delete');
    Route::post('process/{id}/updateprocess', ['as' => 'updateprocess1','uses' => 'ProcessController@updateProcess']);

    //process finishedproduct controller
    Route::get('finishedproduct/{id}/addprocess', 'ProcessController@addProcess');
    Route::post('finishedproduct/{id}/storeprocess', 'ProcessController@storeprocess');
    Route::get('finishedproduct/{rid}/processcost', 'DatatablesController@processcost');
    Route::get('finishedproduct/{id}/viewprocess/{prcid}/view', 'ProcessController@viewprocess');
    Route::get('finishedproduct/{id}/editprocess/{prcid}/edit', ['as' => 'fnsheditproc', 'uses' =>'ProcessController@editprocess']);
    Route::post('updateprocess', 'ProcessController@updatepro')->name('updateprocess');
    Route::DELETE('finishedproduct/{id}/deleteprocess/{prcid}/delete', ['as' =>'delfinishedproc','uses' =>'ProcessController@destroy']);

    // Route::get('{id}/addprocess', 'ProcessController@addProcess');

     //print finish product route
     Route::get('printfinishproduct', ['as'=>'printfinishproduct' ,'uses'=> 'FinishedProductController@printfinishproduct']);
    //end print finish product route

     //badge
     
     //end badge

}); 