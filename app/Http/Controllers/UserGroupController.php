<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Permission;
use App\SecGroup;
use App\SecGroupAcess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use JavaScript;

class UserGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('usergroup.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        JavaScript::put([
            'APP_URL' => url('/'),
        ]);

        return view('usergroup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'description' => 'required|unique:sec_groups,description',
        ]);

        $group = new SecGroup($request->all());
        $group->description = strtoupper($request->description);
        $group->save();

        $permissions = Permission::get();

        foreach ($permissions as $permission) {
            $groupaccess = new SecGroupAcess;
            $groupaccess->_group = $group->id;
            $groupaccess->_restriction = $permission->id;
            $groupaccess->save();
        }

        JavaScript::put([
            'APP_URL' => url('/'),
            'groupid' => $group->id
        ]);

        return redirect('usergroup/' . $group->id . '/edit');

        // return redirect('usergroup');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = SecGroup::find($id);

        JavaScript::put([
            'APP_URL' => url('/'),
            'GROUP' => $group
        ]);

        return view('usergroup.edit', compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $group = SecGroup::find($id);
        $group->fill($request->all());
        $group->description = strtoupper($request->description);
        $group->save();

        return redirect('usergroup');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = SecGroup::find($id);
        $group->permission()->detach();
        SecGroup::find($id)->delete();
        return response()->json(['ok' => 'success']);
    }

    public function getaccesscreate(Request $request)
    {

        $roles = Permission::orderBy('hierarchystring')->get();
        $groups = SecGroup::where('id', $request->groupid)->get();
        $access = SecGroupAcess::where('_group', $request->groupid)->get();

        return response()->json(compact('roles', 'groups', 'access'));
    }

    public function getaccess(Request $request)
    {
        $roles = Permission::orderBy('hierarchystring')->get();
        $groups = SecGroup::where('id', $request->groupid)->get();
        $access = SecGroupAcess::where('_group', $request->groupid)->get();
        $g = SecGroup::where('id', $request->groupid)->first();
        $g->permission()->syncWithoutDetaching($roles);
        return response()->json(compact('roles', 'groups', 'access'));
    }

    public function accessroles(Request $request)
    {
        dd('ddd');
    }

    public function updateaccess(Request $request)
    {
      
        $groupid = $request->groupid;

        $resetpermission = SecGroup::find($groupid);

        foreach ($resetpermission->permission as $rp) {

            $r = $rp->pivot;
            $r->granted = false;
            $r->save();
        }

        if($request->has('selNodes')){
            foreach ($request->selNodes as $nodes) {
                if (strpos($nodes, '_') !== 0) {
                    $p = SecGroup::find($groupid)->permission()->where('id', $nodes)->first()->pivot;
                    $p->granted = true;
                    $p->save();
                }
            }
        }

        return response()->json(['ok' => 'success']);
    }
}