<?php

namespace App\Http\Controllers;

use App\Category;
use App\Clas;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Gate;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-class')) {
            abort(403);
        }
        return view('clss.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-class')) {
            abort(403);
        }

        $maincompany = Category::select('id','name')->get();

        return view('clss.create',compact('maincompany'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required|max:10|unique:mst_class,code',
            'name' => 'required',
            '_category' => 'required',


        ]);

        $cat_id = $request->_category;
        $cat_code = Category::where('id', $cat_id)->pluck('code')->first();

        $company = new Clas($request->all());
        $company->code = strtoupper($cat_code.'-'.$request->code);
        $company->name = strtoupper($request->name);
        $company->createdby= Auth::user()->id;
        $company->save();

        return redirect('clss');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-class')) {
            abort(403);
        }
        $company = Clas::find($id);
        /*$cat_code = Category::where('id', $company->_category)->pluck('code')->first();
        $catcode = $cat_code.'-';
        $classcode = str_replace($catcode,'',$company->code);*/
        $maincompany = Category::select('id','name')->get();
        return view('clss.edit',compact('company','maincompany'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'code' => 'required|max:10',
            'name' => 'required',
            '_category' => 'required',
        ]); 

        $company = Clas::find($id);
        $company->fill($request->all());
        /*$cat_id = $request->_category;
        $cat_code = Category::where('id', $cat_id)->pluck('code')->first();
        $company->code = strtoupper($cat_code.'-'.$request->code);*/
        $company->name = strtoupper($request->name);
        if(is_null($request->isactive)){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }

        $company->save();
        return redirect('clss');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-class')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);

        }
        $company = Clas::find($id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }

    public function view($id)
    {
        /*if (Gate::denies('edit-class')) {
            abort(403);
        }*/
        $company = Clas::find($id);
        /*$cat_code = Category::where('id', $company->_category)->pluck('code')->first();
        $catcode = $cat_code.'-';
        $classcode = str_replace($catcode,'',$company->code);*/
        $maincompany = Category::select('id','name')->get();
        return view('clss.view',compact('company','maincompany'));
    }
}
