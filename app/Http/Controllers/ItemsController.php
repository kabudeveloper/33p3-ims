<?php

namespace App\Http\Controllers;

use App\Category;
use App\Clas;
use App\Item;
use App\SubClas;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('item.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Item::select('code')->orderBy('id', 'desc')->first();
       /* $categories = Category::select('id','name')->get();
        $classess = Clas::select('id','name')->get();
        $subclassess = SubClas::select('id','name')->get();*/

        if($company == null){
            $code = str_pad(0 + 1,10,'0',STR_PAD_LEFT);
        }else{
            $code = str_pad($company->code + 1,10,'0',STR_PAD_LEFT);
        }

        return view('item.create',compact('code'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            '_category' => 'required',
            '_class' => 'required',
            '_subclass' => 'required'


        ]);



        $company = new Item($request->all());
        $company->code = $request->code;
        $company->createdby= Auth::user()->id;
        $company->save();

        return redirect('item');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Item::find($id);

        return view('item.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Item::find($id);
        $company->fill($request->all());
        $company->save();
        return redirect('item');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Item::find($id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }
    
    
    public function getcategory(){

        $categories = Category::select('id','name')->get();
        
        return response()->json(compact('categories'));

    }

    public function getclss(Request $request){


        $classess = Clas::select('id','name')
            ->where('_category',$request->catid)
            ->get();

        return response()->json(compact('classess'));

    }


    public function getsubclass(Request $request){
        $subclassess = SubClas::select('id','name')
            ->where('_class',$request->classid)
            ->get();

        return response()->json(compact('subclassess'));
    }


    public function geteditcategory(Request $request){
        
        $categories = Category::select('id','name')

            ->get();

        return response()->json(compact('categories'));
    }

    public function verifycoderawmat(Request $request)
    {

        $this->validate($request, [
            'rmcode' => 'required'

        ]);

        
        $rawmat = Category::select('id')->where('name','Raw Material')->first();
        //$item = Category::find($rawmat->id)->item()->select('id','code','name')->where('code',$request->rmcode)->first();
        $item = Item::whereIn('_category',$rawmat)
            ->where('code','=',$request->rmcode)
            ->where(function ($query) {
                $query->where(function($q){
                    $q->whereNull('isacombo');
                });
                $query->orWhere(function($q){
                    $q->where('isacombo',0);
                });
            })->first();


        return response()->json(compact('item'));
    }

    public function findrawmaterial(Request $request){

        
        $rawmat = Category::select('id')
            ->where('name','Raw Material')
            ->orWhere('name','Supplies')
            ->get();

        $vendor = Item::whereIn('_category',$rawmat)
               ->where('id','<>',$request->item)
                ->where(function ($query) {
                $query->where(function($q){
                    $q->whereNull('isacombo');
                });
                $query->orWhere(function($q){
                    $q->where('isacombo',0);
                });
            })

            ->get();
        return response()->json(compact('vendor'));
    }

    public function rawmaterialsearch(Request $reqquest)
    {
        $catid = Category::select('id')->where('name','Raw Material')->first();
        if (isset($reqquest->containssearch)) {
            if ($reqquest->criteria === 'vendorname') {
                $vendor = Item::where('name', 'LIKE', '%'.$reqquest->valuesearch.'%')
                    ->where('_category',$catid->id)
                    ->where(function ($query) {
                        $query->where(function($q){
                            $q->whereNull('isacombo');

                        });
                        $query->orWhere(function($q){
                            $q->where('isacombo',0);

                        });
                    })
                    ->get();
            }else if($reqquest->criteria === 'vendorcode'){
                $vendor = Item::where('code', 'LIKE', '%'.$reqquest->valuesearch.'%')
                    ->where('_category',$catid->id)
                    ->where(function ($query) {
                        $query->where(function($q){
                            $q->whereNull('isacombo');

                        });
                        $query->orWhere(function($q){
                            $q->where('isacombo',0);

                        });
                    })
                    ->get();
            }
        } else {
            if ($reqquest->criteria === 'vendorcode') {
                $vendor = Item::where('code', $reqquest->valuesearch)
                    ->where('_category',$catid->id)
                    ->where(function ($query) {
                        $query->where(function($q){
                            $q->whereNull('isacombo');

                        });
                        $query->orWhere(function($q){
                            $q->where('isacombo',0);

                        });
                    })
                    ->get();
            } else if($reqquest->criteria === 'vendorname'){
                $vendor = Item::where('name', $reqquest->valuesearch)
                    ->where('_category',$catid->id)
                    ->where(function ($query) {
                        $query->where(function($q){
                            $q->whereNull('isacombo');

                        });
                        $query->orWhere(function($q){
                            $q->where('isacombo',0);

                        });
                    })
                    ->get();
            }
        }

        return response()->json(compact('vendor'));

    }

    public function rawmaterialsearchid(Request $request){
        $vendor = Item::findOrFail($request->searchid);
        return response()->json(compact('vendor'));
    }
}
