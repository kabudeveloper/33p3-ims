<?php

namespace App\Http\Controllers;

use App\Currency;
use App\Currencyconvertion;
use Cartalyst\Converter\Laravel\Facades\Converter;
use Illuminate\Http\Request;
use App\Http\Requests;
use Gate;
use JavaScript;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-currency')) {
            abort(403);
        }
        return view('currency.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-currency')) {
            abort(403);
        }
        return view('currency.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'symbol' => 'required'
        ]);

        $currency = new Currency($request->all());
        $currency->name = strtoupper($request->name);
        $currency->save();
        return redirect()->action('CurrencyController@edit', [$currency->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-currency')) {
            abort(403);
        }

        $currency = Currency::find($id);
        JavaScript::put([
            'convertbase' => $id,
        ]);
        return view('currency.edit',compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        $currency = Currency::find($id);
        $currency->fill($request->all());
        $currency->name = strtoupper($request->name);
        $currency->save();
        return redirect('currency');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-currency')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
         Currency::find($id)->delete();

         $con = Currencyconvertion::where('base',$id);

          if($con){
              $con->delete();
          }
        return response()->json(['ok'=>'success']);
    }

    public function view($id)
    {
        /*if (Gate::denies('edit-currency')) {
            abort(403);
        }*/

        $currency = Currency::find($id);
        JavaScript::put([
            'convertbase' => $id,
        ]);
        return view('currency.view',compact('currency'));
    }
}
