<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

use App\Http\Requests;
use Gate;

class VendorContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

        if (Gate::denies('create-vendorcontact')) {
            abort(403);
        }
        $customer = ['id'=>$id];
        return view('vendorcontact.create',compact('customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$customer)
    {
        $this->validate($request,[
            'name' => 'required',
            'position' => 'required'

        ]);


        $company = new Contact($request->all());
        $company->name = strtoupper($request->name);
        $company->position = strtoupper($request->position);
        $company->type = 2; // 2 for vendor
        $company->_mstlnk = $customer;
        $company->save();

        return  redirect('vendors/'.$customer.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$conname)
    {
        if (Gate::denies('edit-vendorcontact')) {
            abort(403);
        }
        $company = Contact::find($conname);
        return view('vendorcontact.edit',compact('company','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id,$conname)
    {


        $company = Contact::find($conname);
        $company->fill($request->all());
        $company->name = strtoupper($request->name);
        $company->position = strtoupper($request->position);
        if($request->has('isactive')){
            $company->isprimary =  1;
        }else{
            $company->isprimary =  0;
        }

        $company->save();
        return redirect('vendors/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function destroy($id)
    {
        if (Gate::denies('destroy-vendorcontact')) {
            abort(403);
        }

        $company = Contact::find($id); //note id is name use because prim key is name in our model contact.
        $company->delete();
        return response()->json(['ok'=>'success']);
    }*/

    public function destroy($id)
    {
        if (Gate::denies('delete-vendorcontact')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        $company = Contact::where('name','=',$id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }

    public function view($id,$conname)
    {
        /*if (Gate::denies('edit-vendorcontact')) {
            abort(403);
        }*/
        $company = Contact::find($conname);
        return view('vendorcontact.view',compact('company','id'));
    }
}
