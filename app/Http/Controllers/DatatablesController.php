<?php

namespace App\Http\Controllers;


use App\Box;
use App\Category;
use App\Company;
use App\Currency;
use App\Currencyconvertion;
use App\Customer;
use App\Http\Requests;
use App\Item;
use App\MaterialRequest;
use App\Packaging;
use App\PaymentTerm;
use App\PoInstruction;
use App\Purchasing;
use App\PurchaseOrderDetails;
use App\Receiving;
use App\ReceivingDetails;
use App\Position;
use App\Process;
use App\LnkItemProcess;
use App\SecGroup;
use App\UnitOfMeasure;
use App\Uomconvertion;
use App\User;
use App\Vendor;
use App\VendorType;
use Carbon\Carbon;

use App\JobOrders;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Yajra\Datatables\Facades\Datatables;


class DatatablesController extends Controller
{

    public function customer()
    {

        $customer = Customer::select(['id', 'code', 'name', 'billing_address', 'delivery_address', 'tin', 'zipcode', 'isactive']);

        return Datatables::of($customer)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {

                return '<a href="customer/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="customer/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delcustomer"><i class="fa fa-trash-o delcustomer"  aria-hidden="true"></i> Delete1</a>
         
                ';

            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="customer/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="customer/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="customer/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="customer/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="customer/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="customer/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {

                return '<a href="customer/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="customer/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';

            })
            ->make(true);

    }


    public function company()
    {

        $company = Company::select(['id', 'code', 'name', 'address', 'tin', 'zipcode', 'isactive']);

        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {

                return '<a href="company/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="company/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delcompany"><i class="fa fa-trash-o delcompany" id="delcompany" aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="company/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="company/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="company/' . $c->id . '/edit" class="btn btn-xs btn-danger dim delcompany"><i class="fa fa-trash-o delcompany" id="delcompany" aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="company/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="company/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="company/' . $c->id . '/edit" class="btn btn-xs btn-danger dim delcompany"><i class="fa fa-trash-o delcompany" id="delcompany" aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {

                return '<a href="company/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="company/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })

            ->make(true);
    }

    public function branch()
    {

        $company = DB::table('mst_branches as br')
            ->join('mst_companies as comp', 'br._company', '=', 'comp.id')
            ->select([
                'br.id',
                'br.code',
                'br.name as branchname',
                'br.address',
                'br.tin',
                'br.zipcode',
                'br.isactive',
                'comp.name as companyname'
            ]);


        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {

                    return '<a href="branch/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="branch/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delbranch"><i class="fa fa-trash-o " id="delbranch" aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="branch/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="branch/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="branch/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " id="delbranch" aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="branch/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="branch/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="branch/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " id="delbranch" aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {

                return '<a href="branch/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="branch/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);


    }


    public function paymentterm()
    {
        $company = PaymentTerm::select(['id', 'code', 'description', 'factor', 'isactive']);

        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="paymentterm/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="paymentterm/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delpaymentterm"><i class="fa fa-trash-o " id="delpaymentterm" aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="paymentterm/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="paymentterm/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="paymentterm/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="paymentterm/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="paymentterm/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="paymentterm/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="paymentterm/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="paymentterm/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);

    }

    public function unitofmeasure()
    {
        $company = UnitOfMeasure::select(['id', 'code', 'name', 'isactive']);

        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="unitofmeasure/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="unitofmeasure/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delunitofmeasure"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="unitofmeasure/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="unitofmeasure/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="unitofmeasure/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="unitofmeasure/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="unitofmeasure/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="unitofmeasure/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="unitofmeasure/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="unitofmeasure/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);
    }

    public function locat()
    {

        $company = DB::table('mst_branches as br')->join('mst_locations as loc', 'loc._branch', '=', 'br.id')
            ->join('mst_locationtypes as loctype', 'loc._type', '=', 'loctype.id')
            ->select(['loc.id', 'loc.code', 'br.name as branch', 'loc.name', 'loc.glcode', 'loctype.description', 'loc.isactive']);


        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="location/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="location/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim dellocation"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="location/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="location/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="location/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="location/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="location/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="location/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="location/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="location/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);
    }


    public function box()
    {
        $company = Box::select(['id', 'code', 'name', 'height', 'width', 'length', 'isactive']);

        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="box/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="box/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delbox"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="box/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="box/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="box/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="box/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="box/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="box/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="box/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="box/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);
    }


    public function vendortype()
    {
        $company = VendorType::select(['id', 'code', 'name']);

        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->addColumn('action', function ($c) {
                return '<a href="vendortype/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delvendortype"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);
    }


    public function vendors()
    {
        $company = DB::table('mst_vendors as vend')->join('mst_paymentterms as term', 'vend._paymentterm', '=',
            'term.id')
            ->join('mst_vendortypes as types', 'vend._type', '=', 'types.id')
            ->select([
                'vend.id',
                'vend.code',
                'vend.name',
                'vend.address',
                'term.description',
                'types.name as vtype',
                'vend.tin',
                'vend.zipcode',
                'vend.isactive'
            ]);


        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="vendors/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="vendors/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delvendors"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete1</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="vendors/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="vendors/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="vendors/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="vendors/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="vendors/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="vendors/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="vendors/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="vendors/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);
    }

    public function packaging()
    {
        $company = Packaging::select(['id', 'code', 'name', 'isactive']);

        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="packaging/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="packaging/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delpackaging"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="packaging/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="packaging/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="packaging/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="packaging/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="packaging/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="packaging/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="packaging/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="packaging/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);
    }

    public function category()
    {
        $company = Category::select(['id', 'code', 'name', 'glcode', 'isactive']);

        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="category/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="category/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delcategory"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="category/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="category/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="category/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="category/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="category/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="category/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="category/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="category/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);
    }


    public function clss()
    {

        $company = DB::table('mst_class as clss')->join('mst_categories as cat', 'clss._category', '=', 'cat.id')
            ->select(['clss.id', 'clss.code', 'clss.name', 'cat.name as categname', 'clss.glcode', 'clss.isactive']);


        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="clss/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="clss/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delcss"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="clss/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="clss/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="clss/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="clss/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="clss/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="clss/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="clss/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="clss/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);

    }

    public function subclss()
    {

        $company = DB::table('mst_subclass as subclss')->join('mst_class as clss', 'subclss._class', '=', 'clss.id')
            ->select(['subclss.id', 'subclss.code', 'subclss.name', 'clss.name as clssname', 'subclss.glcode', 'subclss.isactive']);


        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="subclss/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="subclss/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delsubcss"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="subclss/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="subclss/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="subclss/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="subclss/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="subclss/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="subclss/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="subclss/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="subclss/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);

    }

    public function item()
    {

        $company = DB::table('mst_items as itm')->join('mst_categories as cat', 'itm._category', '=', 'cat.id')
            ->join('mst_class as clss', 'itm._class', '=', 'clss.id')
            ->join('mst_subclass as subclss', 'itm._subclass', '=', 'subclss.id')
            ->select([
                'itm.id',
                'itm.code',
                'itm.name',
                'cat.name as categname',
                'clss.name as clssname',
                'subclss.name as subclssname'
            ]);


        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->addColumn('action', function ($c) {
                return '<a href="item/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delitem"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);

    }

    public function custcontact($customerid)
    {

        $company = DB::table('mst_customers as customer')->join('mst_contacts as contact', 'customer.id', '=',
            'contact._mstlnk')
            ->where('contact._mstlnk', '=', $customerid)
            ->select([
                'customer.id',
                'contact.name',
                'contact.position',
                'contact.mobile',
                'contact.landline',
                'contact.fax',
                'contact.email',
                'contact.isprimary'
            ]);

        return Datatables::of($company)
            ->addColumn('action', function ($c) {
                return '<a href="contact/' . $c->name . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="contact/' . $c->name . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                  <a href="#" id="' . $c->name . '" class="btn btn-xs btn-danger dim delcustcontact"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->editColumn('isprimary', function ($data) {
                if ($data->isprimary) {
                    return '<i class="fa fa-check text-navy"></i>';
                } else {
                    return '<i class="fa fa-times-circle text-navy-red" aria-hidden="true"></i>';
                }
            })
            ->make(true);

    }

    public function vendorcontact($vendorid)
    {
        $company = DB::table('mst_vendors as vendor')->join('mst_contacts as contact', 'vendor.id', '=',
            'contact._mstlnk')
            ->where('contact._mstlnk', '=', $vendorid)
            ->select([
                'vendor.id',
                'contact.name',
                'contact.position',
                'contact.mobile',
                'contact.landline',
                'contact.fax',
                'contact.email',
                'contact.isprimary'
            ]);

        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->addColumn('action', function ($c) {
                return '<a href="contact/' . $c->name . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="contact/' . $c->name . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->name . '" class="btn btn-xs btn-danger dim delvendorcontact"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->editColumn('isprimary', function ($data) {
                if ($data->isprimary) {
                    return '<i class="fa fa-check text-navy"></i>';
                } else {
                    return '<i class="fa fa-times-circle text-navy-red" aria-hidden="true"></i>';
                }
            })
            ->make(true);

    }

    public function rawmaterial()
    {

        $company = DB::table('mst_items as itm')->join('mst_categories as cat', 'itm._category', '=', 'cat.id')
            ->join('mst_class as clss', 'itm._class', '=', 'clss.id')
            ->join('mst_subclass as subclss', 'itm._subclass', '=', 'subclss.id')
            ->where('cat.name', '=', 'Raw Material')
            ->select([
                'itm.id',
                'itm.code',
                'itm.name',
                'cat.name as categname',
                'clss.name as clssname',
                'subclss.name as subclssname',
                'itm.isactive'
            ]);


        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="rawmaterial/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="rawmaterial/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delrawmat"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="rawmaterial/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="rawmaterial/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="rawmaterial/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="rawmaterial/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="rawmaterial/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="rawmaterial/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="rawmaterial/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="rawmaterial/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);

    }

    public function finishedproduct()
    {

        $company = DB::table('mst_items as itm')->join('mst_categories as cat', 'itm._category', '=', 'cat.id')
            ->join('mst_class as clss', 'itm._class', '=', 'clss.id')
            ->join('mst_subclass as subclss', 'itm._subclass', '=', 'subclss.id')
            ->where('cat.name', '=', 'Finished Product')
            ->select([
                'itm.id',
                'itm.code',
                'itm.name',
                'cat.name as categname',
                'clss.name as clssname',
                'subclss.name as subclssname',
                'itm.isactive'
            ]);


        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="finishedproduct/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="finishedproduct/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delfinished"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="finishedproduct/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="finishedproduct/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="finishedproduct/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="finishedproduct/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="finishedproduct/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="finishedproduct/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="finishedproduct/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="finishedproduct/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);

    }


    public function rawmatvenditem($rawmat)
    {

        $company = DB::table('mst_vendors as vendor')
            ->join('lnk_vendoritems as lnkvenditems', 'vendor.id', '=', 'lnkvenditems._vendor')
            ->where('lnkvenditems._item', '=', $rawmat)
            ->select([
                'vendor.code',
                'vendor.name',
                'lnkvenditems._item',
                'lnkvenditems._vendor',
                'lnkvenditems.cost',
                'lnkvenditems.isactive'
            ]);
        

        
          

        /* $company = Item::where('id',$rawmat)->with('vendor')->get();*/


        return Datatables::of($company)
            ->editColumn('isactive', function ($data) {
                if ($data->isactive) {
                    return '<i class="fa fa-check text-navy"></i>';
                } else {
                    return '<i class="fa fa-times-circle text-navy-red" aria-hidden="true"></i>';
                }
            })
            ->addColumn('USD', function($usd){
                $d = Currencyconvertion::select('rate')
                    ->where('target',1)
                    ->where('base',3)          
                    ->first();
               
                return (float)$usd->cost / (float)$d->rate;
            })
            ->addColumn('action', function ($c) {
                return '<a href="' . URL::to('/rawmaterial/' . $c->_item . '/venditem/' . $c->_vendor . '/view') . '" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="' . URL::to('/rawmaterial/' . $c->_item . '/venditem/' . $c->_vendor . '/edit') . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="' . URL::to('/rawmaterial/' . $c->_item . '/venditem/' . $c->_vendor) . '" class="btn btn-xs btn-danger dim delrawmatvenditem"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);
    }


    public function rawmatitempackaging($rawmat)
    {
        $company = DB::table('mst_packaging as packaging')
            ->join('lnk_itempacking as itempacking', 'packaging.id', '=', 'itempacking._packaging')
            ->join('mst_unitofmeasures as um', 'um.id', '=', 'itempacking._um')
            ->where('itempacking._item', '=', $rawmat)
            ->select([
                'itempacking.id',
                'itempacking._item',
                'itempacking.description',
                'itempacking.umvalue',
                'itempacking.innerquantity',
                'itempacking._level',
                'um.code as umcode',
                'packaging.code'
            ]);


        return Datatables::of($company)
            ->addColumn('action', function ($c) {
                return '<a href="' . URL::to('/rawmaterial/' . $c->_item . '/itempacking/' . $c->id . '/view') . '" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="' . URL::to('/rawmaterial/' . $c->_item . '/itempacking/' . $c->id . '/edit') . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delrawmatitempacking"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);
    }

    public function otherproducts($otherproduct)
    {

        /*
               $company = DB::table('lnk_itemcomposition as itemcomp')
                    ->join('mst_items as item','item.id', '=', 'itemcomp._subitem' )
                    ->join('lnk_itempacking as itempacking','itemcomp.packing', '=', 'itempacking.id' )
                    ->join('lnk_itemcosting as itemcost','itemcomp._subitem', '=', 'itemcost._item' )
                    ->join('lnk_itembom as bom','itemcost._item','=','bom._item')
                    ->join('ctl_averagecost as ac','bom._subitem','=','ac._item')
                    ->where('itemcomp._item','=',$otherproduct)

                    ->select([
                       'itemcomp._item',
                       'itemcomp._subitem',
                       'item.name',
                       'itempacking.description',
                       'itemcomp.quantity',
                        \DB::raw('( SUM(ac.cost) +  itemcost.omcost + itemcost.lbcost + itemcost.ovcost) as unitprice'),
                        \DB::raw('( itemcomp.quantity * ( SUM(ac.cost) + itemcost.omcost + itemcost.lbcost + itemcost.ovcost)) as totalprice')

                    ])->groupBy('bom._item','itemcomp.quantity','itemcost.omcost','itemcost.lbcost','itemcost.ovcost'
                       ,'itemcomp._item','itemcomp._subitem','item.name','itempacking.description');*/

        $company = DB::table('lnk_itemcomposition as ic')
            ->join('mst_items as i', 'ic._subitem', '=', 'i.id')
            ->join('lnk_itempacking as ip', 'ic.packing', '=', 'ip.id')
            ->join('vw_costprice as cp', 'i.id', '=', 'cp._item')
            ->where('ic._item', $otherproduct)
            ->select([
                'ic._item',
                'ic._subitem',
                'i.name',
                'ip.description',
                'ic.quantity',
                'cp.costprice as unitprice',
                \DB::raw('cp.costprice * ic.quantity as totalprice')
            ]);


        return Datatables::of($company)
            ->editColumn('totalprice', function ($data) {
                return number_format($data->totalprice, 2, '.', ',');
            })
            ->editColumn('unitprice', function ($data) {
                return number_format($data->unitprice, 2, '.', ',');
            })
            ->addColumn('action', function ($c) {
                return '<a href="' . URL::to('/finishedproduct/' . $c->_item . '/otherproduct/' . $c->_subitem . '/view') . '" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="' . URL::to('/finishedproduct/' . $c->_item . '/otherproduct/' . $c->_subitem . '/edit') . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="' . URL::to('/finishedproduct/' . $c->_item . '/otherproduct/' . $c->_subitem) . '" class="btn btn-xs btn-danger dim delfinishedotherproduct"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);

    }

    public function bom($finishedproduct)
    {

        $company = DB::table('mst_items as item')
            ->join('lnk_itembom as itembom', 'item.id', '=', 'itembom._subitem')
            ->join('lnk_itempacking as itempacking', 'itempacking.id', '=', 'itembom.packing')
            ->join('vw_rmcost as avgcost', function ($q) {
                $q->on('avgcost._item', '=', 'itembom._subitem')
                    ->on('itembom.packing', '=', 'avgcost.packing');
            })
            ->where('itembom._item', '=', $finishedproduct)
            ->select([
                'itembom._item',
                'item.name',
                'itempacking.description',
                'itembom.quantity',
                'itembom._subitem',
                'avgcost.cost',
                'itembom.directrm',
                \DB::raw('( itembom.quantity * avgcost.cost ) as totalcost')
            ]);


        return Datatables::of($company)
            ->editColumn('totalcost', function ($data) {
                return number_format($data->totalcost, 2, '.', ', ');
            })
            ->editColumn('cost', function ($data) {
                return number_format($data->cost, 2, '.', ', ');
            })
            ->editColumn('directrm', function ($data) {
                if ($data->directrm) {
                    return '<i class="fa fa-check text-navy"></i>';
                } else {
                    return '<i class="fa fa-times-circle text-navy-red" aria-hidden="true"></i>';
                }
            })
            ->addColumn('USD', function($usd){
                $d = Currencyconvertion::select('rate')
                    ->where('target',1)
                    ->where('base',3)          
                    ->first();
               
                return (float)$usd->cost / (float)$d->rate;
            })
            ->addColumn('totalUSD', function($totalusd){
                $d = Currencyconvertion::select('rate')
                    ->where('target',1)
                    ->where('base',3)          
                    ->first();
               
                return (float)$totalusd->totalcost / (float)$d->rate;
            })
            ->addColumn('action', function ($c) {

                return '<a href="' . URL::to('/finishedproduct/' . $c->_item . '/bom/' . $c->_subitem . '/view') . '" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="' . URL::to('/finishedproduct/' . $c->_item . '/bom/' . $c->_subitem . '/edit') . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="' . URL::to('/finishedproduct/' . $c->_item . '/bom/' . $c->_subitem) . '" class="btn btn-xs btn-danger dim delfinishedprodbom"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);

    }

    public function finishedpacking($fpid)
    {


        $company = DB::table('lnk_itempacking as itempacking')
            ->join('mst_packaging as packaging', 'itempacking._packaging', '=', 'packaging.id')
            ->join('mst_unitofmeasures as um', 'um.id', '=', 'itempacking._um')
            ->where('itempacking._item', '=', $fpid)
            ->select([
                'itempacking.id',
                'itempacking._item',
                'itempacking.description',
                'packaging.code',
                'um.code as umcode',
                'itempacking.umvalue',
                'itempacking.innerquantity',
                'itempacking._level'
            ]);


        return Datatables::of($company)
            ->addColumn('action', function ($c) {

                return '<a href="' . URL::to('/finishedproduct/' . $c->_item . '/finishedpacking/' . $c->id . '/view') . '" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="' . URL::to('/finishedproduct/' . $c->_item . '/finishedpacking/' . $c->id . '/edit') . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="' . URL::to('/finishedproduct/' . $c->_item . '/finishedpacking/' . $c->id) . '" class="btn btn-xs btn-danger dim delfinishedpacking"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);

    }

    public function supplies()
    {

        $company = DB::table('mst_items as itm')->join('mst_categories as cat', 'itm._category', '=', 'cat.id')
            ->join('mst_class as clss', 'itm._class', '=', 'clss.id')
            ->join('mst_subclass as subclss', 'itm._subclass', '=', 'subclss.id')
            ->where('cat.name', '=', 'Supplies')
            ->select([
                'itm.id',
                'itm.code',
                'itm.name',
                'cat.name as categname',
                'clss.name as clssname',
                'subclss.name as subclssname',
                'itm.isactive'
            ]);


        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            /*->addColumn('action', function ($c) {
                return '<a href="supplies/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="supplies/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delsupplies"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })*/
            /*->editColumn('action',function($c){
                if($c->isactive == 1){
                    return '<a href="supplies/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="supplies/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="supplies/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Deactivate</a>';
                }elseif( $c->isactive == 0){
                    return '<a href="supplies/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                    <a href="supplies/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                    <a href="supplies/' . $c->id . '/edit" class="btn btn-xs btn-danger dim"><i class="fa fa-trash-o " aria-hidden="true"></i> Activate</a>';
                }
                //return 'Status Unknown';
            })*/
            ->addColumn('action', function ($c) {
                return '<a href="supplies/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="supplies/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);

    }

    public function suppliesvend($supplyid)
    {

        $company = DB::table('mst_vendors as vendor')
            ->join('lnk_vendoritems as lnkvenditems', 'vendor.id', '=', 'lnkvenditems._vendor')
            ->where('lnkvenditems._item', '=', $supplyid)
            ->select([
                'vendor.code',
                'vendor.name',
                'lnkvenditems._item',
                'lnkvenditems._vendor',
                'lnkvenditems.cost',
                'lnkvenditems.isactive'
            ]);


        return Datatables::of($company)
            ->editColumn('isactive', function ($data) {
                if ($data->isactive) {
                    return '<i class="fa fa-check text-navy"></i>';
                } else {
                    return '<i class="fa fa-times-circle text-navy-red" aria-hidden="true"></i>';
                }
            })
            ->addColumn('USD', function($usd){
                $d = Currencyconvertion::select('rate')
                    ->where('target',1)
                    ->where('base',3)          
                    ->first();
               
                return (float)$usd->cost / (float)$d->rate;
            })
            ->addColumn('action', function ($c) {
                return '<a href="' . URL::to('/supplies/' . $c->_item . '/suppliesvendor/' . $c->_vendor . '/view') . '" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="' . URL::to('/supplies/' . $c->_item . '/suppliesvendor/' . $c->_vendor . '/edit') . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="' . URL::to('/supplies/' . $c->_item . '/suppliesvendor/' . $c->_vendor) . '" class="btn btn-xs btn-danger dim delsuppliesvend"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);
    }

    public function suppliesitempack($rawmat)
    {
        $company = DB::table('mst_packaging as packaging')
            ->join('lnk_itempacking as itempacking', 'packaging.id', '=', 'itempacking._packaging')
            ->join('mst_unitofmeasures as um', 'um.id', '=', 'itempacking._um')
            ->where('itempacking._item', '=', $rawmat)
            ->select([
                'itempacking.id',
                'itempacking._item',
                'itempacking.description',
                'itempacking.umvalue',
                'itempacking.innerquantity',
                'itempacking._level',
                'um.code as umcode',
                'packaging.code'
            ]);


        return Datatables::of($company)
            ->addColumn('action', function ($c) {
                return '<a href="' . URL::to('/supplies/' . $c->_item . '/suppliesitempacking/' . $c->id . '/view') . '" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="' . URL::to('/supplies/' . $c->_item . '/suppliesitempacking/' . $c->id . '/edit') . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
               <a href="' . URL::to('/supplies/' . $c->_item . '/suppliesitempacking/' . $c->id) . '"  class="btn btn-xs btn-danger dim delsuppliestitempacking"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);
    }


    public function securityuser()
    {
        $users = User::select(['id', 'firstname', 'lastname', 'username', 'isactive']);

        return Datatables::of($users)
            ->editColumn('isactive', function ($data) {
                if ($data->isactive) {
                    return '<i class="fa fa-check text-navy"></i>';
                } else {
                    return '<i class="fa fa-times-circle text-navy-red" aria-hidden="true"></i>';
                }
            })
            ->addColumn('action', function ($c) {
                return '<a href="securityuser/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim deluser"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);

    }

    public function usergroup()
    {
        $users = SecGroup::select(['id', 'description']);

        return Datatables::of($users)
            ->addColumn('action', function ($c) {
                return '<a href="usergroup/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delusergroup"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);
    }


    public function joborderlist()
    {
        // dd("ddfd");
        // $ds = JobOrders::all();
        // dd($ds);
        $jo = DB::table('trn_joborders as jo')
            ->join('mst_customers as cust', 'jo._customer', '=', 'cust.id')
            ->whereIn('jo.status',['D','S','A'])               
            ->select([
                'jo.tnumber',
                'jo.tdate',
                'jo.reference',
                'cust.name',
                'jo.notes',
                'jo.status'
            ]);


        return Datatables::of($jo)
            ->addColumn('action', function ($c) {
                return '<a href="joborder/' . $c->tnumber . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="#" id="' . $c->tnumber . '" class="btn btn-xs btn-danger dim deljo"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->editColumn('tdate', function ($c) {
                return Carbon::parse($c->tdate)->format('Y-m-d');
            })
            /*->editColumn('reference', function ($c) {
                return 'S0#'.$c->reference;
            })*/

            ->editColumn('status',function($c){
                if($c->status == 'D'){
                    return 'Draft';
                }elseif($c->status == 'C'){
                    return 'Cancell';
                }elseif( $c->status == 'A'){
                    return 'Approved';
                }elseif($c->status == 'S'){
                    return 'Submitted';
                }
                return 'Status Unknown';
            })
            ->make(true);

    }

    public function jocust()
    {


        $customer = Customer::where('isactive', 1)->select(['id', 'code', 'name']);

        return Datatables::of($customer)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->make(true);
    }

    public function jodetailfinishprod()
    {

        $catid = Category::select('id')->where('name', 'Finished Product')->first();
        $customer = Item::select(['id', 'code', 'name'])->where('_category', $catid->id)->where('isactive', 1)->get();

        return Datatables::of($customer)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->make(true);

    }

    public function jodetaillist($tnumber)
    {

        $jo = DB::table('trn_joborderdetails as jodetail')
            ->join('lnk_itempacking AS itempacking', 'jodetail.packing', '=', 'itempacking.id')
            ->join('mst_items as item', 'jodetail._item', '=', 'item.id')
            ->where('jodetail.tnumber', $tnumber)
            ->select([
                'jodetail.tnumber',
                'jodetail._item',
                'item.name',
                'itempacking.description',
                'jodetail.quantity',
                'jodetail.unitprice',
                \DB::raw('( jodetail.unitprice * jodetail.quantity ) as totalcost')
            ]);


        return Datatables::of($jo)
            ->editColumn('unitprice', function ($data) {
                return number_format($data->unitprice, 2, '.', ',');
            })
            ->editColumn('totalcost', function ($data) {
                return number_format($data->totalcost, 2, '.', ',');
            })
            ->addColumn('action', function ($c) {
                return '<a href="' . URL::to('/joborderdetail/' . $c->tnumber . '/item/' . $c->_item . '/edit') . '"  class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                     <a href="' . URL::to('/joborderdetail/' . $c->tnumber . '/jodetail/' . $c->_item) . '" class="btn btn-xs btn-danger dim deljod"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);
    }


    public function purchaseorderlist()
    {


        $po = DB::table('trn_purchaseorders as po')
            ->join('mst_vendors as vendor', 'po._vendor', '=', 'vendor.id')
            ->select([
                'po.tnumber',
                'po.tdate',
                'vendor.name',
                'po.notes',
                'po.status'
            ]);


        return Datatables::of($po)
            ->addColumn('action', function ($c) {
                return '<a href="purchasing/' . $c->tnumber . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="#" id="' . $c->tnumber . '" class="btn btn-xs btn-danger dim deljo"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->editColumn('status',function($c){
                if($c->status == 'D'){
                    return 'Draft';
                }elseif($c->status == 'C'){
                    return 'Cancel';
                }elseif( $c->status == 'A'){
                    return 'Approved';
                }elseif($c->status == 'S'){
                    return 'Submitted';
                }
                return 'Status Unknown';
            })
            ->make(true);

    }

    public function purchaseorderbalancelist()
    {


        $po = DB::table('trn_purchaseorders as po')
            ->join('mst_vendors as vendor', 'po._vendor', '=', 'vendor.id')
            ->join('trn_receiving as r','po.tnumber','=','r.ponumber')
            ->join('trn_receivingdetails as tr','r.tnumber','=','tr.tnumber')
            ->join('mst_items as m','tr._item','=','m.id')
            ->where('tr.balance', '>', 0)
            // ->where('po.hasbal',1)
            // ->where('po.iscreated',0)
            // ->where('po.status','A')
            ->select([
                'po.tnumber',
                'po.tdate',
                'vendor.name as venname',
                'm.name as item_name',
                'po.notes',
                'po.status',
                'tr.balance',
                'tr.qtydelivered',
                'tr.qtyaccepted',
            ]);
            // dd($po);

        return Datatables::of($po)
            ->addColumn('action', function ($c) {
                // return '<a href="purchasebalance/' . $c->tnumber . '/create" class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Create PO</a>';
                return '<a href="purchasebalance/' . $c->tnumber . '/view" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View</a>';
            })
            ->make(true);

    }

    public function getbom($tnumber)
    {


        $bomrecord = DB::table('vw_jo_bom')
            ->where('tnumber', $tnumber)
            ->select([
               'tnumber', '_item','name', 'packing', 'quantity', 'unitcost', 'totalcost'
            ]);

        if (!$bomrecord->count()) {
             DB::statement('EXEC xp_initialize_jo_bom ?', [$tnumber]);

            $bomrecord = DB::table('vw_jo_bom')
                ->where('tnumber', $tnumber)
                ->select([
                    'tnumber', '_item','name', 'packing', 'quantity', 'unitcost', 'totalcost'
                ]);

        }

        return Datatables::of($bomrecord)
            ->editColumn('unitcost', function ($data) {
                return number_format($data->unitcost, 2, '.', ', ');
            })
            ->editColumn('totalcost', function ($data) {
                return number_format($data->totalcost);
            })
            ->addColumn('action', function ($c) {
                return ' 
                      <a href="' . URL::to('/joborder/' . $c->tnumber . '/joborderbom/' . $c->_item . '/edit') . '"  class="btn btn-xs btn-success"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                     <a href="' . URL::to('/joborder/' . $c->tnumber . '/joborderbom/' . $c->_item) . '"  class="btn btn-xs btn-danger dim deljobom"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';

            })
            ->make(true);

    }


    public function povendor()
    {
        $vendor = Vendor::where('isactive', 1)->select(['id', 'code', 'name', '_type']);

        return Datatables::of($vendor)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->make(true);
    }

    public function jobomlookup()
    {

        $cat = Category::select('id')->where('name','Raw Material')->orWhere('name','Supplies')->pluck('id');
        $items = Item::select(['id', 'code', 'name'])
          ->whereIn('_category',$cat);

        return Datatables::of($items)

            ->setRowId(function ($c) {
                return $c->id;
            })
            ->make(true);
    }

    public function salesoderlist(){
        $salesorder = DB::table('trn_salesorder as so')
                ->join('mst_customers AS customer', 'so._customer', '=', 'customer.id')
                ->whereIn('status',['D','S','A'])
                ->select([
                    'tnumber','tdate','name','status'
                ]);


        return Datatables::of($salesorder)
            ->editColumn('tdate', function ($c) {
                return Carbon::parse($c->tdate)->format('Y-m-d');
            })
            ->addColumn('action', function ($c) {
                return '<a href="' . URL::to('/salesorder/' . $c->tnumber . '/edit') . '"  class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                 <a href="'.$c->tnumber.'" id="' . $c->tnumber . '"   class="btn btn-xs btn-danger dim delso"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->editColumn('status',function($c){
                if($c->status == 'D'){
                    return 'Draft';
                }elseif($c->status == 'C'){
                    return 'Cancelled';
                }elseif( $c->status == 'A'){
                    return 'Approved';
                }elseif($c->status == 'S'){
                    return 'Submitted';
                }
                return 'Status Unknown';
            })
            ->make(true);

    }

    public function sodetaillist($tnumber)
    {

        $jo = DB::table('trn_salesorderdetails as sodetail')
            ->join('vw_costprice AS costpriceview', 'sodetail._item', '=', 'costpriceview._item')
            ->join('lnk_itempacking as itempacking','sodetail.packing','=','itempacking.id')
            ->join('mst_items as item','item.id','=','sodetail._item')
            ->where('sodetail.tnumber', $tnumber)
            ->select([
                'sodetail.tnumber',
                'sodetail._item',
                'sodetail.aup',
                'sodetail.quantity',
                'item.name',
                'item.code',
                'itempacking.description',
                'costpriceview.markup',
                'costpriceview.costprice',
                \DB::raw('( costpriceview.costprice + costpriceview.markup ) as unitprice'),
               // \DB::raw('( (sodetail.unitprice * sodetail.quantity) + costpriceview.markup  ) as totalprice'),
                \DB::raw('( (costpriceview.costprice + costpriceview.markup) * sodetail.quantity)   as totalprice'),
                \DB::raw('( sodetail.quantity * sodetail.aup ) as auptotal')
            ]);


        return Datatables::of($jo)
            ->editColumn('name', function ($data) {
                return $data->code .'-' . $data->name;
            })
            ->editColumn('aup', function ($data) {
                return number_format($data->aup, 2, '.', ',');
            })
            ->editColumn('costprice', function ($data) {
                return number_format($data->costprice, 2, '.', ',');
            })
            ->editColumn('markup', function ($data) {
                return number_format($data->markup, 2, '.', ',');
            })
            ->editColumn('unitprice', function ($data) {
                return number_format($data->unitprice, 2, '.', ',');
            })
            ->editColumn('totalprice', function ($data) {
                return number_format($data->totalprice, 2, '.', ',');
            })
            ->editColumn('auptotal', function ($data) {
                return number_format($data->auptotal, 2, '.', ',');
            })
            ->addColumn('USD', function($usd){
                $d = Currencyconvertion::select('rate')
                    ->where('base',3)
                    ->where('target',1)
                    ->first();
                return (float)$usd->costprice / (float)$d->rate;
            })
            ->addColumn('action', function ($c) {
                return '<a href="' . URL::to('/salesorderdetail/' . $c->tnumber . '/sodetail/' . $c->_item . '/edit') . '"  class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                        <a href="' . $c->_item . '" id="' . $c->aup . '" class="btn btn-xs btn-info aupedit"><i class="fa fa-cog" aria-hidden="true"></i> AUP</a>
                     <a href="' . URL::to('/salesorderdetail/' . $c->tnumber . '/sod/' . $c->_item) . '" class="btn btn-xs btn-danger dim sod"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);
    }
    public function podetaillist($tnumber)
    {

        $po = DB::table('trn_purchaseorderdetails as podetail')
            ->join('lnk_itempacking AS itempacking', 'podetail.packing', '=', 'itempacking.id')
            ->join('mst_items as item', 'podetail._item', '=', 'item.id')
            ->where('podetail.tnumber', $tnumber)
            ->select([
                'podetail.tnumber',
                'podetail._item',
                'item.name',
                'item.code',
                'itempacking.description',
                'podetail.quantity',
                'podetail.unitprice',
                \DB::raw('( podetail.unitprice * podetail.quantity ) as totalcost')
            ]);


        return Datatables::of($po)
            ->editColumn('unitprice', function ($data) {
                return number_format($data->unitprice, 2, '.', ',');
            })
            ->editColumn('totalcost', function ($data) {
                return number_format($data->totalcost, 2, '.', ',');
            })
            ->editColumn('name', function ($data) {
                return $data->code.' - '.$data->name;
            })
            ->addColumn('action', function ($c) {
                $po = Purchasing::find($c->tnumber);
                if($po->status === 'D'){
                return '<a href="' . URL::to('/purchaseorderdetail/' . $c->tnumber . '/item/' . $c->_item . '/edit') . '"  class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                     <a href="' . URL::to('/purchaseorderdetail/' . $c->tnumber . '/podetail/' . $c->_item) . '" class="btn btn-xs btn-danger dim deljod"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
                }
                else{
                    return '';
                }
            })
            ->make(true);
    }

    public function pobaldetaillist($trn)
    {

        $po = DB::table('trn_purchaseorders as po')
            ->join('trn_receiving as r','po.tnumber','=','r.ponumber')
            ->join('trn_receivingdetails as rd','r.tnumber','=','rd.tnumber')
            ->join('mst_items as item', 'rd._item', '=', 'item.id')
            ->join('lnk_itempacking AS itempacking', 'rd.packing', '=', 'itempacking.id')
            ->where('po.tnumber',$trn)
            ->select([
                'item.name',
                'item.code',
                'itempacking.description',
                'rd.qtyaccepted',
                'rd.qtydelivered',
                'po.tnumber'
            ]);

        return Datatables::of($po)
           ->editColumn('balance', function ($c){
                $bal = $c->qtydelivered - $c->qtyaccepted;
                if ($bal > 0) {
                    return $bal;
                }else{
                    return 0;
                }
           })
            ->editColumn('name', function ($data) {
                return $data->code.' - '.$data->name;
            })
            ->make(true);
    }

    public function podetailfinishprod($vendor)
    {

        $cat = Category::select('id')->where('name','Raw Material')->orWhere('name','Supplies')->pluck('id');
        // dd($vendor);
        $items = DB::table('lnk_vendoritems as lnkvendoritems')
            ->join('mst_items as mstitems', 'lnkvendoritems._item', '=', 'mstitems.id')
            ->whereIn('mstitems._category', $cat)
            ->where('lnkvendoritems._vendor', $vendor)
            ->where('mstitems.isactive', 1)
            ->select([
                'mstitems.id',
                'mstitems.code',
                'mstitems.name'
            ]);
        // dd($items);
        return Datatables::of($items)

            ->setRowId(function ($c) {
                return $c->id;
            })
            ->make(true);

    }

    public function poinstructions($tnumber)
    {

        $poinstructions = PoInstruction::select(['id', 'instruction']);

        return Datatables::of($poinstructions)
            ->editColumn('action', function ($c) use($tnumber) {
                $chck = PoInstruction::find($c->id)->purchaseinstruction()->where('tnumber',$tnumber)->first();
                $po = Purchasing::find($tnumber);
                if($po->status === 'D'){
                    if($chck){
                        return '<input type="checkbox" checked="checked" name="poinstructcheck[]" value="'.$c->id.'">';
                    }else{
                        return '<input type="checkbox" name="poinstructcheck[]" value="'.$c->id.'">';
                    }
                }
                else{
                    if($chck){
                        return '<input type="checkbox" checked="checked" name="poinstructcheck[]" value="'.$c->id.'" disabled>';
                    }else{
                        return '<input type="checkbox" name="poinstructcheck[]" value="'.$c->id.'" disabled>';
                    }
                }

            })
            ->make(true);

    }

    public function positions()
    {
        $users = Position::select(['id', 'description']);

        return Datatables::of($users)
            ->addColumn('action', function ($c) {
                return '<a href="positions/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delposition"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);
    }

    public function currency()
    {

        $currency = Currency::select(['id', 'name', 'symbol']);

        return Datatables::of($currency)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->addColumn('action', function ($c) {
               return '
                <a href="currency/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="currency/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delcurrency"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>
                ';


            })
            ->make(true);
    }

    public function currencylist($id)
    {


        $currency = Currencyconvertion::where('base',$id)->select(['base', 'target', 'rate']);

        return Datatables::of($currency)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->editColumn('target', function ($data) {
                return Currency::find($data->target)->name;
            })
            ->addColumn('action', function ( $c ) use( $id ) {
                return '
                     <a href="' . URL::to('/currencyconvertion/' . $c->base . '/convertion/' . $c->target) . '" class="btn btn-xs btn-danger dim delconcurrency"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>
                <a href="'.$id.'" class="btn btn-xs btn-warning manualconvertcurrency" id="'.$c->target.','.$c->rate.' "><i class="fa fa-cog"  aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);
    }

    public function uomconvertion()
    {

        $company = DB::table('mst_unitofmeasures as u1')
            ->join('mst_uomconvertions as c','u1.id','=','c.buying_uom')
            ->join('mst_unitofmeasures as u2','u2.id','=','c.production_uom')
            ->select(['c.id','u1.name as buying','u2.name as prod','c.convertedvalues']);


        return Datatables::of($company)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->addColumn('action', function ($c) {
                return '<a href="unitofmeasureconvertion/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="#" id="' . $c->id . '" class="btn btn-xs btn-danger dim delunitofmeasureconvertion"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->make(true);
    }

    //Receiving

    public function receivinglist()
    {
        $rcv = DB::table('trn_receiving as rcv')
            ->join('mst_vendors as vendor', 'rcv._vendor', '=', 'vendor.id')
            ->select([
                'rcv.tnumber',
                'rcv.trrnumber',
                'rcv.tdate',
                'vendor.name',
                'rcv.notes',
                'rcv.status'
            ]);


        return Datatables::of($rcv)
            ->addColumn('action', function ($c) {
                return '<a href="receiving/' . $c->tnumber . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                      <a href="#" id="' . $c->tnumber . '" class="btn btn-xs btn-danger dim deljo"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
            })
            ->editColumn('status',function($c){
                if($c->status == 'D'){
                    return 'Draft';
                }elseif($c->status == 'C'){
                    return 'Cancel';
                }elseif( $c->status == 'A'){
                    return 'Approved';
                }elseif($c->status == 'S'){
                    return 'Submitted';
                }
                return 'Status Unknown';
            })
            ->make(true);

    }

    public function receivingpolookup()
    {

        $po = DB::table('trn_purchaseorders as po')
            ->join('trn_trr AS trr', 'po.tnumber', '=', 'trr.ponumber')
            ->join('mst_vendors as vendor', 'po._vendor', '=', 'vendor.id')
            ->join('mst_branches as branch', 'po._branch', '=', 'branch.id')
            ->where('po.status', 'A')
            ->select([
                'trr.ponumber',
                'po.jonumber',
                'trr.tnumber',
                'po.tdate',
                'po._vendor',
                'vendor.name',
                'po._branch',
                'branch.name AS branchname',
                'po.notes',
                'po.tmode'
            ]);


        return Datatables::of($po)
            ->setRowId(function ($c) {
                return $c->ponumber;
            })
            ->make(true);
    }

    /*public function receivingpodetail($tnumber)
    {

        $po = DB::table('trn_purchaseorderdetails as podetail')
            ->join('lnk_itempacking AS itempacking', 'podetail.packing', '=', 'itempacking.id')
            ->join('mst_items as item', 'podetail._item', '=', 'item.id')
            ->where('podetail.tnumber', $tnumber)
            ->select([
                'podetail.lno',
                'podetail._item',
                'item.name',
                'item.code',
                'podetail.packing',
                'itempacking.description',
                'podetail.unitprice',
                'podetail.quantity',
                \DB::raw('( podetail.unitprice * podetail.quantity ) as totalcost')
            ]);


        return Datatables::of($po)
            ->editColumn('lno', function ($data) {
                return $data->lno. '<input type="checkbox" name="lno[]" value="'.$data->lno.'" checked style="display:none;">';
            })
            ->editColumn('poprice', function ($data) {
                return number_format($data->unitprice, 2, '.', ','). '<input type="hidden" name="poprice[]" value="'.number_format($data->unitprice, 2, '.', ',').'">';
            })
            ->editColumn('rrprice', function ($data) {
                return number_format($data->unitprice, 2, '.', ','). '<input type="hidden" name="rrprice[]" value="'.number_format($data->unitprice, 2, '.', ',').'">';
            })
            ->editColumn('totalcost', function ($data) {
                return number_format($data->totalcost, 2, '.', ','). '<input type="hidden" name="totalcost[]" value="'.number_format($data->totalcost, 2, '.', ',').'">';
            })
            ->editColumn('name', function ($data) {
                return $data->code.' - '.$data->name. '<input type="hidden" name="_item[]" value="'.$data->_item.'">';
            })
            ->editColumn('description', function ($data) {
                return $data->description. '<input type="hidden" name="packing[]" value="'.$data->packing.'">';
            })
            ->editColumn('qtyordered', function ($data) {
                return $data->quantity. '<input type="hidden" name="qtyordered[]" value="'.$data->quantity.'">';
            })
            ->editColumn('qtyreceived', function ($data) {
                return $data->quantity. '<input type="hidden" name="qtyreceived[]" value="'.$data->quantity.'">';
            })
            ->editColumn('qtyaccepted', function ($data) {
                return $data->quantity. '<input type="hidden" name="qtyaccepted[]" value="'.$data->quantity.'">';
            })
            ->addColumn('action', function ($c) {
                return '<a class="btn btn-xs btn-primary" data-toggle="modal" data-target="#myModal" style="cursor:pointer;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                     ';
            })
            ->make(true);
    }*/

    //Production

    public function productionlist()
    {
        $rcv = DB::table('trn_joborders as j')
            ->join('mst_customers as c', 'j._customer', '=', 'c.id')
            ->where('j.status', '!=', 'D')
            ->where('j.status', '!=', 'S')
            ->select([
                'j.tnumber',
                'j.tdate',
                'c.name',
                'j.status'
            ]);


        return Datatables::of($rcv)
            ->addColumn('action', function ($c) {
                return '<a href="production/' . $c->tnumber . '/joborder" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View Details</a>
                    <a href="production/' . $c->tnumber . '/joborderbom" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View BOM</a>
                    <a href="onproduction?tnumber='.$c->tnumber.'" class="btn btn-xs btn-success"><i class="fa fa-thumbs-up" aria-hidden="true"></i> Put to Production</a>
                    <a href="onhold?tnumber='.$c->tnumber.'" class="btn btn-xs btn-danger"><i class="fa fa-thumbs-down"  aria-hidden="true"></i> Put On-hold</a>
                    <a href="complete?tnumber='.$c->tnumber.'" class="btn btn-xs btn-info"><i class="fa fa-check" aria-hidden="true"></i> Complete</a>
                    <a href="#" onclick="alert('.$c->tnumber.'); localStorage.setItem("tnumber", '.$c->tnumber.');" class="btn btn-xs btn-warning"><i class="fa fa-file-text" aria-hidden="true"></i> Create MR</a>';
            })
            ->editColumn('status',function($c){
                if($c->status == 'A'){
                    return 'Approved';
                }elseif($c->status == 'P'){
                    return 'On-production';
                }elseif( $c->status == 'H'){
                    return 'On-hold';
                }elseif($c->status == 'C'){
                    return 'Complete';
                }
                return 'Status Unknown';
            })
            ->make(true);

    }

    public function prodjobomlist($tnumber)
    {
        $rcv = DB::table('trn_joborderbom as j')
            ->join('mst_items as i', 'j._item', '=', 'i.id')
            ->join('lnk_itempacking as p', 'j.packing', '=', 'p.id')
            ->where('j.tnumber', $tnumber)
            ->select([
                'j.tnumber',
                'i.name',
                'p.description',
                'j.quantity'
            ]);


        return Datatables::of($rcv)
            ->setRowId(function ($c) {
                return $c->tnumber;
            })
            ->make(true);

    }

    //Material Request

    public function mrlist()
    {
        $rcv = DB::table('trn_materialrequests as mr')
            ->join('mst_locations as l', 'mr._locationfrom', '=', 'l.id')
            ->select([
                'mr.tnumber',
                'mr.tdate',
                'mr.jonumber',
                'l.name',
                'mr.status'
            ]);


        return Datatables::of($rcv)
            ->editColumn('action', function ($c) {
                if($c->status == 'A'){
                    return '<a href="materialrequest/' . $c->tnumber . '/view" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View Details</a>
                      <a href="servemr?tnumber='.$c->tnumber.'" class="btn btn-xs btn-success"><i class="fa fa-trash-o"  aria-hidden="true"></i> Serve</a>';
                }
                else{
                    return '<a href="materialrequest/' . $c->tnumber . '/view" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> View Details</a>
                      <a href="#" class="btn btn-xs btn-success" disabled><i class="fa fa-trash-o"  aria-hidden="true"></i> Serve</a>';
                }
            })
            ->editColumn('status',function($c){
                if($c->status == 'D'){
                    return 'Draft';
                }elseif( $c->status == 'A'){
                    return 'Approved';
                }elseif($c->status == 'S'){
                    return 'Submitted';
                }
                return 'Status Unknown';
            })
            ->make(true);

    }

    public function mrjolist()
    {
        $mrjo = DB::table('trn_joborders as jo')
            ->join('mst_customers as c', 'jo._customer', '=', 'c.id')
            ->join('mst_branches as b', 'jo._branch', '=', 'b.id')
            ->where('jo.status', 'P')
            ->select([
                'jo.tnumber',
                'jo.tdate',
                'jo._branch',
                'b.name',
                'jo._customer',
                'c.name AS customername',
                'jo.notes',
                'jo.status'
            ]);


        return Datatables::of($mrjo)
            ->setRowId(function ($c) {
                return $c->tnumber;
            })
            ->make(true);
    }

    public function solist()
    {
        $mrjo = DB::table('trn_salesorder as ts')
        ->select([
            'ts.tnumber',
            'mc.name',
            'ts.status',
            'ts.notes',
            'ts.pricevalidity',
            'ts._currency'
            
        ])
        ->leftJoin('mst_customers as mc', 'ts._customer', '=', 'mc.id')
        ->where('status', 'A');


        return Datatables::of($mrjo)
            ->setRowId(function ($c) {
                return $c->tnumber;
            })
            ->make(true);
    }

    public function mrdetaillist($tnumber)
    {

        $po = DB::table('trn_materialrequestdetails as mrdetail')
            ->join('lnk_itempacking AS itempacking', 'mrdetail.packing', '=', 'itempacking.id')
            ->join('mst_items as item', 'mrdetail._item', '=', 'item.id')
            ->where('mrdetail.tnumber', $tnumber)
            ->select([
                'mrdetail.tnumber',
                'mrdetail._item',
                'item.name',
                'item.code',
                'itempacking.description',
                'mrdetail.quantity'
            ]);


        return Datatables::of($po)
            ->editColumn('name', function ($data) {
                return $data->code.' - '.$data->name;
            })
            
            ->make(true);
    }

    public function mrdetailitems($vendor)
    {

        $mrjolocfrom = MaterialRequest::where('tnumber', $vendor)->pluck('_locationfrom')->first();
        $items = DB::table('mst_items')
            ->where('storagelocation', $mrjolocfrom)
            ->select([
                'id',
                'code',
                'name',
                'production_um'
            ]);

        return Datatables::of($items)

            ->setRowId(function ($c) {
                return $c->id;
            })
            ->make(true);

    }

    public function process(){
        $process = DB::table('mst_process as mp')
        ->leftJoin('mst_locations as ml','mp._location','=','ml.id')
        ->select([
            'mp.id',
            'mp.code',
            'mp.process_name',
            'ml.name'
        ]);


        return Datatables::of($process)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->addColumn('action', function ($c) {
                return '<a href="process/' . $c->id . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="processs/' . $c->id . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>';
            })
            ->make(true);
    }

    public function getprocesses(){
        $process = DB::table('mst_process as mp')
        ->leftJoin('mst_locations as ml','mp._location','=','ml.id')
        ->select([
            'mp.id',
            'mp.code',
            'mp.process_name',
            'ml.name'
        ]);

        return Datatables::of($process)
            ->setRowId(function ($c) {
                return $c->id;
            })
            ->make(true);
    }

    public function processcost($fpid){
        $processcost = DB::table('lnk_itemprocess as li')
        ->leftJoin('mst_process as mp', 'li._process', '=', 'mp.id')
        ->where('li._item', $fpid)
        ->select([
            'mp.id',
            'mp.process_name',
            'li.amount',
            'li.id as id1'
        ]);

        // $processcost = DB::table('mst_items as item')
        //     ->join('lnk_itembom as itembom', 'item.id', '=', 'itembom._subitem')
        //     ->join('lnk_itempacking as itempacking', 'itempacking.id', '=', 'itembom.packing')
        //     ->join('vw_rmcost as avgcost', function ($q) {
        //         $q->on('avgcost._item', '=', 'itembom._subitem')
        //             ->on('itembom.packing', '=', 'avgcost.packing');
        //     })
        //     ->where('itembom._item', '=', $finishedproduct)
        //     ->select([
        //         'itembom._item',
        //         'item.name',
        //         'itempacking.description',
        //         'itembom.quantity',
        //         'itembom._subitem',
        //         'avgcost.cost',
        //         'itembom.directrm',
        //         \DB::raw('( itembom.quantity * avgcost.cost ) as totalcost')
        //     ]);


        return Datatables::of($processcost)
            ->setRowId(function ($data) {
                return $data->id;
            })
            
            ->editColumn('amount', function ($data) {
                return number_format($data->amount, 2, '.', ', ');
            })
            ->addColumn('action', function ($data) {
                return '<a href="viewprocess/' . $data->id1 . '/view" class="btn btn-xs btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View</a>
                <a href="editprocess/' . $data->id1 . '/edit" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                <a href="deleteprocess/' . $data->id1 . '/delete" class="btn btn-xs btn-danger dim delfinishedprodpro"><i class="fa fa-trash-o"  aria-hidden="true"></i> Delete</a>';
                
            })
            ->make(true);
    }

}
