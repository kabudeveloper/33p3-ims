<?php

namespace App\Http\Controllers;


use App\Category;
use App\Item;
use App\LnkItemComposition;
use App\LnkItemCosting;
use App\LnkItemPacking;
use App\Packaging;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use JavaScript;

class OtherProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($itemid)
    {
       
        $item = Item::find($itemid);
        $packaging = LnkItemPacking::all();
        
        
        $bomtotalcost = DB::table('mst_items as item')
            ->join('lnk_itembom as itembom', 'item.id', '=', 'itembom._subitem')
            ->join('lnk_itempacking as itempacking', 'itempacking.id', '=', 'itembom.packing')
            ->join('lnk_vendoritems as vendoritem', 'vendoritem._item', '=', 'itembom._subitem')
            ->where('vendoritem.isactive', '=', 1)
            ->where('itembom._item','=',$itemid)
            ->select([
                \DB::raw('SUM(  itembom.quantity * vendoritem.cost ) as bomtotalcost')

            ])->get();

      

        JavaScript::put([
            'item' => $item,
            'bomtotalcost'=>$bomtotalcost

        ]);
      

        return view('otherproduct.create', compact('item','packaging'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {



        $this->validate($request, [
            '_vendor' => 'required',
            '_packaging' => 'required',
            'quantity' => 'required'

        ]);

        try {

            $company = new LnkItemComposition($request->all());
            $company->_item = $id;
            $company->_subitem = $request->_hiddenvendorid;
            $company->packing = $request->_packaging;
            $company->save();

            
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', 'Sorry this process could not be saved');
            return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }


        return redirect('finishedproduct/' . $id . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($_item,$_subitem)
    {

        $composition = DB::table('lnk_itemcomposition as itemcomp')
            ->join('mst_items as item','item.id', '=', 'itemcomp._subitem' )
            ->join('lnk_itempacking as itempacking','itemcomp.packing', '=', 'itempacking.id' )
            ->join('lnk_itemcosting as itemcost','itemcomp._subitem', '=', 'itemcost._item' )
            ->where('itemcomp._item','=',$_item)
            ->where('itemcomp._subitem','=',$_subitem)
            ->select([
                'itemcomp._item',
                'itemcomp._subitem',
                'itemcomp.packing',
                'item.id',
                'item.code',
                'item.name',
                'itempacking.description',
                'itemcomp.quantity',

            ])->first();

        $packaging = LnkItemPacking::all();


        $bomtotalcost = DB::table('mst_items as item')
            ->join('lnk_itembom as itembom', 'item.id', '=', 'itembom._subitem')
            ->join('lnk_itempacking as itempacking', 'itempacking.id', '=', 'itembom.packing')
            ->join('lnk_vendoritems as vendoritem', 'vendoritem._item', '=', 'itembom._subitem')
            ->where('vendoritem.isactive', '=', 1)
            ->where('itembom._item','=',$_item)
            ->select([
                \DB::raw('SUM(  itembom.quantity * vendoritem.cost ) as bomtotalcost')

            ])->get();

        $item = Item::find($_item);

        $costing = LnkItemCosting::where('_item',$_subitem)->first();
        
        JavaScript::put([
            'item' => $item,
            'bomtotalcost'=>$bomtotalcost,
            'costing'=>$costing,

        ]);

        return view('otherproduct.edit', compact('composition', 'packaging'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id,$subitem)
    {


        try {

            LnkItemComposition::where('_item', $id)
                ->where('_subitem', $subitem)
                ->update(
                    ['_subitem' => $request->_hiddenvendorid,
                        'packing' => $request->_packaging,
                        'quantity' => $request->quantity]

                    );

        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', 'Sorry this process could not be saved');
            return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }


        return redirect('finishedproduct/' . $id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($_item,$_subitem)
    {

         LnkItemComposition::where('_item', '=', $_item)
            ->where('_subitem', '=', $_subitem)->delete();

      /*  LnkItemCosting::find($_subitem)->delete();*/

        return response()->json(['ok' => 'success']);
    }

    public function otherproductverifycode(Request $request)
    {


        $this->validate($request, [
            'productcode' => 'required'

        ]);
        
        $rawmat = Category::select('id')->where('name','Finished Product')->first();
        $item = Category::find($rawmat->id)->item()->select('id','code','name')
            ->where('code',$request->productcode)
            ->where(function ($query) {
                $query->where(function($q){
                    $q->whereNull('isacombo');
                });
                $query->orWhere(function($q){
                    $q->where('isacombo',0);
                });
            })
            ->first();

        return response()->json(compact('item'));
    }

    public function otherproductsearch(Request $reqquest)
    {

        $catid = Category::select('id')->where('name','Finished Product')->first();
        if (isset($reqquest->containssearch)) {
            if ($reqquest->criteria === 'vendorname') {
                $vendor = Item::where('name', 'LIKE', '%'.$reqquest->valuesearch.'%')
                    ->where('_category',10)
                    ->where(function ($query) {
                        $query->where(function($q){
                            $q->whereNull('isacombo');

                        });
                        $query->orWhere(function($q){
                            $q->where('isacombo',0);

                        });
                    })
                    ->get();
            }else if($reqquest->criteria === 'vendorcode'){
                $vendor = Item::where('code', 'LIKE', '%'.$reqquest->valuesearch.'%')
                    ->where('_category',$catid->id)
                    ->where(function ($query) {
                        $query->where(function($q){
                            $q->whereNull('isacombo');

                        });
                        $query->orWhere(function($q){
                            $q->where('isacombo',0);

                        });
                    })
                    ->get();
            }
        } else {
            if ($reqquest->criteria === 'vendorcode') {
                $vendor = Item::where('code', $reqquest->valuesearch)
                    ->where('_category',$catid->id)
                    ->where(function ($query) {
                        $query->where(function($q){
                            $q->whereNull('isacombo');

                        });
                        $query->orWhere(function($q){
                            $q->where('isacombo',0);

                        });
                    })
                    ->get();
            } else if($reqquest->criteria === 'vendorname'){
                $vendor = Item::where('name', $reqquest->valuesearch)
                    ->where('_category',$catid->id)
                    ->where(function ($query) {
                        $query->where(function($q){
                            $q->whereNull('isacombo');

                        });
                        $query->orWhere(function($q){
                            $q->where('isacombo',0);

                        });
                    })
                    ->get();
            }
        }

        return response()->json(compact('vendor'));

    }

    public function otherproductall(Request $request)
    {
       /* $catid = Category::select('id')->where('name','Finished Product')->first();
        $vendor = Item::select('id', 'code', 'name','_category')
            ->where('_category',$catid->id)
            ->where(function ($query) {
                $query->where(function($q){
                    $q->whereNull('isacombo');

                });
                $query->orWhere(function($q){
                    $q->where('isacombo',0);

                });
            })
            ->get();*/

      

        $rawmat = Category::select('id')
            ->where('name','Raw Material')
            ->orWhere('name','Supplies')
            ->get();

        $vendor = Item::whereIn('_category',$rawmat)
            ->where('id','<>',$request->item)
            ->where(function ($query) {
                $query->where(function($q){
                    $q->whereNull('isacombo');
                });
                $query->orWhere(function($q){
                    $q->where('isacombo',0);
                });
            })

            ->get();


        return response()->json(compact('vendor'));
    }

    public function otherproductsearchid(Request $request){

        $vendor = Item::findOrFail($request->searchid);
        return response()->json(compact('vendor'));
    }



    public function getselectedrmotherprod(Request $request){
        
        $itempacking = LnkItemPacking::where('_item',$request->id)->get();
        return response()->json(compact('itempacking'));
    }

    public function view($_item,$_subitem)
    {

        $composition = DB::table('lnk_itemcomposition as itemcomp')
            ->join('mst_items as item','item.id', '=', 'itemcomp._subitem' )
            ->join('lnk_itempacking as itempacking','itemcomp.packing', '=', 'itempacking.id' )
            ->join('lnk_itemcosting as itemcost','itemcomp._subitem', '=', 'itemcost._item' )
            ->where('itemcomp._item','=',$_item)
            ->where('itemcomp._subitem','=',$_subitem)
            ->select([
                'itemcomp._item',
                'itemcomp._subitem',
                'itemcomp.packing',
                'item.id',
                'item.code',
                'item.name',
                'itempacking.description',
                'itemcomp.quantity',

            ])->first();

        $packaging = LnkItemPacking::all();


        $bomtotalcost = DB::table('mst_items as item')
            ->join('lnk_itembom as itembom', 'item.id', '=', 'itembom._subitem')
            ->join('lnk_itempacking as itempacking', 'itempacking.id', '=', 'itembom.packing')
            ->join('lnk_vendoritems as vendoritem', 'vendoritem._item', '=', 'itembom._subitem')
            ->where('vendoritem.isactive', '=', 1)
            ->where('itembom._item','=',$_item)
            ->select([
                \DB::raw('SUM(  itembom.quantity * vendoritem.cost ) as bomtotalcost')

            ])->get();

        $item = Item::find($_item);

        $costing = LnkItemCosting::where('_item',$_subitem)->first();
        
        JavaScript::put([
            'item' => $item,
            'bomtotalcost'=>$bomtotalcost,
            'costing'=>$costing,

        ]);

        return view('otherproduct.view', compact('composition', 'packaging'));


    }

}
