<?php

namespace App\Http\Controllers;

use App\Company;
use App\Currency;
use App\SecGroup;
use App\SecGroupAcess;
use App\User;
use Illuminate\Http\Request;
use Gate;

use Illuminate\Support\Facades\Auth;


class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-company')) {
            abort(403);
         }

        return view('company.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-company')) {
            abort(403);
        }
        $currencies = Currency::all();
        return view('company.create',compact('currencies'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required|max:10|unique:mst_companies,code',
            'name' => 'required',
            'address' => 'required',
            'tin' => 'required',
            'zipcode' => 'required',
        ]);

        
        $company = new Company($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        $company->address = strtoupper($request->address);
        $company->createdby= Auth::user()->id;
        $company->save();

        return redirect('company');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-company')) {
            abort(403);
        }
        $currencies = Currency::all();
        $company = Company::find($id);
        return view('company.edit',compact('company','currencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $company = Company::find($id);
        $company->fill($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        $company->address = strtoupper($request->address);
        if(is_null($request->isactive)){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }

        $company->save();
        return redirect('company');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-company')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        $company = Company::find($id);
        $company->delete();

        /*$company->fill($request->all());
        if($request->isactive == 1){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }*/

        return response()->json(['ok'=>'success']);

    }

     public function view($id)
    {
        /*if (Gate::denies('view-company')) {
            abort(403);
        }*/
        $currencies = Currency::all();
        $company = Company::find($id);
        return view('company.view',compact('company','currencies'));
    }
}
