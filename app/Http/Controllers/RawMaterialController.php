<?php

namespace App\Http\Controllers;

use App\Category;
use App\Clas;
use App\CtlAveragecost;
use App\Item;
use App\LnkItemPacking;
use App\LnkItemStocking;
use App\Lnkvendoritems;
use App\Location;
use App\LocationTypes;
use App\SubClas;
use App\UnitOfMeasure;
use App\Uomconvertion;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use JavaScript;
use Gate;

class RawMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-rawmat')) {
            abort(403);
        }
        return view('rawmaterial.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-rawmat')) {
            abort(403);
        }
        $locations = Location::get();
        $uoms = UnitOfMeasure::get();
        return view('rawmaterial.create',compact('locations','uoms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'code' => 'required|max:25|unique:mst_items,code',
            'name' => 'required',
            '_class' => 'required',
            '_subclass' => 'required'

        ]);

        $catid = Category::select('id')->where('name','Raw Material')->first();

        $company = new Item($request->all());
        $company->code = strtoupper('RM'.$request->code);
        $company->name = strtoupper($request->name);
        $company->_category = $catid->id;
        $company->createdby= Auth::user()->id;
        $company->save();
        $lastid = $company->id;

        $ctlaveragecost = new CtlAveragecost();
        $ctlaveragecost->_item = $company->id;
        $ctlaveragecost->save();

       // return redirect('rawmaterial');

        return redirect()->action('RawMaterialController@edit', [$lastid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-rawmat')) {
            abort(403);
        }
        $company = Item::find($id);
        $locations = Location::get();
        $uoms = UnitOfMeasure::get();
        $buyprods = LnkItemPacking::where('_item',$id)->get();

        JavaScript::put([
            'company' => $company,
        ]);


        return view('rawmaterial.edit',compact('company','locations','uoms','buyprods'));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Item::find($id);

        $company->fill($request->all());
        $company->code = strtoupper($request->code);
        $company->name = strtoupper($request->name);
        if (is_null($request->isactive)) {
            $company->isactive = 0;
        } else {
            $company->isactive = 1;
        }


        $company->save();

        return redirect('rawmaterial');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Gate::denies('delete-rawmat')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        $company = Item::find($id);
        $company->delete();

       $averagecost = CtlAveragecost::where('_item',$id);
        
        if($averagecost){
            $averagecost->delete();
        }
        
        $ip =LnkItemPacking::where('_item',$id);
        
        if($ip){
            $ip->delete();
        }
        
        $ls = LnkItemStocking::where('_item',$id);
        
        if($ls){
            $ls->delete();
        }
       
       $lnkitems = Lnkvendoritems::where('_item',$id);
        
        if($lnkitems){
            $lnkitems->delete();
        }

        return response()->json(['ok'=>'success']);
    }

    public function getcategory(){

        $categories = Category::select('id','name')
            ->where('name','Raw Material')
            ->get();

        return response()->json(compact('categories'));

    }

    public function getclss(Request $request){


        $classess = Clas::select('id','name')
            ->where('_category',$request->catid)
            ->get();

        return response()->json(compact('classess'));

    }

    public function getsubclass(Request $request){
        $subclassess = SubClas::select('id','name')
            ->where('_class',$request->classid)
            ->get();

        return response()->json(compact('subclassess'));
    }

    public function geteditcategory(Request $request){

        $categories = Category::select('id','name')
            ->where('id',$request->catid)
            ->get();
        
        return response()->json(compact('categories'));
    }
    
    public function costchanges(Request $request){

          CtlAveragecost::where('_item',$request->_item)
              ->update($request->except('_item'));

        return response()->json(['ok'=>'success']);
    }
    
    public function getcostchanges(Request $request){
        $cost = CtlAveragecost::where('_item',$request->_item)->first();

        return response()->json(compact('cost'));
        
    }

    public function view($id)
    {
        /*if (Gate::denies('edit-rawmat')) {
            abort(403);
        }*/
        $company = Item::find($id);
        $locations = Location::get();
        $uoms = UnitOfMeasure::get();
        $buyprods = LnkItemPacking::where('_item',$id)->get();

        JavaScript::put([
            'company' => $company,
        ]);


        return view('rawmaterial.view',compact('company','locations','uoms','buyprods'));
       
    }
}


