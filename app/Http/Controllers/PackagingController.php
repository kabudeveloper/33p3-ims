<?php

namespace App\Http\Controllers;

use App\Packaging;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Gate;

class PackagingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-packaging')) {
            abort(403);
        }
        return view('packaging.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-packaging')) {
            abort(403);
        }
        return view('packaging.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required|max:10|unique:mst_packaging,code',
            'name' => 'required'

        ]);

        $company = new Packaging($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        if(is_null($request->isactive)){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }
        $company->createdby= Auth::user()->id;
        $company->save();

        return redirect('packaging');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-packaging')) {
            abort(403);
        }

        $company = Packaging::find($id);
        return view('packaging.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'code' => 'required|max:10',
            'name' => 'required'

        ]);
        $company = Packaging::find($id);
        $company->fill($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        if(is_null($request->isactive)){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }
        $company->save();
        return redirect('packaging');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-packaging')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        $company = Packaging::find($id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }

    public function view($id)
    {
        /*if (Gate::denies('edit-packaging')) {
            abort(403);
        }*/

        $company = Packaging::find($id);
        return view('packaging.view',compact('company'));
    }
}
