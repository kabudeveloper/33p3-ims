<?php

namespace App\Http\Controllers;

use App\Item;
use App\JobOrderBom;
use App\LnkItemPacking;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use JavaScript;
use Gate;

class JobOrderBomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($tnumber)
    {
        if (Gate::denies('bom-jo')) {
            abort(403);
        }

        JavaScript::put([
            'jo' => $tnumber,
            'APP_URL' => url('/'),
        ]);


        return view('joborderbom.index',compact('tnumber'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tnumber)
    {
        if (Gate::denies('createbom-jo')) {
            abort(403);
        }
        JavaScript::put([
            'APP_URL' => url('/')
        ]);

        return view('joborderbom.create',compact('tnumber'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('createbom-jo')) {
            abort(403);
        }
        $this->validate($request, [
            'code' => 'required',
            'packing' => 'required',
            'quantity' => 'required',
            'rmname' => 'required',
            'tnumber'=>'required'
        ]);



      $jobom = JobOrderBom::where('tnumber', '=', $request->tnumber)
            ->where('_item','=',$request->_hiddenrmid)
            ->first();

        if($jobom == null){

            $unitcost = DB::table('vw_rmcost')
                ->where('_item',$request->_hiddenrmid)
                ->where('packing',$request->packing)
                ->select(['cost'])
                ->first();

            $company = new JobOrderBom($request->all());
            $company->_item = $request->_hiddenrmid;
            $company->unitcost = $unitcost->cost;
            $company->save();
        }else{
                return back()->withErrors('This Item is already added in the list!');
        }

        return redirect('joborder/'.$request->tnumber.'/joborderbom');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tnumber,$item)
    {
        if (Gate::denies('editbom-jo')) {
            abort(403);
        }
        $jobom = JobOrderBom::where('tnumber',$tnumber)
            ->where('_item',$item)
            ->first();
        $item = Item::find($item);

        JavaScript::put([
            'jobom' => $jobom,
        ]);

        return view('joborderbom.edit',compact('jobom','item'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('editbom-jo')) {
            abort(403);
        }
        $unitcost = DB::table('vw_rmcost')
            ->where('_item',$request->_hiddenrmid)
            ->where('packing',$request->packing)
            ->select(['cost'])
            ->first();

        JobOrderBom::where('tnumber',$request->tnumber)
            ->where('_item',$request->_hiddenrmid)
            ->update([
                '_item'=>$request->_hiddenrmid,
                'packing'=>$request->packing,
                'quantity'=>$request->quantity,
                'unitcost'=>$unitcost->cost

            ]);

        return redirect('joborder/'.$request->tnumber.'/joborderbom');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$item)
    {
        if (Gate::denies('deletebom-jo')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        JobOrderBom::where('tnumber', '=', $id)
            ->where('_item', '=', $item)->delete();

        return response()->json(['ok' => 'success']);
    }


}
