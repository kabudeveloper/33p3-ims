<?php

namespace App\Http\Controllers;

use App\BranchCompany;
use App\CancellationTerm;
use App\Company;
use App\Currency;
use App\Currencyconvertion;
use App\Customer;
use App\JobOrders;
use App\JoDetail;
use App\LeadTime;
use App\PaymentTerm;
use App\SalesOrder;
use App\SalesOrderDetail;
use App\User;
use Carbon\Carbon;
use Cartalyst\Converter\Laravel\Facades\Converter;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use JavaScript;
use PDF;
use Gate;

class SalesOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-so')) {
            abort(403);
        }
        return view('salesorder.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-so')) {
            abort(403);
        }
        $cancellterm = CancellationTerm::all();
        $leadtimes = LeadTime::all();
      
        return view('salesorder.create', compact('cancellterm','leadtimes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('create-so')) {
            abort(403);
        }
        $this->validate($request, [
            '_customer' => 'required',
            //'_cancellationterm' => 'required',
            'pricevalidity' => 'required',
            '_currency'=>'required'
        ]);

        // $maincompany = BranchCompany::first()->maincompany()->first();
        $curr = Currency::find(1)->convertions()->where('base',$request->_currency)->first();
        // dd($curr->rate);
        if (!$curr) {
            Session::flash('nocurrencyrate', 'This Customer has no currency rate !');
            return back();
        }   

        $so = new SalesOrder($request->all());
        $so->_customer = $request->custidhidden;
        $so->tdate = Carbon::now();
        $so->createdby = Auth::user()->id;
        $so->currencyrate = $curr->rate;
        $so->pricevalidity = Carbon::parse($request->pricevalidity);
        $so->save();

        return redirect('salesorder/' . $so->tnumber . '/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-so')) {
            abort(403);
        }

        $so = SalesOrder::find($id);
        $custname = Customer::find($so->_customer);
        if ($so->status === 'D')
            $sostatus = 'Draft';
        elseif ($so->status === 'C')
            $sostatus = 'Cancelled';
        elseif ($so->status === 'A')
            $sostatus = 'Approved';
        elseif ($so->status === 'S')
            $sostatus = 'Submitted';
        else
            $sostatus = 'Unknown Status';

        $sotransdate = Carbon::parse($so->tdate)->format('Y/m/d');
        $cancellterm = CancellationTerm::all();
        $leadtimes = LeadTime::all();


        JavaScript::put([
            'so' => $so,
        ]);

        return view('salesorder.edit', compact('so', 'custname', 'sostatus', 'sotransdate', 'cancellterm', 'leadtimes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('edit-so')) {
            abort(403);
        }

        $so = SalesOrder::find($id);
        $so->fill($request->except([
            'status', 'tnumber', 'tdate'
        ]));


        $so->_customer = $request->custidhidden;
        $so->currencyrate = $request->currencyrate;
        $so->save();

        return redirect('salesorder');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-so')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        SalesOrder::find($id)->delete();
        $sod = SalesOrderDetail::find($id);
        if ($sod) {
            $sod->delete();
        }

        return response()->json(['ok' => 'success']);
    }

    public function approvesalesorder(Request $request)
    {
        if (Gate::denies('approveso')) {
            abort(403);
        }

        $so = SalesOrder::find($request->tnumber);
        $so->status = "A";
        $so->approveddatetime = Carbon::now();
        $so->approvedby = Auth::user()->id;
        $so->save();
        return redirect()->back();
    }

    public function cancelsalesorder(Request $request)
    {
        if (Gate::denies('cancel-so')) {
            abort(403);
        }

        $so = SalesOrder::find($request->tnumber);
        $so->status = "C";
        $so->cancelleddatetime = Carbon::now();
        $so->cancelledby = Auth::user()->id;
        $so->save();
        return redirect('salesorder');
    }

    public function getpaymenttermstextfield(Request $request)
    {

        $customerpayment = Customer::find($request->customerid)->paymentterm;

        return response()->json(compact('customerpayment'));

    }

    public function printsalesorderquotation(Request $request)
    {
        if (Gate::denies('printsoquotation')) {
            abort(403);
        }
        $so = SalesOrder::find($request->so);
        $customer = SalesOrder::find($request->so)->customer;
        $term = Paymentterm::find($so->_paymentterm);
        $cancelterm = CancellationTerm::find($so->_cancellationterm);
        $contact = $customer->contact()->where('isprimary', 1)->first();
        if (!$contact) {
            Session::flash('nocontact', 'Customer No primary contact is set !');
            return back();
        }

        $approvedby = User::where('id', $so->approvedby)->first()->complete_name;
        $company = Company::where('isactive', 1)->first();
        if (!$company) {
            Session::flash('nocontact', 'Company is not set as active !');
            return back();
        }

        $printdate = Carbon::now()->format('F d, Y');
        $sod = SalesOrderDetail::where('tnumber',$request->so)->orderBy('_item')->get();


        $sumtotal = collect([]);

        foreach ($sod as $j) {
            $amount = $j->total_price_convertion;
            $sumtotal->push($amount);
        }

        // $pdf = PDF::loadView('salesorder.quotation', compact('company', 'printdate', 'customer', 'contact', 'sod', 'term', 'cancelterm', 'so', 'sumtotal', 'approvedby'));
        return view('salesorder.quotation', compact('company', 'printdate', 'customer', 'contact', 'sod', 'term', 'cancelterm', 'so', 'sumtotal', 'approvedby'));
        // return $pdf->stream();
    }

    public function printsalesorder(Request $request)
    {
        if (Gate::denies('printso')) {
            abort(403);
        }
        $so = SalesOrder::find($request->so);
        $customer = SalesOrder::find($request->so)->customer;
        $term = Paymentterm::find($so->_paymentterm);
        $cancelterm = CancellationTerm::find($so->_cancellationterm);
        $contact = $customer->contact()->where('isprimary', 1)->first();
        if (!$contact) {
            Session::flash('nocontact', 'Customer No primary contact is set !');
            return back();
        }

        $approvedby = User::where('id', $so->approvedby)->first()->complete_name;
        $company = Company::where('isactive', 1)->first();
        if (!$company) {
            Session::flash('nocontact', 'Company is not set as active !');
            return back();
        }
        $printdate = Carbon::now()->format('F d, Y');

        $sod = SalesOrderDetail::where('tnumber',$request->so)->orderBy('_item')->get();

        $leadtime = LeadTime::find($so->_leadtime);

        $sumtotal = collect([]);

        foreach ($sod as $j) {
            $amount = $j->quantity * $j->aup;
            $sumtotal->push($amount);
        }


        $sodateapproved = Carbon::parse($so->approveddatetime)->format('F d, Y');
        $deliverydate = Carbon::parse($so->approveddatetime);
        // $printdelierydate = $deliverydate->addWeekdays($leadtime->highervalue);
        $printdelierydate = '';
        $fprintdeliverydate = Carbon::parse($printdelierydate)->format('F d, Y');

        $sods = DB::table('trn_salesorderdetails as sodetail')
            ->join('vw_costprice AS costpriceview', 'sodetail._item', '=', 'costpriceview._item')
            ->join('lnk_itempacking as itempacking', 'sodetail.packing', '=', 'itempacking.id')
            ->join('mst_items as item', 'item.id', '=', 'sodetail._item')
            ->where('sodetail.tnumber', $so->tnumber)
            ->select([
                'sodetail.tnumber',
                'sodetail._item',
                'sodetail.aup',
                'sodetail.quantity',
                'item.name',
                'item.code',
                'itempacking.description',
                'costpriceview.markup',
                'costpriceview.costprice',
                \DB::raw('( costpriceview.costprice + costpriceview.markup ) as unitprice'),
                \DB::raw(' (( costpriceview.costprice + costpriceview.markup ) * sodetail.quantity)  as totalprice'),
                \DB::raw('( sodetail.quantity * sodetail.aup ) as auptotal')
            ])->get();

        $sodtotalprice = DB::table('trn_salesorderdetails as sodetail')
            ->join('vw_costprice AS costpriceview', 'sodetail._item', '=', 'costpriceview._item')
            ->join('lnk_itempacking as itempacking', 'sodetail.packing', '=', 'itempacking.id')
            ->join('mst_items as item', 'item.id', '=', 'sodetail._item')
            ->where('sodetail.tnumber', $so->tnumber)
            ->select([

                \DB::raw(' (( costpriceview.costprice + costpriceview.markup ) * sodetail.quantity)  as totalprice'),

            ])->get();


        $sumtotalprice = collect([]);

        foreach ($sodtotalprice as $s) {

            $sumtotalprice->push($s->totalprice);
        }



        // $pdf = PDF::loadView('salesorder.psalesorder', compact('company', 'printdate', 'customer', 'contact', 'sod', 'term', 'cancelterm', 'so', 'sumtotal','sumtotalprice', 'approvedby', 'sodateapproved', 'sods','fprintdeliverydate'));
         return view('salesorder.psalesorder', compact('company', 'printdate', 'customer', 'contact', 'sod', 'term', 'cancelterm', 'so', 'sumtotal','sumtotalprice', 'approvedby', 'sodateapproved', 'sods','fprintdeliverydate'));
        // return $pdf->stream();
    }


    public function rollbackso(Request $request)
    {
        if (Gate::denies('rollbackso')) {
            abort(403);
        }
        $so = SalesOrder::find($request->tnumber);
        $so->status = "D";
        $so->save();
        return redirect()->back();

    }

    public function currencycustomer(Request $request)
    {
        $custcurrency = Customer::find($request->id);
        return response()->json(compact('custcurrency'));
    }

    public function getcurrencies()
    {
        $currencies = Currency::all();

        return response()->json(compact('currencies'));
    }

    public function convertcurrency(Request $request)
    {
        $to = Currency::find($request->convto);
        $from = Currency::find($request->basecurr);
        $rate = Converter::from('currency.' . strtolower($from->symbol))->to('currency.' . strtolower($to->symbol))->convert(1)->format();
        return response()->json(compact('rate'));
    }

    public function currencyexcept(Request $request)
    {
        $currencies = Currency::where('id', '!=', $request->id)->get();
        return response()->json(compact('currencies'));
    }

    public function genjoborder(Request $request)
    {   
        // $jpo = JobOrders::all();
        // dd($jpo);
        // $tnumber = 17;
        if (Gate::denies('generatejo')) {
            abort(403);
        }

        try {

            $findjo = JobOrders::where('reference',$request->tnumber)->first();
            // dd($tnumber);
            if($findjo){
                return back()->with(['dupjo'=>'This transaction already exist in JobOrder!']);
            }


            $branch = BranchCompany::first();
            $so = SalesOrder::find($request->tnumber);
            // $leadtime = LeadTime::find($so->_leadtime);
            // return $so;
            $today = Carbon::now();

            $jo = new JobOrders;

            $jo->createdby = Auth::user()->id;
            $jo->_customer = $so->_customer;
            $jo->tdate = Carbon::now();
            $jo->status = 'D';
            $jo->reference = $so->tnumber;
            $jo->_paymentterm = $so->_paymentterm;
            $jo->_paymentterm = $so->_paymentterm;
            $jo->_branch = $branch->id;
            $jo->fromso = 1;
            $jo->deliverydate = Carbon::now();
            // $jo->deliverydate = '';
            $jo->save();

            $sods = SalesOrderDetail::where('tnumber',$request->tnumber)->get();

            foreach ($sods as $sod) {
                $sellingprice = DB::table('vw_sellingprice')
                    ->where('_item', $sod->_item)
                    ->get();

                $jodetail = new JoDetail;
                $jodetail->tnumber = $jo->tnumber;
                $jodetail->_item = $sod->_item;
                $jodetail->lno = 0; // default sa not use ni
                $jodetail->quantity = $sod->quantity;
                $jodetail->packing = $sod->packing;
                $jodetail->unitprice = $sellingprice[0]->sellingprice;
                $jodetail->save();

            }


            $so->status = 'J';
            $so->joby = Auth::user()->id;
            $so->jodatetime = Carbon::now();
            $so->save();
           
            return back()->with(['message' => 'Job Order Created Successfully']);

        } catch (\Illuminate\Database\QueryException $e) {
            if ($e->getCode() == 23000) {
                $msg = $e->getMessage();
            } else {
                $msg = "There was an error creating Job Order !, Please contact your Administrator";
            }
            Session::flash('duplicate', $msg);
            return back();
        }
    }

    public function getcurrenciesrate(Request $request){
        
        $curr = Currencyconvertion::where('base', $request->_currency)->first();
  
        return response()->json(compact('curr'));

    }

    public function submitso(Request $request){
        if (Gate::denies('submit-so')) {
            abort(403);
        }

        $so = SalesOrder::find($request->tnumber);
          $so->status = 'S';
          $so->save();
          return back();
    }

}
