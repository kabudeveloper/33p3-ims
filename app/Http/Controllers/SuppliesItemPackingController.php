<?php

namespace App\Http\Controllers;

use App\Item;
use App\LnkItemPacking;
use App\Packaging;
use App\UnitOfMeasure;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class SuppliesItemPackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($item)
    {
        $item = Item::find($item);
        $packaging = Packaging::all();
        $um = UnitOfMeasure::all();

        return view('suppliesitempacking.create', compact('item','packaging','um'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {


        $this->validate($request,[
            '_packaging' => 'required',
            '_um' => 'required',
            'umvalue' => 'required',
            'innerquantity' => 'required',
            'description' => 'required',
            '_level' => 'required',
        ]);

        $company = new LnkItemPacking($request->all());
        $company->_item = $id;
        $company->description = strtoupper($request->description);
        $company->save();



        return redirect('supplies/' . $id . '/edit');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($itemid,$itempackingid)
    {
        $item = Item::find($itemid);
        $packaging = Packaging::all();
        $um = UnitOfMeasure::all();


        $itempacking = DB::table('mst_packaging as packaging')
            ->join('lnk_itempacking as itempacking', 'packaging.id', '=', 'itempacking._packaging')
            ->join('mst_unitofmeasures as um', 'um.id', '=', 'itempacking._um')
            ->where('itempacking.id', '=', $itempackingid)
            ->select([
                'itempacking.id',
                'itempacking._packaging',
                'itempacking._um',
                'itempacking.description',
                'itempacking.umvalue',
                'itempacking.innerquantity',
                'itempacking._level',
                'um.code',
                'packaging.code'
            ])->first();


        return view('suppliesitempacking.edit', compact('item', 'itempacking','packaging','um'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $itemid,$itempackingid)
    {

        $company = LnkItemPacking::find($itempackingid);
        $company->fill($request->all());
        $company->_item = $itemid;
        $company->description = strtoupper($request->description);
        $company->save();
        return redirect('rawmaterial/'.$itemid.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$itempackingid)
    {

        $company = LnkItemPacking::find($itempackingid);
        $company->delete();

        return response()->json(['ok'=>'success']);

    }

    public function view($itemid,$itempackingid)
    {
        $item = Item::find($itemid);
        $packaging = Packaging::all();
        $um = UnitOfMeasure::all();


        $itempacking = DB::table('mst_packaging as packaging')
            ->join('lnk_itempacking as itempacking', 'packaging.id', '=', 'itempacking._packaging')
            ->join('mst_unitofmeasures as um', 'um.id', '=', 'itempacking._um')
            ->where('itempacking.id', '=', $itempackingid)
            ->select([
                'itempacking.id',
                'itempacking._packaging',
                'itempacking._um',
                'itempacking.description',
                'itempacking.umvalue',
                'itempacking.innerquantity',
                'itempacking._level',
                'um.code',
                'packaging.code'
            ])->first();


        return view('suppliesitempacking.view', compact('item', 'itempacking','packaging','um'));
    }
}
