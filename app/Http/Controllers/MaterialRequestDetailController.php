<?php

namespace App\Http\Controllers;

use App\Item;
use App\MaterialRequestDetail;
use App\LnkItemPacking;
use App\Lnkvendoritems;
use App\MaterialRequest;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\RedirectResponse;
use JavaScript;
use PDF;

class MaterialRequestDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $mrd = $request->tnumber;

        return view('materialrequestdetail.create',compact('mrd'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //try{
            $this->validate($request,[
                 'quantity' => 'required|numeric',
                 '_item' => 'required',
                 'tnumber' => 'required',
                 'packing' => 'required',
             ]);

            // $sellingprice = DB::table('lnk_vendoritems')
            //        ->where('_item',$request->itemhidden)
            //       ->get();
            // $sellingprice = Lnkvendoritems::where('_item',$request->itemhidden)->get();

            $mrdetail = new MaterialRequestDetail($request->all());
            $mrdetail->_item = $request->itemhidden;
            $mrdetail->save();

        /*} catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', "Error in Saving...");
            return back()
                ->withErrors(['error' => 'Duplicate Item']);
        }*/
        
        //return redirect()->action('MaterialRequestController@view', [$request->tnumber]);
        return redirect('materialrequest/' . $request->tnumber . '/view');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getselectedrmformr(Request $request){

           $itempacking = LnkItemPacking::where('_item',$request->id)->get();
           return response()->json(compact('itempacking'));
    }
}
