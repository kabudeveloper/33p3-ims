<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\Http\Requests;


class ProductionJobOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
        $jodi = DB::select('select j.reference, p.description, j.notes, j.tnumber,j.tdate, j.status, j.deliverydate, b.name as bname, c.name as cname from trn_joborders as j INNER JOIN mst_customers as c ON j._customer = c.id INNER JOIN mst_branches as b ON j._branch = b.id INNER JOIN mst_paymentterms as p ON j._paymentterm = p.id where j.tnumber =  ?', [$id]);
        
        //$jod = DB::table('trn_joborderdetails')->get();
        $jod = DB::select('select j.tnumber,  p.description, i.name, j.quantity, j.lno from trn_joborderdetails as j inner join lnk_itempacking as p on j.packing = p.id  INNER JOIN mst_items as i on j._item = i.id where j.tnumber =?', [$id]);
        return view('production.joborderdetails')->with(['jodi'=>$jodi])->with(['jod'=>$jod]);
        
        
                
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
