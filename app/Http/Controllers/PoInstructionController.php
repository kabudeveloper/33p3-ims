<?php

namespace App\Http\Controllers;

use App\PoInstruction;
use App\PurchaseOrderDetails;
use App\PurchaseOrderInstruction;
use Illuminate\Http\Request;

use App\Http\Requests;

class PoInstructionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $trn = $request->trnid;
        return view('poinstructions.create',compact('trn'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'instruction' => 'required',
        ]);

        $poinstruction = new PoInstruction($request->all());
        $poinstruction->save();

        $poorderins = new PurchaseOrderInstruction();
        $poorderins->tnumber = $request->trn;
        $poorderins->_instruction = $poinstruction->id;
        $poorderins->save();

        return redirect('purchase');
        //return response()->json(['return' => 'some data']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
