<?php

namespace App\Http\Controllers;

use App\BranchCompany;
use App\Customer;
use App\JobOrders;
use App\Location;
use App\MaterialRequest;
use App\User;
use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\RedirectResponse;
use JavaScript;
use PDF;

class MaterialRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('materialrequest.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $location = Location::select('id','name')->get();
        return view('materialrequest.create',compact('location'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            '_location' => 'required',
            'jonumber' => 'required',
        ]);

        $userlocation = User::where('id', Auth::user()->id)->pluck('locationassignment')->first();

        $mr = new MaterialRequest($request->all());
        $mr->createdby = Auth::user()->id;
        $mr->_locationfrom = $request->_location;
        $mr->_locationto = $userlocation;
        $mr->jonumber = $request->jonumber;
        $mr->tdate = Carbon::now();
        $mr->status = 'D';  
        $mr->save();
        $lastid = $mr->tnumber;

        return redirect()->action('MaterialRequestController@view', [$lastid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function view($id)
    {
        $mr = MaterialRequest::find($id);
        $branches = BranchCompany::select('id','name')->get();
        $location = Location::select('id','name')->get();
        $jobranch = JobOrders::where('tnumber', $mr->jonumber)->pluck('_branch')->first();

        // $vendor = Vendor::select('id','name')->get();
        // $vendorname = Vendor::where('id', $po->_vendor)->pluck('name')->first();

        $mrstatus = '';
        if($mr->status == 'R'){
            $mrstatus = 'Served';
        }elseif( $mr->status == 'D'){
            $mrstatus = 'Draft';
        }elseif( $mr->status == 'A'){
            $mrstatus = 'Approved';
        }elseif( $mr->status == 'S'){
            $mrstatus = 'Submitted';
        }

        // JavaScript::put([
        //     'po' => $po,
        // ]);


        return view('materialrequest.view',compact('mr','location','branches','jobranch','mrstatus'));
    }

    public function getbranch(Request $request){
        
        $vendor= JobOrders::find($request->vendorid);
        $terms = BranchCompany::all();
        
        return response()->json(compact('terms','vendor'));
    }

    public function submitmr(Request $request)
    {
        $mr = MaterialRequest::find($request->tnumber);
        $mr->status = "S";
        $mr->submitteddatetime = Carbon::now();
        $mr->submittedby =  Auth::user()->id;
        $mr->save();
        return redirect()->back();
    }

    public function approvemr(Request $request)
    {
        $mr = MaterialRequest::find($request->tnumber);
        $mr->status = "A";
        $mr->approveddatetime = Carbon::now();
        $mr->approvedby = Auth::user()->id;
        $mr->save();
        return redirect()->back();
    }

    public function servemr(Request $request)
    {
        $mr = MaterialRequest::find($request->tnumber);
        $mr->status = "R";
        $mr->serveddatetime = Carbon::now();
        $mr->servedby = Auth::user()->id;
        $mr->save();
        return redirect()->back();
    }

    public function rollbackmr(Request $request)
    {
        $mr = MaterialRequest::find($request->tnumber);
        $mr->status = "D";
        $mr->save();
        return redirect()->back();
    }

}
