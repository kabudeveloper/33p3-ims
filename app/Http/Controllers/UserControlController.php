<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\UserControl;
use Illuminate\Http\Request;

class UserControlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect('usercontrol/1/edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usercontrol = UserControl::find($id);

        return view('usercontrol.edit', compact('usercontrol'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $user = UserControl::find($id);

        $user->fill($request->all());

        if(isset($request->enableloginlocking)){
            $user->enableloginlocking = 1;
        }else{
            $user->enableloginlocking = 0;
        }
        if(isset($request->enablepasswordexpiry)){
            $user->enablepasswordexpiry = 1;
        }else{
            $user->enablepasswordexpiry = 0;
        }

        if(isset($request->enableaccountexpiry)){
            $user->enableaccountexpiry = 1;
        }else{
            $user->enableaccountexpiry = 0;
        }

        $user->save();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
