<?php

namespace App\Http\Controllers;

use App\BillOfMaterial;
use App\Http\Requests;
use App\Item;
use App\LnkItemPacking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use JavaScript;
use Gate;

class BillOfMaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($fp)
    {
        /*if (Gate::denies('create')) {
            abort(403);
        }*/

        $item = Item::find($fp);
        $itempacking = LnkItemPacking::select('id', 'description')
            ->where('_item','=',$fp)
            ->get();

        return view('billofmaterial.create', compact('item', 'itempacking'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'rmcode' => 'required',
            'packing' => 'required',
            'quantity' => 'required'
        ]);

       
        try {
            $company = new BillOfMaterial($request->all());
            $company->_item = $id;
            $company->_subitem = $request->_hiddenrmid;
            $company->directrm = 1;
            $company->save();

        } catch (\Illuminate\Database\QueryException $e) {

            Session::flash('duplicate', 'Duplicate Item');
            return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }

        return redirect('finishedproduct/' . $id . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($fpid,$bomid)
    {
       /* if (Gate::denies('edit')) {
            abort(403);
        }*/

        $vendoritem = DB::table('mst_items as item')
            ->join('lnk_itembom as itembom', 'item.id', '=', 'itembom._subitem')
            ->join('lnk_itempacking as itempacking', 'itempacking.id', '=', 'itembom.packing')
            ->join('ctl_averagecost as avgcost', 'avgcost._item', '=', 'itembom._subitem')
            ->where('itembom._item','=',$fpid)
            ->where('itembom._subitem','=',$bomid)
            ->select([
                'itembom._item',
                'itembom.packing',
                'itembom._subitem',
                'itempacking.description',
                'itembom.quantity',
                'itembom._subitem',
                'avgcost.cost',
                'itembom.directrm',

            ])->first();


    
        $itempacking = LnkItemPacking::select('id', 'description')
            ->where('_item',$bomid)
            ->get();

       

        $mainitem = Item::find($vendoritem->_item);
        $subitem = Item::find($vendoritem->_subitem);

        JavaScript::put([
             'bom' => $vendoritem,
             'subitem'=>$subitem,
             
           
        ]);

        return view('billofmaterial.edit', compact('itempacking','vendoritem','subitem','mainitem'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $fpid, $subitem)
    {


        $this->validate($request, [
            'rmcode' => 'required'
        ]);
        
        try {

            if (is_null($request->directrm)) {
                $active = 0;
            } else {
                $active = 1;
            }
            BillOfMaterial::where('_item', $fpid)
                ->where('_subitem', $subitem)
                ->update(
                    [
                        'packing' => $request->packing,
                        'quantity' => $request->quantity,
                        'directrm' => $active
                       
                    ]
                );

        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', 'Problem in update');
            return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }
        return redirect('finishedproduct/' . $fpid . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($item,$subitem)
    {
       /* if (Gate::denies('delete')) {
            abort(403);
        }*/

        $company = BillOfMaterial::where('_item', '=', $item)
            ->where('_subitem', '=', $subitem)->delete();

        return response()->json(['ok' => 'success']);

    }

    public function getselectedrm(Request $request){

           $itempacking = LnkItemPacking::where('_item',$request->id)->get();
           return response()->json(compact('itempacking'));
    }

    public function view($fpid,$bomid)
    {
       /* if (Gate::denies('edit')) {
            abort(403);
        }*/

        $vendoritem = DB::table('mst_items as item')
            ->join('lnk_itembom as itembom', 'item.id', '=', 'itembom._subitem')
            ->join('lnk_itempacking as itempacking', 'itempacking.id', '=', 'itembom.packing')
            ->join('ctl_averagecost as avgcost', 'avgcost._item', '=', 'itembom._subitem')
            ->where('itembom._item','=',$fpid)
            ->where('itembom._subitem','=',$bomid)
            ->select([
                'itembom._item',
                'itembom.packing',
                'itembom._subitem',
                'itempacking.description',
                'itembom.quantity',
                'itembom._subitem',
                'avgcost.cost',
                'itembom.directrm',

            ])->first();


    
        $itempacking = LnkItemPacking::select('id', 'description')
            ->where('_item',$bomid)
            ->get();

       

        $mainitem = Item::find($vendoritem->_item);
        $subitem = Item::find($vendoritem->_subitem);

        JavaScript::put([
             'bom' => $vendoritem,
             'subitem'=>$subitem,
             
           
        ]);

        return view('billofmaterial.view', compact('itempacking','vendoritem','subitem','mainitem'));

    }
}
