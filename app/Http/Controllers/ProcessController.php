<?php

namespace App\Http\Controllers;

use App\LnkItemProcess;
use App\Process;
use App\Category;
use App\Clas;
use App\CtlAveragecost;
use App\Item;
use App\LnkItemPacking;
use App\LnkItemStocking;
use App\Lnkvendoritems;
use App\Location;
use App\UnitOfMeasure;
use App\Uomconvertion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use JavaScript;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Gate;


class ProcessController extends Controller{
	public function index(){
		return view('process.index');
	}
	public function create(){
		if (Gate::denies('create-supplies')) {
            abort(403);
        }
        $locations = Location::get();
        $uoms = UnitOfMeasure::get();
		return view('process.create', compact('locations','uoms'));
	}
	public function getcategory(){

        $categories = Category::select('id','name')
            ->where('name','Process')
            ->get();

        return response()->json(compact('categories'));

    }
    public function store(Request $request){
    	$process = new Process;
    	$process->code = $request->code;
    	$process->process_name = $request->process_name;
    	$process->_location = $request->_location;
        $process->_category = $request->_category;
        $process->cost = $request->p_cost;
    	$process->save();
    	return redirect('process');
    }

    public function view($id){
        if (Gate::denies('create-supplies')) {
            abort(403);
        }
        $process = DB::table('mst_process as mp')
        ->select([
            'mp.id as mpid',
            'mp.code',
            'mp.process_name',
            'mp.cost',
            'ml.name as locname',
            'mc.name as catname'
        ])
        ->leftJoin('mst_categories as mc', 'mp._category', '=', 'mc.id')
        ->leftJoin('mst_locations as ml', 'mp._location', '=', 'ml.id')
        ->where('mp.id', $id)
        ->first();
        return view('process.view', compact('process'));
    }

    public function edit($id){
        $locations = Location::get();
        $uoms = UnitOfMeasure::get();
        $process = DB::table('mst_process as mp')
        ->select([
            'mp.id as mpid',
            'mp.code',
            'mp.process_name',
            'mp.cost',
            'ml.name as locname',
            'mp._location',
            'mc.name as catname'
        ])
        ->leftJoin('mst_categories as mc', 'mp._category', '=', 'mc.id')
        ->leftJoin('mst_locations as ml', 'mp._location', '=', 'ml.id')
        ->where('mp.id', $id)
        ->first();

        return view('process.edit', compact('process','locations','uoms'));
    }

    public function addProcess($fp){
        $item = Item::find($fp);
        $itempacking = LnkItemPacking::select('id', 'description')
            ->where('_item','=',$fp)
            ->get();

        return view('process.addprocess', compact('item', 'itempacking'));
    }

    public function storeprocess(Request $request, $id){
         
            $ch = LnkItemProcess::where('_item', $id)
            ->where('_process', $request->_hiddenpid)
            ->get();

            if(count($ch) > 0){
               Session::flash('duplicate', 'Duplicate Item');
                return back()
                ->withErrors(['error' => 'Duplicate Key']);
            }else{
                $lnkitemprocess = new LnkItemProcess;
                $lnkitemprocess->_item = $id;
                $lnkitemprocess->_process = $request->_hiddenpid;
                $lnkitemprocess->amount = $request->quantity;
                $lnkitemprocess->save();
            }      
        
        return redirect('finishedproduct/'.$id.'/edit');
    }

    public function updateProcess(Request $request, $id){
        // dd($request->all());
        $updateprocess = Process::find($id);
        // dd($updateprocess);
        $updateprocess->code = $request->code;
        $updateprocess->process_name = $request->process_name;
        $updateprocess->_location = $request->_location;
        $updateprocess->_category = $request->_category;
        $updateprocess->cost = $request->cost;
        $updateprocess->save();

        return redirect('process');
    }
    public function viewprocess(Request $request, $id, $prcid){
        $proc = LnkItemProcess::select('lnk_itemprocess.id as procid','proc.code as code','proc.process_name as name','loc.name as location','lnk_itemprocess.amount as amount','lnk_itemprocess._item as item')
        ->leftJoin('mst_process as proc', 'lnk_itemprocess._process', '=', 'proc.id')
        ->leftJoin('mst_locations as loc', 'proc._location', '=', 'loc.id')
        ->where('_item', $id)
        ->where('lnk_itemprocess.id', $prcid)
        ->first();
        
        // $proc = $lnkitemprocess::find($prcid);
        return view('process.fnshprod.view', compact('proc'));
    }
    public function editprocess(Request $request, $id, $prcid){
     
        $proc = LnkItemProcess::select('lnk_itemprocess.id as procid','proc.code as code','proc.process_name as name','loc.name as location','lnk_itemprocess.amount as amount','lnk_itemprocess._item as item')
        ->leftJoin('mst_process as proc', 'lnk_itemprocess._process', '=', 'proc.id')
        ->leftJoin('mst_locations as loc', 'proc._location', '=', 'loc.id')
        ->where('_item', $id)
        ->where('lnk_itemprocess.id', $prcid)
        ->first();
        // dd($proc);
        return view('process.fnshprod.edit', compact('proc','prcid'));
    }
    public function updatepro (Request $request){
        $id = $request->id;
        $procode = $request->pcode;
        $proname = $request->pname; 
        $amount = $request->quantity;
        $item = $request->item;

        $updatepro = LnkItemProcess::find($id);
        // dd($updatepro); 
        $updatepro->amount = $amount;
        $updatepro->save();
        return redirect('finishedproduct/'.$item.'/edit');
    }

    public function destroy($id, $prcid){
        
        $proc = LnkItemProcess::find($prcid);
      
        $proc->delete();

        return response()->json(['ok'=>'success']);
    }
    
}