<?php

namespace App\Http\Controllers;

use App\Location;
use App\LocationTypes;
use App\Position;
use App\SecGroup;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserSecurityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('securityuser.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locationtypes = Location::get();
        $positions = Position::all();
        $group = SecGroup::all();
        return view('securityuser.create',compact('group','locationtypes','positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            '_group' => 'required',
        ]);

        $user = new User($request->all());
        $user->password = Hash::make('john123');

        if (is_null($request->resetpassword)) {
            $user->resetpassword = 0;
        } else {
            $user->resetpassword = 1;
        }

        if (is_null($request->isactive)) {
            $user->isactive = 0;
        } else {
            $user->isactive = 1;
        }

        if (is_null($request->passwordwillexpire)) {
            $user->passwordwillexpire = 0;
        } else {
            $user->passwordwillexpire = 1;
        }

        if (is_null($request->accountwillexpire)) {
            $user->accountwillexpire = 0;
        } else {
            $user->accountwillexpire = 1;
        }

        if (is_null($request->locked)) {
            $user->locked = 0;
        } else {
            $user->locked = 1;
        }

        $user->createdby = Auth::user()->id;
        $user->firstname = strtoupper($request->firstname);
        $user->lastname = strtoupper($request->lastname);
        $user->middlename = strtoupper($request->middlename);
        $user->description = strtoupper($request->description);
        $user->save();

        return redirect('securityuser');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $locationtypes = Location::get();
        $user = User::find($id);
        $group = SecGroup::all();
        $positions = Position::all();
        return view('securityuser.edit',compact('user','group','locationtypes','positions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            '_group' => 'required',
        ]);

        $user = User::find($id);
        $user->fill($request->all());
        
        $user->password = Hash::make('john123');

        if (is_null($request->resetpassword)) {
            $user->resetpassword = 0;
        } else {
            $user->resetpassword = 1;
        }

        if (is_null($request->isactive)) {
            $user->isactive = 0;
        } else {
            $user->isactive = 1;
        }

        if (is_null($request->passwordwillexpire)) {
            $user->passwordwillexpire = 0;
        } else {
            $user->passwordwillexpire = 1;
        }

        if (is_null($request->accountwillexpire)) {
            $user->accountwillexpire = 0;
        } else {
            $user->accountwillexpire = 1;
        }

        if (is_null($request->locked)) {
            $user->locked = 0;
        } else {
            $user->locked = 1;
        }

        $user->createdby = Auth::user()->id;
        $user->firstname = strtoupper($request->firstname);
        $user->lastname = strtoupper($request->lastname);
        $user->middlename = strtoupper($request->middlename);
        $user->description = strtoupper($request->description);
        $user->save();

        return redirect('securityuser');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();

        return response()->json(['ok'=>'success']);
    }
    
    
    public function resetpassword(Request $request){

     
        $this->validate($request, [
            // 'username' => 'required|exists:sec_users,username',
            // 'currentpassword' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        ]);



        User::where('username',$request->username)
            ->update([
                'password'=> Hash::make($request->password)
            ]);


        return response()->json(['ok'=>'success']);

    }
}
