<?php

namespace App\Http\Controllers;

use App\PaymentTerm;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Gate;

class PaymentTermController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-payment')) {
            abort(403);
        }
        return view('paymentterm.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-payment')) {
            abort(403);
        }
        return view('paymentterm.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required|max:10|unique:mst_paymentterms,code',
            'description' => 'required',
            'factor' => 'required|integer',

        ]);



        $company = new PaymentTerm($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->description = strtoupper($request->description);
        $company->save();

        return redirect('paymentterm');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-payment')) {
            abort(403);
        }
        $company = PaymentTerm::find($id);
   
        return view('paymentterm.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = PaymentTerm::find($id);
        $company->fill($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->description = strtoupper($request->description);
        if(is_null($request->isactive)){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }
        $company->save();
        return redirect('paymentterm');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-payment')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        $company = PaymentTerm::find($id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }

    public function view($id)
    {
        /*if (Gate::denies('edit-payment')) {
            abort(403);
        }*/
        $company = PaymentTerm::find($id);
   
        return view('paymentterm.view',compact('company'));
    }
}
