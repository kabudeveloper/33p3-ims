<?php

namespace App\Http\Controllers;

use App\BranchCompany;
use App\Contact;
use App\Customer;
use App\Vendor;
use App\PaymentTerm;
use App\PoInstruction;
use App\Purchasing;
use App\PurchaseOrderDetails;
use App\TrnTrr;
use App\User;
use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\RedirectResponse;
use JavaScript;
use PDF;

class PurchasingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd('index');
        return view('purchasing.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $branches = BranchCompany::where('isactive', 1)->select('id','name')->get();
        $vendor = Vendor::select('id','name')->get();
        return view('purchasing.create',compact('branches','vendor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            '_branch' => 'required',
            '_vendor' => 'required',
            '_paymentterm' => 'required',
        ]);

        $po = new Purchasing($request->all());
        $po->createdby= Auth::user()->id;
        $po->_vendor= $request->vendoridhidden;
        $po->tmode= $request->tmode;
        $po->jonumber= $request->reference;
        $po->tdate= Carbon::now();
        $po->status = 'D';
        $po->save();
        $lastid = $po->tnumber;

        return redirect()->action('PurchasingController@edit', [$lastid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $po = Purchasing::find($id);
        $branches = BranchCompany::select('id','name')->get();
        $paymentterm = PaymentTerm::select('id','description')->get();
        $vendor = Vendor::select('id','name')->get();
        $vendorname = Vendor::where('id', $po->_vendor)->pluck('name')->first();

        $postatus = '';
        if($po->status == 'C'){
            $postatus = 'Cancel';
        }elseif( $po->status == 'D'){
            $postatus = 'Draft';
        }elseif( $po->status == 'A'){
            $postatus = 'Approved';
        }elseif( $po->status == 'S'){
            $postatus = 'Submitted';
        }

        JavaScript::put([
            'po' => $po,
        ]);


        return view('purchasing.edit',compact('po','branches','paymentterm','vendor','vendorname','postatus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $po = Purchasing::find($id);
        $po->fill($request->except([
            'status', 'tnumber', 'tdate'
        ]));
        $po->_vendor = $request->vendoridhidden;
        $po->tmode= $request->tmode;

        $poinstructcheck = Input::get('poinstructcheck');

        DB::table('trn_purchaseorderinstructions')->where('tnumber', $id)->delete();

        foreach ($poinstructcheck as $poinstruct)
        {
            DB::insert('INSERT INTO trn_purchaseorderinstructions (tnumber,_instruction) VALUES (?, ?)', array($id, $poinstruct));
        }

        $po->save();

        return redirect('purchase');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Purchasing::find($id)->delete();
         /*$jod = JoDetail::find($id);
          if($jod){
              $jod->delete();
          }*/
        
        return response()->json(['ok'=>'success']);
    }

    public function paymenttermsget(Request $request){
        
        $vendor= Vendor::find($request->vendorid);
        $terms = PaymentTerm::where('isactive', 1)->select('id', 'description')->get();
        //$terms = PaymentTerm::all();
        
        return response()->json(compact('terms','vendor'));
    }

    public function cancelpo(Request $request)
    {
        $jo = Purchasing::find($request->tnumber);
        $jo->status = "C";
        $jo->cancelleddatetime = Carbon::now();
        $jo->cancelledby = Auth::user()->id;
        $jo->save();
        return response()->json(['ok' => 'success']);
    }

    public function submitpo(Request $request)
    {
        $jo = Purchasing::find($request->tnumber);
        $jo->status = "S";
        $jo->submitteddatetime = Carbon::now();
        $jo->submittedby =  Auth::user()->id;
        $jo->save();
        return redirect()->back();
    }

    public function approvepo(Request $request)
    {
        $jo = Purchasing::find($request->tnumber);
        $jo->status = "A";
        $jo->approveddatetime = Carbon::now();
        $jo->approvedby = Auth::user()->id;
        $jo->save();
        return redirect()->back();
    }

    public function receivepo(Request $request)
    {
        $jo = Purchasing::find($request->tnumber);
        $jo->status = "R";
        $jo->save();
        return redirect()->back();
    }

    public function rollbackpo(Request $request)
    {
        $jo = Purchasing::find($request->tnumber);
        $jo->status = "D";
        $jo->save();
        return redirect()->back();
    }

    public function printpo(Request $request)
    {
        // dd($request->all());
        $po = Purchasing::find($request->tnumber);
        $paymenttermdesc = PaymentTerm::where('id', $po->_paymentterm)->pluck('description')->first();
        $vendorname = Vendor::where('id', $po->_vendor)->pluck('name')->first();
        $vendorcode = Vendor::where('id', $po->_vendor)->pluck('code')->first();
        $vendoraddress = Vendor::where('id', $po->_vendor)->pluck('address')->first();
        $podeldate = Carbon::parse($po->deliverydate)->format('m/d/Y');
        $printdate = Carbon::now()->format('m/d/Y');
        $contactname = Contact::where('_mstlnk', $po->_vendor)->
                        where('type', '2')->
                        where('isprimary', '1')->
                        pluck('name')->first();
        $contactemail = Contact::where('_mstlnk', $po->_vendor)->
                        where('type', '2')->
                        where('isprimary', '1')->
                        pluck('email')->first();
        $contactmobile = Contact::where('_mstlnk', $po->_vendor)->
                        where('type', '2')->
                        where('isprimary', '1')->
                        pluck('mobile')->first();
        $podetail = PurchaseOrderDetails::where('tnumber', $request->tnumber)->orderBy('_item')->get();
        $poinstruct = PoInstruction::select('id','instruction')->get();
        // dd($poinstruct);
        $trninstruct = DB::table('trn_purchaseorderinstructions')
            ->where('tnumber', $request->tnumber)
            ->select([
                '_instruction'
            ])->get();
             // dd($trninstruct);
        $creatorfname = User::where('id', $po->createdby)->pluck('firstname')->first();
        $creatorlname = User::where('id', $po->createdby)->pluck('lastname')->first();
        $approverfname = User::where('id', $po->approvedby)->pluck('firstname')->first();
        $approverlname = User::where('id', $po->approvedby)->pluck('lastname')->first();

        $sumtotal = collect([]);
        foreach ($podetail as $p) {
            $amount = $p->quantity * $p->unitprice;
            $sumtotal->push($amount);
        }

        $postatus = '';
        if($po->status == 'C'){
            $postatus = 'Cancelled';
        }elseif( $po->status == 'D'){
            $postatus = 'Draft Copy';
        }elseif( $po->status == 'A'){
          if($po->timesprinted == 0){
            $postatus = 'Original Copy';
          }
          else{
            $postatus = 'Reprint Copy #'.$po->timesprinted;
          }
        }elseif( $po->status == 'S'){
            $postatus = 'For Approval';
        }
            
            //dd($trninstruct);

        // $pdf = PDF::loadView('purchasing.poprint', compact('customers', 'custname', 'branch', 'jo', 'term', 'jod', 'sumtotal','jodatetransaction','jodateapproved','jocancell','josubmitted'));
        // $pdf = PDF::loadView('purchasing.poprint', compact('po', 'paymenttermdesc', 'podeldate', 'vendorname', 'vendorcode', 'vendoraddress', 'printdate', 'contactname', 'contactemail', 'contactmobile', 'podetail', 'sumtotal', 'trninstruct', 'poinstruct', 'creatorfname', 'creatorlname', 'approverfname', 'approverlname', 'postatus'));
        return view('purchasing.poprint', compact('po', 'paymenttermdesc', 'podeldate', 'vendorname', 'vendorcode', 'vendoraddress', 'printdate', 'contactname', 'contactemail', 'contactmobile', 'podetail', 'sumtotal', 'trninstruct', 'poinstruct', 'creatorfname', 'creatorlname', 'approverfname', 'approverlname', 'postatus'));

        // return $pdf->stream();
    }

    public function createtrr(Request $request)
    {

        $po = Purchasing::find($request->tnumber);
        $paymenttermdesc = PaymentTerm::where('id', $po->_paymentterm)->pluck('description')->first();
        $vendorname = Vendor::where('id', $po->_vendor)->pluck('name')->first();
        $vendorcode = Vendor::where('id', $po->_vendor)->pluck('code')->first();
        $vendoraddress = Vendor::where('id', $po->_vendor)->pluck('address')->first();
        $podeldate = Carbon::parse($po->deliverydate)->format('m/d/Y');
        $printdate = Carbon::now()->format('m/d/Y');
        $datetimecreated= Carbon::now();
        $createdby= Auth::user()->id;
        $contactname = Contact::where('_mstlnk', $po->_vendor)->
                        where('type', '2')->
                        where('isprimary', '1')->
                        pluck('name')->first();
        $contactemail = Contact::where('_mstlnk', $po->_vendor)->
                        where('type', '2')->
                        where('isprimary', '1')->
                        pluck('email')->first();
        $contactmobile = Contact::where('_mstlnk', $po->_vendor)->
                        where('type', '2')->
                        where('isprimary', '1')->
                        pluck('mobile')->first();
        $podetail = PurchaseOrderDetails::where('tnumber', $request->tnumber)->orderBy('_item')->get();
        $poinstruct = PoInstruction::select('id','instruction')->get();
        $trninstruct = DB::table('trn_purchaseorderinstructions')
            ->where('tnumber', $request->tnumber)
            ->select([
                '_instruction'
            ])->get();
        $creatorfname = User::where('id', $po->createdby)->pluck('firstname')->first();
        $creatorlname = User::where('id', $po->createdby)->pluck('lastname')->first();
        $approverfname = User::where('id', $po->approvedby)->pluck('firstname')->first();
        $approverlname = User::where('id', $po->approvedby)->pluck('lastname')->first();

        $sumtotal = collect([]);
        foreach ($podetail as $p) {
            $amount = $p->quantity * $p->unitprice;
            $sumtotal->push($amount);
        }

        $postatus = '';
        if($po->status == 'C'){
            $postatus = 'Cancelled';
        }elseif( $po->status == 'D'){
            $postatus = 'Draft Copy';
        }elseif( $po->status == 'A'){
          if($po->timesprinted == 0){
            $postatus = 'Original Copy';
          }
          else{
            $postatus = 'Reprint Copy #'.$po->timesprinted;
          }
        }elseif( $po->status == 'S'){
            $postatus = 'For Approval';
        }

        $vtrr = '';
        $trr = TrnTrr::where('ponumber', $request->tnumber)->pluck('tnumber')->first();

        if($trr){
            $vtrr = $trr;
        }
        else{
            DB::table('trn_trr')->insert([
                ['ponumber' => $request->tnumber, 'datetimecreated' => $datetimecreated, 'createdby' => $createdby]
            ]);
            $vtrr = TrnTrr::where('ponumber', $request->tnumber)->pluck('tnumber')->first();
        }

        // $pdf = PDF::loadView('purchasing.poprint', compact('customers', 'custname', 'branch', 'jo', 'term', 'jod', 'sumtotal','jodatetransaction','jodateapproved','jocancell','josubmitted'));
        // $pdf = PDF::loadView('purchasing.createtrr', compact('po', 'paymenttermdesc', 'podeldate', 'vendorname', 'vendorcode', 'vendoraddress', 'printdate', 'contactname', 'contactemail', 'contactmobile', 'podetail', 'sumtotal', 'trninstruct', 'poinstruct', 'creatorfname', 'creatorlname', 'approverfname', 'approverlname', 'postatus', 'vtrr'));
        // return $pdf->stream();
        return view('purchasing.createtrr', compact('po', 'paymenttermdesc', 'podeldate', 'vendorname', 'vendorcode', 'vendoraddress', 'printdate', 'contactname', 'contactemail', 'contactmobile', 'podetail', 'sumtotal', 'trninstruct', 'poinstruct', 'creatorfname', 'creatorlname', 'approverfname', 'approverlname', 'postatus', 'vtrr'));
        // return $pdf->stream();
    }
}
