<?php

namespace App\Http\Controllers;

use App\UnitOfMeasure;
use App\Uomconvertion;
use Illuminate\Http\Request;

use App\Http\Requests;

class UomconvertionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view('unitofmeasureconvertion/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $uoms = UnitOfMeasure::get();
        return view('unitofmeasureconvertion/create',compact('uoms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'buying_uom' => 'required',
            'production_uom' => 'required',
            'convertedvalues' => 'required'
        ]);

        $company = new Uomconvertion($request->all());
        $company->save();
        return redirect('unitofmeasureconvertion');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $convertion = Uomconvertion::find($id);
        $uoms = UnitOfMeasure::get();
        return view('unitofmeasureconvertion.edit',compact('uoms','convertion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'buying_uom' => 'required',
            'production_uom' => 'required',
            'convertedvalues' => 'required'
        ]);

        $company = Uomconvertion::find($id);
        $company->fill($request->all());
        $company->save();
        return redirect('unitofmeasureconvertion');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Uomconvertion::find($id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }
}
