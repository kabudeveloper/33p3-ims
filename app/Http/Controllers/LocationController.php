<?php

namespace App\Http\Controllers;

use App\BranchCompany;
use App\Location;
use App\LocationTypes;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Gate;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-location')) {
            abort(403);
        }
        return view('location.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-location')) {
            abort(403);
        }
        $code = Carbon::now()->timestamp;
        $maincompany = BranchCompany::select('id','name')->get();
        $locationtypes = LocationTypes::get();


        return view('location.create',compact('code','maincompany','locationtypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required|max:10|unique:mst_locations,code',
            'name' => 'required|unique:mst_locations,name',
            '_branch' => 'required',
            '_type' => 'required'
        ]);



        $company = new Location($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        $company->createdby= Auth::user()->id;
        $company->save();

        return redirect('location');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-location')) {
            abort(403);
        }
        $company = Location::find($id);
        $maincompany = BranchCompany::select('id','name')->get();
        $locationtypes = LocationTypes::get();
        return view('location.edit',compact('company','maincompany','locationtypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'code' => 'required|max:10',
            'name' => 'required',
            '_branch' => 'required',
            '_type' => 'required'
        ]);
        $company = Location::find($id);
        $company->fill($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        if(is_null($request->isactive)){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }

        $company->save();

        return redirect('location');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-location')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        $company = Location::find($id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }

    public function view($id)
    {
        /*if (Gate::denies('edit-location')) {
            abort(403);
        }*/
        $company = Location::find($id);
        $maincompany = BranchCompany::select('id','name')->get();
        $locationtypes = LocationTypes::get();
        return view('location.view',compact('company','maincompany','locationtypes'));
    }
}
