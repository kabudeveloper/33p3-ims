<?php

namespace App\Http\Controllers;

use App\Clas;
use App\SubClas;
use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Gate;

class SubClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-subclass')) {
            abort(403);
        }
        return view('subclss.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-subclass')) {
            abort(403);
        }

        $maincompany = Clas::select('id','name')->get();
        return view('subclss.create',compact('maincompany'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required|unique:mst_subclass,code',
            'name' => 'required',
            '_class' => 'required',

        ]);



        $company = new SubClas($request->all());
        $class_id = $request->_class;
        $class_code = Clas::where('id', $class_id)->pluck('code')->first();
        $cat_id = Clas::where('id', $class_id)->pluck('_category')->first();
        //$cat_code = Category::where('id', $cat_id)->pluck('code')->first();
        $company->code = strtoupper($class_code.'-'.$request->code);
        $company->name = strtoupper($request->name);
        $company->createdby= Auth::user()->id;
        $company->save();

        return redirect('subclss');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-subclass')) {
            abort(403);
        }
        $company = SubClas::find($id);
        $maincompany = Clas::select('id','name')->get();
        return view('subclss.edit',compact('company','maincompany'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'code' => 'required|max:50',
            'name' => 'required',
            '_class' => 'required',

        ]);
        $company = SubClas::find($id);
        $company->fill($request->all());
        $company->code = strtoupper($request->code);
        $company->name = strtoupper($request->name);
        if(is_null($request->isactive)){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }

        $company->save();
        return redirect('subclss');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-subclass')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);

        }
        $company = SubClas::find($id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }

    public function view($id)
    {
        /*if (Gate::denies('edit-subclass')) {
            abort(403);
        }*/
        $company = SubClas::find($id);
        $maincompany = Clas::select('id','name')->get();
        return view('subclss.view',compact('company','maincompany'));
    }
}
