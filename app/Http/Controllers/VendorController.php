<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\PaymentTerm;
use App\Vendor;
use App\VendorType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Gate;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-vendor')) {
            abort(403);
        }
        return view('vendors.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (Gate::denies('create-vendor')) {
            abort(403);
        }
        $paymentterms = PaymentTerm::select('id', 'description')->get();
        $vendortypes = VendorType::select('id', 'name')->get();


        return view('vendors.create', compact('paymentterms', 'vendortypes'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required|max:15|unique:mst_vendors,code',
            'name' => 'required',
            'address' => 'required',
            'tin' => 'required',
            'zipcode' => 'required',
            '_paymentterm' => 'required',
            '_type' => 'required',

        ]);


        $company = new Vendor($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        $company->address = strtoupper($request->address);
        $company->createdby = Auth::user()->id;
        $company->save();

        return redirect('vendors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-vendor')) {
            abort(403);
        }
        $company = Vendor::find($id);
        $paymentterms = PaymentTerm::select('id', 'description')->get();


        $vendortypes = VendorType::select('id', 'name')->get();
        return view('vendors.edit', compact('company', 'paymentterms', 'vendortypes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Vendor::find($id);
        $company->fill($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        $company->address = strtoupper($request->address);

        if (is_null($request->isactive)) {
            $company->isactive = 0;
        } else {
            $company->isactive = 1;
        }
        $company->save();
        return redirect('vendors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-vendor')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        $company = Vendor::find($id);
        $company->delete();

        return response()->json(['ok' => 'success']);
    }

    public function verifycode(Request $request)
    {

        $this->validate($request, [
            '_vendor' => 'required'
        ]);

        $vendor = Vendor::select('id', 'code', 'name')->where('code', $request->_vendor)->first();
        return response()->json(compact('vendor'));
    }

    public function vendorall()
    {

        $vendor = Vendor::select('id', 'code', 'name')->get();
        return response()->json(compact('vendor'));
    }

    public function vendorsearch(Request $reqquest)
    {

        if (isset($reqquest->containssearch)) {
            if ($reqquest->criteria === 'vendorname') {
                $vendor = Vendor::where('name', 'LIKE', '%'.$reqquest->valuesearch.'%')->get();
            }else if($reqquest->criteria === 'vendorcode'){
                $vendor = Vendor::where('code', 'LIKE', '%'.$reqquest->valuesearch.'%')->get();
            }
        } else {
            if ($reqquest->criteria === 'vendorcode') {
                $vendor = Vendor::where('code', $reqquest->valuesearch)->get();
            } else if($reqquest->criteria === 'vendorname'){
                $vendor = Vendor::where('name', $reqquest->valuesearch)->get();
            }
        }

        return response()->json(compact('vendor'));

    }
    
    public function vendorsearchid(Request $request){
        
        $vendor = Vendor::findOrFail($request->searchid);
        return response()->json(compact('vendor'));
    }

    public function view($id)
    {
        /*if (Gate::denies('edit-vendor')) {
            abort(403);
        }*/
        $company = Vendor::find($id);
        $paymentterms = PaymentTerm::select('id', 'description')->get();


        $vendortypes = VendorType::select('id', 'name')->get();
        return view('vendors.view', compact('company', 'paymentterms', 'vendortypes'));
    }
}
