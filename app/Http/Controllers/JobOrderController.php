<?php

namespace App\Http\Controllers;

use App\BranchCompany;
use App\Customer;
use App\JobOrders;
use App\JoDetail;
use App\LeadTime;
use App\PaymentTerm;
use App\SalesOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\RedirectResponse;
use JavaScript;
use PDF;
use Gate;

class JobOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-jo')) {
            abort(403);
        }
        return view('joborder.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-jo')) {
            abort(403);
        }
        $branches = BranchCompany::where('isactive', 1)->select('id', 'name')->get();
        $customers = Customer::select('id', 'name')->get();
        return view('joborder.create', compact('branches', 'customers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('create-jo')) {
            abort(403);
        }

        $this->validate($request, [
            '_branch' => 'required',
            '_customer' => 'required',
            '_paymentterm' => 'required',
            // '_sonumber' => 'required',
        ]);

        $refcheck = JobOrders::where('reference',$request->reference)->first();
        if ($refcheck) {
            Session::flash('refdup', 'Reference number already use !');
            return back();
        }

        $jo = new JobOrders($request->all());
        $jo->createdby = Auth::user()->id;
        $jo->_customer = $request->custidhidden;
        $jo->tdate = Carbon::now();
        $jo->status = 'D';
        $jo->save();
        $lastid = $jo->tnumber;

        return redirect()->action('JobOrderController@edit', [$lastid]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        if (Gate::denies('edit-jo')) {
            abort(403);
        }
        $jo = JobOrders::find($id);

        if($jo->fromso == 1){
            $branches = BranchCompany::select('id', 'name')->get();
            $customers = Customer::select('id', 'name')->get();
            $custname1 = Customer::find($jo->_customer);
            $custname = $custname1->name;
            $custid = $custname1->id;
            $so = SalesOrder::find($jo->reference);
            $leadtime = LeadTime::find($so->_leadtime);
            // $d = Carbon::parse($jo->createddatetime);

            // $maxdate = $d->addWeekdays($leadtime->highervalue);
            $maxdate = '';

            // $custd = Customer::all();
            // dd($custname->name);

            $jostatus = '';
            if ($jo->status == 'C') {
                $jostatus = 'Cancelled';
            } elseif ($jo->status == 'D') {
                $jostatus = 'Draft';
            } elseif ($jo->status == 'A') {
                $jostatus = 'Approved';
            } elseif ($jo->status == 'S') {
                $jostatus = 'Submitted';
            }

            JavaScript::put([
                'jo' => $jo,
                'manual' => 1,
                'custid' => $custid
            ]);
            $manual = 1;
            $referenceformat = 'SO#'.$jo->reference;
            return view('joborder.edit', compact('jo', 'branches', 'customers', 'custname', 'jostatus','referenceformat','manual'));

        }else{
            $branches = BranchCompany::select('id', 'name')->get();
            $customers = Customer::select('id', 'name')->get();
            $custname1 = Customer::find($jo->_customer);
            $custname = $custname1->name;
// dd($jo->tdate);
            JavaScript::put([
                'jo' => $jo,
                'manual' => 0
            ]);
            $manual = 0;
            // dd($jo);
            $jostatus = '';
            if ($jo->status == 'C') {
                $jostatus = 'Cancell';
            } elseif ($jo->status == 'D') {
                $jostatus = 'Draft';
            } elseif ($jo->status == 'A') {
                $jostatus = 'Approved';
            } elseif ($jo->status == 'S') {
                $jostatus = 'Submitted';
            }

            $referenceformat = $jo->reference;

            return view('joborder.edit', compact('jo', 'branches', 'customers', 'custname', 'jostatus','manual','referenceformat'));

        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('edit-jo')) {
            abort(403);
        }
        $jo = JobOrders::find($id);
        $jo->fill($request->except([
            'status', 'tnumber', 'tdate'
        ]));
        $jo->_customer = $request->custidhidden;
        $jo->save();

        return redirect('joborder');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-jo')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        JobOrders::find($id)->delete();
        $jod = JoDetail::find($id);
        if ($jod) {
            $jod->delete();
        }

        return response()->json(['ok' => 'success']);
    }

    public function getpaymentterms(Request $request)
    {

        $customer = Customer::find($request->customerid);
        $terms = PaymentTerm::where('isactive', 1)->select('id', 'description')->get();
        //$terms = PaymentTerm::all();

        return response()->json(compact('terms', 'customer'));
    }

    public function canceljoborder(Request $request)
    {
        if (Gate::denies('cancel-jo')) {
            abort(403);
        }
        $jo = JobOrders::find($request->tnumber);
        $jo->status = "C";
        $jo->cancelleddatetime = Carbon::now();
        $jo->cancelledby = Auth::user()->id;
        $jo->save();
        return response()->json(['ok' => 'success']);
    }

    public function submitjo(Request $request)
    {
        if (Gate::denies('submit-jo')) {
            abort(403);
        }
        $jo = JobOrders::find($request->tnumber);
        $jo->status = "S";
        $jo->fromso = 0;
        $jo->submitteddatetime = Carbon::now();
        $jo->submittedby = Auth::user()->id;
        $jo->save();
        return redirect()->back();
    }

    public function approvejo(Request $request)
    {
        if (Gate::denies('approve-jo')) {
            abort(403);
        }
        $jo = JobOrders::find($request->tnumber);
        $jo->status = "A";
        $jo->approveddatetime = Carbon::now();
        $jo->approvedby = Auth::user()->id;
        $jo->save();
        return redirect()->back();
    }

    public function rollbackjo(Request $request)
    {
        if (Gate::denies('rollback-jo')) {
            abort(403);
        }
        $jo = JobOrders::find($request->tnumber);
        $jo->status = "D";
        $jo->save();
        return redirect()->back();
    }

    public function printjo(Request $request)
    {
        if (Gate::denies('print-jo')) {
            abort(403);
        }

        $jo = JobOrders::find($request->tnumber);
        $branch = JobOrders::find($request->tnumber)->branch()->first();
        $term = Customer::find($jo->_customer)->paymentterm()->first();
        $custname = Customer::find($jo->_customer)->pluck('name')->first();

        $jod = JoDetail::where('tnumber',$request->tnumber)->orderBy('_item')->get();
        $jodatetransaction = Carbon::parse($jo->tdate)->format('Y/m/d');
        $jodateapproved = Carbon::parse($jo->approveddatetime)->format('Y/m/d');
        $jocancell = Carbon::parse($jo->cancelleddatetime)->format('Y/m/d');
        $josubmitted = Carbon::parse($jo->submitteddatetime)->format('Y/m/d');


        $sumtotal = collect([]);
        foreach ($jod as $j) {
            $amount = $j->quantity * $j->unitprice;
            $sumtotal->push($amount);
        }


        // $pdf = PDF::loadView('joborder.joprint', compact('customers', 'custname', 'branch', 'jo', 'term', 'jod', 'sumtotal', 'jodatetransaction', 'jodateapproved', 'jocancell', 'josubmitted'));

         return view('joborder.joprint', compact('customers', 'custname', 'branch', 'jo', 'term', 'jod', 'sumtotal', 'jodatetransaction', 'jodateapproved', 'jocancell', 'josubmitted'));

        // return $pdf->stream();
    }

    public function printjomgmt(Request $request)
    {
        if (Gate::denies('printmgmnt-jo')) {
            abort(403);
        }

        $jo = JobOrders::find($request->tnumber);
        $branch = JobOrders::find($request->tnumber)->branch()->first();
        $term = Customer::find($jo->_customer)->paymentterm()->first();
        $custname = Customer::find($jo->_customer)->pluck('name')->first();
        $jod = JoDetail::where('tnumber',$request->tnumber)->orderBy('_item')->get();
        $jodatetransaction = Carbon::parse($jo->tdate)->format('Y/m/d');
        $jodateapproved = Carbon::parse($jo->approveddatetime)->format('Y/m/d');
        $jocancell = Carbon::parse($jo->cancelleddatetime)->format('Y/m/d');
        $josubmitted = Carbon::parse($jo->submitteddatetime)->format('Y/m/d');

        $sumtotal = collect([]);
        foreach ($jod as $j) {
            $amount = $j->quantity * $j->unitprice;
            $sumtotal->push($amount);
        }


        // $pdf = PDF::loadView('joborder.joprintmgmt', compact('customers', 'custname', 'branch', 'jo', 'term', 'jod', 'sumtotal', 'jodatetransaction', 'jodateapproved', 'jocancell', 'josubmitted'));

         return view('joborder.joprintmgmt', compact('customers', 'custname', 'branch', 'jo', 'term', 'jod', 'sumtotal', 'jodatetransaction', 'jodateapproved', 'jocancell', 'josubmitted'));
         
        // return $pdf->stream();
    }


}
