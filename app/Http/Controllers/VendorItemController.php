<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Item;
use App\Lnkvendoritems;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Validator;
use Gate;


class VendorItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($item)
    {
        /*if (Gate::denies('create')) {
            abort(403);
        }*/
        $item = Item::find($item);

        return view('vendoritem.create', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

        $this->validate($request, [
            '_vendor' => 'required',
            'cost' => 'required'

        ]);

        try {

            $vendor = Vendor::find($request->_hiddenvendorid);
            if (is_null($request->isactive)) {
                $request->isactive = 0;
            }
            Lnkvendoritems::where('_item', $id)
                ->update(['isactive' => 0]);
            $vendor->item()->attach($id, ['cost' => $request->cost, 'isactive' => $request->isactive]);

        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', 'Vendor has already this item');
            return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }


        return redirect('rawmaterial/' . $id . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($itemid, $vendorid)
    {
        /*if (Gate::denies('edit')) {
            abort(403);
        }*/
        $item = Item::find($itemid);

        $vendoritem = DB::table('mst_vendors as vendor')
            ->join('lnk_vendoritems as lnkvenditems', 'vendor.id', '=', 'lnkvenditems._vendor')
            ->where('_vendor', $vendorid)
            ->where('lnkvenditems._item', '=', $itemid)
            ->select([
                'vendor.code',
                'vendor.name',
                'lnkvenditems._vendor',
                'lnkvenditems.cost',
                'lnkvenditems.isactive'
            ])->first();


        return view('vendoritem.edit', compact('item', 'vendoritem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $itemid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $itemid, $vendorid)
    {
        try {
            
            if (is_null($request->isactive)) {
                $active = 0;
            } else {
                $active = 1;
            }

            Lnkvendoritems::where('_item', $itemid)
                ->update(['isactive' => 0]);

            Lnkvendoritems::where('_vendor', $vendorid)
                ->where('_item', $itemid)
                ->update(
                    [
                        '_vendor' => $request->_hiddenvendorid,
                        '_item' => $itemid,
                        'cost' => $request->cost,
                        'isactive' => $active
                    ]
                );
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', 'Vendor has already this item');
            return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }
        return redirect('rawmaterial/' . $itemid . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $itemid
     * @return \Illuminate\Http\Response
     */
    public function destroy($itemid, $vendorid)
    {
       /* if (Gate::denies('delete')){
            abort(403);
        }*/

        $company = Lnkvendoritems::where('_vendor', '=', $vendorid)
            ->where('_item', '=', $itemid)->delete();

        return response()->json(['ok' => 'success']);

    }

    public function view($itemid, $vendorid)
    {
        /*if (Gate::denies('edit')) {
            abort(403);
        }*/
        $item = Item::find($itemid);

        $vendoritem = DB::table('mst_vendors as vendor')
            ->join('lnk_vendoritems as lnkvenditems', 'vendor.id', '=', 'lnkvenditems._vendor')
            ->where('_vendor', $vendorid)
            ->where('lnkvenditems._item', '=', $itemid)
            ->select([
                'vendor.code',
                'vendor.name',
                'lnkvenditems._vendor',
                'lnkvenditems.cost',
                'lnkvenditems.isactive'
            ])->first();


        return view('vendoritem.view', compact('item', 'vendoritem'));
    }
}
