<?php

namespace App\Http\Controllers;
use App\BranchCompany;
use App\Item;
use App\PaymentTerm;
use App\PurchaseOrderDetails;
use App\LnkItemPacking;
use App\Lnkvendoritems;
use App\Purchasing;
use App\Vendor;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use JavaScript;

class PurchaseBalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('purchasebalance.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createpo(Request $request,$trn)
    {
      
        $po = DB::table('trn_purchaseorders as po')
            ->select(
                    'po._branch',
                    'po.tnumber',
                    'po.tdate',
                    'po._vendor as vendor',
                    'po.tmode as tmode',
                    'po.jonumber',
                    'po.notes',
                    'po._paymentterm',
                    'po.status',
                    'rd.qtyaccepted',
                    'rd.qtydelivered',
                    'rd._item',
                    'rd.packing',
                    'rd.poprice'
                )
            ->leftjoin('mst_vendors as vendor', 'po._vendor', '=', 'vendor.id')
            ->leftjoin('trn_receiving as r','po.tnumber','=','r.ponumber')
            ->leftjoin('trn_receivingdetails as rd','r.tnumber','=','rd.tnumber')
            ->where('po.tnumber',$trn)
            ->get();

        $find = Purchasing::find($trn);
        $find->iscreated = 1;
        $find->save();
        $getnew = Purchasing::where('tnumber',$trn)->first();
        $newpo = new Purchasing;
        $newpo->createdby= Auth::user()->id;
        $newpo->tmode= $getnew->tmode;
        $newpo->_branch= $getnew->_branch;
        $newpo->_vendor= $getnew->_vendor;
        $newpo->_paymentterm= $getnew->_paymentterm;
        $newpo->jonumber= $getnew->jonumber;
        $newpo->tdate= Carbon::now();
        $newpo->createddatetime = Carbon::today();
        $newpo->deliverydate = Carbon::today();
        $newpo->status = 'D';
        $newpo->save();
        $lastid = $newpo->tnumber;

       
        foreach ($po as $p) {
            $bal = $p->qtydelivered - $p->qtyaccepted;
            if ($bal == 0) {
                # code...
            }else{
                $podetail = new PurchaseOrderDetails;
                $podetail->tnumber = $lastid;
                $podetail->_item = $p->_item;
                $podetail->packing = $p->packing;
                $podetail->quantity = $bal;
                $podetail->unitprice = $p->poprice;
                $podetail->lno = 0;
                $podetail->save();
            }
            
        }

        return redirect()->action('PurchasingController@edit', [$lastid]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $po = Purchasing::find($id);
        $branches = BranchCompany::select('id','name')->get();
        $paymentterm = PaymentTerm::select('id','description')->get();
        $vendor = Vendor::select('id','name')->get();
        $vendorname = Vendor::where('id', $po->_vendor)->pluck('name')->first();

        $postatus = '';
        if($po->status == 'C'){
            $postatus = 'Cancel';
        }elseif( $po->status == 'D'){
            $postatus = 'Draft';
        }elseif( $po->status == 'A'){
            $postatus = 'Approved';
        }elseif( $po->status == 'S'){
            $postatus = 'Submitted';
        }

        JavaScript::put([
            'po' => $po,
        ]);


        return view('purchasebalance.view',compact('po','branches','paymentterm','vendor','vendorname','postatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$item)
    {
         $podetail = PurchaseOrderDetails::
              where('tnumber',$id)
             ->where('_item',$item)
             ->first();

        $item = Item::find($item);
        $po = Purchasing::select('_vendor')->where('tnumber', $id)->first();


        JavaScript::put([
            'podetail' => $podetail,
        ]);

        return view('purchaseorderdetail.edit',compact('podetail','item','po'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        PurchaseOrderDetails::where('tnumber',$request->tnumber)
             ->where('_item',$id)
             ->update([
                 'packing'=>$request->packing,
                 'quantity'=>$request->quantity
             ]);

        return redirect()->action('PurchasingController@edit', [$request->tnumber]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$item)
    {
          PurchaseOrderDetails::where('tnumber',$id)
              ->where('_item',$item)->delete();

        return response()->json(['ok'=>'success']);
    }

}
