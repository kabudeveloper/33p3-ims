<?php

namespace App\Http\Controllers;

use App\Item;
use App\SalesOrderDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use JavaScript;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Mockery\Tests\React_WritableStreamInterface;
use Gate;

class SalesOrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (Gate::denies('create-sod')) {
            abort(403);
        }
        $trn = $request->trnid;

        return view('salesorderdetail.create', compact('trn'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('create-sod')) {
            abort(403);
        }

        $this->validate($request, [
            'quantity'=>'required',
        ]);

        try {
            $costprice = DB::table('vw_costprice')
                ->where('_item', $request->itemhidden)
                ->select(['costprice'])
                ->pluck('costprice');

            $sod = new SalesOrderDetail($request->all());
            $sod->_item = $request->itemhidden;
            $sod->unitprice = $costprice[0];
            $sod->aup = $request->finalsellingprice ; //$costprice[0];
            $sod->save();
            return redirect('salesorder/' . $request->tnumber . '/edit');
        } catch (\Illuminate\Database\QueryException $e) {

            if($e->getCode() == 23000){
                $msg = "Duplicate Item is not Allowed !";
            }else{
                $msg = "There was an error saving the record, Please contact your Administrator";
            }
            Session::flash('duplicate', $msg);
            return back();

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $item)
    {
        if (Gate::denies('edit-sod')) {
            abort(403);
        }
        $sod = SalesOrderDetail::where('tnumber', $id)
            ->where('_item', $item)
            ->first();
        $item = Item::find($sod->_item);

        JavaScript::put([
            'sod' => $sod,
        ]);

        return view('salesorderdetail.edit', compact('sod', 'item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('edit-sod')) {
            abort(403);
        }
        SalesOrderDetail::where('tnumber', $request->tnumber)
            ->where('_item', $request->itemhidden)
            ->update([
                'packing' => $request->packing,
                'quantity' => $request->quantity

            ]);

        return redirect()->action('SalesOrderController@edit', [$request->tnumber]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $item)
    {
        if (Gate::denies('delete-sod')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }

        SalesOrderDetail::where('tnumber', $id)
            ->where('_item', $item)->delete();

        return response()->json(['ok' => 'success']);
    }

    public function getrmcost(Request $request)
    {
        $id = $request->id;
        $isacombo = Item::select('isacombo')->find($id);
        if ($isacombo->isacombo) {
            $rmcost = DB::table('vw_totalrmcost as rm')
                ->join('vw_opcost as op', 'rm._item', '=', 'op._item')
                ->where('rm._item', $id)
                ->select(
                    \DB::raw('(rm.rmcost + op.opcost) as totalprice')
                )->pluck('totalprice');
        } else {
            $rmcost = DB::table('vw_totalrmcost as rm')
                ->where('rm._item', $id)
                ->select(
                    'rmcost as totalprice'
                )->pluck('totalprice');
        }


        return response()->json(compact('rmcost'));

    }

    public function updateaup(Request $request)
    {
        if (Gate::denies('aup-sod')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }

        SalesOrderDetail::where('tnumber', $request->tnumber)
            ->where('_item', $request->item)
            ->update([
                'aup' => $request->aup
            ]);


        return response()->json(['ok' => 'success']);
    }
}
