<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Gate;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-category')) {
            abort(403);
        }
        return view('category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (Gate::denies('create-category')) {
            abort(403);
        }
        return view('category.create');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required|max:10|unique:mst_categories,code',
            'name' => 'required'

        ]);

        $company = new Category($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        $company->createdby= Auth::user()->id;
        $company->save();

        return redirect('category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-category')) {
            abort(403);
        }
        $company = Category::find($id);
        return view('category.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Category::find($id);
        $company->fill($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        if(is_null($request->isactive)){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }

        $company->save();
        return redirect('category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-category')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);

        }
        $company = Category::find($id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }

    public function view($id)
    {
        /*if (Gate::denies('edit-category')) {
            abort(403);
        }*/
        $company = Category::find($id);
        return view('category.view',compact('company'));
    }
}
