<?php

namespace App\Http\Controllers;

use App\BranchCompany;
use App\Company;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Gate;

class BranchCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-branch')) {
            abort(403);
        }
        return view('compbranch.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (Gate::denies('create-branch')) {
            abort(403);
        }
        $maincompany = Company::select('id','name')->get();

        /*if($company == null){
            $code = str_pad(0 + 1,10,'0',STR_PAD_LEFT);
        }else{
            $code = str_pad($company->code + 1,10,'0',STR_PAD_LEFT);
        }*/

        return view('compbranch.create',compact('code','maincompany'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code'=> 'required|max:10|unique:mst_branches,code',
            'name' => 'required',
            'address' => 'required',
            '_company' => 'required',
            'tin' => 'required',
            'zipcode' => 'required',

        ]);



        $company = new BranchCompany($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        $company->address = strtoupper($request->address);
        $company->createdby= Auth::user()->id;
        $company->save();

        return redirect('branch');
        
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-branch')) {
            abort(403);
        }
        $company = BranchCompany::find($id);
        $maincompany = Company::select('id','name')->get();
        return view('compbranch.edit',compact('company','maincompany'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $company = BranchCompany::find($id);
        $company->fill($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        $company->address = strtoupper($request->address);

        if(is_null($request->isactive)){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }

        $company->save();
        return redirect('branch');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-branch')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        $company = BranchCompany::find($id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }

    public function view($id)
    {
        /*if (Gate::denies('edit-branch')) {
            abort(403);
        }*/
        $company = BranchCompany::find($id);
        $maincompany = Company::select('id','name')->get();
        return view('compbranch.view',compact('company','maincompany'));
    }
}
