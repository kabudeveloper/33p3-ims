<?php

namespace App\Http\Controllers;

use App\BillOfMaterial;
use App\Category;
use App\Clas;
use App\Company;
use App\Http\Requests;
use App\Item;
use App\LnkItemComposition;
use App\LnkItemCosting;
use App\LnkItemPacking;
use App\LnkitemProcess;
use App\Location;
use App\LocationTypes;
use App\SubClas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;
use JavaScript;
use Gate;

class FinishedProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-finish')) {
            abort(403);
        }
        return view('finishedproduct/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create-finish')) {
            abort(403);
        }
        $locations = Location::get();
        return view('finishedproduct.create',compact('locations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'code' => 'required|max:25|unique:mst_items,code',
            'name' => 'required',
            '_class' => 'required',
            '_subclass' => 'required'

        ]);

        $catid = Category::select('id')->where('name', 'Finished Product')->first();
        $company = new Item($request->all());
        $company->code = strtoupper('FP'.$request->code);
        $company->name = strtoupper($request->name);
        $company->_category = $catid->id;
        $company->createdby = Auth::user()->id;

        if ($request->hasFile('image')) {
            $filename = $request->image->getClientOriginalName();
            $image = $request->image->getRealPath();
            $path = public_path() . '/fiinsishedproducts/';
            $fimage = Image::make($image)->resize(200, 200);
            $fimage->save($path . $filename);
            $company->image = $filename;
        }


        $company->save();
        $lastid = $company->id;
        return redirect()->action('FinishedProductController@edit', [$lastid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-finish')) {
            abort(403);
        }
        $locations= Location::get();
        $company = Item::find($id);
        $isacombo = Item::select('isacombo')->find($id);
        if($company->image == null){
            $imagepath = url('/') . '/fiinsishedproducts/npy.jpg';
        }else{
            $imagepath = url('/') . '/fiinsishedproducts/'.$company->image;
        }
        

        if ($isacombo->isacombo) {
            $rmcost = DB::table('vw_totalrmcost as rm')
                ->join('vw_opcost as op', 'rm._item', '=', 'op._item')
                ->where('rm._item', $id)
                ->select(
                    \DB::raw('(rm.rmcost + op.opcost) as totalprice')
                )->pluck('totalprice');
        } else {
            $rmcost = DB::table('vw_totalrmcost as rm')
                ->where('rm._item', $id)
                ->select(
                    'rmcost as totalprice'
                )->pluck('totalprice');
        }


        JavaScript::put([
            'mainitem' => $company,
            'rmcost' => $rmcost
        ]);


        return view('finishedproduct.edit', compact('company','imagepath','locations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $company = Item::find($id);
        $company->fill($request->all());
        $company->code = strtoupper($request->code);
        $company->name = strtoupper($request->name);

        if (is_null($request->isacombo)) {
            $combo = 0;
        } else {
            $combo = 1;
        }
        $company->isacombo = $combo;

        if ($request->hasFile('image')) {
            $filename = $request->image->getClientOriginalName();
            $image = $request->image->getRealPath();
            $path = public_path() . '/fiinsishedproducts/';
            $fimage = Image::make($image)->resize(200, 200);
            $fimage->save($path . $filename);
            $company->image = $filename;
        }
        
        $company->save();

        return redirect('finishedproduct/' . $id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-finish')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }
        $company = Item::find($id);
        $company->delete();

        $bom = BillOfMaterial::where('_item', $id);
        if ($bom) {
            $bom->delete();
        }

        $lnkcom = LnkItemComposition::where('_item', $id);

        if ($lnkcom) {
            $lnkcom->delete();
        }

        $lnkcos = LnkItemCosting::where('_item', $id);

        if ($lnkcos) {
            $lnkcos->delete();
        }

        $ip = LnkItemPacking::where('_item', $id);

        if ($ip) {
            $ip->delete();
        }


        return response()->json(['ok' => 'success']);
    }

    public function getcategory()
    {


        $categories = Category::select('id', 'name')
            ->where('name', 'Finished Product')
            ->get();

        return response()->json(compact('categories'));

    }

    public function getclss(Request $request)
    {


        $classess = Clas::select('id', 'name')
            ->where('_category', $request->catid)
            ->get();

        return response()->json(compact('classess'));

    }

    public function getsubclass(Request $request)
    {
        $subclassess = SubClas::select('id', 'name')
            ->where('_class', $request->classid)
            ->get();

        return response()->json(compact('subclassess'));
    }

    public function geteditcategory(Request $request)
    {

        $categories = Category::select('id', 'name')
            ->where('id', $request->catid)
            ->get();

        return response()->json(compact('categories'));
    }

    public function costingsave(Request $request)
    {

        $this->validate($request, [
            'lbcost' => 'required',
            'markup' => 'required',
            'ovcost' => 'required',
            'omcost' => 'required'

        ]);

        $itemcost = LnkItemCosting::where('_item', '=', $request->item)->first();

        if($itemcost === null){
            $lnkcost = new LnkItemCosting();
            $lnkcost->_item = $request->item;
            $lnkcost->omcost = $request->omcost;
            $lnkcost->lbcost = $request->lbcost;
            $lnkcost->ovcost = $request->ovcost;
            $lnkcost->markup = $request->markup;
            $lnkcost->rmcost = $request->rmcost;
            $lnkcost->commitedprice = $request->cprice;
            $lnkcost->ocost = $request->ocost;
            $lnkcost->save();
        }else{
           $lnkcost =  LnkItemCosting::find($request->item);
            $lnkcost->_item = $request->item;
            $lnkcost->omcost = $request->omcost;
            $lnkcost->lbcost = $request->lbcost;
            $lnkcost->ovcost = $request->ovcost;
            $lnkcost->markup = $request->markup;
            $lnkcost->rmcost = $request->rmcost;
            $lnkcost->commitedprice = $request->cprice;
            $lnkcost->ocost = $request->ocost;
            $lnkcost->save();
        }


        return response()->json(compact(['ok' => 'success']));

    }

    public function costingupdate(Request $request)
    {

        $this->validate($request, [
            'lbcost' => 'required',
            'markup' => 'required',
            'ovcost' => 'required',
            'omcost' => 'required'

        ]);

        LnkItemCosting::where('_item', $request->_item)
            ->update($request->except(['subitem', 'rmcost', 'rmcosthidden', 'costprice', 'sellingprice']));

        return response()->json(compact(['ok' => 'success']));

    }

    public function pullcosting(Request $request)
    {

        $itemcosting = LnkItemCosting::where('_item', $request->_item)->first();

        return response()->json(compact('itemcosting'));

    }

    public function view($id)
    {
        /*if (Gate::denies('edit-finish')) {
            abort(403);
        }*/
        $locations= Location::get();
        $company = Item::find($id);
        $isacombo = Item::select('isacombo')->find($id);
        if($company->image == null){
            $imagepath = url('/') . '/fiinsishedproducts/npy.jpg';
        }else{
            $imagepath = url('/') . '/fiinsishedproducts/'.$company->image;
        }

        if ($isacombo->isacombo) {
            $rmcost = DB::table('vw_totalrmcost as rm')
                ->join('vw_opcost as op', 'rm._item', '=', 'op._item')
                ->where('rm._item', $id)
                ->select(
                    \DB::raw('(rm.rmcost + op.opcost) as totalprice')
                )->pluck('totalprice');
        } else {
            $rmcost = DB::table('vw_totalrmcost as rm')
                ->where('rm._item', $id)
                ->select(
                    'rmcost as totalprice'
                )->pluck('totalprice');
        }


        JavaScript::put([
            'mainitem' => $company,
            'rmcost' => $rmcost
        ]);


        return view('finishedproduct.view', compact('company','imagepath','locations'));
    }

    public function printfinishproduct(Request $request)
    { 
              
        $id1 = Item::find($request->id);
        if (count($id1)>0) {
        $costing = LnkItemCosting::where('_item',$request->id)->first();
        if (count($costing)>0) {
            $costprice = $costing->omcost + $costing->lbcost+ $costing->ovcost+ $costing->rmcost;
            $sellingprice = $costprice + $costing->markup;
        }else{
            Session::flash('duplicate', 'Missing Bill of Material data');
                return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }
       
        }else{
            Session::flash('duplicate', 'Missing data');
                return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }
        
   
        
        $bom = DB::table('mst_items as item')
        ->select(
                'itembom._item',
                'item.name',
                'itempacking.description',
                'itembom.quantity',
                'itembom._subitem',
                'avgcost.cost',
                'itembom.directrm',
                \DB::raw('( itembom.quantity * avgcost.cost ) as totalcost')
            )
            ->join('lnk_itembom as itembom', 'item.id', '=', 'itembom._subitem')
            ->join('lnk_itempacking as itempacking', 'itempacking.id', '=', 'itembom.packing')
            ->join('vw_rmcost as avgcost', function ($q) {
                $q->on('avgcost._item', '=', 'itembom._subitem')
                    ->on('itembom.packing', '=', 'avgcost.packing');
            })           
            ->where('itembom._item', '=', $request->id)
            ->get();
        if (count($bom)>0) {
            $total = 0;
            foreach ($bom as $bm) { $total = $total + $bm->totalcost; }
        }else{
            Session::flash('duplicate', 'Missing Costing data');
                return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }
                
        $processcost = DB::table('lnk_itemprocess as li')
        ->select(
            'li.id',
            'mp.process_name',
            'mp.cost',
            'li.amount'
        )
        ->leftJoin('mst_process as mp', 'li._process', '=', 'mp.id')
        ->where('li._item', $request->id)
        ->get();
         if (count($processcost)>0) {
           $ptotal = 0;
        foreach ($processcost as $prcs) { $ptotal = $ptotal + $prcs->amount; }
        }else{
            Session::flash('duplicate', 'Missing Process data');
                return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }
        

        $company = Company::where('isactive', 1)->first();
        if (!$company) {
            Session::flash('nocontact', 'Company is not set as active !');
            return back();
        }

        $printdate = Carbon::now()->format('F d, Y');
        

        return view ('finishedproduct.pfinishproduct', compact('company','printdate','id1','costing','costprice','sellingprice','bom','total','ptotal','processcost'));

    }
}
