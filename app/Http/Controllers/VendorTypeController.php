<?php

namespace App\Http\Controllers;

use App\VendorType;
use Illuminate\Http\Request;

use App\Http\Requests;
use Gate;

class VendorTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list')) {
            abort(403);
        }
        return view('vendortype.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create')) {
            abort(403);
        }
        return view('vendortype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'code' => 'required',
            'name' => 'required'


        ]);



        $company = new VendorType($request->all());
        $company->id = '3';
        $company->save();

        return redirect('vendortype');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit')) {
            abort(403);
        }
        $company = VendorType::find($id);

        return view('vendortype.edit',compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = VendorType::find($id);
        $company->fill($request->all());
        $company->save();
        return redirect('vendortype');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete')) {
            abort(403);
        }
        $company = VendorType::find($id);
        $company->delete();

        return response()->json(['ok'=>'success']);
    }
}
