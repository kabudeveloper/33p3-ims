<?php

namespace App\Http\Controllers;



use App\Item;
use App\JoDetail;
use App\LnkItemPacking;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use JavaScript;



class JobOrderDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $trn = $request->trnid;
        return view('joborderdetail.create',compact('trn'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{



        $this->validate($request,[
             'quantity' => 'required|numeric',
             '_item' => 'required',
             'tnumber' => 'required',
             'packing' => 'required',
             'unitprice' => 'required',
         ]);


         $sellingprice = DB::table('vw_sellingprice')
              ->where('_item',$request->itemhidden)
             ->get();

         $jodetail = new JoDetail($request->all());
         $jodetail->_item = $request->itemhidden;
         $jodetail->lno = 0; // default sa
         $jodetail->unitprice = $sellingprice[0]->sellingprice;
         $jodetail->save();

        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', "Error in Saving...");
            return back()
                ->withErrors(['error' => 'Duplicate Items']);
        }
        
         return redirect()->action('JobOrderController@edit', [$request->tnumber]);
     

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$item)
    {
        $jodetail = JoDetail::
              where('tnumber',$id)
             ->where('_item',$item)
             ->first();

        $item = Item::find($item);


        JavaScript::put([
            'jodetail' => $jodetail,
        ]);

        return view('joborderdetail.edit',compact('jodetail','item'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


         JoDetail::where('tnumber',$request->tnumber)
             ->where('_item',$id)
             ->update([
                 'packing'=>$request->packing,
                 'quantity'=>$request->quantity
             ]);

        return redirect()->action('JobOrderController@edit', [$request->tnumber]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$item)
    {
          JoDetail::where('tnumber',$id)
              ->where('_item',$item)->delete();

        return response()->json(['ok'=>'success']);
    }
    
    public function getselecteditempacking(Request $request){

        $itempacking = LnkItemPacking::where('id',$request->id)->get();
        return response()->json(compact('itempacking'));
    }
    
    public function getjodsellingprice(Request $request){
        $sellingprice = DB::table('vw_sellingprice')
            ->where('_item',$request->id)
            ->get();

        return response()->json(compact('sellingprice'));
    }
}
