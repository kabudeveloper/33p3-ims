<?php

namespace App\Http\Controllers;

use App\Item;
use App\PurchaseOrderDetails;
use App\LnkItemPacking;
use App\Lnkvendoritems;
use App\Purchasing;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use JavaScript;

class PurchaseOrderDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $trn = $request->trnid;
        $po = Purchasing::select('_vendor')->where('tnumber', $trn)->first();

        return view('purchaseorderdetail.create',compact('trn', 'po'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $this->validate($request,[
                 'quantity' => 'required|numeric',
                 '_item' => 'required',
                 'tnumber' => 'required',
                 'packing' => 'required',
                 'unitprice' => 'required',
             ]);

            // $sellingprice = DB::table('lnk_vendoritems')
            //        ->where('_item',$request->itemhidden)
            //       ->get();
            // $sellingprice = Lnkvendoritems::where('_item',$request->itemhidden)->get();

            $podetail = new PurchaseOrderDetails($request->all());
            $podetail->balance = $request->quantity;
            $podetail->qtydelivered = 0;
            $podetail->qtyaccepted = 0;
            $podetail->_item = $request->itemhidden;
            $podetail->lno = 0; // default sa
            $podetail->unitprice = $request->unitprice;
            $podetail->save();

        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', "Error in Saving...");
            return back()
                ->withErrors(['error' => 'Duplicate Item']);
        }
        
         return redirect()->action('PurchasingController@edit', [$request->tnumber]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,$item)
    {
         $podetail = PurchaseOrderDetails::
              where('tnumber',$id)
             ->where('_item',$item)
             ->first();

        $item = Item::find($item);
        $po = Purchasing::select('_vendor')->where('tnumber', $id)->first();


        JavaScript::put([
            'podetail' => $podetail,
        ]);

        return view('purchaseorderdetail.edit',compact('podetail','item','po'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        PurchaseOrderDetails::where('tnumber',$request->tnumber)
             ->where('_item',$id)
             ->update([
                 'packing'=>$request->packing,
                 'quantity'=>$request->quantity
             ]);

        return redirect()->action('PurchasingController@edit', [$request->tnumber]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$item)
    {
          PurchaseOrderDetails::where('tnumber',$id)
              ->where('_item',$item)->delete();

        return response()->json(['ok'=>'success']);
    }

    public function getposellingprice(Request $request){

        $cost = Lnkvendoritems::where('_item',$request->id)->get();
        return response()->json(compact('cost'));

    }
}
