<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Currency;
use App\Customer;
use App\PaymentTerm;
use App\SecGroupAcess;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Gate;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('list-customer')) {
            abort(403);
        }
        return view('customer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        if (Gate::denies('create-customer')) {
            abort(403);
        }

        $terms = PaymentTerm::all();
        $currencies = Currency::all();
        return view('customer.create',compact('terms','currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'code' => 'required|max:15|unique:mst_customers,code',
            'name' => 'required',
            'billing_address' => 'required',
            'tin' => 'required',
            'zipcode' => 'required',
            '_paymentterm' => 'required'

        ]);

        $company = new Customer($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->name = strtoupper($request->name);
        $company->billing_address = strtoupper($request->billing_address);
        $company->delivery_address = strtoupper($request->delivery_address);
        $company->createdby= Auth::user()->id;
        $company->save();

        return redirect('customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('edit-customer')) {
            abort(403);
        }
        $company = Customer::find($id);
        $terms = PaymentTerm::all();
        $currencies = Currency::all();

        return view('customer.edit',compact('company','terms','currencies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Customer::find($id);
        $company->fill($request->all());
        $company->code = str_replace('-','',strtoupper($request->code));
        $company->updatedby= Auth::user()->id;

        if(is_null($request->isactive)){
            $company->isactive =  0;
        }else{
            $company->isactive =  1;
        }


        $company->save();

        return redirect('customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('delete-customer')) {
            return response()->json(['error' => 'You don\'t have permission to access!.'],403);
        }

       $contact = Contact::where('_mstlnk',$id)->delete();
        if($contact){
            $company = Customer::find($id);
            $company->delete();

        }
        return response()->json(['ok'=>'success']);

    }

    public function view($id)
    {
        /*if (Gate::denies('edit-customer')) {
            abort(403);
        }*/
        $company = Customer::find($id);
        $terms = PaymentTerm::all();
        $currencies = Currency::all();

        return view('customer.view',compact('company','terms','currencies'));
    }
}
