<?php

namespace App\Http\Controllers;

use App\Item;
use App\LnkItemStocking;
use App\Lnkvendoritems;
use App\Vendor;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SuppliesVendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($item)
    {
        $item = Item::find($item);

        return view('suppliesvendor.create', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $this->validate($request, [
            '_vendor' => 'required',
            'cost' => 'required'

        ]);

        try {

            $vendor = Vendor::find($request->_hiddenvendorid);
            if (is_null($request->isactive)) {
                $request->isactive = 0;
            }
            Lnkvendoritems::where('_item', $id)
                ->update(['isactive' => 0]);
            $vendor->item()->attach($id, ['cost' => $request->cost, 'isactive' => $request->isactive]);

          

        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', 'Vendor has already this item');
            return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }


        return redirect('supplies/' . $id . '/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($itemid, $vendorid)
    {

        $item = Item::find($itemid);

        $vendoritem = DB::table('mst_vendors as vendor')
            ->join('lnk_vendoritems as lnkvenditems', 'vendor.id', '=', 'lnkvenditems._vendor')
            ->where('_vendor', $vendorid)
            ->where('lnkvenditems._item', '=', $itemid)
            ->select([
                'vendor.code',
                'vendor.name',
                'lnkvenditems._vendor',
                'lnkvenditems.cost',
                'lnkvenditems.isactive'
            ])->first();


        return view('suppliesvendor.edit', compact('item', 'vendoritem'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $itemid, $vendorid)
    {
        try {

            if (is_null($request->isactive)) {
                $active = 0;
            } else {
                $active = 1;
            }

            Lnkvendoritems::where('_item', $itemid)
                ->update(['isactive' => 0]);

            Lnkvendoritems::where('_vendor', $vendorid)
                ->where('_item', $itemid)
                ->update(
                    [
                        '_vendor' => $request->_hiddenvendorid,
                        '_item' => $itemid,
                        'cost' => $request->cost,
                        'isactive' => $active
                    ]
                );
        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', 'Vendor has already this item');
            return back()
                ->withErrors(['error' => 'Duplicate Key']);
        }
        return redirect('supplies/' . $itemid . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($itemid, $vendorid)
    {
        $company = Lnkvendoritems::where('_vendor', '=', $vendorid)
            ->where('_item', '=', $itemid)->delete();

        return response()->json(['ok' => 'success']);

    }

    public function view($itemid, $vendorid)
    {

        $item = Item::find($itemid);

        $vendoritem = DB::table('mst_vendors as vendor')
            ->join('lnk_vendoritems as lnkvenditems', 'vendor.id', '=', 'lnkvenditems._vendor')
            ->where('_vendor', $vendorid)
            ->where('lnkvenditems._item', '=', $itemid)
            ->select([
                'vendor.code',
                'vendor.name',
                'lnkvenditems._vendor',
                'lnkvenditems.cost',
                'lnkvenditems.isactive'
            ])->first();


        return view('suppliesvendor.view', compact('item', 'vendoritem'));
    }
}
