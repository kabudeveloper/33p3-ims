<?php

namespace App\Http\Controllers;

use DB;
use App\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ProductionRawMaterialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $rms = DB::select('select r.tdate, r.tnumber, r.jonumber, l.name, r.status from trn_materialrequests as r INNER JOIN mst_locations as l on r._locationfrom = l.id');
        return view('production.productionrawmaterials')->with(['rms'=>$rms]);
       //return view('production.productionaddrawmaterials');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $locs = DB::select('select id, name from mst_locations');
        return view('production.productionaddrawmaterials')->with(['locs'=>$locs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        /*$rms = DB::select('select r.tdate, r.tnumber, r.jonumber, l.name, r.status from trn_materialrequests as r INNER JOIN mst_locations as l on r._locationfrom = l.id where r.jonumber = ?', [$id]);
        return view('production.productionrawmaterials')->with(['rms'=>$rms]);*/
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getinfo(Request $request){
       
       //$id = Input::get('id');// $id = Input::post('id');
        $id=$request->id;
       $data = DB::select('select j.tnumber, j.tdate, j._branch, j.status, b.name from trn_joborders as j INNER JOIN mst_branches as b on j._branch = b.id where tnumber  = ?', [$id]);
       
        return \Response::json($data);       
    }
    
    
    public function creatermr(Request $request){
         
        $d = date('Y/m/d H:m:s');
        $j=$request->jid;
        $b=$request->bid;
        $s=$request->sid;   
        
        $u = Auth::user()->id;
      
        $msg="";
        $data = DB::insert("insert into trn_materialrequests(status,jonumber,_locationfrom,_locationto,createddatetime,createdby) values('D',".$j.",".$s.",".$b.",'".$d."',".$u.")");
        
        if($data){
            $msg="ok";
            
        }else{
            $msg="No";
        }
        
        return \Response::json(compact('msg')); 
    }

}
