<?php

namespace App\Http\Controllers;

use App\Currencyconvertion;
use Illuminate\Http\Request;

use App\Http\Requests;
use Converter;
use App\Currency;
use Illuminate\Support\Facades\Session;
use JavaScript;

class CurrencyconvertionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $currency = Currency::find($id);
        $currencies = Currency::get();
        return view('currency.currencyconvertion',compact('currency','currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {

            $convertion = new Currencyconvertion($request->all());
            $convertion->save();
            return redirect('currency/'.$request->base.'/edit');

        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', "Error in Saving...Duplicate conversion");
            return back();

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        JavaScript::put([
            'convertbase' => $id,
        ]);
            return view('currency.showconvertion');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('currency.showconvertion');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($base,$target)
    {
        Currencyconvertion::where('base', $base)
            ->where('target', $target)->delete();

        return response()->json(['ok'=>'success']);
    }

    public function updatecurrency(Request $request)
    {
        try {

            Currencyconvertion::where('base',$request->bcurrencyhidden)
                ->where('target',$request->manualcurrencyhidden)
                ->update(['target'=>$request->manualcurrency,'rate'=>$request->rate]);

            return back();

        } catch (\Illuminate\Database\QueryException $e) {
            Session::flash('duplicate', $e->getMessage());
            return back();

        }


    }
}
