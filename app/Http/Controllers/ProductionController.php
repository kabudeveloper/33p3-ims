<?php

namespace App\Http\Controllers;

use App\JobOrders;
use App\JoDetail;
use App\JobOrderBom;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\RedirectResponse;
use JavaScript;
use PDF;

class ProductionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('production.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function joborder($id)
    {
        $jo = JobOrders::find($id);
        $jodetail = JoDetail::where('tnumber', '=', $id)->get();
        return view('production.joborder',compact('jo', 'jodetail'));
    }

    public function joborderbom($id)
    {
        return view('production.joborderbom');
    }

    public function onproduction(Request $request)
    {
        $jo = JobOrders::find($request->tnumber);
        $jo->status = "P";
        $jo->save();
        return redirect()->back();
    }

    public function onhold(Request $request)
    {
        $jo = JobOrders::find($request->tnumber);
        $jo->status = "H";
        $jo->save();
        return redirect()->back();
    }

    public function complete(Request $request)
    {
        $jo = JobOrders::find($request->tnumber);
        $jo->status = "C";
        $jo->save();
        return redirect()->back();
    }
}
