<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\Http\Requests;

use Illuminate\Support\Facades\Redirect;

class ProductionStatusAndControl extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    
     public function index()
    {
        //
         //$jobOrders = DB::select('select * from dbo.trn_joborders where st = ?', [1]);
         $sac = DB::select('select j.tdate, j.tnumber, j.status, c.name from trn_joborders as j INNER JOIN mst_customers as c ON j._customer = c.id where j.status = ?', ['A']);

        return view('production.statusandcontrol', ['sac' => $sac]);
 
    /* return View::('production.statusandcontrol', compact(''));*/
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
     
        
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function complete($id)
    {
        $com = DB::update("update trn_joborders set status = 'C' where tnumber = ?",[$id]);
   return Redirect::action('ProductionStatusAndControl@index');
    //return Redirect::route('statusandcontrol');
        //return Redirect::to('dashboard');
        
    }
    public function production($id)
    {
        $com = DB::update("update trn_joborders set status = 'P' where tnumber = ?",[$id]);
   return Redirect::action('ProductionStatusAndControl@index');
    }
    public function hold($id)
    {
        $com = DB::update("update trn_joborders set status = 'H' where tnumber = ?",[$id]);
   return Redirect::action('ProductionStatusAndControl@index');
    }
   
}
