<?php

namespace App\Http\Controllers;

use App\BranchCompany;
use App\CtlAveragecost;
use App\InvControl;
use App\Item;
use App\LnkItemPacking;
use App\Lnkvendoritems;
use App\PurchaseOrderDetails;
use App\Purchasing;
use App\Receiving;
use App\ReceivingDetails;
use App\TrnTrr;
use App\User;
use App\Vendor;
use Carbon\Carbon;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\RedirectResponse;
use JavaScript;
use PDF;

class ReceivingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('receiving.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('receiving.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd('sdsd');
        $this->validate($request,[
            'ponumber' => 'required',
            'invoicenumber' => 'required',
            'deliverydate' => 'required',
        ]);

        $rc = new Receiving($request->all());
        $rc->tdate= Carbon::now();
        $rc->tmode= $request->tmode;
        $rc->_branch= $request->branchidhidden;
        $rc->_vendor= $request->vendoridhidden;
        $rc->ponumber= $request->ponumber;
        $rc->trrnumber= $request->trrnumber;
        $rc->invoicenumber= $request->invoicenumber;
        $rc->invoicedate= $request->deliverydate;
        $rc->status = 'D';
        $rc->notes= $request->notes;
        $rc->createddatetime= Carbon::now();
        $rc->createdby= Auth::user()->id; 
        $rc->save();
        $lastid = $rc->tnumber;

        $rcvdetail = Input::get('rcvdetail');
        foreach ($rcvdetail as $rdetail)
        {

            list($lno, $item, $packing, $poprice, $rrprice, $qtyordered, $qtydelivered, $qtyaccepted, $balance) = explode(",", $rdetail);
            // $bal = $qtyordered - $qtyaccepted;

            $updatepo = PurchaseOrderDetails::where('tnumber', $request->ponumber)
            ->where('_item', $item)
            ->update([
                'qtydelivered' => $qtydelivered,
                'qtyaccepted' => $qtyaccepted,
                'balance' => $balance
            ]);

            DB::insert('INSERT INTO trn_receivingdetails (tnumber,_item,packing,poprice,rrprice,qtyordered,qtydelivered,qtyaccepted,lno,balance) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?)', array($lastid, $item, $packing, $poprice, $rrprice, $qtyordered, $qtydelivered, $qtyaccepted, $lno,$balance));
        }
        return redirect()->action('ReceivingController@edit', [$lastid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rr = Receiving::find($id);
        $vendorname = Vendor::where('id', $rr->_vendor)->pluck('name')->first();
        $branchname = BranchCompany::where('id', $rr->_branch)->pluck('name')->first();
        $rrdetail = ReceivingDetails::where('tnumber', '=', $id)->get();

        $rrstatus = '';
        if($rr->status == 'C'){
            $rrstatus = 'Cancelled';
        }elseif( $rr->status == 'D'){
            $rrstatus = 'Draft';
        }elseif( $rr->status == 'A'){
            $rrstatus = 'Approved';
        }elseif( $rr->status == 'S'){
            $rrstatus = 'Submitted';
        }

        JavaScript::put([
            'rr' => $rr,
        ]);

        return view('receiving.edit',compact('rr', 'vendorname', 'branchname', 'rrdetail', 'rrstatus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd('sdsd');
        $rr = Receiving::find($id);
        $rr->fill($request->except([
            'status', 'tnumber', '_branch', '_vendor'
        ]));
        $rr->invoicenumber = $request->invoicenumber;
        $rr->invoicedate= $request->deliverydate;
        $rr->notes= $request->notes;

        DB::table('trn_receivingdetails')->where('tnumber', $id)->delete();

        $rcvdetail = Input::get('rcvdetail');
        foreach ($rcvdetail as $rdetail)
        {
            list($lno, $item, $packing, $poprice, $rrprice, $qtyordered, $qtydelivered, $qtyaccepted,$balance) = explode(",", $rdetail);
            // $bal = $qtydelivered - $qtyaccepted;

             $updatepo = PurchaseOrderDetails::where('tnumber', $rr->ponumber)
            ->where('_item', $item)
            ->update([
                'qtydelivered' => $qtydelivered,
                'qtyaccepted' => $qtyaccepted,
                'balance' => $balance
            ]);

            DB::insert('INSERT INTO trn_receivingdetails (tnumber,_item,packing,poprice,rrprice,qtyordered,qtydelivered,qtyaccepted,lno,balance) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?)', array($id, $item, $packing, $poprice, $rrprice, $qtyordered, $qtydelivered, $qtyaccepted, $lno,$balance));
        }

        $rr->save();

        return redirect('receiving');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Receiving::find($id)->delete();
         /*$jod = JoDetail::find($id);
          if($jod){
              $jod->delete();
          }*/
        
        return response()->json(['ok'=>'success']);
    }

    public function getpurchaseorder(Request $request){
        
        $po= Purchasing::find($request->ponumber);
        $trr = TrnTrr::where('ponumber', $request->ponumber)->pluck('tnumber')->first();
        $vendorname = Vendor::where('id', $po->_vendor)->pluck('name')->first();
        $branchname = BranchCompany::where('id', $po->_branch)->pluck('name')->first();
        
        return response()->json(compact('po', 'trr', 'vendorname', 'branchname'));
    }

    public function receivingpodetail(Request $request){
        $podetail = PurchaseOrderDetails::where('tnumber', $request->ponumber)->get();
        /*$podetail = DB::table('trn_purchaseorderdetails as podetail')
            ->join('lnk_itempacking AS itempacking', 'podetail.packing', '=', 'itempacking.id')
            ->join('mst_items as item', 'podetail._item', '=', 'item.id')
            ->where('podetail.tnumber', $tnumber)
            ->select([
                'podetail.lno',
                'podetail._item',
                'item.name',
                'item.code',
                'podetail.packing',
                'itempacking.description',
                'podetail.unitprice',
                'podetail.quantity',
                \DB::raw('( podetail.unitprice * podetail.quantity ) as totalcost')
            ]);*/
        //$item = Item::where('id', $podetail->_item)->get();
        //$itemcode = Item::where('id', $podetail->_item)->get();
        //$packing = LnkItemPacking::where('id', $podetail->packing)->get();

        //return response()->json(['podetails'=>$podetail], ['items'=>$item], ['packings'=>$packing]);
        return response()->json(['podetails'=>$podetail]);
    }

    public function cancelrr(Request $request)
    {
        $jo = Receiving::find($request->tnumber);
        $jo->status = "C";
        $jo->cancelleddatetime = Carbon::now();
        $jo->cancelledby = Auth::user()->id;
        $jo->save();
        return response()->json(['ok' => 'success']);
    }

    public function submitrr(Request $request)
    {
        $jo = Receiving::find($request->tnumber);
        $jo->status = "S";
        $jo->submitteddatetime = Carbon::now();
        $jo->submittedby =  Auth::user()->id;
        $jo->save();
        $po = $jo->ponumber;

        $getdetails = ReceivingDetails::where('tnumber',$request->tnumber)->first();
        $bal = $getdetails->qtydelivered - $getdetails->qtyaccepted;

        if ($bal > 0) {
            $getbal = Purchasing::find($po);
            $getbal->hasbal = 1;
            $getbal->iscreated = 0;
            $getbal->save();   
        }
       
        return redirect()->back();
    }

    public function approverr(Request $request)
    {
        $jo = Receiving::find($request->tnumber);
        $vendorid = $jo->_vendor;
        $rrdetail = ReceivingDetails::where('tnumber', $request->tnumber)->get();
        $lnkitem = Lnkvendoritems::where('_vendor', $vendorid)->get();
        $datetoday = Carbon::now();
        foreach($rrdetail as $r) {
            $item = $r->_item;
            foreach($lnkitem as $l) {
                $newprice = $r->rrprice;
                $oldprice = $l->cost;
                $reference = 'RR#'.$request->tnumber;
                if($l->_item == $r->_item){
                    DB::insert('INSERT INTO log_listcost (_vendor,_item,reference,oldcost,newcost,entrydatetime) VALUES (?, ?, ?, ?, ?, ?)', array($vendorid, $item, $reference, $oldprice, $newprice, $datetoday));
                    DB::table('lnk_vendoritems')->where('_item', $item)->update(['cost' => $newprice]);
                }
            }

            $item_location = Item::where('id', $item)->pluck('storagelocation')->first();
            $avecost = CtlAveragecost::where('_item', $item)->pluck('cost')->first();
            $inv_quantity = InvControl::where('_location', $item_location)->where('_item', $item)->pluck('quantity')->first();
            
            if(is_null($avecost)){
                $avecost = 0;
            }
            if(is_null($inv_quantity)){
                $inv_quantity = 0;
            }

            $cost1 = $inv_quantity*$avecost;
            $cost2 = $r->qtyaccepted*$r->rrprice;
            $cost3 = $cost1+$cost2;

            $cost4 = $inv_quantity+$r->qtyaccepted;
            $cost5 = $cost3/$cost4;
            $newavecost = $cost5;

            if($inv_quantity == 0){
                DB::insert('INSERT INTO inv_control (_location,_item,quantity) VALUES (?, ?, ?)', array($item_location, $item, $r->qtyaccepted));
            }
            //dd($avecost);

            DB::insert('INSERT INTO inv_ledger (entrydatetime,_location,_item,quantity,type,_transaction,_tnumber) VALUES (?, ?, ?, ?, ?, ?, ?)', array($datetoday, $item_location, $item, $r->qtyaccepted, 1, 'Receiving', $request->tnumber));
            DB::table('ctl_averagecost')->where('_item', $item)->update(['cost' => $newavecost]);
        }

        $jo->status = "A";
        $jo->approveddatetime = Carbon::now();
        $jo->approvedby = Auth::user()->id;
        $jo->save();
        return redirect()->back();
    }

    public function rollbackrr(Request $request)
    {
        $jo = Receiving::find($request->tnumber);
        $jo->status = "D";
        $jo->save();
        return redirect()->back();
    }

    public function createrr(Request $request)
    {

        $rcv = Receiving::find($request->tnumber);
        $rcvdate = Carbon::parse($rcv->tdate)->format('m/d/Y');
        $printdate = Carbon::now()->format('m/d/Y');
        $vendorname = Vendor::where('id', $rcv->_vendor)->pluck('name')->first();
        $invoicedate = Carbon::parse($rcv->invoicedate)->format('m/d/Y');
        $rcvdetail = ReceivingDetails::where('tnumber', $request->tnumber)->where('qtyaccepted', '<>', 0)->orderBy('lno')->get();

        $creatorfname = User::where('id', $rcv->createdby)->pluck('firstname')->first();
        $creatorlname = User::where('id', $rcv->createdby)->pluck('lastname')->first();
        $approverfname = User::where('id', $rcv->approvedby)->pluck('firstname')->first();
        $approverlname = User::where('id', $rcv->approvedby)->pluck('lastname')->first();

        $sumtotal = collect([]);
        foreach ($rcvdetail as $r) {
            $amount = $r->qtyaccepted * $r->rrprice;
            $sumtotal->push($amount);
        }
        $rcvstatus = '';
        if($rcv->status == 'C'){
            $rcvstatus = 'Cancelled';
        }elseif( $rcv->status == 'D'){
            $rcvstatus = 'Draft Copy';
        }elseif( $rcv->status == 'A'){
          if($rcv->timesprinted == 0){
            $rcvstatus = 'Original Copy';
          }
          else{
            $rcvstatus = 'Reprint Copy #'.$rcv->timesprinted;
          }
        }elseif( $rcv->status == 'S'){
            $rcvstatus = 'For Approval';
        }

        //$pdf = PDF::loadView('purchasing.createtrr', compact('po', 'paymenttermdesc', 'podeldate', 'vendorname', 'vendorcode', 'vendoraddress', 'printdate', 'contactname', 'contactemail', 'contactmobile', 'podetail', 'sumtotal', 'trninstruct', 'poinstruct', 'creatorfname', 'creatorlname', 'approverfname', 'approverlname', 'postatus', 'vtrr'));
        // $pdf = PDF::loadView('receiving.createrr', compact('rcv', 'rcvdate', 'printdate', 'vendorname', 'invoicedate', 'rcvdetail', 'amount', 'sumtotal', 'rcvstatus', 'creatorfname', 'creatorlname', 'approverfname', 'approverlname'));

        return view('receiving.createrr', compact('rcv', 'rcvdate', 'printdate', 'vendorname', 'invoicedate', 'rcvdetail', 'amount', 'sumtotal', 'rcvstatus', 'creatorfname', 'creatorlname', 'approverfname', 'approverlname'));
        // return $pdf->stream();
    }
    // public function updatebal(Request $request){
    
    //     $find = ReceivingDetails::where('tnumber',$request->rr)
    //         ->where('_item',$request->item)
    //         ->where('qtydelivered',$request->delivered)
    //         ->where('lno',$request->lno)
    //         ->first();            
    //         $bal = $find->qtydelivered - $request->accepted;
    //         $find->qtyaccepted = $request->accepted;
    //         $find->balance = $bal;
    //         $find->update();
    //     return $find->_item;
    // }
}
