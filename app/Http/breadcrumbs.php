<?php
/**
 * Created by PhpStorm.
 * User: eldenz
 * Date: 4/24/2016
 * Time: 8:49 PM
 */

Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('dashboard'));
});

Breadcrumbs::register('customer', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Customer', route('customer'));
    
});

Breadcrumbs::register('company', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Company', route('company'));
});

Breadcrumbs::register('branch', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Branch', route('branch'));
});

Breadcrumbs::register('paymentterm', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Payment term', route('paymentterm'));
});


Breadcrumbs::register('unitofmeasure', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Unit of measure', route('unitofmeasure'));
});

Breadcrumbs::register('location', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Location', route('location'));
});

Breadcrumbs::register('box', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Box', route('box'));
});

Breadcrumbs::register('vendortype', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Vendor Type', route('vendortype'));
});

Breadcrumbs::register('currency', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Currency', route('currency'));
});



Breadcrumbs::register('vendors', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Vendor', route('vendors'));
});

Breadcrumbs::register('vendorcontact', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Vendor Contact', route('vendorcontact'));
});

Breadcrumbs::register('packaging', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Packaging', route('packaging'));
});

Breadcrumbs::register('category', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Category', route('category'));
});


Breadcrumbs::register('clss', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Class', route('clss'));
});


Breadcrumbs::register('subclss', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Sub Class', route('subclss'));
});

Breadcrumbs::register('item', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Item', route('item'));
});

Breadcrumbs::register('contact', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Contact', route('contact'));
});

Breadcrumbs::register('rawmaterial', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Raw Material', route('rawmaterial'));
});




Breadcrumbs::register('finishedproduct', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Finished Product', route('finishedproduct'));
});


Breadcrumbs::register('purchasing', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Transactions');
    $breadcrumbs->push('Purchasing', route('purchase'));
});

Breadcrumbs::register('purchasebalance', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Transactions');
    $breadcrumbs->push('Purchase Balance', route('purchasebalance'));
});

Breadcrumbs::register('receiving', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Transactions');
    $breadcrumbs->push('Receiving', route('receiving'));
});

Breadcrumbs::register('vendoritem', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Vendor Item', route('vendoritem'));
});

Breadcrumbs::register('supplies', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Supplies', route('supplies'));
});

Breadcrumbs::register('production', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Transactions');
    $breadcrumbs->push('Production', route('production'));
});

Breadcrumbs::register('materialrequest', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Transactions');
    $breadcrumbs->push('Material Request', route('materialrequest'));
});

Breadcrumbs::register('salesorder', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Transactions');
    $breadcrumbs->push('Sales Order', route('salesorder'));
});

Breadcrumbs::register('process', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Maintenance');
    $breadcrumbs->push('Process', route('process'));
});