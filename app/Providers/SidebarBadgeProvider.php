<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\JobOrders;

class SidebarBadgeProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {

       

        view()->composer('layouts.sidebar', function($view) {
            
            $badge = JobOrders::where('fromso','=',1)->get();
            $count = count($badge);
            
            $view->with('count', $count);
          
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
