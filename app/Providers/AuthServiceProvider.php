<?php

namespace App\Providers;


use App\Permission;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
      // \App\SecGroupAcess::class =>\App\Policies\RolesPermissionPolicy::class


    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {

        parent::registerPolicies($gate);

       foreach ($this->getPermissions() as $permission) {
            $gate->define($permission->description, function ($user) use ($permission) {
                return $user->hasPermission($permission);
            });
        }

      /*  $gate->define('addcompany', function ($user, $group) {
            return $user->_group === $group->_group;
        });*/
    }

    protected function getPermissions(){
        return Permission::with('roles')->get();
    }
}
