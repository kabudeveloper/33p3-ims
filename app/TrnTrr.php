<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrnTrr extends Model
{
    protected $table = 'trn_trr';
    public $timestamps = false;
    protected $fillable = ['tnumber','ponumber','datetimecreated','createdby'];


    public $primaryKey = 'tnumber';

    /*public function branch(){
        return $this->belongsTo('App\BranchCompany','_branch');
    }*/
}
