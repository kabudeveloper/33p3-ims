<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialRequest extends Model
{
    protected $table = 'trn_materialrequests';
    public $timestamps = false;
    protected $fillable = ['tnumber','tdate','status','jonumber','_locationfrom','_locationto'

    ];

    public $primaryKey = 'tnumber';
}
