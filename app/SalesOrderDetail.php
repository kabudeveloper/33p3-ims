<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderDetail extends Model
{
    protected $table = 'trn_salesorderdetails';
    public $timestamps = false;
    public $primaryKey = 'tnumber';

    protected $fillable = [
        'tnumber','packing','quantity','unitprice','aup'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item','_item');
    }

    public function packing()
    {
        return $this->belongsTo('App\LnkItemPacking','packing');
    }

    public function getTotalPriceAttribute() {

        return $this->quantity * $this->aup;
    }

    public function itemcost(){
        return $this->hasOne('App\VwCostPrice','_item');
    }

    public function so(){
        return $this->belongsTo(SalesOrder::class,'tnumber');
    }

    public function getConvertUnitPriceAttribute() {
       // $vw = VwCostprice::where('_item',$this->_item)->first();
        $so = SalesOrderDetail::find($this->tnumber)->so;
        $sod = SalesOrderDetail::where('_item',$this->_item)
            ->where('tnumber',$this->tnumber)->first();

        //return (($vw->costprice + $vw->markup)  /  $so->currencyrate);
        return (  number_format(($sod->aup  /  $so->currencyrate),2,'.',',')  );
    }

    public function getTotalPriceConvertionAttribute(){
        return number_format(($this->quantity * $this->getConvertUnitPriceAttribute()),2,'.',',');
    }

    
}
