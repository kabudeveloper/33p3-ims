<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LnkItemCosting extends Model
{
    protected $table = 'lnk_itemcosting';
    public $timestamps = false;
    public $primaryKey = '_item';
    protected $fillable = ['_item','omcost','lbcost','ovcost','markup', 'rmcost', 'commitedprice', 'ocost'];
}
