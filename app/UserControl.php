<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserControl extends Model
{
    protected $table = 'sec_control';
    public $timestamps = false;

    protected $fillable =['enablepasswordexpiry','passwordexpiryinterval','enableloginlocking','loginlockingthreshold',
       'loginlockingduration','enableaccountexpiry','accountexpiryinterval'
    ];
}
