<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VwCostprice extends Model
{
    protected $table = 'vw_costprice';
    public $timestamps = false;


    public function costitem(){
        return $this->belongsTo('App\SalesOrderDetail','_item');
    }

}
