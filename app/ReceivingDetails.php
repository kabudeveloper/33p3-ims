<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReceivingDetails extends Model
{
    protected $table ='trn_receivingdetails';
    public $timestamps = false;
    protected $fillable = ['tnumber','_item','packing','poprice','rrprice','qtyordered','qtydelivered','qtyaccepted','lno','balance'];
    public $primaryKey = 'tnumber';

    public function item()
    {
        return $this->belongsTo('App\Item','_item');
    }

    public function packing()
    {
        return $this->belongsTo('App\LnkItemPacking','packing');
    }

    public function getTotalPriceAttribute() {
        return $this->quantity * $this->unitprice;
    }
}
