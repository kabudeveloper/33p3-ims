<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOrderBom extends Model
{
    protected $table = 'trn_joborderbom';
    public $timestamps = false;
    protected $fillable = ['tnumber','_item','packing','quantity','unitcost'];
    public $incrementing = false;

}
