<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lnkvendoritems extends Model
{


    protected $table = 'lnk_vendoritems';
    protected $fillable = ['_item','cost','isactive'];

    public $timestamps = false;

   
    
    
}
