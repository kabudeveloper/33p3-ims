<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'mst_categories';

    protected $fillable = ['code','name','glcode'];

    public $timestamps = false;

    public function item(){
        return $this->hasMany('App\Item','_category');
    }
}
