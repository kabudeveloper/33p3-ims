<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    protected $table = 'mst_boxes';

    public $timestamps = false;

    protected $fillable = ['code','name','height','width','length'];
}
