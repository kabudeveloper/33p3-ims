<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LnkItemProcess extends Model
{
    protected $table = 'lnk_itemprocess';
    protected $fillable = ['_item','_process','amount'];
    public $timestamps = false;
}
