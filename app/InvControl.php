<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvControl extends Model
{
    protected $table = 'inv_control';
    protected $fillable = ['_location','_item','quantity'];

    public $timestamps = false;
}
