<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CtlAveragecost extends Model
{
    protected $table = 'ctl_averagecost';
    public $timestamps = false;
    protected $fillable = ['_item','cost'];



}
