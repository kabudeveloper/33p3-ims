<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetails extends Model
{
    protected $table ='trn_purchaseorderdetails';
    public $timestamps = false;
    protected $fillable = ['tnumber','_item','quantity','packing','unitprice','lno'];
    public $primaryKey = 'tnumber';

    public function item()
    {
        return $this->belongsTo('App\Item','_item');
    }

    public function packing()
    {
        return $this->belongsTo('App\LnkItemPacking','packing');
    }

    public function getTotalPriceAttribute() {
        return $this->quantity * $this->unitprice;
    }
}
