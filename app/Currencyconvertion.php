<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currencyconvertion extends Model
{
    protected $table ='lnk_currencyconversions';
    public $timestamps = false;
    public $fillable = ['base','target','rate'];

    public function currency(){
        return $this->belongsTo('App/Currency');
    }

}
