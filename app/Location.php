<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'mst_locations';

    public $timestamps = false;

    protected $fillable = ['code','name','_branch','glcode','_type'];
}
