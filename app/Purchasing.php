<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchasing extends Model
{
    protected $table = 'trn_purchaseorders';
    public $timestamps = false;
    protected $fillable = ['_branch','_vendor','tmode','jonumber','_paymentterm','notes'
        ,'deliverydate'
        ,'tdate'
        ,'status'

    ];


    public $primaryKey = 'tnumber';

    public function branch(){
        return $this->belongsTo('App\BranchCompany','_branch');
    }
}
