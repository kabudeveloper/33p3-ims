<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MaterialRequestDetail extends Model
{
    protected $table ='trn_materialrequestdetails';
    public $timestamps = false;
    protected $fillable = ['tnumber','_item','packing','quantity'];
    public $primaryKey = 'tnumber';

    public function item()
    {
        return $this->belongsTo('App\Item','_item');
    }

    public function packing()
    {
        return $this->belongsTo('App\LnkItemPacking','packing');
    }
}
