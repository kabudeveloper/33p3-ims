<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table ='mst_currencies';
    public $timestamps = false;
    protected $fillable = ['name','symbol'];

    public function convertions(){
        return $this->hasMany(Currencyconvertion::class,'target');
    }

}
