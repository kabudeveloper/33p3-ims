<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LnkItemComposition extends Model
{
    protected $table = 'lnk_itemcomposition';

    public $timestamps = false;
    protected $fillable = ['_item','_subitem','packing','quantity'];
}
