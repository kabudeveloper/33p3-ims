<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $table = 'mst_process';
    public $timestamps = false;
    protected $fillable = ['code','process_name','_location', '_category'];
}
