<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentTerm extends Model
{
    protected $table = 'mst_paymentterms';

    public $timestamps = false;

    protected $fillable = ['code','description','factor'];

    public function customer(){
        return $this->hasMany('App\Customer','_paymentterm');
    }
}
