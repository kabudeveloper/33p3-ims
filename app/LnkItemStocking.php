<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LnkItemStocking extends Model
{
    protected $table = 'lnk_itemstocking';
    protected $fillable = ['min','max'];
    public $timestamps = false;
    protected $primaryKey = null;
    public $incrementing = false;
    
    public function item(){
        return $this->belongsTo('App\Item');
    }
}
