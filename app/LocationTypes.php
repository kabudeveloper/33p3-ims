<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationTypes extends Model
{
    protected $table = 'mst_locationtypes';

    public $timestamps = false;
}
