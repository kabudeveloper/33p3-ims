<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{

    protected $table = 'mst_contacts';

    public $timestamps = false;

    public $incrementing = false;

    protected $primaryKey = 'name';
    
    protected $fillable = ['name','position','mobile','landline','email','fax','isprimary'];

    public function customer(){
        return $this->belongsTo('App\Customer','_mstlnk');
    }

}
