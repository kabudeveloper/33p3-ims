<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $appends = ['complete_name']; //accessor

    protected $table = 'sec_users';
    protected $fillable = [
        'firstname',
        'lastname',
        'middlename',
        'description',
        '_group',
        'username',
        'locationassignment',
        '_position'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * I added , this is how to use the accessor to display fullname.
     *
     */
    public function getCompleteNameAttribute()
    {
        return $this->attributes['firstname'] .
        ' ' . $this->attributes['lastname'];
    }

    public function roles()
    {
        return $this->belongsTo(SecGroup::class,'_group');
    }

    /**
     * Determine if the user has the given role.
     *
     * @param  mixed $role
     * @return boolean
     */
    public function hasRole($role)
    {
            foreach ($role as $r) {
                  if($r->id == $this->_group && $r->pivot->granted == 1){

                      return true;
                  }
            }
            return false;
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param  Permission $permission
     * @return boolean
     */
    public function hasPermission(Permission $permission)
    {
        return $this->hasRole($permission->roles);
    }

    public function owns($group){
        return $this->_group === $group->_group;
    }

    public function position(){
        return $this->belongsTo('App\Position','_position');
    }
}
