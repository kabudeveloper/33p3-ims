<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PoInstruction extends Model
{
    protected $table = 'mst_poinstructions';

    public $timestamps = false;

    protected $fillable = ['instruction'];


    public function purchaseinstruction(){
        return $this->hasMany('App\PurchaseOrderInstruction','_instruction');
    }
}
