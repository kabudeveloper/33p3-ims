<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitOfMeasure extends Model
{
    protected $table = 'mst_unitofmeasures';

    public $timestamps = false;

    protected $fillable = ['code','name'];
}
