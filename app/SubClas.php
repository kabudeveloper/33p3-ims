<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubClas extends Model
{
    protected $table = 'mst_subclass';

    public $timestamps = false;

    protected $fillable = ['code','name','_class','glcode'];
}
