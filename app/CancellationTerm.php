<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancellationTerm extends Model
{
    protected $table = 'mst_cancellationterms';

    public $timestamps = false;

}
