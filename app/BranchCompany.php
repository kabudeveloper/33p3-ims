<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchCompany extends Model
{
    protected $table = 'mst_branches';

    protected $fillable = ['code','name','address','tin','zipcode','_company','glcode'];

    public $timestamps = false;

    public function jobranch(){
        return $this->hasMany('App\JobOrders','_branch');
    }

    public function maincompany(){
        return $this->belongsTo(Company::class,'_company');
    }
}
