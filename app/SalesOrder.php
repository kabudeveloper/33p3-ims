<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrder extends Model
{
    protected $table = 'trn_salesorder';
    public $timestamps = false;
    public $primaryKey = 'tnumber';

    protected $fillable = [
           'tnumber','tdate','_customer','status','notes','_paymentterm','_cancellationterm','pricevalidity','_leadtime','_currency'
        ];

    public function customer(){
        return $this->belongsTo('App\Customer','_customer');
    }

    public function sod(){
        return $this->hasMany(SalesOrderDetail::class,'tnumber');
    }

}