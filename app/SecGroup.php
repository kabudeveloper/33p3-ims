<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SecGroup
 * @package App
 * SecGroup A.K.A -> Role
 */
class SecGroup extends Model
{
    protected $table = 'sec_groups';
    public $timestamps = false;
    protected $fillable = ['description'];

    public function permission(){
        return $this->belongsToMany('App\Permission','sec_groupaccessrights','_group','_restriction')->withPivot('granted');
    }

    public function users(){
        return $this->hasMany('App\User','_group');
    }
}
