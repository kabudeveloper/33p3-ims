<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'sec_restrictions';

    public $timestamps = false;

    public function roles(){
        return $this->belongsToMany('App\SecGroup','sec_groupaccessrights','_restriction','_group')->withPivot('granted');
    }
}
