<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VendorType extends Model
{
    protected $table = 'mst_vendortypes';

    public $timestamps = false;

    protected $fillable = ['code','name'];
}
