<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packaging extends Model
{
    protected $table = 'mst_packaging';

    public $timestamps = false;

    protected $fillable = ['code','name'];
}
