<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderInstruction extends Model
{
    protected $table = 'trn_purchaseorderinstructions';
    protected $fillable = ['tnumber','_instruction'];
    public $timestamps = false;

    public function instruction(){
        return $this->belongsTo('App\PoInstruction','_instrucion');
    }
}
