<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clas extends Model
{
    protected $table = 'mst_class';

    public $timestamps = false;
    
    protected $fillable = ['code','name','_category','glcode'];
}
