<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOrders extends Model
{
    protected $table = 'trn_joborders';
    public $timestamps = false;

    protected $fillable = ['_branch','_customer','reference','_paymentterm','notes'
        ,'deliverydate'
        ,'tdate'
        ,'status'

    ];


    public $primaryKey = 'tnumber';

    public function branch(){
        return $this->belongsTo('App\BranchCompany','_branch');
    }


}
