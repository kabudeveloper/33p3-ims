$.ajax({
    type: 'GET',
    dataType: 'json',
    data: {catid: catid},
    url: '/item/geteditcategory',
    success: function (data) {
        var category = data.categories;
        $.each(category, function (index, item) {
            if (item.id == catid) {
                $('[name="_category"]').append($('<option>', {
                    value: item.id,
                    text: item.name,
                    selected: "selected"

                }));
            } else {
                $('[name="_category"]').append($('<option>', {
                    value: item.id,
                    text: item.name

                }));
            }

        });

        optclass(catid);

    }

});


$('[name="_category"]').on('change', function (e) {
    var catid = $(this).val();

    $('[name="_class"]')
        .find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('');


    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {catid: catid},
        url: '/item/getclss',
        success: function (data) {
            var classess = data.classess;
            $.each(classess, function (index, item) {
                $('[name="_class"]').append($('<option>', {
                    value: item.id,
                    text: item.name

                }));
            });
        }

    });

});


$('[name="_class"]').on('change', function (e) {
    var classid = $(this).val();

    $('[name="_subclass"]')
        .find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('');


    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {classid: classid},
        url: '/item/getsubclass',
        success: function (data) {
            var subclassess = data.subclassess;
            $.each(subclassess, function (index, item) {
                $('[name="_subclass"]').append($('<option>', {
                    value: item.id,
                    text: item.name

                }));
            });
        }

    });

});


var optclass = function (catid) {

    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {catid: catid},
        url: '/item/getclss',
        success: function (data) {
            var classess = data.classess;

            $.each(classess, function (index, item) {
               
                if (item.id == classid) {
                    $('[name="_class"]').append($('<option>', {
                        value: item.id,
                        text: item.name,
                        selected: "selected"

                    }));
                } else {
                    $.each(classess, function (index, item) {
                        $('[name="_class"]').append($('<option>', {
                            value: item.id,
                            text: item.name

                        }));
                    });
                }
            });
            optsubclass(catid);
        }

    });


};


var optsubclass = function (catid) {



    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {classid: classid},
        url: '/item/getsubclass',
        success: function (data) {
            var subclassess = data.subclassess;
            $.each(subclassess, function (index, item) {
                if (item.id == subclassid) {
                    $('[name="_subclass"]').append($('<option>', {
                        value: item.id,
                        text: item.name,
                        selected: "selected"

                    }));
                } else {
                    $('[name="_subclass"]').append($('<option>', {
                        value: item.id,
                        text: item.name

                    }));
                }
            });
        }
    });
};


/*
 var resetOption = function (id) {

 $('[name=""]')
 .find('option')
 .remove()
 .end()
 .append('<option></option>')
 .val('')

 };

 */
