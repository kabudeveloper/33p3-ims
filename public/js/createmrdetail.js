$(function(){
    var vendorid = $('[name="tnumber"]').val();
});

var item = $('#tablepodetail').DataTable({
    processing: true,
    serverSide: true,
    ajax:APP_URL +  '/getmrdetail/' + $('[name="tnumber"]').val(),
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'},
        {data: 'production_um', name: 'production_um'}
    ],
    columnDefs:[
        { targets: [2], visible: false }

    ]
});


$('#tablepodetail').on('dblclick','tr',function(){
      var itemid = $(this).closest('tr').attr('id');

      var production = item.rows(this).data()[0].production_um;

      $('[name="code"]').val($(this).closest('tr').find('td:eq(0)').text());
      $('[name="_item"]').val($(this).closest('tr').find('td:eq(1)').text());
      $('[name="itemhidden"]').val(itemid);
      getpacking(itemid);
      $('#finishprodlistmodal').modal('hide');
     // getCustomerPaymentTerm(custid);
});


$('[name="btnjodetailfin"]').on('click',function (e) {
    $('#finishprodlistmodal').modal('show');

});



function getCustomerPaymentTerm(id){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{customerid:id},
        url: APP_URL + '/getpaymentterms',
        success:function (data) {
            $('[name="_paymentterm"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;
            $.each(term, function (index, item) {
              if(  data.customer ){
                  if(  data.customer._paymenttterm == term[index].id ){
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description,
                          selected:'selected'
                      }));
                  }else{
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description

                      }));
                  }
              }else{
                  $('[name="_paymentterm"]')
                      .find('option')
                      .remove()
                      .end()
                      .append('<option></option>')
                      .val('');
              }


            });

        }

    });
}




function getpacking(id){

    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getselectedrmformr',
        success:function(data){
          
            getSellingPrice(id);
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description,
                    selected:'selected'
                }));
            });
        }
    });
    
}

function getSellingPrice(id){
    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getposellingprice',
        success:function(data){
            $('[name="unitprice"]').val(numeral(data.cost[0].cost).format('0,0.00'));
        }
    });
}