
var vendor = $('#tablejolist').DataTable({
    processing: true,
    serverSide: true,
    ajax:APP_URL +  '/mrjolist',
    columns: [
        {data: 'tnumber', name: 'jo.tnumber'},
        {data: 'tdate', name: 'jo.tdate'},
        {data: '_branch', name: 'jo._branch'},
        {data: 'name', name: 'b.name'},
        {data: '_customer', name: 'jo._customer'},
        {data: 'customername', name: 'c.name'},
        {data: 'notes', name: 'jo.notes'},
        {data: 'status', name: 'jo.status'}
    ],

    columnDefs:[
        { targets: [2], visible: false },
        { targets: [4], visible: false },
        { targets: [7], visible: false }

    ]
});


$('#tablejolist').on('dblclick','tr',function(){
      var tnumber = vendor.rows(this).data()[0].tnumber;
      var branch = vendor.rows(this).data()[0]._branch;
      var customer = vendor.rows(this).data()[0]._customer;
      var tdate = $(this).closest('tr').attr('tdate');
      var status = $(this).closest('tr').attr('status');



      //alert(vendor.rows(this).data()[0].tnumber);

      $('[name="jonumber"]').val(vendor.rows(this).data()[0].tnumber);
      $('[name="branchidhidden"]').val(vendor.rows(this).data()[0]._branch);
      $('[name="customeridhidden"]').val(vendor.rows(this).data()[0]._customer);
      $('[name="tnumber2"]').val(vendor.rows(this).data()[0].tnumber);
      $('[name="tdate"]').val(vendor.rows(this).data()[0].tdate);
      $('[name="status"]').val(vendor.rows(this).data()[0].status);
      $('#jonumberlistmodal').modal('hide');
      //getVendorPaymentTerm(vendorid);
      getBranch(tnumber);

});


$('[name="deliverydate"]').datetimepicker({
    format: 'YYYY-MM-DD'
});


$('[name="_vendor"]').on('change',function (e) {
    var vendorid = $(this).val();
    getVendorPaymentTerm(vendorid);
});

$('[name="btnjonumber"]').on('click',function (e) {
    $('#jonumberlistmodal').modal('show');

});



function getBranch(id){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{vendorid:id},
        url: APP_URL + '/getbranch',
        success:function (data) {
            $('[name="_branch"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;
            $.each(term, function (index, item) {
              if(  data.vendor ){
                  if(  data.vendor._branch == term[index].id ){
                      $('[name="_branch"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].name,
                          selected:'selected'
                      }));
                  }else{
                      $('[name="_branch"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].name

                      }));
                  }
              }else{
                  $('[name="_branch"]')
                      .find('option')
                      .remove()
                      .end()
                      .append('<option></option>')
                      .val('');
              }


            });

        }

    });
}
