var podetail = $('#mr-detail-table').DataTable({
    processing: true,
    serverSide: true,
    ajax:'mrdetaillist',
    columns: [
        {data: 'tnumber', name: 'mrdetail.tnumber'},
        {data: 'name', name: 'item.name'},
        {data: 'description', name: 'itempacking.description'},
        {data: 'quantity', name: 'podetail.quantity'}
    ],
    "columnDefs": [
        { "width": "15%", "targets": 3 }

    ],
    
});


$('#mr-detail-table').on('click','.deljod',function(e){
    e.preventDefault();

    var URL = $(this).attr('href');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                    type:'DELETE',
                    dataType:'json',
                    url:URL,
                    success:function(data){

                        podetail.ajax.reload();
                        if(data.ok == 'success'){
                            bootbox.alert({
                                size: 'small',
                                message: "Delete Successfully",
                                callback: function(){ /* your callback code */ }
                            })
                        }
                    }

                });

            }

        }
    })

});



$('[name="btnvendor"]').on('click',function (e) {
    $('#vendorlistmodal').modal('show');

});

$('[name="cancel"]').on('click',function (e) {
    bootbox.confirm({
        size: 'small',
        message: "Do you want to cancel ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                    type:'get',
                    dataType:'json',
                    data:{tnumber:points.po.tnumber},
                    url: APP_URL + '/cancelpo',
                    success:function(data){
                        if(data.ok == 'success'){
                            bootbox.alert({
                                size: 'small',
                                message: "Cancelled Successfully",
                                callback: function(){ location.reload(); }
                            })
                        }
                    }
                });

            }

        }
    });


});


/*$('#tablevendorlist').on('dblclick','tr',function(){
    var vendorid = $(this).closest('tr').attr('id');
      var ventype = $(this).closest('tr').attr('_type');

    $('[name="_vendor"]').val($(this).closest('tr').find('td:eq(1)').text());
      $('[name="vendoridhidden"]').val(vendorid);
      $('[name="tmode"]').val(vendor.rows(this).data()[0]._type);
      $('#vendorlistmodal').modal('hide');
      getVendorPaymentTerm(vendorid);

});



$(function () {
    $('[name="vendoridhidden"]').val(points.po._vendor);
    $('[name="tmode"]').val(points.po.tmode);
    getVendorPaymentTerm(points.po._vendor);
});


function getVendorPaymentTerm(id){

    $.ajax({
        type:'get',
        dataType:'json',
        data:{vendorid:id},
        url: APP_URL + '/paymenttermsget',
        success:function (data) {
            $('[name="_paymentterm"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;
            $.each(term, function (index, item) {
              if(  data.vendor ){

                  if(  data.vendor._paymentterm == term[index].id ){
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description,
                          selected:'selected'
                      }));
                  }else{
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description

                      }));
                  }
              }else{
                  $('[name="_paymentterm"]')
                      .find('option')
                      .remove()
                      .end()
                      .append('<option></option>')
                      .val('');
              }


            });

        }

    });
}





function getpacking(id){

    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getselectedrm',
        success:function(data){
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description
                }));
            });
        }
    });
}*/


