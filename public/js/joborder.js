

$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});




var comptable = $('#joborder-table').DataTable({

    processing: true,
    serverSide: true,
    ajax: 'dashboard/joborderlist',
    "order": [[ 1, "desc" ]],
    columns: [
        {data: 'tnumber', name: 'jo.tnumber'},
        {data: 'tdate', name: 'jo.tdate'},
        {data: 'reference', name: 'jo.reference'},

        {data: 'name', name: 'cust.name'},
        {data: 'notes', name: 'jo.notes'},
        {data: 'status', name: 'jo.status'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#joborder-table').on('click','.deljo',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'joborder/' + compid,
                   success:function(data){
                      
                       comptable.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Delete Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } ,error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }


                });

            }

        }
    })
    
});


