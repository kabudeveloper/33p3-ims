var rawmat = $('#process-list-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'process/getprocess',
    columns: [
        {data: 'code', name: 'mp.code'},
        {data: 'process_name', name: 'mp.process_name'},
        {data: 'name', name: 'ml.name'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});