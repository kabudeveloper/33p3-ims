
$(function () {
    var box = $('#currencyconvertion-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: APP_URL + '/conlist/'+points.convertbase,
        columns: [
            {data: 'target', name: 'target'},
            {data: 'rate', name: 'rate'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        "columnDefs": [
            { "width": "35%", "targets": 2 }

        ]
    });


    $('#currencyconvertion-table').on('click','.delconcurrency',function(e){
        e.preventDefault();

        var base = $(this).attr('href');


        bootbox.confirm({
            size: 'small',
            message: "Are you sure you want to delete this record ?",
            callback: function( ok ){
                if( ok ){

                    $.ajax({
                        type:'DELETE',
                        dataType:'json',
                        url: base ,
                        success:function(data){

                            box.ajax.reload();
                            if(data.ok == 'success'){
                                bootbox.alert({
                                    size: 'small',
                                    message: "Delete Successfully",
                                    callback: function(){ /* your callback code */ }
                                })
                            }
                        }

                    });

                }

            }
        })
    });





    var bcurrency;
    var target;
    $('#currencyconvertion-table').on('click','.convertcurrency',function(e){
        e.preventDefault();

        bcurrency = $(this).attr('href');
        $('#convertmodal').modal('show');
        getCurrencAll(bcurrency);

    });

    $('#currencyconvertion-table').on('click','.manualconvertcurrency',function(e){
        e.preventDefault();

        bcurrency = $(this).attr('href');
        target = $(this).attr('id').split(',');

        $('#manualconvertmodal').modal('show');
        getCurrencAllManual(bcurrency,target[0],target[1]);

    });

    function getCurrencAll(currid){
        $.ajax({
            type:'get',
            dataType:'json',
            data:{id:currid},
            url: APP_URL + '/getcurrencies',
            success:function(data){

                $('[name="_currency"]')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option></option>')
                    .val('');
                var currency = data.currencies;
                $.each(currency, function (index, item) {
                    if( currency[index].id == currid){
                        $('[name="bcurrency"]').val(currency[index].name);
                        $('[name="bcurrencyhidden"]').val(currency[index].id);
                    }else{
                        $('[name="_currency"]').append($('<option>', {
                            value: currency[index].id,
                            text: currency[index].name
                        }));
                    }
                });
            }
        });
    }

    function getCurrencAllManual(currid,t,r){
        $.ajax({
            type:'get',
            dataType:'json',
            data:{id:currid},
            url: APP_URL + '/getcurrencies',
            success:function(data){
                $('[name="rate"]').val(r);

                $('[name="_currency"]')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option></option>')
                    .val('');
                var currency = data.currencies;
                $.each(currency, function (index, item) {
                    if( currency[index].id == currid){
                        $('[name="bcurrency"]').val(currency[index].name);
                        $('[name="bcurrencyhidden"]').val(currency[index].id);
                    }else{

                        if( currency[index].id == t){
                            $('[name="manualcurrencyhidden"]').val(currency[index].id);
                            $('[name="manualcurrency"]').append($('<option>', {
                                value: currency[index].id,
                                text: currency[index].name,
                                selected: 'selected'
                            }));
                        }
                        $('[name="manualcurrency"]').append($('<option>', {
                            value: currency[index].id,
                            text: currency[index].name
                        }));
                    }
                });
            }
        });
    }

    $('[name="_currency"]').on('change',function(){
        var id = $(this).val();
        getconvertion(id);
    });

    function getconvertion(conid){
        $.ajax({
            type:'get',
            dataType:'json',
            data:{convto:conid,basecurr:bcurrency},
            url:'convertcurrency',
            success:function(data){
                $('#rate').val(data.rate);
            }
        });
    }





});





