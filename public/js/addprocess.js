var item = $('#tableprocesslist').DataTable({
    processing: true,
    serverSide: true,
    ajax: APP_URL + '/getprocesses',
    columns: [
        {data: 'code', name: 'mp.code'},
        {data: 'process_name', name: 'mp.process_name'},
        {data: 'name', name: 'ml.name'}
    ]
});

$('#tableprocesslist').on('dblclick', 'tr', function () {
    var itemid = $(this).closest('tr').attr('id');
    $('[name="_hiddenpid"]').val(itemid);
    $('[name="pcode"]').val($(this).closest('tr').find('td:eq(0)').text());
    $('[name="pname"]').val($(this).closest('tr').find('td:eq(1)').text());
    $('[name="location"]').val($(this).closest('tr').find('td:eq(2)').text());
    $('#processmodallist').modal('hide');


});

$('[name="findproc"]').on('click', function(e){
	e.preventDefault();
	$('#processmodallist').modal("show");
});