$.ajax({
   type:'GET',
   dataType:'json',
   url:'getcategory',
   success:function(data){
       var category = data.categories;
       $.each(category, function (index, item) {
          $('[name="_category"]').val(item.id);
           $('[name="_category1"]').append($('<option>', {
               value: item.id,
               text: item.name,
               selected:'selected'
           }));
       });
   } 
    
});