
var customer = $('#tablecustomerlist').DataTable({
    processing: true,
    serverSide: true,
    ajax:APP_URL +  '/jocust',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'}
    ]
});


$('#tablecustomerlist').on('dblclick','tr',function(){
      var custid = $(this).closest('tr').attr('id');
      
      // alert($(this).find('td:eq(1)').text());
      $('[name="_customer"]').val($(this).find('td:eq(1)').text());
      $('[name="custidhidden"]').val(custid);
      $('#customerlistmodal').modal('hide');

      getCustomerPaymentTerm(custid);
});


$('[name="deliverydate"]').datetimepicker({
    format: 'YYYY-MM-DD'
});


$('[name="_customer"]').on('change',function (e) {
    var custid = $(this).val();
    
    getCustomerPaymentTerm(custid);
});

$('[name="btncustomer"]').on('click',function (e) {
    $('#customerlistmodal').modal('show');

});

$('[name="btnsonumber"]').on('click',function (e) {
    $('#sonumbermodal').modal('show');

});

var vendor = $('#tablesolist').DataTable({
    processing: true,
    serverSide: true,
    ajax:APP_URL +  '/solist',
    columns: [
        {data: 'tnumber', name: 'ts.tnumber'},
        {data: 'name', name: 'mc.name'},
        {data: 'notes', name: 'ts.notes'},
        {data: 'pricevalidity', name: 'ts.pricevalidity'},
        {data: '_currency', name: 'ts._currency'}
    ]
});

function getCustomerPaymentTerm(id){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{customerid:id},
        url: APP_URL + '/getpaymentterms',
        success:function (data) {
            console.log(data);
            $('[name="_paymentterm"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;

            $.each(term, function (index, item) {

              if(  data.customer ){

                  if(  data.customer._paymentterm == term[index].id ){
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description,
                          selected:'selected'
                      }));
                  }else{
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description

                      }));
                  }
              }else{
                  $('[name="_paymentterm"]')
                      .find('option')
                      .remove()
                      .end()
                      .append('<option></option>')
                      .val('');
              }


            });

        }

    });
}







function getpacking(id){

    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getselectedrm',
        success:function(data){
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description
                }));
            });
        }
    });


}
