

$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});

var subclss = $('#subclass-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/subclss',
    columns: [
        {data: 'code', name: 'subclss.code'},
        {data: 'name', name: 'subclss.name'},
        {data: 'clssname', name: 'clss.name'},
        {data: 'glcode', name: 'subclss.glcode'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#subclass-table').on('click','.delsubcss',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'subclss/' + compid,
                   success:function(data){
                      
                       subclss.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } ,error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }

                });

            }

        }
    })
    
});


