

var finishedproduct = $('#finishedproduct-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/finishedproduct',
    columns: [
        {data: 'code', name: 'itm.code'},
        {data: 'name', name: 'itm.name'},
        {data: 'clssname', name: 'clss.name'},
        {data: 'subclssname', name: 'subclss.name'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#finishedproduct-table').on('click','.delfinished',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'finishedproduct/' + compid,
                   success:function(data){

                       finishedproduct.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } ,error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }


                });

            }

        }
    })
    
});


