

$(function () {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {groupid: points.GROUP.id},
        url: points.APP_URL + '/getaccess',
        success: function (data) {
            var treedata = helperformtree(data);

            $("#tree3").dynatree({
                checkbox: true,
                selectMode: 3,
                children: treedata,
                cookieId: "dynatree-Cb3",
                idPrefix: "dynatree-Cb3-"
            });
        }
    });
});

$("#formrolesbtn").on('click', function (e) {
    e.preventDefault();
    var tree = $("#tree3").dynatree("getTree");
    formData = tree.serializeArray();

    if (tree.getActiveNode()) {
        formData.push({name: "activeNode", value: tree.getActiveNode().data.key});
    }
    formData.push({name: "groupid", value: points.GROUP.id});
    $.ajax({
        type: 'get',
        dataType:'json',
        data: jQuery.param(formData),
        url: points.APP_URL + '/updateaccess',
        success: function (data) {

            if(data.ok == 'success'){
                $("#tree3").dynatree("getTree").redraw();
                var n = noty({
                    text: 'Access Rights Updated!',
                    type : 'success',
                    layout:'topCenter',
                    theme: 'relax',
                    animation: {
                        open: {height: 'toggle'},
                        close: {height: 'toggle'},
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 2000
                });
            }
        },error:function(data){
            var n = noty({
                text: 'Error in update !',
                type : 'Error',
                layout:'topCenter',
                theme: 'relax',
                animation: {
                    open: {height: 'toggle'},
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 500
                },
                timeout: 2000
            });
        }
    });
});
