
var vendor = $('#tablereceivinglist').DataTable({
  processing: true,
  serverSide: true,
  ajax:APP_URL +  '/receivingpolookup',
  columns: [
    {data: 'ponumber', name: 'trr.ponumber'},
    {data: 'jonumber', name: 'po.jonumber'},
    {data: 'tnumber', name: 'trr.tnumber'},
    {data: 'tdate', name: 'po.tdate'},
    {data: '_vendor', name: 'po._vendor'},
    {data: 'name', name: 'vendor.name'},
    {data: '_branch', name: 'po._branch'},
    {data: 'branchname', name: 'branch.name'},
    {data: 'notes', name: 'po.notes'},
    {data: 'tmode', name: 'po.tmode'}
  ],

  columnDefs:[
    { targets: [4], visible: false },
    { targets: [6], visible: false },
    { targets: [9], visible: false }
  ]
});

function getreceivepodetail(id){
  var rows = '';
  $.ajax({
    type:'get',
    dataType:'json',
    data:{ponumber:id},
    url: APP_URL + '/receivingpodetail',
    success: function(data){
      // alert(data);
      console.log(data);
      var lno = 1
      for (var i = 0; i < data.podetails.length; i++) {
        //console.log(data.podetails[i]._item);
        var totalcost = data.podetails[i].unitprice * data.podetails[i].quantity;
        var qtydel = (data.podetails[i].qtydelivered == null) ? 0 : data.podetails[i].qtydelivered;
        var qtyacct = (data.podetails[i].qtyaccepted == null) ? 0 : data.podetails[i].qtyaccepted;
        var balance = (data.podetails[i].balance == null) ? data.podetails[i].quantity : data.podetails[i].balance;

        var addon = '<a class="btn btn-xs btn-primary" onclick="editLineItem('+lno+')" data-toggle="modal" data-target="#myModal" style="cursor:pointer;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>' ;

        rows = '<tr><td>' + lno + '<input type="checkbox" style="display:none" id="rcvdetail'+lno+'" name="rcvdetail[]" value="'+lno+','+data.podetails[i]._item+','+data.podetails[i].packing+','+data.podetails[i].unitprice+','+data.podetails[i].unitprice+','+data.podetails[i].quantity+','+data.podetails[i].qtydelivered+','+data.podetails[i].qtyaccepted+','+data.podetails[i].balance+'" checked></td><td>' 
        + data.podetails[i]._item + '<input type="hidden" id="item'+lno+'" value="'+data.podetails[i]._item+'"></td><td>' 
        + data.podetails[i].packing + '<input type="hidden" id="packing'+lno+'" value="'+data.podetails[i].packing+'"></td><td>' 
        + data.podetails[i].unitprice + '<input type="hidden" id="poprice'+lno+'" value="'+data.podetails[i].unitprice+'"></td><td class="tdrrprice'+lno+'">' 
        + data.podetails[i].unitprice + '<input type="hidden" id="rrprice'+lno+'" value="'+data.podetails[i].unitprice+'"></td><td>' 
        + data.podetails[i].quantity + '<input type="hidden" id="qtyordered'+lno+'" value="'+data.podetails[i].quantity+'"></td><td class="tdqtydel'+lno+'">' 
        + qtydel + '<input type="hidden" id="qtydelivered'+lno+'" value="'+qtydel+'"></td><td class="tdqtyacc'+lno+'">' 
        + qtyacct + '<input type="hidden" id="qtyaccepted'+lno+'" value="'+qtyacct+'"></td>'
        + '<td class="tdbalance'+lno+'">' + balance + '<input type="hidden" id="balance'+lno+'" value="'+balance+'"></td>'
        +'<td class="tdtotal'+lno+'">' + totalcost + '<input type="hidden" id="totalcost'+lno+'" value="'+totalcost+'"></td><td>'+addon+'</td></tr>';
        $(rows).appendTo("#tablereceivepodetail tbody");
        lno++;
      };
      //var rowCount = $('#tablereceivepodetail tbody tr').length;
      //alert(rowCount);
    },
    //error: function(){},
  });
}

function editLineItem(j){
  //var rowCount = $('#tablereceivepodetail tbody tr').length;
  //alert(rowCount);
  var balance = $("#balance"+j).val();
  var qto = $("#quantity"+j).val();

  if(balance == qto){
    $("#editlno").val(j);
    $("#itemdesc").val($("#item"+j).val());
    $("#editrrprice").val($("#rrprice"+j).val());
    $("#editqtyreceived").val($("#qtyordered"+j).val());
    $("#editqtyaccepted").val($("#qtyordered"+j).val());
  }else{
    $("#editlno").val(j);
    $("#itemdesc").val($("#item"+j).val());
    $("#editrrprice").val($("#rrprice"+j).val());
    $("#editqtyreceived").val(balance);
    $("#editqtyaccepted").val(balance);
  }
  

}

/*function saveLineItem(k){

}*/

$("#saveLineItem").on("click",function(){
  var lno = $("#editlno").val();
  var item = $("#item"+lno).val();
  var packing = $("#packing"+lno).val();
  var poprice = $("#poprice"+lno).val();
  var editrrprice = $("#editrrprice").val();
  var qtyordered = $("#qtyordered"+lno).val();
  var editqtyreceived = $("#editqtyreceived").val();
  var editqtyaccepted = $("#editqtyaccepted").val();
  var obalance = $("#balance"+lno).val();

  if(Number(editqtyreceived) >= Number(editqtyaccepted)){

    var getbal = (Number(obalance) == Number(qtyordered)) ? Number(qtyordered) - Number(editqtyaccepted) : Number(editqtyreceived) - Number(editqtyaccepted) ;

    var balance = getbal;

    // if(balance == 0){
    //   editqtyreceived = qtyordered;
    //   editqtyaccepted = qtyordered;
    // }

    var checkitem = lno+','+item+','+packing+','+poprice+','+editrrprice+','+qtyordered+','+editqtyreceived+','+editqtyaccepted+','+balance;

    var totalcost = editrrprice * editqtyaccepted;
    $(".tdrrprice"+lno).html(editrrprice+'<input type="hidden" id="rrprice'+lno+'" value="'+editrrprice+'">');
    $(".tdqtydel"+lno).html(editqtyreceived+'<input type="hidden" id="qtydelivered'+lno+'" value="'+editqtyreceived+'">');
    $(".tdqtyacc"+lno).html(editqtyaccepted+'<input type="hidden" id="qtyaccepted'+lno+'" value="'+editqtyaccepted+'">');
    $(".tdtotal"+lno).html(totalcost+'<input type="hidden" id="totalcost'+lno+'" value="'+totalcost+'">');
    $(".tdbalance"+lno).html(balance+'<input type="hidden" id="balance'+lno+'" value="'+balance+'">');
    $("#rcvdetail"+lno).val(checkitem);
    //$("#rcvdetail"+lno+":checked").val(lno, $("#item"+lno).val());
    $(".close").click();
    //alert($("#editlno").val());
  }
  else{
    alert('Your accepted quantity must not be bigger than the delivered quantity');
  }
});

$('#tablereceivinglist').on('dblclick','tr',function(){
    $("#tablereceivepodetail tbody > tr").remove();
    var ponumber = $(this).closest('tr').attr('id');
    var vendorid = $(this).closest('tr').attr('_vendor');
    var branchid = $(this).closest('tr').attr('_branch');

    //alert(vendor.rows(this).data()[0]._type);

    $('[name="ponumber"]').val(ponumber);
    $('[name="trrnumber"]').val($(this).closest('tr').find('td:eq(2)').text());
    $('[name="branchidhidden"]').val(vendor.rows(this).data()[0]._branch);
    $('[name="_branch"]').val($(this).closest('tr').find('td:eq(5)').text());
    $('[name="vendoridhidden"]').val(vendor.rows(this).data()[0]._vendor);
    $('[name="_vendor"]').val($(this).closest('tr').find('td:eq(4)').text());
    $('[name="tmode"]').val(vendor.rows(this).data()[0].tmode);

    getreceivepodetail(ponumber);      

    $('#vendorlistmodal').modal('hide');
    // getVendorPaymentTerm(vendorid);
});

$('#verify').on('click',function(){
  //alert($('[name="ponumber"]').val());
  $("#tablereceivepodetail tbody > tr").remove();
  var id = $('[name="ponumber"]').val();
  var rows = "";
  $.ajax({
    type:'get',
    dataType:'json',
    data:{ponumber:id},
    url: APP_URL + '/getpurchaseorder',
    success: function(data){
      //alert(data);
      if(data.po.status == 'A'){
        $('[name="branchidhidden"]').val(data.po._branch);
        $('[name="vendoridhidden"]').val(data.po._vendor);
        $('[name="trrnumber"]').val(data.trr);
        $('[name="_branch"]').val(data.branchname);
        $('[name="_vendor"]').val(data.vendorname);
        $('[name="tmode"]').val(data.po.tmode);
        $("#tablereceivepodetail tbody > tr").remove();
        //rows = data.podetails;
        //rows = '<tr><td>'+data.podetail.lno+'</td><td>'+data.podetail._item+'</td><td>'+data.podetail.packing+'</td><td>'+data.podetail.unitprice+'</td><td>'+data.podetail.unitprice+'</td><td>'+data.podetail.quantity+'</td><td>'+data.podetail.quantity+'</td><td>'+data.podetail.quantity+'</td><td>'+data.podetail.quantity+'</td><td>'+data.podetail.quantity+'</td></tr>';
        
        //$(rows).appendTo("#tablereceivepodetail tbody");
        getreceivepodetail(id);
        
      }
      else if(data.po.status != 'A'){
        $('[name="branchidhidden"]').val('');
        $('[name="vendoridhidden"]').val('');
        $('[name="trrnumber"]').val('');
        $('[name="_branch"]').val('');
        $('[name="_vendor"]').val('');
        $("#tablereceivepodetail tbody > tr").remove();
        rows = '<tr><td colspan="10" align="center">No data available in table</td></tr>';
        $(rows).appendTo("#tablereceivepodetail tbody");
      }
      else if(data.po.tnumber == null){
        $('[name="branchidhidden"]').val('');
        $('[name="vendoridhidden"]').val('');
        $('[name="trrnumber"]').val('');
        $('[name="_branch"]').val('');
        $('[name="_vendor"]').val('');
        $("#tablereceivepodetail tbody > tr").remove();
        rows = '<tr><td colspan="10" align="center">No data available in table</td></tr>';
        $(rows).appendTo("#tablereceivepodetail tbody");
      }
    },
    error: function(){},
  });
});

$('[name="deliverydate"]').datetimepicker({
    format: 'YYYY-MM-DD'
});


/*$('[name="_vendor"]').on('change',function (e) {
    var vendorid = $(this).val();
    getVendorPaymentTerm(vendorid);
});*/

$('[name="btnvendor"]').on('click',function (e) {
    $('#vendorlistmodal').modal('show');

});
