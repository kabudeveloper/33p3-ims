
var item = $('#tablejodetail').DataTable({
    processing: true,
    serverSide: true,
    ajax:APP_URL +  '/jodetailfinishprod',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'}
    ]
});


$('#tablejodetail').on('dblclick','tr',function(){
      var itemid = $(this).closest('tr').attr('id');

      $('[name="code"]').val($(this).closest('tr').find('td:eq(0)').text());
      $('[name="_item"]').val($(this).closest('tr').find('td:eq(1)').text());
      $('[name="itemhidden"]').val(itemid);
      getpacking(itemid);
      $('#finishprodlistmodal').modal('hide');
     // getCustomerPaymentTerm(custid);
});


$('[name="btnjodetailfin"]').on('click',function (e) {
    $('#finishprodlistmodal').modal('show');

});



function getCustomerPaymentTerm(id){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{customerid:id},
        url: APP_URL + '/getpaymentterms',
        success:function (data) {
            $('[name="_paymentterm"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;
            $.each(term, function (index, item) {
              if(  data.customer ){
                  if(  data.customer._paymenttterm == term[index].id ){
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description,
                          selected:'selected'
                      }));
                  }else{
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description

                      }));
                  }
              }else{
                  $('[name="_paymentterm"]')
                      .find('option')
                      .remove()
                      .end()
                      .append('<option></option>')
                      .val('');
              }


            });

        }

    });
}




function getpacking(id){

    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getselectedrm',
        success:function(data){
            getSellingPrice(id);
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description
                }));
            });
        }
    });
    
}

function getSellingPrice(id){
    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getjodsellingprice',
        success:function(data){
            $('[name="unitprice"]').val(numeral(data.sellingprice[0].sellingprice).format('0,0.00'));
        }
    });
}