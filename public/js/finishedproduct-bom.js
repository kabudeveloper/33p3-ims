

var finishedproduct = $('#finishedproduct-bom-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'fpbom',
    bFilter: false,
    columns: [
        {data: 'name', name: 'item.name'},
        {data: 'description', name: 'itempacking.description'},
        {data: 'quantity', name: 'itembom.quantity'},
        {data: 'cost', name: 'avgcost.cost'},
        {data: 'USD', name: 'USD'},
        {data: 'totalcost', name: 'totalcost',searchable: false},
        {data: 'totalUSD', name: 'totalUSD'},
        {data: 'directrm', name: 'itembom.directrm',searchable: false},
        {data: 'action', name: 'action', orderable: false, searchable: false}

    ],
    "columnDefs": [
        { "width": "10%", "targets": 3 },
        { "width": "15%", "targets": 4 }
    ],
    "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api();
        var intVal = function ( i ) {
            return typeof i === 'string' ?
            i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };
        if (api.column(5).data().length){
          
            var total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                 
                    return intVal(a) + intVal(b);
                } ) }
        else{ total = 0};
        if (api.column(5).data().length){
            var pageTotal = api
                .column( 5 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } ) }
        else{ pageTotal = 0};
        $( api.column( 5 ).footer() ).html(
            ''+ numeral(pageTotal).format('0,0.00') +' ( '+ numeral(total).format('0,0.00') +' grand total)'

        );
       
    }

});





$('#finishedproduct-bom-table').on('click','.delfinishedprodbom',function(e){
    e.preventDefault();

   var URL = $(this).attr('href');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:URL,
                   success:function(data){

                       finishedproduct.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /!* your callback code *!/ }
                           })
                       }
                   } 

                });

            }

        }
    })
    
});


