/*
paceOptions = {
    ajax: false, // disabled
    document: false, // disabled
    eventLag: false, // disabled
    elements: {
        selectors: ['.cover']
    }
};


Pace.once("done", function(){
    $(".cover").fadeOut(2000);
});
*/


$('#customer-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/customer',
    columns: [
        {data: 'id', name: 'id'},
        {data: 'name', name: 'name'},
        {data: 'contactperson', name: 'contactperson'},
        {data: 'emailaddress', name: 'emailaddress'}
    ]
});

var comptable = $('#company-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/company',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'},
        {data: 'address', name: 'address'},
        {data: 'tin', name: 'tin'},
        {data: 'zipcode', name: 'zipcode'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});


var compbranch = $('#company-branch-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/branch',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'},
        {data: 'address', name: 'address'},
        {data: 'tin', name: 'tin'},
        {data: 'zipcode', name: 'zipcode'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});





$('#company-table').on('click','.delcompany',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'company/' + compid,
                   success:function(data){
                      
                       comptable.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Delete Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } 

                });

            }

        }
    })

    

});


$('#company-branch-table').on('click','.delbranch',function(e){
    e.preventDefault();

    var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                    type:'DELETE',
                    dataType:'json',
                    url:'branch/' + compid,
                    success:function(data){

                        compbranch.ajax.reload();
                        if(data.ok == 'success'){
                            bootbox.alert({
                                size: 'small',
                                message: "Delete Successfully",
                                callback: function(){ /* your callback code */ }
                            })
                        }
                    }

                });

            }

        }
    })



});
