$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});


$.ajax({
    type: 'GET',
    dataType: 'json',
    data: {catid: catid},
    url: APP_URL + '/rawmaterial/geteditcategory',
    success: function (data) {
        var category = data.categories;
        $.each(category, function (index, item) {
            if (item.id == catid) {
                $('[name="_category"]').append($('<option>', {
                    value: item.id,
                    text: item.name,
                    selected: "selected"

                }));
            }

        });

        optclass(catid);

    }

});





$('[name="_class"]').on('change', function (e) {
    var classid = $(this).val();

    $('[name="_subclass"]')
        .find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('');


    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {classid: classid},
        url:  APP_URL +'/rawmaterial/getsubclass',
        success: function (data) {
            var subclassess = data.subclassess;
            $.each(subclassess, function (index, item) {
                $('[name="_subclass"]').append($('<option>', {
                    value: item.id,
                    text: item.name

                }));
            });
        }

    });

});


var optclass = function (catid) {
    
    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {catid: catid},
        url: APP_URL +'/rawmaterial/getclss',
        success: function (data) {
            var classess = data.classess;

            $.each(classess, function (index, item) {

                if (item.id == classid) {

                    $('[name="_class"]').append($('<option>', {
                        value: item.id,
                        text: item.name,
                        selected: "selected"

                    }));
                } else {

                        $('[name="_class"]').append($('<option>', {
                            value: item.id,
                            text: item.name

                        }));

                }
            });
            
            optsubclass(catid);
        }

    });


};


var optsubclass = function (catid) {

    $('[name="_subclass"]')
        .find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('');

    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {classid: classid},
        url: APP_URL +'/rawmaterial/getsubclass',
        success: function (data) {
            var subclassess = data.subclassess;
            $.each(subclassess, function (index, item) {
                if (item.id == subclassid) {
                    $('[name="_subclass"]').append($('<option>', {
                        value: item.id,
                        text: item.name,
                        selected: "selected"

                    }));
                } else {
                    $('[name="_subclass"]').append($('<option>', {
                        value: item.id,
                        text: item.name

                    }));
                }
            });
        }
    });
};



$('#editrawmat').on('click',function(e){
    if($(e.target).hasClass('iac')){
       $('#iacmodal').modal('show');

        var itemid = points.company.id;

        $.ajax({
            type: 'get',
            dataType: 'json',
            data: '_item='+itemid,
            url: APP_URL + '/getcostchanges',
            success: function (data) {
               
                $('#cost').val(parseFloat(data.cost.cost).toFixed(2));

            }

        });

    }



});


$('#iacmodal').on('click',function(e){

    if($(e.target).hasClass('savecostchanges')){

        var formcost = $('#formcostchanges').serialize();
        var itemid = points.company.id;

        console.log(formcost);
        $.ajax({
            type: 'post',
            dataType: 'json',
            data: formcost + '&_item='+itemid,
            url: APP_URL + '/costchanges',
            success: function (data) {

                   if(data.ok === 'success'){
                       var n = noty({
                           text: 'Cost Updated',
                           type : 'success',
                           layout:'topCenter',
                           theme: 'relax',
                           animation: {
                               open: {height: 'toggle'},
                               close: {height: 'toggle'},
                               easing: 'swing',
                               speed: 500
                           },
                           timeout: 2000,
                       });
                       $('#iacmodal').modal('hide');
                   }
            }

        });

    }
});

$('.modal').on('hidden.bs.modal', function(){
    $(this).find('form')[0].reset();
});