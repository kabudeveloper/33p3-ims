

$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});




var comptable = $('#purchaseorder-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/purchaseorderlist',
    columns: [
        {data: 'tnumber', name: 'po.tnumber'},
        {data: 'tdate', name: 'po.tdate'},
        {data: 'name', name: 'vendor.name'},
        {data: 'notes', name: 'po.notes'},
        {data: 'status', name: 'po.status'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});

var comptable = $('#purchaseorder-balance-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/purchaseorderbalancelist',
    columns: [
        {data: 'tnumber', name: 'po.tnumber'},
        {data: 'tdate', name: 'po.tdate'},
        {data: 'venname', name: 'vendor.name'},
        {data: 'item_name', name: 'm.name'},
        {data: 'balance', name: 'balance'}
    ],
    "rowCallback": function(row, data, index){
        if (data["balance"] == 0) {
          $(row).hide();
            
        }
    } 
    
});


$('#purchaseorder-table').on('click','.deljo',function(e){
    e.preventDefault();

   var poid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'purchasing/' + poid,
                   success:function(data){
                      
                       comptable.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Delete Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } 

                });

            }

        }
    })
    
});


