
$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});



var compbranch = $('#securityuser-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/securityuser',
    columns: [
        {data: 'firstname', name: 'firstname'},
        {data: 'lastname', name: 'lastname'},
        {data: 'username', name: 'username'},
        {data: 'isactive', name: 'isactive'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});


$('#securityuser-table').on('click','.deluser',function(e){
    e.preventDefault();

    var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                    type:'DELETE',
                    dataType:'json',
                    url:'securityuser/' + compid,
                    success:function(data){

                        compbranch.ajax.reload();
                        if(data.ok == 'success'){
                            bootbox.alert({
                                size: 'small',
                                message: "Delete Successfully",
                                callback: function(){ /* your callback code */ }
                            })
                        }
                    }

                });

            }

        }
    })
    
});




$('#formsecurityuser').on('click',function(e){
    if($(e.target).hasClass('resetpassword')){

        $('#resetpasswordmodal').modal('show');

    }

});


$('#resetpass').on('click',function (e) {

    var formdata = $('#userresetpassword').serialize();

    $.ajax({
        type:'PUT',
        dataTpe:'json',
        data: formdata,
        url: APP_URL + '/resetpassword',
        success:function(data){
            $('#resetpasswordmodal .form-group').removeClass('has-error help-block error');
            $('#resetpasswordmodal div').find('span').remove();
            $('#resetpasswordmodal').modal('hide');

            var n = noty({
                text: 'Password has been changed!',
                type : 'success',
                layout:'topCenter',
                theme: 'relax',
                animation: {
                    open: {height: 'toggle'},
                    close: {height: 'toggle'},
                    easing: 'swing',
                    speed: 500
                },
                timeout: 2000,
            });

        },error:function(data){
            var errors = data.responseJSON;

            $.each(errors, function (key, value) {
               
                $('.' + key).html(
                    '<span class="help-block error">' +
                    value +
                    '</span>'
                )
                    .closest('.form-group').addClass('has-error')
            });


        }

    });
});



$('.modal').on('hidden.bs.modal', function(){
    $(this).find('form')[0].reset();
});