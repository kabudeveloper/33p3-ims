

var item = $('#item-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/item',
    columns: [
        {data: 'code', name: 'itm.code'},
        {data: 'name', name: 'itm.name'},
        {data: 'categname', name: 'cat.name'},
        {data: 'clssname', name: 'clss.name'},
        {data: 'subclssname', name: 'subclss.name'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#item-table').on('click','.delitem',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'item/' + compid,
                   success:function(data){
                      
                       item.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Delete Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } 

                });

            }

        }
    })
    
});


