// alert(APP_URL+"finishedproduct/1682/processcost");
var pageTotal = 0;
var item = $('#process-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'processcost',
    columns: [
        {data: 'process_name', name: 'mp.process_name'},

        {data: 'amount', name: 'li.amount'},
    
        {data: 'action', name: 'action'}
    ],
    "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api();
        var intVal = function ( i ) {
            return typeof i === 'string' ?
            i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };
        if (api.column(1).data().length){
          
            var total = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                 
                    return intVal(a) + intVal(b);
                } ) }
        else{ total = 0};
        if (api.column(1).data().length){
             pageTotal = api
                .column( 1 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } ) }
        else{ pageTotal = 0};
        $( api.column( 1 ).footer() ).html(
            ''+ numeral(pageTotal).format('0,0.00') +' ( '+ numeral(total).format('0,0.00') +' grand total)'

        );
       
    }
});

function ppageTotal(){

    this.pageTotal = pageTotal;
}

$('#process-table').on('click','.delfinishedprodpro',function(e){
    e.preventDefault();

   var URL = $(this).attr('href');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:URL,
                   success:function(data){

                       item.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /!* your callback code *!/ }
                           })
                       }
                       
                   }    

                });

            }

        }
    })
    
});