

var finishedproductpacking = $('#finishedproduct-packing-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'fppack',
    bFilter: false,
    columns: [
        {data: 'description', name: 'itempacking.description'},
        {data: 'code', name: 'packaging.code'},
        {data: 'umcode', name: 'um.code'},
        {data: 'umvalue', name: 'itempacking.umvalue'},
        {data: 'innerquantity', name: 'itempacking.innerquantity'},
        {data: '_level', name: 'itempacking._level'},
        {data: 'action', name: 'action', orderable: false, searchable: false}

    ],
    "columnDefs": [
        { "width": "10%", "targets": 3 },
        { "width": "15%", "targets": 4 }
    ]
});





$('#finishedproduct-packing-table').on('click','.delfinishedpacking',function(e){
    e.preventDefault();

   var URL = $(this).attr('href');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:URL,
                   success:function(data){

                       finishedproductpacking.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /!* your callback code *!/ }
                           })
                       }
                   } 

                });

            }

        }
    })
    
});



