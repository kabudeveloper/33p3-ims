

$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});




var comptable = $('#production-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/productionlist',
    columns: [
        {data: 'tnumber', name: 'j.tnumber'},
        {data: 'tdate', name: 'j.tdate'},
        {data: 'tnumber', name: 'j.tnumber'},
        {data: 'name', name: 'c.name'},
        {data: 'status', name: 'j.status'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#production-table').on('click','.deljo',function(e){
    e.preventDefault();

   var poid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'receiving/' + poid,
                   success:function(data){
                      
                       comptable.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Delete Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } 

                });

            }

        }
    })
    
});


