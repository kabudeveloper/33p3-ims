




var customercontact = $('#contacts-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'custcontact',
    columns: [
        {data: 'name', name: 'contact.name'},
        {data: 'position', name: 'contact.position'},
        {data: 'mobile', name: 'contact.mobile'},
        {data: 'landline', name: 'contact.landline'},
        {data: 'fax', name: 'contact.fax'},
        {data: 'email', name: 'contact.email'},
        {data: 'isprimary', name: 'contact.isprimary'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#contacts-table').on('click','.delcustcontact',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');
    
   bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url: APP_URL + '/contact/delcustcontact/' + compid,
                   success:function(data){

                       customercontact.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /!* your callback code *!/ }
                           })
                       }
                   } 

                });

            }

        }
    });
    
});


