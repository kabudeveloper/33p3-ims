


var position = $('#position-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/positions',
    columns: [
        {data: 'description', name: 'description'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ],
    "columnDefs": [
        { "width": "15%", "targets": 0 },
        { "width": "15%", "targets": 1 }
    ]
});


$('#position-table').on('click','.delposition',function(e){
    e.preventDefault();

    var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                    type:'DELETE',
                    dataType:'json',
                    url:'positions/' + compid,
                    success:function(data){

                        position.ajax.reload();
                        if(data.ok == 'success'){
                            bootbox.alert({
                                size: 'small',
                                message: "Delete Successfully",
                                callback: function(){ /* your callback code */ }
                            })
                        }
                    }

                });

            }

        }
    })
    
});
