$(function(){

    $.ajax({
        type: 'GET',
        dataType: 'json',
        data:{groupid:points.groupid},
        url: points.APP_URL + '/getaccesscreate',
        success: function (data) {

            var treeData = tData(data);

            $("#tree3").dynatree({
                checkbox: true,
                selectMode: 3,
                children: treeData,
                onSelect: function (select, node) {
                    var selKeys = $.map(node.tree.getSelectedNodes(), function (node) {
                        return node.data.key;
                    });
                    $("#echoSelection3").text(selKeys.join(", "));

                    var selRootNodes = node.tree.getSelectedNodes(true);

                    var selRootKeys = $.map(selRootNodes, function (node) {
                        return node.data.key;
                    });
                    $("#echoSelectionRootKeys3").text(selRootKeys.join(", "));
                    $("#echoSelectionRoots3").text(selRootNodes.join(", "));
                },
                onDblClick: function (node, event) {
                    node.toggleSelect();
                },
                onKeydown: function (node, event) {
                    if (event.which == 32) {
                        node.toggleSelect();
                        return false;
                    }
                },

                cookieId: "dynatree-Cb3",
                idPrefix: "dynatree-Cb3-"
            });
        }
    });

});



var tData = (function iife() {
    function addChildren(group, id, data) {
        function matchingId(roleInfo) {
            return Number(roleInfo.parent) === Number(id);
        }

        var children = data.roles.filter(matchingId)
            .map(function nestedChildren(roleInfo) {
                var role = {
                    title: roleInfo.description
                };
                if (id !== null) {
                    role.key = roleInfo.id;
                }

                var roleChildren = addChildren(role, roleInfo.id, data);

                if (roleChildren.length > 0) {
                    role.children = roleChildren;
                }
                return role;
            });

        if (children.length === 0) {
            return [];
        }
        group.children = children;
        return group;
    }

    return function tdconverter(data) {
        return data.groups.map(function getGroupChildren(groupInfo) {
            var groupData = {
                title: groupInfo.description,
                key: groupInfo.id
            };
            var group = addChildren(groupData, null, data);
            return group;
        });
    };
}());


$("#formrolesbtn").on('click', function (e) {
    e.preventDefault();


    var tree = $("#tree3").dynatree("getTree");
    formData = tree.serializeArray();

    console.log(points.groupid);
 /*   if (tree.getActiveNode()) {
        formData.push({name: "activeNode", value: tree.getActiveNode().data.key});
    }
    formData.push({name: "group", value: points.groupid});*/
    /*alert("POSTing this:\n" + jQuery.param(formData));*/
    formData.push({name: "group", value: points.groupid});
    $.ajax({
        type: 'GET',
        dataType: 'json',
        data:jQuery.param(formData),
        url: points.APP_URL + '/addingrole',
        success: function (data) {

        }
    });

});

