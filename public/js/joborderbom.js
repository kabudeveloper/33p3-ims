var tablebomlist;

$(function(){
     tablebomlist = $('#joborder-bom-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'joborderbomgetbom',
        columns: [
            {data: 'name', name: 'name'},
            {data: 'packing', name: 'packing'},
            {data: 'quantity', name: 'quantity'},
            {data: 'unitcost', name: 'unitcost'},
            {data: 'totalcost', name: 'totalcost'},
            {data: 'action', name: 'action', orderable: false, searchable: false}

        ],
         "footerCallback": function ( row, data, start, end, display ) {
             var api = this.api();
             var intVal = function ( i ) {
                 return typeof i === 'string' ?
                 i.replace(/[\$,]/g, '')*1 :
                     typeof i === 'number' ?
                         i : 0;
             };
             if (api.column(4).data().length){

                 var total = api
                     .column( 4 )
                     .data()
                     .reduce( function (a, b) {

                         return intVal(a) + intVal(b);
                     } ) }
             else{ total = 0}
             if (api.column(4).data().length){
                 var pageTotal = api
                     .column( 4 , { page: 'current'} )
                     .data()
                     .reduce( function (a, b) {
                         return intVal(a) + intVal(b);
                     } ) }
             else{ pageTotal = 0}
             $( api.column( 4 ).footer() ).html(
                 ''+ numeral(pageTotal).format('0,0.00') +' ( '+ numeral(total).format('0,0.00') +' grand total)'

             );

         }
    });

});

$('#joborder-bom-table').on('click','.deljobom',function(e){
    e.preventDefault();

    var URL = $(this).attr('href');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                    type:'DELETE',
                    dataType:'json',
                    url:URL,
                    success:function(data){

                        tablebomlist.ajax.reload();
                        if(data.ok == 'success'){
                            bootbox.alert({
                                size: 'small',
                                message: "Delete Successfully",
                                callback: function(){ /!* your callback code *!/ }
                            })
                        }
                    } ,error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }

                });

            }

        }
    })

});