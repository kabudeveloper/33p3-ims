var item = $('#tablejodetail').DataTable({
    processing: true,
    serverSide: true,
    ajax: APP_URL + '/jodetailfinishprod',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'}
    ]
});

$('#tablejodetail').on('dblclick', 'tr', function () {
    var itemid = $(this).closest('tr').attr('id');

    $('[name="code"]').val($(this).closest('tr').find('td:eq(0)').text());
    $('[name="_item"]').val($(this).closest('tr').find('td:eq(1)').text());
    $('[name="itemhidden"]').val(itemid);
    getpacking(itemid);
    $('#finishprodlistmodal').modal('hide');
    getRmCost(itemid);


});


$('[name="btnjodetailfin"]').on('click', function (e) {
    e.preventDefault();
    $('#finishprodlistmodal').modal('show');

});


function getCustomerPaymentTerm(id) {
    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {customerid: id},
        url: APP_URL + '/getpaymentterms',
        success: function (data) {
            $('[name="_paymentterm"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;
            $.each(term, function (index, item) {
                if (data.customer) {
                    if (data.customer._paymenttterm == term[index].id) {
                        $('[name="_paymentterm"]').append($('<option>', {
                            value: term[index].id,
                            text: term[index].description,
                            selected: 'selected'
                        }));
                    } else {
                        $('[name="_paymentterm"]').append($('<option>', {
                            value: term[index].id,
                            text: term[index].description

                        }));
                    }
                } else {
                    $('[name="_paymentterm"]')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option></option>')
                        .val('');
                }
            });
        }
    });
}


function getpacking(id) {

    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {id: id},
        url: APP_URL + '/getselectedrm',
        success: function (data) {
            getSellingPrice(id);
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description
                }));
            });
        }
    });
}

$(function(){
    getpackingByid(points.sod.packing);
    getRmCost(points.sod._item);

});


function getpackingByid(id){
    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getselecteditempacking',
        success:function(data){

            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;

            $.each(itempack, function (index, item) {
                if(itempack[index].id == points.sod.packing){
                    $('[name="packing"]').append($('<option>', {
                        value: itempack[index].id,
                        text: itempack[index].description,
                        selected:'selected'
                    }));
                }

            });


        }
    });
}

function getSellingPrice(id) {
    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {id: id},
        url: APP_URL + '/getjodsellingprice',
        success: function (data) {
            $('[name="unitprice"]').val(numeral(data.sellingprice[0].sellingprice).format('0,0.00'));
        }
    });
}


var costing = function (itemid) {
    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {_item: itemid},
        url: APP_URL + '/pullcosting',
        success: function (data) {
            if (data.itemcosting) {
                $('[name="lbcost"]').val(numeral(data.itemcosting.lbcost).format('0,0.00'));
                $('[name="ovcost"]').val(numeral(data.itemcosting.ovcost).format('0,0.00'));
                $('[name="omcost"]').val(numeral(data.itemcosting.omcost).format('0,0.00'));
                $('[name="markup"]').val(numeral(data.itemcosting.markup).format('0,0.00'));
                compute();
            }
        }
    });
};

function compute() {
    var totalcostprice = 0;
    totalcostprice = parseFloat($('[name="rmcost"]').val()) +
        parseFloat($('[name="lbcost"]').val()) +
        parseFloat($('[name="ovcost"]').val()) +
        parseFloat($('[name="omcost"]').val());
    var finaltotalcostprice = totalcostprice;
    $('[name="costprice"]').val(numeral(finaltotalcostprice).format('0,0.00'));
    var totalsellingprice = totalcostprice + parseFloat($('[name="markup"]').val());
    $('[name="sellingprice"]').val(numeral(totalsellingprice).format('0,0.00'));
    $('[name="finalsellingprice"]').val(numeral(totalsellingprice).format('0,0.00'));
}

function getRmCost(itemsids){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{id:itemsids} ,
        url: APP_URL + '/getrmcost',
        success:function(data){

            $('[name="rmcost"]').val(parseFloat(data.rmcost[0]).toFixed(2));
            costing(itemsids);
        }

    });
}