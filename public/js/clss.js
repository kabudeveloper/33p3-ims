


$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});



var clss = $('#class-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/clss',
    columns: [
        {data: 'code', name: 'clss.code'},
        {data: 'name', name: 'clss.name'},
        {data: 'categname', name: 'cat.name'},
        {data: 'glcode', name: 'clss.glcode'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#class-table').on('click','.delcss',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'clss/' + compid,
                   success:function(data){
                      
                       clss.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   }  ,error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }


                });

            }

        }
    })
    
});


