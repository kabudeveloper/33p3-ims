

var customer = $('#rawmateiallist').DataTable({
    processing: true,
    serverSide: true,
    ajax:APP_URL +  '/jobomlookup',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'}

    ]
});


$('[name="findrawmatbtn"]').on('click',function (e) {
    $('#rawmaterialmodal').modal('show');

});




$('#rawmateiallist').on('dblclick','tr',function(){
    var custid = $(this).closest('tr').attr('id');

    $('[name="code"]').val($(this).closest('tr').find('td:eq(0)').text());
    $('[name="rmname"]').val($(this).closest('tr').find('td:eq(1)').text());
    $('[name="_hiddenrmid"]').val(custid);
    $('#rawmaterialmodal').modal('hide');
    console.log(custid);
    getpacking(custid);
});





function getCustomerPaymentTerm(id){

    $.ajax({
        type:'get',
        dataType:'json',
        data:{customerid:id},
        url: APP_URL + '/getpaymentterms',
        success:function (data) {
            $('[name="_paymentterm"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;
            $.each(term, function (index, item) {
              if(  data.customer ){

                  if(  data.customer._paymentterm == term[index].id ){
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description,
                          selected:'selected'
                      }));
                  }else{
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description

                      }));
                  }
              }else{
                  $('[name="_paymentterm"]')
                      .find('option')
                      .remove()
                      .end()
                      .append('<option></option>')
                      .val('');
              }


            });

        }

    });
}





function getpacking(id){

    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getselectedrm',
        success:function(data){
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description
                }));
            });
        }
    });
}

