$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});


$.ajax({
    type: 'GET',
    dataType: 'json',
    data: {catid: catid},
    url: APP_URL + '/rawmaterial/geteditcategory',
    success: function (data) {
        var category = data.categories;
        $.each(category, function (index, item) {
            if (item.id == catid) {
                $('[name="_category"]').append($('<option>', {
                    value: item.id,
                    text: item.name,
                    selected: "selected"

                }));
            }

        });

        optclass(catid);

    }

});





$('[name="_class"]').on('change', function (e) {
    var classid = $(this).val();

    $('[name="_subclass"]')
        .find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('');


    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {classid: classid},
        url:  APP_URL +'/rawmaterial/getsubclass',
        success: function (data) {
            var subclassess = data.subclassess;
            $.each(subclassess, function (index, item) {
                $('[name="_subclass"]').append($('<option>', {
                    value: item.id,
                    text: item.name

                }));
            });
        }

    });

});


var optclass = function (catid) {
    
    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {catid: catid},
        url: APP_URL +'/rawmaterial/getclss',
        success: function (data) {
            var classess = data.classess;

            $.each(classess, function (index, item) {

                if (item.id == classid) {

                    $('[name="_class"]').append($('<option>', {
                        value: item.id,
                        text: item.name,
                        selected: "selected"

                    }));
                } else {

                        $('[name="_class"]').append($('<option>', {
                            value: item.id,
                            text: item.name

                        }));

                }
            });
            
            optsubclass(catid);
        }

    });


};


var optsubclass = function (catid) {

    $('[name="_subclass"]')
        .find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('');

    $.ajax({
        type: 'GET',
        dataType: 'json',
        data: {classid: classid},
        url: APP_URL +'/rawmaterial/getsubclass',
        success: function (data) {
            var subclassess = data.subclassess;
            $.each(subclassess, function (index, item) {
                if (item.id == subclassid) {
                    $('[name="_subclass"]').append($('<option>', {
                        value: item.id,
                        text: item.name,
                        selected: "selected"

                    }));
                } else {
                    $('[name="_subclass"]').append($('<option>', {
                        value: item.id,
                        text: item.name

                    }));
                }
            });
        }
    });
};




$('[name="isacombo"]').on('ifChanged',function(e){

    if ($(this).prop('checked')) {
        $('#divotherproduct').removeClass('hidediv');

    }else{
        $('#divotherproduct').addClass('hidediv');
    }
});

 if( $('input[name="isacombo"]').is(":checked")){
     $('#divotherproduct').removeClass('hidediv');

 }else{
     $('#divotherproduct').addClass('hidediv');
 }



$('#costing').on('click',function(e){
    e.preventDefault();

  var pageTotal = new ppageTotal();
  var pullitemid = points.mainitem.id;
  var rmcost = parseFloat(points.rmcost).toFixed(2);
    $.ajax({
       type:'get',
       dataType:'json',
       data: {_item:pullitemid},
       url: APP_URL + '/pullcosting',
       success:function(data){
          if(data.itemcosting){
                $('[name="lbcost"]').val(parseFloat(data.itemcosting.lbcost).toFixed(2));
                $('[name="ovcost"]').val(parseFloat(data.itemcosting.ovcost).toFixed(2));
                $('[name="omcost"]').val(parseFloat(data.itemcosting.omcost).toFixed(2));
                $('[name="markup"]').val(parseFloat(data.itemcosting.markup).toFixed(2));
                $('[name="ocost"]').val(parseFloat(pageTotal.pageTotal).toFixed(2));
                $('[name="cprice"]').val(parseFloat(data.itemcosting.commitedprice).toFixed(2));

              /* var totprice =  parseFloat(points.bom[0]) + parseFloat(data.itemcosting.lbcost)
               + parseFloat(data.itemcosting.ovcost) + parseFloat(data.itemcosting.omcost);

               $('[name="costprice"]').val(numeral(totprice).format('0,0.00'));

               var sellingprice = totprice + parseFloat(data.itemcosting.markup);

               $('[name="sellingprice"]').val(numeral(sellingprice).format('0,0.00'));*/
              compute();

          }else if(rmcost == "NaN"){
            $('[name="lbcost"]').val(0);
            $('[name="ovcost"]').val(0);
            $('[name="omcost"]').val(0);
            $('[name="markup"]').val(0);
            $('[name="rmcost"]').val(0);
            $('[name="ocost"]').val(0);
            $('[name="cprice"]').val(0);
          }else{
            $('[name="lbcost"]').val(0);
            $('[name="ovcost"]').val(0);
            $('[name="omcost"]').val(0);
            $('[name="markup"]').val(0);
            $('[name="ocost"]').val(0);
            $('[name="cprice"]').val(0);
          }

       }

    });



    $('[name="mainitem"]').val(points.mainitem.code + ' - ' + points.mainitem.name);
    $('[name="_item"]').val(points.mainitem.id);

/*
    var totalrm =0;

    if( typeof points.otherproduct[0] !== 'undefined'){
         totalrm = parseFloat(points.bom[0]) + parseFloat(points.otherproduct[0]['totalprice']);
    }else{
         totalrm = parseFloat(points.bom[0]);
    }


    $('[name="rmcost"]').val(totalrm.toFixed(2));
    (!points.bom[0]) ? $('[name="rmcosthidden"]').val(0) : $('[name="rmcosthidden"]').val(totalrm) ;*/

    $('[name="rmcost"]').val(parseFloat(points.rmcost).toFixed(2));


    $('#costingmodal').modal('show');


});


$('#costingmodal').on('click',function(e){
    if($(e.target).hasClass('compute')){
        var totalcostprice = 0;
        totalcostprice = parseFloat($('[name="rmcost"]').val()) +
            parseFloat($('[name="ocost"]').val()) +
            parseFloat($('[name="lbcost"]').val()) +
            parseFloat($('[name="ovcost"]').val()) +
            parseFloat($('[name="omcost"]').val());
        var finaltotalcostprice  = totalcostprice;
        $('[name="costprice"]').val(finaltotalcostprice);
        var totalsellingprice = totalcostprice + parseFloat($('[name="markup"]').val());
        $('[name="sellingprice"]').val(totalsellingprice);

    }
});

$('#savecosting').on('click',function(e){
    // e.preventDefault();
    var pullitemid = points.mainitem.id;
    var costingform = $('#costingform').serialize();
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: costingform + '&item='+pullitemid,
        url: APP_URL + '/costingsave',
        success:function(data){

            $('#costingmodal').modal('hide');
        }
    });
});


$('.modal').on('hidden.bs.modal', function(){
    $(this).find('form')[0].reset();
});

function compute(){
    var totalcostprice = 0;
    totalcostprice = parseFloat($('[name="rmcost"]').val()) +
        parseFloat($('[name="lbcost"]').val()) +
        parseFloat($('[name="ovcost"]').val()) +
        parseFloat($('[name="ocost"]').val()) +
        parseFloat($('[name="omcost"]').val());
    var finaltotalcostprice  = totalcostprice;
    $('[name="costprice"]').val(finaltotalcostprice);
    var totalsellingprice = totalcostprice + parseFloat($('[name="markup"]').val());
    $('[name="sellingprice"]').val(totalsellingprice);
}


