
var customer = $('#tablecustomerlist').DataTable({
    processing: true,
    serverSide: true,
    ajax:APP_URL +  '/jocust',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'}
    ]
});


$('#tablecustomerlist').on('dblclick','tr',function(){
    resetForm();
    var custid = $(this).closest('tr').attr('id');
    $('[name="_customer"]').val($(this).closest('tr').children('td:eq(1)').text());
    $('[name="custidhidden"]').val(custid);
    $('#customerlistmodal').modal('hide');
    $('[name="_currency"]')
        .find('option')
        .remove()
        .end()
        .append('<option value="">Loading...</option>');

    getPaymentTermTextField(custid);
});


$('[name="pricevalidity"]').datetimepicker({
    format: 'YYYY-MM-DD HH:mm'
});


$('[name="_customer"]').on('change',function (e) {
    var custid = $(this).val();
    getCustomerPaymentTerm(custid);
});

$('[name="btncustomer"]').on('click',function (e) {
    $('#customerlistmodal').modal('show');
});



function getCustomerPaymentTerm(id){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{customerid:id},
        url: APP_URL + '/getpaymentterms',
        success:function (data) {

            $('[name="_paymentterm"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;

            $.each(term, function (index, item) {

                if(  data.customer ){

                    if(  data.customer._paymentterm == term[index].id ){
                        $('[name="_paymentterm"]').append($('<option>', {
                            value: term[index].id,
                            text: term[index].description,
                            selected:'selected'
                        }));
                    }else{
                        $('[name="_paymentterm"]').append($('<option>', {
                            value: term[index].id,
                            text: term[index].description

                        }));
                    }
                }else{
                    $('[name="_paymentterm"]')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option></option>')
                        .val('');
                }
            });
        }
    });
}


function getpacking(id){

    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getselectedrm',
        success:function(data){
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description
                }));
            });
        }
    });
}

function getPaymentTermTextField(id){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{customerid:id},
        url: APP_URL + '/getpaymenttermstextfield',
        success:function (data) {
            var payment =  data.customerpayment;
            $('[name="_paymentterm"]').val(payment.id);
            $('[name="paymentterm"]').val(payment.description);
            getCurrencyByCustomer(id);
        }
    });
}

function getCurrencyByCustomer(id){
    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getcurrencycustomer',
        success:function(data){
            getCurr(data.custcurrency._currency);
        }
    });
}

function getCurr(custcurrency){

    $.ajax({
        type:'get',
        dataType: 'json',
        url: APP_URL + '/getcurrencies',
        success:function(data){
            $('[name="_currency"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var currency = data.currencies;

            $.each(currency, function (index, item) {
                if( currency[index].id == custcurrency){
                    $('[name="_currency"]').append($('<option>', {
                        value: currency[index].id,
                        text: currency[index].name,
                        selected:'selected'
                    }));
                }else{
                    $('[name="_currency"]').append($('<option>', {
                        value: currency[index].id,
                        text: currency[index].name
                    }));
                }
            });

            getCurrencyRate(custcurrency);

        }
    });
}

function getCurrencyRate(currency){
       $.ajax({
           type:'get',
           dataType: 'json',
           data: {_currency:currency},
           url: APP_URL + '/getcurrenciesrate',
           success:function(data) {
                console.log('rate: '+data.curr.rate);
               $('[name="currencyrate"]').val(data.curr.rate);
           }

       });
}

function resetForm(){
    $('[name="paymentterm"]').val('');
    $('[name="currencyrate"]').val('');
}
