/*
paceOptions = {
    ajax: false, // disabled
    document: false, // disabled
    eventLag: false, // disabled
    elements: {
        selectors: ['.cover']
    }
};


Pace.once("done", function(){
    $(".cover").fadeOut(2000);
});
*/

$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});



var compbranch = $('#company-branch-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/branch',
    columns: [
        {data: 'code', name: 'br.code'},
        {data: 'branchname', name: 'br.name'},
        {data: 'address', name: 'br.address'},
        {data: 'zipcode', name: 'br.zipcode'},
        {data: 'companyname', name: 'comp.name'},
        {data: 'tin', name: 'br.tin'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});


// $('#company-branch-table').on('click','.delbranch',function(e){
//     e.preventDefault();

//     var compid = $(this).attr('id');

//     bootbox.confirm({
//         size: 'small',
//         message: "Are you sure you want to delete this record ?",
//         callback: function( ok ){
//             if( ok ){

//                 $.ajax({
//                     type:'DELETE',
//                     dataType:'json',
//                     url:'branch/' + compid,
//                     success:function(data){

//                         compbranch.ajax.reload();
//                         if(data.ok == 'success'){
//                             bootbox.alert({
//                                 size: 'small',
//                                 message: "Deleted Successfully",
//                                 callback: function(){ /* your callback code */ }
//                             })
//                         }
//                     },error:function (x, a, t) {
//                         var m = JSON.parse(x.responseText);
//                         bootbox.alert({
//                             size: 'small',
//                             message: '<h5 class="help-block">'+ m.error +'</h5>',
//                             callback: function(){ /* your callback code */ }
//                         })
//                  }


//             });

//             }

//         }
//     })
    
// });
