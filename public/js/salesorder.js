

$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});




var salesorder = $('#salesorder-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/salesoderlist',
    columns: [
        {data: 'tdate', name: 'so.tdate'},
        {data: 'tnumber', name: 'so.tnumber'},
        {data: 'name', name: 'customer.name'},
        {data: 'status', name: 'jo.status',orderable: false, searchable: false},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#salesorder-table').on('click','.delso',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'salesorder/' + compid,
                   success:function(data){

                       salesorder.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Delete Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   }
                    ,error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }



                });

            }

        }
    })
    
});


