

var box = $('#currency-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/currency',
    columns: [
        {data: 'name', name: 'name'},
        {data: 'symbol', name: 'symbol'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ],
    "columnDefs": [
        { "width": "35%", "targets": 2 }

    ]
});




$('#currency-table').on('click','.delcurrency',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');


    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'currency/' + compid,
                   success:function(data){
                      
                       box.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } ,error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }


                });

            }

        }
    })
});

var bcurrency;
$('#currency-table').on('click','.convertcurrency',function(e){
    e.preventDefault();

    bcurrency = $(this).attr('href');
    $('#convertmodal').modal('show');
    getCurrencAll(bcurrency);

});

$('#currency-table').on('click','.manualconvertcurrency',function(e){
    e.preventDefault();

    bcurrency = $(this).attr('href');
    $('#manualconvertmodal').modal('show');
    getCurrencAllManual(bcurrency);

});

function getCurrencAll(currid){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{id:currid},
        url:'getcurrencies',
        success:function(data){

            $('[name="_currency"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var currency = data.currencies;
            $.each(currency, function (index, item) {
                if( currency[index].id == currid){
                    $('[name="bcurrency"]').val(currency[index].name);
                    $('[name="bcurrencyhidden"]').val(currency[index].id);
                }else{
                    $('[name="_currency"]').append($('<option>', {
                        value: currency[index].id,
                        text: currency[index].name
                    }));
                }
            });
        }
    });
}

function getCurrencAllManual(currid){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{id:currid},
        url:'getcurrencies',
        success:function(data){

            $('[name="_currency"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var currency = data.currencies;
            $.each(currency, function (index, item) {
                if( currency[index].id == currid){
                    $('[name="bcurrency"]').val(currency[index].name);
                    $('[name="bcurrencyhidden"]').val(currency[index].id);
                }else{
                    $('[name="manualcurrency"]').append($('<option>', {
                        value: currency[index].id,
                        text: currency[index].name
                    }));
                }
            });
        }
    });
}



$('[name="_currency"]').on('change',function(){
    var id = $(this).val();
    getconvertion(id);
});

function getconvertion(conid){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{convto:conid,basecurr:bcurrency},
        url:'convertcurrency',
        success:function(data){
            $('#rate').val(data.rate);
        }
    });
}
