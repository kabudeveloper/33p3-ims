
$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});




var customer = $('#customer-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/customer',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'},
        {data: 'billing_address', name: 'billing_address'},
        {data: 'delivery_address', name: 'delivery_address'},
        {data: 'tin', name: 'tin'},
        {data: 'zipcode', name: 'zipcode'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#customer-table').on('click','.delcustomer',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'customer/' + compid,
                   success:function(data){
                      
                       customer.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   }  ,error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }




                });

            }

        }
    })
    
});


