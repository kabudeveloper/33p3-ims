$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});


var vendors = $('#vendors-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/vendors',
    columns: [

        {data: 'code', name: 'vend.code'},
        {data: 'name', name: 'vend.name'},
        {data: 'address', name: 'vend.address'},
        {data: 'zipcode', name: 'vend.zipcode'},
        {data: 'tin', name: 'vend.tin'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#vendors-table').on('click','.delvendors',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'vendors/' + compid,
                   success:function(data){

                       vendors.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } ,error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }

                });

            }

        }
    })
    
});


