$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});



var rawmatvendoritem = $('#supplies-vendor-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'suppliesvend',
    bFilter: false,
    columns: [
        {data: 'code', name: 'vendor.code'},
        {data: 'name', name: 'vendor.name'},
        {data: 'cost', name: 'lnkvenditem.cost'},
        {data: 'USD', name: 'USD'},
        {data: 'isactive', name: 'lnkvenditem.isactive'},
        {data: 'action', name: 'action', orderable: false, searchable: false}

    ],
    "columnDefs": [
        { "width": "10%", "targets": 3 },
        { "width": "15%", "targets": 4 }
    ]
});




$('#supplies-vendor-table').on('click','.delsuppliesvend',function(e){
    e.preventDefault();

   var URL = $(this).attr('href');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:URL,
                   success:function(data){

                       rawmatvendoritem.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } 

                });

            }

        }
    })
    
});


