

$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});




var comptable = $('#mr-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/mrlist',
    columns: [
        {data: 'tnumber', name: 'mr.tnumber'},
        {data: 'tdate', name: 'mr.tdate'},
        {data: 'tnumber', name: 'mr.tnumber'},
        {data: 'jonumber', name: 'mr.jonumber'},
        {data: 'name', name: 'l.name'},
        {data: 'status', name: 'mr.status'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#receiving-table').on('click','.deljo',function(e){
    e.preventDefault();

   var poid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'receiving/' + poid,
                   success:function(data){
                      
                       comptable.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Delete Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } 

                });

            }

        }
    })
    
});


