$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});



$('#vc').on('click', function (e) {
    e.preventDefault();
    var verify = $(this).button('loading');
    var vendorcode = $('[name="_vendor"]').val().trim();

    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {productcode: vendorcode},
        url: APP_URL + '/otherproductverifycode',
        success: function (data) {
            $('[name="vendorname"]').val('');
            $('#addvendoritem .form-group').removeClass('has-error help-block error');
            $('#addvendoritem div').find('span').remove();
            if (data.item) {
                $('[name="vendorname"]').val(data.item.name);
                $('[name="_hiddenvendorid"]').val(data.item.id);
            } else {
                var n = noty({
                    text: 'No record found !',
                    type : 'information',
                    layout:'topCenter',
                    theme: 'relax',
                    animation: {
                        open: {height: 'toggle'},
                        close: {height: 'toggle'},
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 2000,
                });
            }

            verify.button('reset');
        }, error: function (data) {
            verify.button('reset');
            var errors = data.responseJSON;
            $('[name="vendorname"]').val('');
            $('[name="_hiddenvendorid"]').val('');
            $.each(errors, function (key, value) {

                if(key == 'productcode'){
                    key = '_vendor';
                }
                
                $('.' + key).html(
                    '<span class="help-block error">' +
                    value +
                    '</span>'
                )
                    .closest('.form-group').addClass('has-error')
            });
        }

    });

});


$('#fv').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'GET',
        dataType: 'json',
        data:{item:points.item.id},
        url: APP_URL + '/finished-otherproduct',
        success: function (data) {
            $('#findVendorModal').modal('show');
            $('#findrawvendortable').find("tr:gt(0)").remove();
            $.each(data.vendor, function (key, value) {

                $('#findrawvendortable tbody').append('<tr>' +
                    '<td id="' + value.id + '">' + value.code + '</td>' +
                    '<td>' + value.name + '</td>' +
                    '</tr>');
            });
        }
    });
});


$('#searchvendor').on('click', function (e) {
    e.preventDefault();
    var formsearch = $('#formvendorsearch').serialize();
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: formsearch,
        url: APP_URL + '/otherproductsearch',
        success: function (data) {
            $('#findrawvendortable').find("tr:gt(0)").remove();

            if(data.vendor.length == 0){
                $('#findrawvendortable tbody').append('<tr>' +
                    '<td colspan="2"><p>No Record Found!</p></td>' +

                    '</tr>');
            }
            $.each(data.vendor, function (key, value) {
                $('#findrawvendortable tbody').append('<tr>' +
                    '<td id="' + value.id + '">' + value.code + '</td>' +
                    '<td>' + value.name + '</td>' +
                    '</tr>');
            });
        }
    });

});


$('#findrawvendortable').on('dblclick','tr',function(e){
    e.preventDefault();
    var id = $(this).find('td').attr('id');
    console.log(id);
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: {searchid:id},
        url: APP_URL + '/otherproductsearchid',
        success: function (data) {
            $('#findVendorModal').modal('hide');
            $('[name="vendorname"]').val(data.vendor.name);
            $('[name="_hiddenvendorid"]').val(data.vendor.id);
            $('[name="_vendor"]').val(data.vendor.code);
        },error: function (data) {

            var errors = data.responseJSON;
            alert(errors);

        }


    });

});


var searvendorid;

$('#findrawvendortable').on('click','tr',function(e){
    e.preventDefault();
    searvendorid = $(this).find('td').attr('id');

});


$('#findselectvendor').on('click',function(e){
    e.preventDefault();
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: {searchid:searvendorid},
        url: APP_URL + '/otherproductsearchid',
        success: function (data) {
            $('#findVendorModal').modal('hide');
            $('[name="vendorname"]').val(data.vendor.name);
            $('[name="_hiddenvendorid"]').val(data.vendor.id);
            $('[name="_vendor"]').val(data.vendor.code);
        },error: function(XMLHttpRequest, textStatus, errorThrown){
            //alert('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
        }

    });
});


$('#costing').on('click',function(e){
    e.preventDefault();

    var itemname = $('[name="vendorname"]').val();
    var _item = $('[name="_hiddenvendorid"]').val();

    if(itemname == ''){
        alert('Select Product First');
        return false;
    }


    $('[name="subitem"]').val(itemname);
    $('[name="_item"]').val(_item);
    $('[name="rmcost"]').val(numeral(points.bomtotalcost[0].bomtotalcost).format('0,0'));
    $('[name="rmcosthidden"]').val(points.bomtotalcost[0].bomtotalcost);
    $('[name="lbcost"]').val(parseFloat(points.costing.lbcost).toFixed(2));
    $('[name="ovcost"]').val(parseFloat(points.costing.ovcost).toFixed(2));
    $('[name="omcost"]').val(parseFloat(points.costing.omcost).toFixed(2));
    $('[name="markup"]').val(parseFloat(points.costing.markup).toFixed(2));

    $('#costingmodal').modal('show');

});


$('#costingmodal').on('click',function(e){
    if($(e.target).hasClass('compute')){
        var totalcostprice = 0;
        totalcostprice = parseFloat($('[name="rmcosthidden"]').val()) + parseFloat($('[name="lbcost"]').val()) + parseFloat($('[name="ovcost"]').val()) + parseFloat($('[name="omcost"]').val());
        var finaltotalcostprice  = totalcostprice;
        $('[name="costprice"]').val(numeral(finaltotalcostprice).format('0,0'));
        var totalsellingprice = totalcostprice + parseFloat($('[name="markup"]').val());


        $('[name="sellingprice"]').val(numeral(totalsellingprice).format('0,0'));


    }

});

$('#savecosting').on('click',function(e){
    e.preventDefault();

    var costingform = $('#costingform').serialize();
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: costingform,
        url: APP_URL + '/costingupdate',
        success:function(data){
            $('#saveotherproduct').prop('disabled', false);
            $('#costingmodal').modal('hide');
        }

    });


});


$("#findrawvendortable").on('click','.tablemodal tr',function(){
    $(this).addClass("selected").siblings().removeClass("selected");
});