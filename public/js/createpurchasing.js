
var vendor = $('#tablevendorlist').DataTable({
    processing: true,
    serverSide: true,
    ajax:APP_URL +  '/povendor',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'},
        {data: '_type', name: '_type'}
    ],

    columnDefs:[
        { targets: [2], visible: false }

    ]
});


$('#tablevendorlist').on('dblclick','tr',function(){
      var vendorid = $(this).closest('tr').attr('id');
      var ventype = $(this).closest('tr').attr('_type');

      //alert(vendor.rows(this).data()[0]._type);

      $('[name="_vendor"]').val($(this).closest('tr').find('td:eq(1)').text());
      $('[name="vendoridhidden"]').val(vendorid);
      $('[name="tmode"]').val(vendor.rows(this).data()[0]._type);
      $('#vendorlistmodal').modal('hide');
      getVendorPaymentTerm(vendorid);
});


$('[name="deliverydate"]').datetimepicker({
    format: 'YYYY-MM-DD'
});


$('[name="_vendor"]').on('change',function (e) {
    var vendorid = $(this).val();
    getVendorPaymentTerm(vendorid);
});

$('[name="btnvendor"]').on('click',function (e) {
    $('#vendorlistmodal').modal('show');

});



function getVendorPaymentTerm(id){
    $.ajax({
        type:'get',
        dataType:'json',
        data:{vendorid:id},
        url: APP_URL + '/paymenttermsget',
        success:function (data) {
            $('[name="_paymentterm"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;
            $.each(term, function (index, item) {
              if(  data.vendor ){
                  if(  data.vendor._paymentterm == term[index].id ){
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description,
                          selected:'selected'
                      }));
                  }else{
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description

                      }));
                  }
              }else{
                  $('[name="_paymentterm"]')
                      .find('option')
                      .remove()
                      .end()
                      .append('<option></option>')
                      .val('');
              }


            });

        }

    });
}







function getpacking(id){

    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getselectedrm',
        success:function(data){
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description
                }));
            });
        }
    });


}
