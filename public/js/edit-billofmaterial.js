

$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});

$(function(){
    $('[name="packing"]')
        .find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('');

    $.each(packing, function (index, item) {
     if(points.bom.packing == item.id){
         $('[name="packing"]').append($('<option>', {
             value: item.id,
             text: item.description,
             selected:'selected'
         }));
     }else{
         $('[name="packing"]').append($('<option>', {
             value: item.id,
             text: item.description

         }));
     }

    });

});



$('#vc').on('click',function(e){
    e.preventDefault();

    var verify = $(this).button('loading');
    var vendorcode = $('[name="rmcode"]').val().trim();

    $.ajax({
       type:'get',
       dataType:'json',
       data:{rmcode:vendorcode},
       url:APP_URL + '/verifycoderawmat',
       success:function (data) {
           $('[name="rmname"]').val('');
           $('#addvendoritem .form-group').removeClass('has-error help-block error');
           $('#addvendoritem div').find('span').remove();
           if (data.item) {
               $('[name="rmname"]').val(data.item.name);
               $('[name="_hiddenrmid"]').val(data.item.id);
           }else{
               var n = noty({
                   text: 'No record found !',
                   type : 'information',
                   layout:'topCenter',
                   theme: 'relax',
                   animation: {
                       open: {height: 'toggle'},
                       close: {height: 'toggle'},
                       easing: 'swing',
                       speed: 500
                   },
                   timeout: 2000,
               });
           }

           verify.button('reset');
       },error:function(data){
            verify.button('reset');
            var errors = data.responseJSON;
            $('[name="rmname"]').val('');
            $('[name="_hiddenrmid"]').val('');
            $.each( errors, function( key, value ) {
               
                $('.'+key).html(
                '<span class="help-block error">'+
                    value+
                '</span>'
                )
                .closest('.form-group').addClass('has-error')
            });
        }
        
    });

});


$('#fv').on('click', function (e) {
    e.preventDefault();
   
    $.ajax({
        type: 'GET',
        dataType: 'json',
        data:{item:points.bom._item},
        url: APP_URL + '/findrawmaterial',
        success: function (data) {
            $('#findRawmMaterialModal').modal('show');
            $('#findrawvendortable').find("tr:gt(0)").remove();
            $.each(data.vendor, function (key, value) {
                $('#findrawvendortable tbody').append('<tr>' +
                    '<td id="' + value.id + '">' + value.code + '</td>' +
                    '<td>' + value.name + '</td>' +
                    '</tr>');
            });
        }
    });
});


$('#searchvendor').on('click', function (e) {
    e.preventDefault();
    var formsearch = $('#formvendorsearch').serialize();
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: formsearch,
        url: APP_URL + '/rawmaterial-search',
        success: function (data) {
            $('#findrawvendortable').find("tr:gt(0)").remove();

            if(data.vendor.length == 0){
                $('#findrawvendortable tbody').append('<tr>' +
                    '<td colspan="2"><p>No Record Found!</p></td>' +

                    '</tr>');
            }
            $.each(data.vendor, function (key, value) {
                $('#findrawvendortable tbody').append('<tr>' +
                    '<td id="' + value.id + '">' + value.code + '</td>' +
                    '<td>' + value.name + '</td>' +
                    '</tr>');
            });
        }
    });

});


$('#findrawvendortable').on('dblclick','tr',function(e){
    e.preventDefault();
    var id = $(this).find('td').attr('id');
    console.log(id);
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: {searchid:id},
        url: APP_URL + '/rawmaterialsearchid',
        success: function (data) {
            $('#findRawmMaterialModal').modal('hide');
            $('[name="rmname"]').val(data.vendor.name);
            $('[name="_hiddenrmid"]').val(data.vendor.id);
            $('[name="rmcode"]').val(data.vendor.code);
            getpacking(data.vendor.id);
        },error: function (data) {

            var errors = data.responseJSON;
            alert(errors);

        }


    });

});


var searvendorid;

$('#findrawvendortable').on('click','tr',function(e){
    e.preventDefault();
    searvendorid = $(this).find('td').attr('id');

});


$('#findselectvendor').on('click',function(e){
    e.preventDefault();
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: {searchid:searvendorid},
        url: APP_URL + '/rawmaterialsearchid',
        success: function (data) {
            $('#findRawmMaterialModal').modal('hide');
            $('[name="rmname"]').val(data.vendor.name);
            $('[name="_hiddenrmid"]').val(data.vendor.id);
            $('[name="rmcode"]').val(data.vendor.code);
            getpacking(data.vendor.id);
        },error: function(XMLHttpRequest, textStatus, errorThrown){
            //alert('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
        }

    });
});



function getpacking(id){

    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getselectedrm',
        success:function(data){
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description
                }));
            });
        }
    });

}



$("#findrawvendortable").on('click','.tablemodal tr',function(){
    $(this).addClass("selected").siblings().removeClass("selected");
});