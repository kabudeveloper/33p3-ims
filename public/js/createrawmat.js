



$.ajax({
   type:'GET',
   dataType:'json',
   url:'getcategory',
   success:function(data){
       var category = data.categories;
       $.each(category, function (index, item) {
           $('[name="_category"]').append($('<option>', {
               value: item.id,
               text: item.name,
               selected:'selected'
           }));
       });

      
       getClss(category[0].id)
   } 
    
});


$('[name="_category"]').on('change',function(e){
   var catid = $(this).val();

    $('[name="_class"]')
        .find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('');
    
    
    $.ajax({
        type:'GET',
        dataType:'json',
        data:{catid:catid},
        url:'getclss',
        success:function(data){
            var classess = data.classess;
            $.each(classess, function (index, item) {
                $('[name="_class"]').append($('<option>', {
                    value: item.id,
                    text: item.name



                }));
            });
        }

    });
    
});


function getClss(catid){

    $.ajax({
        type:'GET',
        dataType:'json',
        data:{catid:catid},
        url:'getclss',
        success:function(data){
            var classess = data.classess;
            $.each(classess, function (index, item) {
                $('[name="_class"]').append($('<option>', {
                    value: item.id,
                    text: item.name



                }));
            });
        }

    });
}




$('[name="_class"]').on('change',function(e){
    var classid = $(this).val();

    $('[name="_subclass"]')
        .find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('');


    $.ajax({
        type:'GET',
        dataType:'json',
        data:{classid:classid},
        url:'getsubclass',
        success:function(data){
            var subclassess = data.subclassess;
            $.each(subclassess, function (index, item) {
                $('[name="_subclass"]').append($('<option>', {
                    value: item.id,
                    text: item.name

                }));
            });
        }

    });

});





/*
var resetOption = function (id) {

    $('[name=""]')
        .find('option')
        .remove()
        .end()
        .append('<option></option>')
        .val('')

};

*/
