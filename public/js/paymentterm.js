

$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});



var term = $('#paymentterm-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/paymentterm',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'description', name: 'description'},
        {data: 'factor', name: 'factor'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#paymentterm-table').on('click','.delpaymentterm',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'paymentterm/' + compid,
                   success:function(data){
                      
                       term.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   },error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }


                });

            }

        }
    })
    
});


