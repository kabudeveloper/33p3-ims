var data = {
	labels: ['2010', '2011', '2012'],
	datasets: [
		{
			label: 'CTU',
			fill: true,
			data: [32, 25,44],
			backgroundColor: [
                '#4472c4','#ffc000', '#23e148'
            ],
            borderColor: [
            ],
            borderWidth: 2

		}
	]
}

var options = {
	responsive: true,
	title: {
				display: true,
				text: 'Scorecard Completion'
			},
	tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			}
}

var context = document.querySelector("#myChart").getContext("2d");

 new Chart(context , {
	type: "pie",
	data: data, 
	options: options,
	 	title: {
		display: true,
		text: 'Scorecards'
	}
});

 //dfdfd

 var datas = {
	labels: ['Cebu City', 'Mandaue City', 'Carcar City', 'Cebu Province'],
	datasets: [
		{
			label: 'Progress',
			fill: false,
			data: [50, 40, 50, 25,0],
			backgroundColor: [
                '#4472c4',
                '#4472c4',
                '#4472c4',
                '#4472c4'
            ],
            borderColor: [
            ],
            borderWidth: 1

		},
		{
			label: 'Not started',
			fill: false,
			data: [35, 25, 70, 35,0],
			backgroundColor: [
                '#ffc000',
                '#ffc000',
                '#ffc000',
                '#ffc000'
            ],
            borderColor: [
            ],
            borderWidth: 1

		},
		{
			label: 'Completed',
			fill: false,
			data: [70, 90, 60, 85,0],
			backgroundColor: [
                '#23e148',
                '#23e148',
                '#23e148',
                '#23e148'
            ],
            borderColor: [
            ],
            borderWidth: 1

		},

	]
}

var optionss = {
	responsive: true,
	title: {
				display: true,
				text: 'School Scorecard'
			},
	tooltips: {
				mode: 'index',
				intersect: false,
			},
	 hover: {
				mode: 'nearest',
				intersect: true
			},


}

var contexts = document.querySelector("#myBarChart").getContext("2d");

 new Chart(contexts , {
	type: "bar",
	data: datas, 
	options: optionss,
	 	title: {
		display: true,
		text: 'Scorecards'
	}
});

 	$('.numbers').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 1000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});