
$(function(){
    if(points.manual == 1 ){
        $('[name="deliverydate"]').datetimepicker({
            format: 'YYYY-MM-DD',
        });
    }else{
        $('[name="deliverydate"]').datetimepicker({
            format: 'YYYY-MM-DD'
        });
    }

});



$('[name="_customer"]').on('change',function (e) {
    var custid = $(this).val();
    getCustomerPaymentTerm(custid);
});


var customer = $('#tablecustomerlist').DataTable({
    processing: true,
    serverSide: true,
    ajax:APP_URL +  '/jocust',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'}

    ]
});


var jodetail = $('#jo-detail-table').DataTable({
    processing: true,
    serverSide: true,
    ajax:'jodetaillist',
    columns: [
        {data: 'name', name: 'item.name'},
        {data: 'description', name: 'itempacking.description'},
        {data: 'quantity', name: 'jodetail.quantity'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ],
    "columnDefs": [
        { "width": "15%", "targets": 3 }

    ]

});

$('#jo-detail-table').on('click','.deljod',function(e){
    e.preventDefault();

    var URL = $(this).attr('href');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                    type:'DELETE',
                    dataType:'json',
                    url:URL,
                    success:function(data){

                        jodetail.ajax.reload();
                        if(data.ok == 'success'){
                            bootbox.alert({
                                size: 'small',
                                message: "Delete Successfully",
                                callback: function(){ /* your callback code */ }
                            })
                        }
                    }

                });

            }

        }
    })

});



$('[name="btncustomer"]').on('click',function (e) {
    $('#customerlistmodal').modal('show');

});

$('[name="cancel"]').on('click',function (e) {
    bootbox.confirm({
        size: 'small',
        message: "Do you want to cancel ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                    type:'get',
                    dataType:'json',
                    data:{tnumber:points.jo.tnumber},
                    url: APP_URL + '/canceljoborder',
                    success:function(data){
                        if(data.ok == 'success'){
                            bootbox.alert({
                                size: 'small',
                                message: "Cancelled Successfully",
                                callback: function(){ location.reload(); }
                            })
                        }
                    }
                });

            }

        }
    });


});


$('#tablecustomerlist').on('dblclick','tr',function(){
    var custid = $(this).closest('tr').attr('id');

    $('[name="_customer"]').val($(this).closest('tr').find('td:eq(1)').text());
    $('[name="custidhidden"]').val(custid);
    $('#customerlistmodal').modal('hide');
    getCustomerPaymentTerm(custid);
});



$(function () {
    if(points.manual == 1 ){
        $('[name="custidhidden"]').val(points.custid);
        getCustomerPaymentTerm(points.jo._customer);
    }else{
        $('[name="custidhidden"]').val(points.jo._customer);
        getCustomerPaymentTerm(points.jo._customer);
    }
    
});


function getCustomerPaymentTerm(id){

    $.ajax({
        type:'get',
        dataType:'json',
        data:{customerid:id},
        url: APP_URL + '/getpaymentterms',
        success:function (data) {
            $('[name="_paymentterm"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;
            $.each(term, function (index, item) {
              if(  data.customer ){

                  if(  data.customer._paymentterm == term[index].id ){
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description,
                          selected:'selected'
                      }));
                  }else{
                      $('[name="_paymentterm"]').append($('<option>', {
                          value: term[index].id,
                          text: term[index].description

                      }));
                  }
              }else{
                  $('[name="_paymentterm"]')
                      .find('option')
                      .remove()
                      .end()
                      .append('<option></option>')
                      .val('');
              }


            });

        }

    });
}





function getpacking(id){

    $.ajax({
        type:'get',
        dataType: 'json',
        data: {id:id},
        url: APP_URL + '/getselectedrm',
        success:function(data){
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description
                }));
            });
        }
    });
}

