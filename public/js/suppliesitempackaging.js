

var rawmatitempackaging = $('#supplies-item-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'suppliesitempack',
    bFilter: false,
    columns: [
        {data: 'description', name: 'itempacking.description'},
        {data: 'code', name: 'packaging.code'},
        {data: 'umcode', name: 'um.code'},
        {data: 'umvalue', name: 'itempacking.umvalue'},
        {data: 'innerquantity', name: 'itempacking.innerquantity'},
        {data: '_level', name: 'itempacking._level'},
        {data: 'action', name: 'action', orderable: false, searchable: false}

    ],
    "columnDefs": [
      
        { "width": "15%", "targets": 4 }
    ]
});




$('#supplies-item-table').on('click','.delsuppliestitempacking',function(e){
    e.preventDefault();

    var URL = $(this).attr('href');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                    type:'DELETE',
                    dataType:'json',
                    url:URL,
                    success:function(data){

                        rawmatitempackaging.ajax.reload();
                        if(data.ok == 'success'){
                            bootbox.alert({
                                size: 'small',
                                message: "Deleted Successfully",
                                callback: function(){ /* your callback code */ }
                            })
                        }
                    }

                });

            }

        }
    })
    
});


