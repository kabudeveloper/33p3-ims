function showAlert(msgtitle,msg,msgtype){
    var icon="";
    if(msgtype == "warning"){
        icon = "exclamation-circle";
    }else if(msgtype == "danger"){
        icon = "exclamation-triangle";
    }else if(msgtype == "success"){
        icon = "check-circle";
    }else if(msgtype == "info"){
        icon = "info-circle";
    }else{
        icon = "info-circle";
    }
    
   
    $("#msgarea").append('<div id= "alertbox" class="alert alert-'+ msgtype +' fade in alert-lg"><a href="#" class="close" data-dismiss="alert" arial-label="close">&times</a><span><i class="fa fa-'+ icon +' fa-lg fa-fw" aria-hidden="true"></i><strong> '+ msgtitle +' </strong> '+msg+' </span>');         
    
}