$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});


var unitofmeasureconvertion = $('#unitofmeasureconvertion-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/uomconvertion',
    columns: [

        {data: 'buying', name: 'u1.name'},
        {data: 'prod', name: 'u2.name'},
        {data: 'convertedvalues', name: 'convertedvalues'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});




$('#unitofmeasureconvertion-table').on('click','.delunitofmeasureconvertion',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'unitofmeasureconvertion/' + compid,
                   success:function(data){

                       unitofmeasureconvertion.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Delete Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   } 

                });

            }

        }
    })
    
});


