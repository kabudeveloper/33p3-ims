

var otherfinishedproduct = $('#finishedproduct-otherproduct-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'otherproducts',
    bFilter: false,
    columns: [
        {data: 'name', name: 'i.name'},
        {data: 'description', name: 'ip.description'},
        {data: 'quantity', name: 'ic.quantity'},
        {data: 'unitprice', name: 'unitprice',searchable: false},
        {data: 'totalprice', name: 'totalprice',searchable: false},
        {data: 'action', name: 'action', orderable: false, searchable: false}

    ],
    "footerCallback": function ( row, data, start, end, display ) {
        var api = this.api();
        var intVal = function ( i ) {
            return typeof i === 'string' ?
            i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                    i : 0;
        };
        if (api.column(4).data().length){

            var total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {

                    return intVal(a) + intVal(b);
                } ) }
        else{ total = 0};
        if (api.column(4).data().length){
            var pageTotal = api
                .column( 4 , { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                } ) }
        else{ pageTotal = 0};
        $( api.column( 4 ).footer() ).html(
            ''+ numeral(pageTotal).format('0,0.00') +' ( '+ numeral(total).format('0,0.00') +' grand total)'

        );

    }
   


});





$('#finishedproduct-otherproduct-table').on('click','.delfinishedotherproduct',function(e){
    e.preventDefault();

   var URL = $(this).attr('href');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:URL,
                   success:function(data){

                       otherfinishedproduct.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /!* your callback code *!/ }
                           })
                       }
                   } 

                });

            }

        }
    })
    
});



