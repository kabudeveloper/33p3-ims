
$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});


var loc = $('#location-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/locat',
    columns: [
        {data: 'code', name: 'loc.code'},
        {data: 'name', name: 'loc.name'},
        {data: 'branch', name: 'br.name'},
        {data: 'description', name: 'loctype.description'},
        {data: 'glcode', name: 'loc.glcode'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ]
});








$('#location-table').on('click','.dellocation',function(e){
    e.preventDefault();

   var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                   type:'DELETE',
                   dataType:'json',
                   url:'location/' + compid,
                   success:function(data){
                      
                       loc.ajax.reload();
                       if(data.ok == 'success'){
                           bootbox.alert({
                               size: 'small',
                               message: "Deleted Successfully",
                               callback: function(){ /* your callback code */ }
                           })
                       }
                   },error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }


                });

            }

        }
    })
    
});


