


var compbranch = $('#usergroup-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'dashboard/usergroup',
    columns: [
        {data: 'description', name: 'description'},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ],
    "columnDefs": [
        { "width": "15%", "targets": 0 },
        { "width": "15%", "targets": 1 }
    ]
});


$('#usergroup-table').on('click','.delusergroup',function(e){
    e.preventDefault();

    var compid = $(this).attr('id');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function( ok ){
            if( ok ){

                $.ajax({
                    type:'DELETE',
                    dataType:'json',
                    url:'usergroup/' + compid,
                    success:function(data){

                        compbranch.ajax.reload();
                        if(data.ok == 'success'){
                            bootbox.alert({
                                size: 'small',
                                message: "Delete Successfully",
                                callback: function(){ /* your callback code */ }
                            })
                        }
                    }

                });

            }

        }
    })
    
});
