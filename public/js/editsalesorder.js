jQuery.fn.dataTable.Api.register('sum()', function () {
    return this.flatten().reduce(function (a, b) {
        if (typeof a === 'string') {
            a = a.replace(/[^\d.-]/g, '') * 1;
        }
        if (typeof b === 'string') {
            b = b.replace(/[^\d.-]/g, '') * 1;
        }

        return a + b;
    }, 0);
});


$('[name="deliverydate"]').datetimepicker({
    format: 'YYYY-MM-DD'
});


$('[name="_customer"]').on('change', function (e) {
    var custid = $(this).val();
    getCustomerPaymentTerm(custid);
});

$('[name="pricevalidity"]').datetimepicker({
    format: 'YYYY-MM-DD HH:mm'
});


var customer = $('#tablecustomerlist').DataTable({
    processing: true,
    serverSide: true,
    ajax: APP_URL + '/jocust',
    columns: [
        {data: 'code', name: 'code'},
        {data: 'name', name: 'name'}

    ]
});


var sodetail = $('#salesorder-detail-table').DataTable({
    processing: true,
    serverSide: true,
    ajax: 'sodetaillist',
    columns: [
        {data: 'name', name: 'item.name', orderable: false, searchable: false},
        {data: 'description', name: 'itempacking.description'},
        {data: 'quantity', name: 'sodetail.quantity'},
        {data: 'costprice', name: 'costpriceview.costprice'},
        {data: 'USD', name:'USD'},
        {data: 'markup', name: 'costpriceview.markup'},
        {data: 'unitprice', name: 'unitprice', orderable: false, searchable: false},
        {data: 'aup', name: 'sodetail.aup'},
        {data: 'totalprice', name: 'totalprice', orderable: false, searchable: false},
        {data: 'auptotal', name: 'auptotal', orderable: false, searchable: false},
        {data: 'action', name: 'action', orderable: false, searchable: false}
    ],
    "columnDefs": [
        {"width": "20%", "targets": 9}

    ],

    "drawCallback": function () {
        var api = this.api();
        $(".to").html('')
            .append(numeral(api.column(7, {page: 'current'}).data().sum()).format('0,0.00'));


    },

    "footerCallback": function (row, data, start, end, display) {
        var api = this.api();
        var intVal = function (i) {
            return typeof i === 'string' ?
            i.replace(/[\$,]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };
        if (api.column(8).data().length) {

            var total = api
                .column(8)
                .data()
                .reduce(function (a, b) {

                    return intVal(a) + intVal(b);
                })
        }
        else {
            total = 0
        }
        if (api.column(8).data().length) {
            var pageTotal = api
                .column(8, {page: 'current'})
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                })
        }
        else {
            pageTotal = 0
        }
        $(api.column(8).footer()).html(
            numeral(pageTotal).format('0,0.00')
        );

    }


});

sodetail.column(7).data().sum();

$('#salesorder-detail-table').on('click', '.sod', function (e) {
    e.preventDefault();

    var URL = $(this).attr('href');

    bootbox.confirm({
        size: 'small',
        message: "Are you sure you want to delete this record ?",
        callback: function (ok) {
            if (ok) {

                $.ajax({
                    type: 'DELETE',
                    dataType: 'json',
                    url: URL,
                    success: function (data) {

                        sodetail.ajax.reload();
                        if (data.ok == 'success') {
                            bootbox.alert({
                                size: 'small',
                                message: "Delete Successfully",
                                callback: function () { /* your callback code */
                                }
                            })
                        }
                    } ,error:function (x, a, t) {
                        var m = JSON.parse(x.responseText);
                        bootbox.alert({
                            size: 'small',
                            message: '<h5 class="help-block">'+ m.error +'</h5>',
                            callback: function(){ /* your callback code */ }
                        })
                    }

                });

            }

        }
    })

});

$('#salesorder-detail-table').on('click', '.aupedit', function (e) {
    e.preventDefault();

    var _item = $(this).attr('href');

    var aup = $(this).attr('id');
    $('#aupmodal').modal('show');
    $('#aup').val(parseFloat(aup).toFixed(2));
    $('#itemhidden').val(_item);


});


$('[name="btnupdateaup"]').on('click', function (e) {
    e.preventDefault();
    var _item = $('#itemhidden').val();
    var aup = $('#aup').val();
    var tnumber = points.so.tnumber;
    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {item: _item, aup: aup, tnumber: tnumber},
        url: APP_URL + '/updateaup',
        success: function (data) {
            if (data.ok == 'success') {
                sodetail.ajax.reload();
                bootbox.alert({
                    size: 'small',
                    message: "Update Successfully",
                    callback: function () {
                        $('#aupmodal').modal('hide');
                    }
                })
            }
        } ,error:function (x, a, t) {
            var m = JSON.parse(x.responseText);
            bootbox.alert({
                size: 'small',
                message: '<h5 class="help-block">'+ m.error +'</h5>',
                callback: function(){ /* your callback code */ }
            })
        }

    });


});


$('[name="btncustomer"]').on('click', function (e) {
    $('#customerlistmodal').modal('show');
});

$('[name="cancel"]').on('click', function (e) {
    bootbox.confirm({
        size: 'small',
        message: "Do you want to cancel ?",
        callback: function (ok) {
            if (ok) {

                $.ajax({
                    type: 'get',
                    dataType: 'json',
                    data: {tnumber: points.jo.tnumber},
                    url: APP_URL + '/canceljoborder',
                    success: function (data) {
                        if (data.ok == 'success') {
                            bootbox.alert({
                                size: 'small',
                                message: "Cancelled Successfully",
                                callback: function () {
                                    location.reload();
                                }
                            })
                        }
                    }
                });

            }

        }
    });


});


$('#tablecustomerlist').on('dblclick', 'tr', function () {
    var custid = $(this).closest('tr').attr('id');
    $('[name="_customer"]').val($(this).closest('tr').find('td:eq(1)').text());
    resetForm();
    $('[name="custidhidden"]').val(custid);
    $('#customerlistmodal').modal('hide');
    $('[name="_currency"]')
        .find('option')
        .remove()
        .end()
        .append('<option value="">Loading...</option>');
    getCustomerPaymentTerm(custid);
});


$(function () {

    getPaymentTermTextField(points.so._customer);
});


function getCustomerPaymentTerm(id) {

    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {customerid: id},
        url: APP_URL + '/getpaymentterms',
        success: function (data) {
            $('[name="_paymentterm"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var term = data.terms;
            $.each(term, function (index, item) {
                if (data.customer) {

                    if (data.customer._paymentterm == term[index].id) {
                        $('[name="_paymentterm"]').append($('<option>', {
                            value: term[index].id,
                            text: term[index].description,
                            selected: 'selected'
                        }));
                    } else {
                        $('[name="_paymentterm"]').append($('<option>', {
                            value: term[index].id,
                            text: term[index].description

                        }));
                    }
                } else {
                    $('[name="_paymentterm"]')
                        .find('option')
                        .remove()
                        .end()
                        .append('<option></option>')
                        .val('');
                }


            });
            getCurrencyByCustomer(id);

        }

    });
}


function getpacking(id) {

    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {id: id},
        url: APP_URL + '/getselectedrm',
        success: function (data) {
            $('[name="packing"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var itempack = data.itempacking;
            $.each(itempack, function (index, item) {

                $('[name="packing"]').append($('<option>', {
                    value: itempack[index].id,
                    text: itempack[index].description
                }));
            });
        }
    });
}


function getPaymentTermTextField(id) {
    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {customerid: id},
        url: APP_URL + '/getpaymenttermstextfield',
        success: function (data) {
            var payment = data.customerpayment;
            $('[name="_currency"]')
                .find('option')
                .remove()
                .end()
                .append('<option value="">Loading...</option>');
            $('[name="_paymentterm"]').val(payment.id);
            $('[name="paymentterm"]').val(payment.description);
            getCurrencyByCustomer(points.so._currency);

        }
    });
}


function getCurrencyByCustomer(id) {
    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {id: id},
        url: APP_URL + '/getcurrencycustomer',
        success: function (data) {

            getCurr(data.custcurrency._currency);
        }
    });
}

function getCurr(custcurrency) {
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: APP_URL + '/getcurrencies',
        success: function (data) {
            $('[name="_currency"]')
                .find('option')
                .remove()
                .end()
                .append('<option></option>')
                .val('');
            var currency = data.currencies;

            $.each(currency, function (index, item) {
                if (currency[index].id == custcurrency) {
                    $('[name="_currency"]').append($('<option>', {
                        value: currency[index].id,
                        text: currency[index].name,
                        selected: 'selected'
                    }));
                } else {
                    $('[name="_currency"]').append($('<option>', {
                        value: currency[index].id,
                        text: currency[index].name
                    }));
                }

            });

            getCurrencyRate(custcurrency);
        }
    });
}

function getCurrencyRate(currency){
    $.ajax({
        type:'get',
        dataType: 'json',
        data: {_currency:currency},
        url: APP_URL + '/getcurrenciesrate',
        success:function(data) {
            $('[name="currencyrate"]').val(data.curr.rate);
        }

    });
}

function resetForm(){
    $('[name="currencyrate"]').val('');
}
