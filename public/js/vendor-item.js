$('input').iCheck({
    checkboxClass: 'icheckbox_square-green',
    radioClass: 'iradio_square-green',
    increaseArea: '20%' // optional
});


$('#vc').on('click', function (e) {
    e.preventDefault();
    var verify = $(this).button('loading');
    var vendorcode = $('[name="_vendor"]').val().trim();

    $.ajax({
        type: 'get',
        dataType: 'json',
        data: {_vendor: vendorcode},
        url: APP_URL + '/verifycode',
        success: function (data) {
            $('[name="vendorname"]').val('');
            $('#addvendoritem .form-group').removeClass('has-error help-block error');
            $('#addvendoritem div').find('span').remove();
            if (data.vendor) {
                $('[name="vendorname"]').val(data.vendor.name);
                $('[name="_hiddenvendorid"]').val(data.vendor.id);
            } else {
                var n = noty({
                    text: 'No record found !',
                    type : 'information',
                    layout:'topCenter',
                    theme: 'relax',
                    animation: {
                        open: {height: 'toggle'},
                        close: {height: 'toggle'},
                        easing: 'swing',
                        speed: 500
                    },
                    timeout: 2000
                });
            }

            verify.button('reset');
        }, error: function (data) {
            verify.button('reset');
            var errors = data.responseJSON;
            $('[name="vendorname"]').val('');
            $('[name="_hiddenvendorid"]').val('');
            $.each(errors, function (key, value) {

                $('.' + key).html(
                    '<span class="help-block error">' +
                    value +
                    '</span>'
                )
                    .closest('.form-group').addClass('has-error')
            });
        }

    });

});


$('#fv').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: APP_URL + '/vendoritem-all-vendor',
        success: function (data) {
            $('#findVendorModal').modal('show');
            $('#findrawvendortable').find("tr:gt(0)").remove();
            $.each(data.vendor, function (key, value) {

                $('#findrawvendortable tbody').append('<tr>' +
                    '<td id="' + value.id + '">' + value.code + '</td>' +
                    '<td>' + value.name + '</td>' +
                    '</tr>');

            });
        }
    });
});


$('#searchvendor').on('click', function (e) {
    e.preventDefault();
    var formsearch = $('#formvendorsearch').serialize();
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: formsearch,
        url: APP_URL + '/vendoritem-search',
        success: function (data) {
            $('#findrawvendortable').find("tr:gt(0)").remove();

            if(data.vendor.length == 0){
                $('#findrawvendortable tbody').append('<tr>' +
                    '<td colspan="2"><p>No Record Found!</p></td>' +

                    '</tr>');
            }
            $.each(data.vendor, function (key, value) {
                $('#findrawvendortable tbody').append('<tr>' +
                    '<td id="' + value.id + '">' + value.code + '</td>' +
                    '<td>' + value.name + '</td>' +
                    '</tr>');
            });
        }
    });

});


$('#findrawvendortable').on('dblclick','tr',function(e){
    e.preventDefault();
     var id = $(this).find('td').attr('id');
     console.log(id);
      $.ajax({
          type: 'post',
          dataType: 'json',
          data: {searchid:id},
          url: APP_URL + '/vendorsearchid',
          success: function (data) {
              $('#findVendorModal').modal('hide');
              $('[name="vendorname"]').val(data.vendor.name);
              $('[name="_hiddenvendorid"]').val(data.vendor.id);
              $('[name="_vendor"]').val(data.vendor.code);
          },error: function (data) {

              var errors = data.responseJSON;
              alert(errors);

          }


      });

});


var searvendorid;

$('#findrawvendortable').on('click','tr',function(e){
    e.preventDefault();
    searvendorid = $(this).find('td').attr('id');

});


$('#findselectvendor').on('click',function(e){
    e.preventDefault();
    $.ajax({
        type: 'post',
        dataType: 'json',
        data: {searchid:searvendorid},
        url: APP_URL + '/vendorsearchid',
        success: function (data) {
            $('#findVendorModal').modal('hide');
            $('[name="vendorname"]').val(data.vendor.name);
            $('[name="_hiddenvendorid"]').val(data.vendor.id);
            $('[name="_vendor"]').val(data.vendor.code);
        },error: function(XMLHttpRequest, textStatus, errorThrown){
            //alert('status:' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText);
        }

    });
});