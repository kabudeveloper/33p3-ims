@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of User</h2>
            {!! Breadcrumbs::render('company') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add User</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form class="form-horizontal" method="post" action="{{ route('securityuser.store') }}">
                                      {{ csrf_field() }}
                                      <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Firstname</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="firstname"  value="{{ old('firstname') }}" class="form-control">
                                              @if ($errors->has('firstname'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('firstname') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                      <div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Last Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="lastname"  value="{{ old('lastname') }}" class="form-control">
                                            @if ($errors->has('lastname'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('lastname') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>


                                      <div class="form-group {{ $errors->has('middlename') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Middle Name</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="middlename" value="{{ old('middlename') }}" class="form-control">
                                              @if ($errors->has('middlename'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('middlename') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Description</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="description"  value="{{ old('description') }}" class="form-control" placeholder="">
                                              @if ($errors->has('description'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('description') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_group') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Select Group</label>

                                          <div class="col-sm-8">
                                              <select name="_group" class="form-control">
                                                  <option></option>
                                                  @foreach($group as $main)
                                                      <option value="{{ $main->id }}">{{ $main->description }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_group'))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first('_group') }}</strong>
                                                  </span>
                                              @endif
                                           </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('locationassignment') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Location Assignment</label>

                                          <div class="col-sm-8">
                                              <select name="locationassignment" class="form-control">
                                                  <option></option>
                                                  @foreach($locationtypes as $locationtype)
                                                      <option value="{{ $locationtype->id }}">{{ $locationtype->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('locationassignment'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_type') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_position') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Position</label>

                                          <div class="col-sm-8">
                                              <select name="_position" class="form-control">
                                                  <option></option>
                                                  @foreach($positions as $position)
                                                      <option value="{{ $position->id }}">{{ $position->description }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_position'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_position') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                          <input type="checkbox"  name="isactive">  Active
                                                  </label>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Login Name</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="username"  value="{{ old('username') }}" class="form-control" placeholder="">
                                              @if ($errors->has('username'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('username') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-sm-6 col-sm-offset-4">
                                                  <div>
                                                      <label>
                                                          <input type="checkbox"  name="resetpassword"> Reset password next login
                                                      </label>
                                                  </div>
                                              </div>


                                          </div>

                                      </div>

                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      <input type="checkbox"  name="passwordwillexpire"> Password will expire on
                                                  </label>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      <input type="checkbox"  name="accountwillexpire"> Account will expire on
                                                  </label>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      <input type="checkbox"  name="locked"> Account is locked on
                                                  </label>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      <input type="checkbox"  name=""> Account is in use
                                                  </label>
                                              </div>
                                          </div>
                                      </div>




                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('securityuser') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection



@section('customjs')

    <script src="{{ URL::asset('js/branchcompany.js') }}"></script>

@endsection