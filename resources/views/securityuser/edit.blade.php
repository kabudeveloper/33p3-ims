@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Companies</h2>
            {!! Breadcrumbs::render('company') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit User</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form  class="form-horizontal" method="POST" id="formsecurityuser" action="{{ route('securityuser.update',$user->id) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">

                                      <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Firstname</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="firstname"  value="{{ $user->firstname }}" class="form-control">
                                              @if ($errors->has('firstname'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('firstname') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                      <div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Last Name</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="lastname"  value="{{ $user->lastname  }}" class="form-control">
                                              @if ($errors->has('lastname'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('lastname') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group {{ $errors->has('middlename') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Middle Name</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="middlename" value="{{ $user->middlename  }}" class="form-control">
                                              @if ($errors->has('middlename'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('middlename') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Description</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="description"  value="{{$user->description  }}" class="form-control" placeholder="">
                                              @if ($errors->has('description'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('description') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_group') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Select Group</label>

                                          <div class="col-sm-8">
                                              <select name="_group" class="form-control">
                                                  <option></option>
                                                  @foreach($group as $main)
                                                      <option value="{{ $main->id }}" {{ ($main->id == $user->_group ? "selected":"") }}>{{ $main->description }}</option>

                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_group'))
                                                  <span class="help-block">
                                                      <strong>{{ $errors->first('_group') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('locationassignment') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Location Assignment</label>

                                          <div class="col-sm-8">
                                              <select name="locationassignment" class="form-control">
                                                  <option></option>
                                                  @foreach($locationtypes as $locationtype)
                                                      <option value="{{ $locationtype->id }}" {{ ($locationtype->id == $user->locationassignment ) ? 'selected' : '' }}>{{ $locationtype->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('locationassignment'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_type') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_position') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Position</label>

                                          <div class="col-sm-8">
                                              <select name="_position" class="form-control">
                                                  <option></option>
                                                  @foreach($positions as $position)
                                                      <option value="{{ $position->id }}" {{ ($position->id == $user->_position) ? 'selected=selected' : '' }}>{{ $position->description }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_position'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_position') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($user->isactive)
                                                          <input type="checkbox" checked name="isactive"> Active
                                                      @else
                                                          <input type="checkbox" name="isactive">  Active
                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Login Name</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="username"  value="{{ $user->username }}" class="form-control" placeholder="">
                                              @if ($errors->has('username'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('username') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="row">
                                              <div class="col-sm-6 col-sm-offset-4">
                                                  <div>
                                                      <label>
                                                          @if($user->resetpassword)
                                                              <input type="checkbox" checked name="resetpassword"> Reset password next login
                                                          @else
                                                              <input type="checkbox" name="resetpassword">  Reset password next login

                                                          @endif


                                                      </label>
                                                  </div>
                                              </div>

                                              <div class="col-sm-2">
                                                  <input type="button" class="btn btn-default btn-sm resetpassword" name="" value="Reset" >
                                              </div>
                                          </div>

                                      </div>

                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($user->passwordwillexpire)
                                                          <input type="checkbox" checked name="passwordwillexpire"> Password will expire on
                                                      @else
                                                          <input type="checkbox" name="passwordwillexpire"> Password will expire on

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($user->accountwillexpire)
                                                          <input type="checkbox" checked name="accountwillexpire"> Account will expire on
                                                      @else
                                                          <input type="checkbox" name="accountwillexpire"> Account will expire on

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($user->locked)
                                                          <input type="checkbox" checked name="locked"> Account is locked on
                                                      @else
                                                          <input type="checkbox" name="locked"> Account is locked on

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($user->locked)
                                                          <input type="checkbox" checked name="">  Account is in use
                                                      @else
                                                          <input type="checkbox" name="">  Account is in use

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>


                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <button type="submit" class="btn btn-white">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="modal fade" tabindex="-1" id="resetpasswordmodal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Reset Password</h4>
                    </div>
                    <div class="modal-body">

                        <form class="form-horizontal" id="userresetpassword">

                            <div class="form-group">
                                <label for="_item" class="col-sm-3 control-label">Username</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="username" value="{{ $user->username }}" readonly>
                                    <div class="username">

                                    </div>
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <label for="currentpassword" class="col-sm-3 control-label">Current Password</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control"  name="currentpassword" value=" ">
                                    <div class="currentpassword">

                                    </div>
                                </div>
                            </div> -->

                            <div class="form-group">
                                <label for="password" class="col-sm-3 control-label">New Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control"  name="password" >
                                    <div class="password">

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password_confirmation" class="col-sm-3 control-label">Confirm Password</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control"  name="password_confirmation" >
                                    <div class="password_confirmation">

                                    </div>
                                </div>
                            </div>



                        </form>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button"  class="btn btn-primary" id="resetpass">Reset</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </div>


@endsection



@section('customjs')

    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>


    <script src="{{ URL::asset('js/securityuser.js') }}"></script>

@endsection