@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2></h2>
            {!! Breadcrumbs::render('contact') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Item Packing</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-7">



                                  <form class="form-horizontal" method="post" id="addvendoritem"  action="{{ route('rawmaterial.itempacking.store',$item['id']) }}">
                                      {{ csrf_field() }}

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Item</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ $item->name }}" readonly class="form-control">

                                        </div>
                                    </div>


                                      <div class="form-group {{ $errors->has('_packaging') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Packaging</label>

                                          <div class="col-sm-8">
                                              <select name="_packaging" class="form-control">
                                                  <option></option>
                                                  @foreach($packaging as $pack)
                                                      <option value="{{ $pack->id }}" {{ (old("_packaging") == $pack->id ? "selected":"") }}>{{ $pack->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_packaging'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_packaging') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group {{ $errors->has('_um') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Unit Of Measure</label>

                                          <div class="col-sm-8">
                                              <select name="_um" class="form-control">
                                                  <option></option>
                                                  @foreach($um as $ums)
                                                      <option value="{{ $ums->id }}" {{ (old("_um") == $ums->id ? "selected":"") }}>{{ $ums->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_um'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_um') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                      <div class="form-group {{ $errors->has('umvalue') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">UM Value</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="umvalue" value="{{ old('umvalue') }}"  class="form-control">
                                              @if ($errors->has('umvalue'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('umvalue') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('innerquantity') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Inner Quantity</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="innerquantity" value="{{ old('innerquantity') }}"   class="form-control">
                                              @if ($errors->has('innerquantity'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('innerquantity') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Description</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="description" value="{{ old('description') }}"  class="form-control">
                                              @if ($errors->has('description'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('description') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_level') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Level</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="_level" value="{{ old('_level') }}"  class="form-control">
                                              @if ($errors->has('_level'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_level') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>





                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ URL::to('rawmaterial/'.$item['id']).'/edit' }}" class="btn  denson btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection
<script type="text/javascript">

    var APP_URL = {!! json_encode(url('/')) !!};
</script>


@section('customjs')

    <script src="{{ URL::asset('js/vendor-item.js') }}"></script>

@endsection