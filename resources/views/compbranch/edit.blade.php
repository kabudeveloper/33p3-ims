@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Companies</h2>
            {!! Breadcrumbs::render('branch') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Company</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form  class="form-horizontal" method="POST" action="{{ route('branch.update',$company->id) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">
                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code"  value="{{ $company->code }}" class="form-control">

                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ $company->name }}" class="form-control">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                      <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Address</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="address" value="{{ $company->address }}" class="form-control">
                                              @if ($errors->has('address'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('address') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('zipcode') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">ZipCode</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="zipcode"  value="{{ $company->zipcode }}" class="form-control" placeholder="">
                                              @if ($errors->has('zipcode'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('zipcode') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_company') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Select Company</label>

                                          <div class="col-sm-8">
                                              <select name="_company" class="form-control">
                                                  <option></option>
                                                  @foreach($maincompany as $main)

                                                      @if($main->id == $company->_company)
                                                          <option value="{{ $main->id }}" selected="selected">{{ $main->name }}</option>
                                                      @else
                                                        <option value="{{ $main->id }}">{{ $main->name }}</option>
                                                       @endif
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_company'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_company') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>





                                    <div class="form-group {{ $errors->has('tin') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Tin</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="tin" value="{{ $company->tin }}" class="form-control">
                                            @if ($errors->has('tin'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('tin') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>


                                      <div class="form-group {{ $errors->has('glcode') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">GL Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="glcode" value="{{ $company->glcode }}" class="form-control">
                                              @if ($errors->has('glcode'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('glcode') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($company->isactive)
                                                       <input type="checkbox" value="1" checked name="isactive">  Active
                                                      @else
                                                          <input type="checkbox" name="isactive">  Active

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>



                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('branch') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection



@section('customjs')

    <script src="{{ URL::asset('js/branchcompany.js') }}"></script>

@endsection