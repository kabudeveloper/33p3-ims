@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Branches</h2>
            {!! Breadcrumbs::render('branch') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Branch<small></small></h5>
                        <div class="ibox-tools">

                            {{--<a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                <i class="fa fa-cog"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ URL::to('branch/create') }}">Add Branch</a>
                                </li>

                            </ul>

--}}
                            <a class="btn btn-primary btn-rounded btn-sm" href="{{ URL::to('branch/create') }}">Add Branch</a>
                        </div>
                    </div>






                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="company-branch-table">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Zip code</th>
                                    <th>Company</th>
                                    <th>Tin</th>
                                    <th></th>


                                </tr>
                                </thead>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection


@section('customjs')

    <script src="{{ URL::asset('js/branchcompany.js') }}"></script>

@endsection