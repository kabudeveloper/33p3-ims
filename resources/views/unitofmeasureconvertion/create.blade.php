@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Unit Of Measure</h2>
            {!! Breadcrumbs::render('unitofmeasure') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Convertion</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form class="form-horizontal" method="post" action="{{ route('unitofmeasureconvertion.store') }}">
                                      {{ csrf_field() }}



                                      <div class="form-group {{ $errors->has('buying_uom') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Buying</label>

                                          <div class="col-sm-8">
                                              <select name="buying_uom" class="form-control">
                                                  <option></option>
                                                  @foreach($uoms as $uom)
                                                      <option value="{{ $uom->id }}">{{ $uom->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('buying_uom'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('buying_uom') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('production_uom') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Production</label>

                                          <div class="col-sm-8">
                                              <select name="production_uom" class="form-control">
                                                  <option></option>
                                                  @foreach($uoms as $uom)
                                                      <option value="{{ $uom->id }}">{{ $uom->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('production_uom'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('production_uom') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('convertedvalues') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Convertion</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="convertedvalues"  value="{{ old('convertedvalues') }}" class="form-control">
                                              @if ($errors->has('convertedvalues'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('convertedvalues') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('unitofmeasureconvertion') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection


@section('customjs')

    <script src="{{ URL::asset('js/unitofmeasure.js') }}"></script>

@endsection