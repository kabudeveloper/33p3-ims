@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Unit Of Measure</h2>

        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Unit of measure convertion
                            <small></small>
                        </h5>
                        <div class="ibox-tools">


                            <a class="btn btn-primary  btn-sm" href="{{ URL::to('unitofmeasureconvertion/create') }}">Add Convertion</a>

                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="unitofmeasureconvertion-table">
                                <thead>
                                <tr>

                                    <th>Buying</th>
                                    <th>Production</th>
                                    <th>Convertion Rate</th>
                                    <th>Action</th>

                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection

@section('customjs')

    <script src="{{ URL::asset('js/unitofmeasureconvertion.js') }}"></script>


@endsection