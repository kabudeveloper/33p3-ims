@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Companies</h2>
            {!! Breadcrumbs::render('paymentterm') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>View Payment Term</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form  class="form-horizontal" method="POST" action="{{ route('paymentterm.update',$company->id) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">
                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code"  value="{{ $company->code }}" class="form-control" readonly>

                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Description</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="description"  value="{{ $company->description }}" class="form-control" readonly>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('description') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('factor') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Factor ( in days )</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="factor" value="{{ $company->factor }}" class="form-control" readonly>
                                            @if ($errors->has('factor'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('factor') }}</strong>
                                                  </span>
                                            @endif

                                        </div>
                                    </div>

                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($company->isactive)
                                                          <input type="checkbox" value="1" checked name="isactive" disabled>  Active
                                                      @else
                                                          <input type="checkbox" name="isactive" disabled>  Active

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>









                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('paymentterm') }}" class="btn btn-white">Back</a>
                                            <!-- <button type="submit" class="btn btn-primary">Save </button> -->
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection




@section('customjs')

    <script src="{{ URL::asset('js/paymentterm.js') }}"></script>


@endsection