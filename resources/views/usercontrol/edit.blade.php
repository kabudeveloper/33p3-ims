@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Categories</h2>
            {!! Breadcrumbs::render('category') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit User Control</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('duplicate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ Session::get('duplicate') }}
                                </div>
                            @endif
                               <div class="col-lg-7">

                                  <form  class="form-horizontal" method="POST" id="addvendoritem"  action="{{ route('usercontrol.update',$usercontrol->id) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">


                                      <div class="form-group {{ $errors->has('enablepasswordexpiry') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Password Expiry</label>

                                          <div class="col-sm-8">
                                              @if($usercontrol->enablepasswordexpiry)
                                                  <input type="checkbox" checked name="enablepasswordexpiry">
                                              @else
                                                  <input type="checkbox"  name="enablepasswordexpiry">

                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('passwordexpiryinterval') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Password Expiry Interval</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="passwordexpiryinterval" value="{{ $usercontrol->passwordexpiryinterval }}" class="form-control">
                                              @if ($errors->has('passwordexpiryinterval'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('passwordexpiryinterval') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>



                                      <div class="form-group {{ $errors->has('enableloginlocking') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Login Locking</label>

                                          <div class="col-sm-8">
                                              @if($usercontrol->enableloginlocking)
                                                  <input type="checkbox" checked name="enableloginlocking">

                                              @else
                                                  <input type="checkbox"   name="enableloginlocking">

                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('loginlockingthreshold') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Login Locking Threshold</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="loginlockingthreshold" value="{{ $usercontrol->loginlockingthreshold }}" class="form-control">
                                              @if ($errors->has('loginlockingthreshold'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('loginlockingthreshold') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('loginlockingduration') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Login Locking Duration</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="loginlockingduration" value="{{ $usercontrol->loginlockingduration }}"  class="form-control">
                                              @if ($errors->has('loginlockingduration'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('loginlockingthreshold') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                      <div class="form-group {{ $errors->has('enableaccountexpiry') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Account Expiry</label>

                                          <div class="col-sm-8">
                                              @if($usercontrol->enableaccountexpiry)

                                                  <input type="checkbox"  checked name="enableaccountexpiry">

                                              @else
                                                  <input type="checkbox" name="enableaccountexpiry">

                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group {{ $errors->has('accountexpiryinterval') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Account Expiry Interval</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="accountexpiryinterval" value="{{ $usercontrol->accountexpiryinterval }}"   class="form-control">
                                              @if ($errors->has('accountexpiryinterval'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('accountexpiryinterval') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>




                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <button type="submit" class="btn btn-white">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>





        </div>
    </div>


@endsection




@section('customjs')

    <script src="{{ URL::asset('js/editusercontrol.js') }}"></script>

@endsection