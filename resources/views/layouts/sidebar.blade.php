<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#"><img src="{{ asset('public/img/logo.gif') }}" style="height:50px"></a>

    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="logout-link"><a class="text-white" href="{{ url('/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
    </ul>
    <!-- /.navbar-top-links -->


    <div  class="navbar-default sidebar"  role="navigation">
        <div id="inner-content-div">
            <div  class="sidebar-nav navbar-collapse">


                <ul class="nav" id="side-menu" >

                    <li class="nav-header">

                        <div dropdown="" class="dropdown profile-element">
                            <img src="{{ URL::asset('img/profile_small.png') }}" class="img-circle" alt="image">
                            <a href="http://192.168.2.7/33p3-ims/dashboard">
                            <span class="clear">
                                <span class="block labelname">
                                    <strong class="login-font-bold"> {{ Auth::user()->complete_name  }}</strong>
                             </span>
                            </span>
                            </a>

                        </div>

                    </li>



                    <li>
                        <a href="{{ url('/dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-cog fa-fw" aria-hidden="true"></i> Maintenance<span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level" >
                            <li>
                                <a href="{{ route('company') }}" >Company</a>
                            </li>
                            <li>
                                <a href="{{ route('branch') }}">Branch</a>
                            </li>
                            <li>
                                <a href="{{ route('location') }}">Plant Location</a>
                            </li>
                            <li>
                                <a href="{{ route('customer') }}">Customer Information</a>
                            </li>
                            <li>
                                <a href="{{ route('paymentterm') }}">Payment Terms</a>
                            </li>

                            <li>
                                <a href="{{ route('vendors') }}">Vendor Information</a>
                            </li>

                            <li>
                                <a href="{{ route('currency') }}">Currency</a>
                            </li>

                            <li>
                                <a href="#">
                                    Item Hierarchy<span
                                            class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{ route('category') }}">Category</a>
                                    </li>

                                    <li>
                                        <a href="{{ route('clss') }}">Class</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('subclss') }}">SubClass</a>
                                    </li>


                                </ul>
                            </li>
                            <li>
                                <a href="{{ route('box') }}">Box</a>
                            </li>

                            <li>
                                <a href="{{ route('packaging') }}">Packaging</a>
                            </li>

                            <li>
                                <a href="{{ route('unitofmeasure') }}">Unit Of Measure</a>
                            </li>


                            <li>
                                <a href="#">Item <span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="{{ route('rawmaterial') }}">Raw Material</a>
                                    </li>

                                    <li>
                                        <a href="{{ route('finishedproduct') }}">Finished Product</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('supplies') }}">Supplies</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('process') }}">Process</a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-users" aria-hidden="true"></i>
                            User Managements<span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ route('usercontrol') }}">User Control</a>
                            </li>
                            <li>
                                <a href="{{ route('securityuser') }}">Users</a>
                            </li>

                            <li>
                                <a href="{{ route('usergroup') }}">Groups</a>
                            </li>
                            <li>
                                <a href="{{ route('positions') }}">Positions</a>
                            </li>


                        </ul>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-tasks fa-fw" aria-hidden="true"></i> Transactions<span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{ route('salesorder') }}">Sales Orders</a>
                            </li>
                            <li>
                                <a href="{{ route('joborder') }}">Job Orders <span class="label label-default">{{$count}}</span></a>
                            </li>

                            <li>
                                <a href="{{ route('purchase')}}">Purchasing</a>
                            </li>

                            <li>
                                <a href="{{ route('purchasebalance')}}">Purchase Order Balance</a>
                            </li>
                            
                            <li>
                                <a href="{{ route('receiving')}}">Receiving</a>
                            </li>
                            <li>
                                <a href="{{ route('materialrequest')}}">Material Request</a>
                            </li>
                            <li>
                                <a href="{{ route('production')}}">Production</a>
                            </li>
                            <li>
                                <a href="#">Shipping</a>
                            </li>
                            <li>
                                <a href="#">Load Plan</a>
                            </li>

                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-file-pdf-o fa-fw" aria-hidden="true"></i> Reports<span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Periodic Purchasing Report</a>
                            </li>
                            <li>
                                <a href="#">Periodic Purhasing Summary</a>
                            </li>
                            <li>
                                <a href="#">Production Status Report</a>
                            </li>
                            <li>
                                <a href="#">Iventory On-hand</a>
                            </li>
                            <li>
                                <a href="#">Reject Report By</a>
                            </li>


                        </ul>
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-wrench fa-fw" aria-hidden="true"></i> Tools<span
                                    class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Inventory Adjustment</a>
                            </li>


                        </ul>
                    </li>


                </ul>

            </div>
            <!-- /.sidebar-collapse -->

        </div>
    </div>


    <!-- /.navbar-static-side -->
</nav>


