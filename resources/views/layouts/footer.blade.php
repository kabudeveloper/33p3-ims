<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(function(){
        $('#inner-content-div').slimScroll({
            height: '600px',
            opacity: 0.5

        });
    });


</script>