<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>33points</title>

    <!-- Bootstrap Core CSS -->

    <link rel="stylesheet" href="{{   asset('public/css/bootstrap.min.css')     }}">
    <!-- MetisMenu CSS -->

    <link rel="stylesheet" href="{{ URL::asset('css/metisMenu.min.css') }}">
    <link href="{{ URL::asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">


    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap-switch.min.css') }}">
    <link href=" {{ asset('css/bootstrap-datetimepicker.min.css') }} " rel="stylesheet" type='text/css'>
    <link rel="stylesheet" href="{{ URL::asset('skins/square/green.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/sb-admin-2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/dynatree/skin/ui.dynatree.css') }}"  id="skinSheet">
    <link rel="stylesheet" href="{{ URL::asset('css/customstyle.css') }}">

    <!-- Custom Fonts -->{{----}}

    {{--<link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}">--}}

    <link href="{{ asset('public/css/font-awesome.min.css') }}" rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
   @include('layouts.sidebar')
    @include('layouts.sidemenu')


    <div id="page-wrapper" style="margin-top: 5.5em;">
        @yield('breadcrumbpage')

        @yield('content')

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<script src="{{ URL::asset('js/pace.min.js') }}"></script>

<!-- jQuery -->


<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->

<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<!-- Metis Menu Plugin JavaScript -->

<script src="{{ URL::asset('js/metisMenu.min.js') }}"></script>
<!-- Custom Theme JavaScript -->

<script src="{{ URL::asset('js/sb-admin-2.js') }}"></script>

<script src=" {{ URL::asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('js/dataTables.bootstrap.min.js') }}"></script>

<script src="{{ URL::asset('js/bootbox.min.js') }}"></script>
<script src="{{ URL::asset('js/icheck.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-switch.min.js') }}"></script>
<script src="{{ URL::asset('js/numeral.min.js') }}"></script>
<script src="{{ URL::asset('js/noty/packaged/jquery.noty.packaged.min.js') }}"></script>
<script src="{{ URL::asset('js/noty/themes/bootstrap.js') }}"></script>
<script src="{{ URL::asset('js/moment.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap-datetimepicker.min.js') }}"></script>


<script src="{{ URL::asset('js/jquery/jquery-ui.custom.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery/jquery.cookie.js') }}"></script>
<script src="{{ URL::asset('public/dynatree/jquery.dynatree.min.js') }}"></script>

{{--<script src="{{ URL::asset('js/custom.js') }}"></script>--}}

@yield('customjs')
@include('layouts.footer')
</body>

</html>
