@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of SubClass</h2>
            {!! Breadcrumbs::render('subclss') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>SubClass
                            <small></small>
                        </h5>
                        <div class="ibox-tools">

                            {{--<a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                <i class="fa fa-cog"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ URL::to('subclss/create') }}">Add SubClass</a>
                                </li>

                            </ul>--}}
                            <a class="btn btn-primary btn-rounded btn-sm" href="{{ URL::to('subclss/create') }}">Add Subclass</a>

                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="subclass-table">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Subclass</th>
                                    <th>Class</th>
                                    <th>GL Code</th>
                                    <th>Action</th>

                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection

@section('customjs')

    <script src="{{ URL::asset('js/subclss.js') }}"></script>


@endsection