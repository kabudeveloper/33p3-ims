@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Raw Material</h2>
            {!! Breadcrumbs::render('rawmaterial') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>View Raw Material</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('nouom'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    <strong>{{ Session::get('nouom') }} {{$errors->first()}}</strong>
                                </div>
                            @endif


                               <div class="col-lg-6">

                                  <form  class="form-horizontal" id="editrawmat" method="POST" action="{{ route('rawmaterial.update',$company->id) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">
                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code"  value="{{ $company->code }}" class="form-control" readonly>

                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Item Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ $company->name }}" class="form-control" readonly>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                     <div class="form-group {{ $errors->has('_category') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Category</label>

                                          <div class="col-sm-8">
                                              <select name="_category" class="form-control" disabled>
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_category'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_category') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_class') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Class</label>

                                          <div class="col-sm-8">
                                              <select name="_class" class="form-control" disabled>
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_class'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_class') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group {{ $errors->has('_subclass') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">SubClass</label>

                                          <div class="col-sm-8">
                                              <select name="_subclass" class="form-control" disabled>
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_subclass'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_subclass') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('storagelocation') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Location</label>

                                          <div class="col-sm-8">
                                              <select name="storagelocation" class="form-control" disabled>
                                                  <option></option>
                                                  @foreach($locations as $location)
                                                      <option value="{{ $location->id }}" {{ ($location->id == $company->storagelocation ) ? 'selected' : '' }}>{{ $location->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('storagelocation'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_type') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('buying_um') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Buying UOM</label>

                                          <div class="col-sm-8">
                                              <select name="buying_um" class="form-control" disabled>
                                                  <option></option>
                                                  @foreach($buyprods as $uom)
                                                      <option value="{{ $uom->id }}" {{ ( $uom->id == $company->buying_um) ? 'selected=selected' : '' }}>{{ $uom->description }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('buying_um'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('buying_um') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('production_um') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Production UOM</label>

                                          <div class="col-sm-8">
                                              <select name="production_um" class="form-control" disabled>
                                                  <option></option>
                                                  @foreach($buyprods as $uom)
                                                      <option value="{{ $uom->id }}" {{ ( $uom->id == $company->production_um) ? 'selected=selected' : '' }}>{{ $uom->description }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('production_um'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('production_um') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($company->isactive)
                                                          <input type="checkbox" value="1" checked name="isactive" disabled>  Active
                                                      @else
                                                          <input type="checkbox" name="isactive" disabled>  Active

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="col-sm-8 col-sm-offset-4">
                                              <button type="button" class="btn btn-info iac" disabled>I.A.C</button>

                                          </div>
                                      </div>

                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('rawmaterial') }}" class="btn btn-white">Back</a>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Item Vendor
                            <small></small>
                        </h5>
                        <div class="ibox-tools">

                            <!-- <a class="btn btn-primary btn-rounded btn-sm" href="{{ URL::to('rawmaterial/'.$company->id.'/venditem/create') }}">Add New</a> -->

                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="rawmat-vendor-item-table">
                                <thead>
                                <tr>
                                    <th width="10%">Code</th>
                                    <th width="30%">Name</th>
                                    <th width="15%">Cost</th>
                                    <th width="15%">USD</th>
                                    <th width="10%">Active</th>
                                    <th></th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Item Packaging
                            <small></small>
                        </h5>
                        <div class="ibox-tools">

                            <!-- <a class="btn btn-primary btn-rounded btn-sm" href="{{ URL::to('rawmaterial/'.$company->id.'/itempacking/create') }}">Add New</a> -->

                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="rawmat-packaging-item-table">
                                <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Packaging</th>
                                    <th>UM</th>
                                    <th>UM Value</th>
                                    <th>Inner Qty</th>
                                    <th>Level</th>
                                    <th></th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>



        <!-- Modal -->
        <div class="modal fade" id="iacmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Cost</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-inline" id="formcostchanges">
                            <div class="form-group">
                                <label for="cost">Cost</label>
                                <input type="text" name="cost" class="form-control" id="cost">

                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary savecostchanges" >Save changes</button>
                    </div>
                </div>
            </div>
        </div>



    </div>




@endsection



@section('customjs')
   <script type="text/javascript">

    var catid = '{{ $company->_category }}';
    var classid = '{{ $company->_class }}';
    var subclassid = '{{ $company->_subclass }}';
    var APP_URL = {!! json_encode(url('/')) !!};
  </script>

    <script src="{{ URL::asset('js/editrawmat.js') }}"></script>
    <script src="{{ URL::asset('js/rawmat-vendor-item.js') }}"></script>
    <script src="{{ URL::asset('js/rawmat-item-packaging.js') }}"></script>

@endsection