@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Raw Material</h2>
            {!! Breadcrumbs::render('rawmaterial') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Raw Material</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('nouom'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    <strong>{{ Session::get('nouom') }} {{$errors->first()}}</strong>
                                </div>
                            @endif
                               <div class="col-lg-6">



                                  <form class="form-horizontal" method="post" action="{{ route('rawmaterial.store') }}">
                                      {{ csrf_field() }}
                                      <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code" class="form-control">
                                              @if ($errors->has('code'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('code') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Item Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ old('name') }}" class="form-control">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                      <div class="form-group {{ $errors->has('_category') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Category</label>

                                          <div class="col-sm-8">
                                              <select name="_category" class="form-control" disabled="disabled">
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_category'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_category') }}</strong>
                                                  </span>
                                              @endif
                                           </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_class') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Select Class</label>

                                          <div class="col-sm-8">
                                              <select name="_class" class="form-control">
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_class'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_class') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_class') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Select SubClass</label>

                                          <div class="col-sm-8">
                                              <select name="_subclass" class="form-control">
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_subclass'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_subclass') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group {{ $errors->has('storagelocation') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Location</label>

                                          <div class="col-sm-8">
                                              <select name="storagelocation" class="form-control">
                                                  <option></option>
                                                  @foreach($locations as $location)
                                                      <option value="{{ $location->id }}">{{ $location->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('storagelocation'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_type') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      {{--<div class="form-group {{ $errors->has('buyinguom') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Buying UOM</label>

                                          <div class="col-sm-8">
                                              <select name="buyinguom" class="form-control">
                                                  <option></option>
                                                  @foreach($uoms as $uom)
                                                      <option value="{{ $uom->id }}">{{ $uom->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('buyinguom'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('buyinguom') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>--}}

                                     {{-- <div class="form-group {{ $errors->has('produom') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Production UOM</label>

                                          <div class="col-sm-8">
                                              <select name="produom" class="form-control">
                                                  <option></option>
                                                  @foreach($uoms as $uom)
                                                      <option value="{{ $uom->id }}">{{ $uom->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('produom'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('produom') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>--}}





                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('rawmaterial') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection



@section('customjs')

    <script src="{{ URL::asset('js/createrawmat.js') }}"></script>
@endsection