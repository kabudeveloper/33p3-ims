@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Raw Material</h2>
            {!! Breadcrumbs::render('rawmaterial') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Item<small></small></h5>
                        <div class="ibox-tools">

                            {{--<a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                <i class="fa fa-cog"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ URL::to('rawmaterial/create') }}">Add Item</a>
                                </li>

                            </ul>--}}
                            <a class="btn btn-primary btn-rounded btn-sm" href="{{ URL::to('rawmaterial/create') }}">Add Raw Material</a>

                        </div>
                    </div>






                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="rawmat-table">
                                <thead>
                                <tr>
                                    <th>Item Code</th>
                                    <th>Item Name</th>
                                    <th>Class</th>
                                    <th>SubClass</th>
                                    <th>Action</th>

                                </tr>
                                </thead>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection


@section('customjs')

    <script src="{{ URL::asset('js/rawmaterial.js') }}"></script>

@endsection