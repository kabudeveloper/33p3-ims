<!doctype html>
<html lang="en">
<head>
    <script>
        window.print();
        setTimeout(function() {
            window.location.href = "salesorder";
        }, 20); 
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{ asset('css/printstyle.css') }}">
    <title>Quotation</title>
    <style type="text/css">
        .left{
            margin-left: 17px;
        }
        .page-break {
            page-break-after: always;
        }

        .header,
         {
            width: 100%;
            text-align: right;
            position: fixed;
        }
        .footer {
            /*width: 100%;*/
            /*text-align: right;*/

            position: fixed;
        }
        .header {
            position: fixed;
            top: -50px;
            left: 0px;
            right: 0px;
            height: 50px;
            padding: .5em;
            text-align: center;
        }

        .footer {
            bottom: 5em;
        }

        .pagenum:before {
            content: counter(page);
        }

    </style>


</head>
<body>
<div class="header">
    Page <span class="pagenum"></span>
</div>
<div class="contentbody">


    <table class="noborder">
        <tr>
            <td>
                <img class="left" src="{{ asset('public/img/logo.gif') }}">
            </td>
            <td style="vertical-align: text-top; line-height: .4em;">
                <h3><strong>{{ $company->name }}</strong></h3>
                <p>{{ $company->address }}</p>
                <p>{{ $company->telephone }}</p>

            </td>
        </tr>

    </table>

    <br/>

    <table class="noborder left">
        <tr>
            <td class="printdate">
                {{  $printdate }}
            </td>
        </tr>

    </table>

    <br/>

    <table class="noborder left left">
        <tr>
            <td>
                {{ $contact->name }}
            </td>
        </tr>
        <tr>
            <td>
                {{ $customer->name }}
            </td>
        </tr>
        <tr>
            <td>
                {{ $customer->address }}
            </td>
        </tr>

        <tr>
            <td>
                {{ $contact->mobile }} / {{ $contact->landline }}
            </td>
        </tr>

        <tr>
            <td>
                {{ $contact->email }}
            </td>
        </tr>

    </table>

    <br/>
    <h4 style="padding: 3px;margin-left: 17px;">Dear Sir/Mam</h4>
    <br/>
    <p class="left">Please find our quotation for consideration.</p>



    <table class="quotation_order_item left">
        <tr><td colspan="7"><h2>Order Detail:</h2></td></tr>
        <tbody>
        <tr>
            <th>Lno</th>
            <th>Product</th>
            <th>Unit</th>
            <th>Quantity</th>
            <th colspan="2">Unit Price</th>
            <th>Total Price</th>

        </tr>

        @foreach($sod as $item =>$value)

            <tr>
                <td style="text-align: right;width: 0.2in">{{ ++$item }}</td>
                <td>{{ $value->item()->first()->code }} - {{ $value->item()->first()->name }}</td>
                <td style="width: 0.3in">{{ $value->packing()->first()->description }}</td>
                <td style="text-align: right;width: 0.2in">{{ $value->quantity }}</td>
                <td colspan="2" style="text-align: right;width:1in;">{{ $value->convert_unit_price }}</td>
                <td style="text-align: right;width:1in">{{ $value->total_price_convertion  }}</td>
            </tr>

        @endforeach


        </tbody>


        <tr>
            <td colspan="4" style="text-align: right;"></td>
            <td colspan="2" style="text-align: right;width:1in;"><strong>GRAND TOTAL:</strong></td>
            <td class="change_order_total_col"><strong> {{ number_format($sumtotal->sum(),2,'.',',')  }}</strong></td>
        </tr>
    </table>

    <table class="noborder left">
        <tr><td colspan="7"><h2>Terms & Conditions:</h2></td></tr>
        <tr>
            <td>Payment Term</td>
            @if(!is_null($term))
                <td>{{ $term->description }} </td>
            @else
                <td>&nbsp;</td>
            @endif
        </tr>
        <tr>
            <td> Cancellation Term  </td>
            @if(!is_null($cancelterm))
                <td>{{ $cancelterm->description }} </td>
            @else
                <td>&nbsp;</td>
            @endif

        </tr>
        <tr>
            <td> Price Validity </td>
            <td>{{ $so->pricevalidity }} </td>
        </tr>
        <tr>
            <td>Notice </td>
            <td> {{ $so->notes }} </td>
        </tr>

    </table>

    <br/>
    <br/>

    <table class="noborder left">
        <tr>
            <td>
                For order confirmation, kindly issue Purchase Order to <strong><em>33 Point 3 Exports, Inc.</em></strong>
            </td>
        </tr>

    </table>

    <br/>

    <table class="noborder left" width="100%">
        <tr>
            <td>Thank you for this opportunity to quote. We hope this will meet with your kind approval. For further inquiries and clarification on the above matters, please feel free to contact the undersigned.</td>
        </tr>
    </table>

    <table class="noborder left" width="20%">

        <tr>
            <td>Telephone Number</td>
            <td>{{ $company->telephone }}</td>
        </tr>
        <tr>
            <td>Fax Number</td>
            <td>{{ $company->fax }}</td>
        </tr>
        <tr>
            <td>Email Number</td>
            <td>{{ $company->email }}</td>
        </tr>
    </table>
    <br/>



    <!-- <div class="footer">
        <table class="noborder">

            <tr>
                <td>Respectfully Yours,<br/><br/><br/></td>


            </tr>


            <tr>
                <td><span class="underlinefield">{{ $approvedby }}</span><br/>{{ Auth::user()->position()->pluck('description')->first() }}<br/>{{ $company->name }}</td>
                <td>Conforme <span style="padding-left: 1in" class="underlinefield">&nbsp;</span>Date <span style="padding-left: 1in" class="underlinefield">&nbsp;</span><br/>
                    <br/>
                    Authorized Signature Over Printed Name
                </td>
            </tr>


        </table>
    </div> -->

     <div class="footer">
        <table class="noborder left">

            <tr>
                <td>Respectfully Yours,<br/><br/><br/></td>


            </tr>


            <tr>

                <td><span class="underlinefield">{{ $approvedby }}</span><br/>{{ Auth::user()->position()->pluck('description')->first() }}<br/>{{ $company->name }}</td>
                <td>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</td>
                <td class="">Conforme <span style="padding-left: 1in" class="underlinefield">&nbsp;</span>Date <span style="padding-left: 1in" class="underlinefield">&nbsp;</span><br/>
                    <br/>
                    Authorized Signature Over Printed Name
                </td>
            </tr>


        </table>
    </div>

</body>
</html>