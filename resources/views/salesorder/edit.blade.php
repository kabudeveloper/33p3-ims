@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Sales Orders</h2>
            {!! Breadcrumbs::render('salesorder') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Sales Order</h5>

                    </div>
                    <div class="box-content white-bg">
                        <div class="row">


                            <div class="col-lg-12">
                                @if(Session::has('nocontact'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                        <strong>{{ Session::get('nocontact') }} {{$errors->first()}}</strong>
                                    </div>
                                @endif

                                    @if(Session::has('message'))
                                        <div class="alert alert-success alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <i class="fa fa-check" aria-hidden="true"></i>
                                            <strong>{{ Session::get('message') }} </strong>
                                        </div>
                                    @endif

                                    @if(Session::has('duplicate'))
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                        aria-hidden="true">&times;</span></button>
                                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                            <strong>{{ Session::get('duplicate') }} {{$errors->first()}}</strong>
                                        </div>
                                    @endif
                                    @if(Session::has('dupjo'))
                                        <div class="alert alert-danger alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                            <strong>{{ Session::get('dupjo') }} </strong>
                                        </div>
                                    @endif

                                <form class="form-horizontal" method="POST"
                                      action="{{ route('salesorder.update',$so->tnumber) }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="col-lg-6">
                                        <div class="form-group {{ $errors->has('_customer') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Customer</label>

                                            <div class="col-sm-8">

                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <input type="hidden" name="custidhidden" value="{{ $so->_customer }}">
                                                        <input type="text" name="_customer"
                                                               value="{{ $custname->name }}"
                                                               readonly class="form-control">
                                                        @if ($errors->has('_customer'))
                                                            <span class="help-block">
                                                              <strong>{{ $errors->first('_customer') }}</strong>
                                                          </span>
                                                        @endif
                                                    </div>
                                                    @if($so->status !== 'A')
                                                        <div class="col-sm-2 wd-cust">

                                                            <input class="btn btn-default btn-sm" name="btncustomer"
                                                                   type="button" value="....">

                                                        </div>
                                                    @endif


                                                </div>


                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('_currency') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Currency</label>

                                            <div class="col-sm-8">
                                                @if($so->status === 'A')
                                                <select disabled name="_currency" class="form-control">
                                                    <option></option>


                                                </select>

                                                @else
                                                    <select name="_currency" class="form-control">
                                                        <option></option>


                                                    </select>
                                                @endif

                                                @if ($errors->has('_currency'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('_currency') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        @if(Auth::user()->can('edit-exchangerate'))
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Currency Rate</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="currencyrate"  value="{{ $so->currencyrate }}"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        @else
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">Currency Rate</label>
                                                <div class="col-sm-8">
                                                    <input type="text" readonly name="currencyrate"  value="{{ $so->currencyrate }}"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        @endif


                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Payment Term</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="paymentterm" readonly
                                                       class="form-control">
                                                <input type="hidden" name="_paymentterm">
                                            </div>
                                        </div>

                                        <!-- <div class="form-group {{ $errors->has('_cancellationterm') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Cancellation Term</label>

                                            <div class="col-sm-8">
                                                @if($so->status === 'A')
                                                    <select disabled name="_cancellationterm" class="form-control">
                                                        <option></option>
                                                        @foreach($cancellterm as $cancel)
                                                            <option value="{{ $cancel->id }}" {{ ( $so->_cancellationterm == $cancel->id ) ? "selected=selected" : '' }}>{{ $cancel->description }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('_cancellationterm'))
                                                        <span class="help-block">
                                                  <strong>{{ $errors->first('_cancellationterm') }}</strong>
                                                  </span>
                                                    @endif
                                                @else
                                                    <select  name="_cancellationterm" class="form-control">
                                                        <option></option>
                                                        @foreach($cancellterm as $cancel)
                                                            <option value="{{ $cancel->id }}" {{ ( $so->_cancellationterm == $cancel->id ) ? "selected=selected" : '' }}>{{ $cancel->description }}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('_cancellationterm'))
                                                        <span class="help-block">
                                                  <strong>{{ $errors->first('_cancellationterm') }}</strong>
                                                  </span>
                                                    @endif
                                                @endif

                                            </div>
                                        </div> -->




                                        <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Notes</label>
                                            @if($so->status === 'A')
                                                <div class="col-sm-8">
                                                <textarea name="notes" readonly class="form-control"
                                                          rows="3"> {{ $so->notes }}</textarea>
                                                @if ($errors->has('notes'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('notes') }}</strong>
                                                  </span>
                                                @endif
                                                     </div>
                                            @else
                                                <div class="col-sm-8">
                                                <textarea name="notes"  class="form-control"
                                                          rows="3"> {{ $so->notes }}</textarea>
                                                    @if ($errors->has('notes'))
                                                        <span class="help-block">
                                                  <strong>{{ $errors->first('notes') }}</strong>
                                                  </span>
                                                    @endif
                                                </div>
                                           @endif

                                        </div>

                                    </div>


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">SO Number</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tnumber" value="{{ $so->tnumber }}" readonly
                                                       class="form-control">

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">SO Date</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tdate" value="{{ $sotransdate }}" readonly
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Status</label>

                                            <div class="col-sm-8">

                                                <input type="text" name="status" value="{{ $sostatus }}"
                                                       readonly class="form-control">

                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('pricevalidity') ? ' has-error' : '' }}">
                                            <label class="col-sm-4 control-label">Price Date Validity</label>

                                            <div class="col-sm-8">
                                                @if($so->status === 'A')
                                                <input type="text" readonly name="pricevalidity"  value="{{ $so->pricevalidity }}"
                                                       class="form-control">
                                                @if ($errors->has('pricevalidity'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('pricevalidity') }}</strong>
                                                  </span>
                                                @endif
                                                @else
                                                    <input type="text" name="pricevalidity"  value="{{ $so->pricevalidity }}"
                                                           class="form-control">
                                                    @if ($errors->has('pricevalidity'))
                                                        <span class="help-block">
                                                  <strong>{{ $errors->first('pricevalidity') }}</strong>
                                                  </span>
                                                    @endif
                                                 @endif
                                            </div>
                                        </div>
                                        <!-- <div class="form-group {{ $errors->has('_leadtime') ? ' has-error' : '' }}">
                                            <label class="col-sm-4 control-label">Lead Time</label>

                                            <div class="col-sm-8">
                                                @if($so->status === 'A')
                                                    <select disabled  name="_leadtime" class="form-control">
                                                        <option></option>
                                                        @foreach($leadtimes as $leadtime)
                                                            <option value="{{ $leadtime->id }}" {{ ( $leadtime->id == $so->_leadtime ) ? "selected=selected" : '' }}>{{ $leadtime->description }}</option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <select   name="_leadtime" class="form-control">
                                                        <option></option>
                                                        @foreach($leadtimes as $leadtime)
                                                            <option value="{{ $leadtime->id }}" {{ ( $leadtime->id == $so->_leadtime ) ? "selected=selected" : '' }}>{{ $leadtime->description }}</option>
                                                        @endforeach
                                                    </select>
                                                 @endif
                                                    @if ($errors->has('_leadtime'))
                                                        <span class="help-block">
                                                          <strong>{{ $errors->first('_leadtime') }}</strong>
                                                      </span>
                                                    @endif
                                            </div>
                                        </div> -->


                                    </div>

                                    <div class="col-lg-12">
                                        <div class="ibox-tools mg-bottom">
                                            <a href="{{ route('salesorderdetail.create',['trnid'=>$so->tnumber]) }}"
                                               class="btn btn-primary btn-sm">Add New</a>
                                        </div>

                                        <div class="table-responsive">
                                            <table width="100%" class="table table-bordered table-striped"
                                                   id="salesorder-detail-table">
                                                <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Packing</th>
                                                    <th>Quantity</th>
                                                    <th>Cost Price</th>
                                                    <th>USD</th>
                                                    <th>Mark Up</th>
                                                    <th>Unit Price</th>
                                                    <th>A.U.P</th>
                                                    <th>Total Price</th>
                                                    <th>A.U.P Total</th>
                                                    <th></th>
                                                </tr>
                                                </thead>

                                                <tfoot>
                                                    <tr>
                                                        <th colspan="8" style="text-align:right;"><span class="to"></span></th>
                                                        <th colspan="2"></th>
                                                    </tr>
                                                </tfoot>



                                            </table>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-8 mg-top">

                                                @if($so->status === 'A')
                                                 <a href="{{ route('printsalesorder',['so'=>$so->tnumber]) }}"
                                                     class="btn btn-primary" target="_blank" name="submit">Print Sales Order</a>

                                                <a href="{{ route('printquotation',['so'=>$so->tnumber]) }}"
                                                   class="btn btn-primary" target="_blank" name="submit">Print Sales Quotation</a>


                                                    <a href="{{ route('generatejoborder',['tnumber'=>$so->tnumber]) }}"
                                                       class="btn btn-primary" name="submit">Generate Job Order</a>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-8 ">
                                                @if($so->status !== 'A')
                                                <button type="submit"
                                                   class="btn btn-primary" name="submit">Save</button>

                                                @endif

                                                    @if($so->status === 'D')
                                                        <a href="{{ route('submitsalesorder',['tnumber'=>$so->tnumber]) }}"
                                                           class="btn btn-success">Submit</a>

                                                    @endif

                                                @if($so->status == 'S')
                                                    <a href="{{ route('approvesalesorder',['tnumber'=>$so->tnumber]) }}"
                                                       class="btn btn-success" name="submit">Approved</a>

                                                @endif
                                                <a href="{{ route('rollbacksalesorder',['tnumber'=>$so->tnumber]) }}"
                                                   class="btn btn-success" name="submit">Rollback</a>

                                                <a href="{{ route('cancelsalesorder',['tnumber'=>$so->tnumber]) }}"
                                                   class="btn btn-danger" name="submit">Cancel</a>


                                            </div>
                                        </div>


                                    </div>


                                </form>
                            </div>


                        </div>


                    </div>
                </div>



            </div>

        </div>

        <!-- Modal -->
        <div class="modal fade" id="customerlistmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Customer List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablecustomerlist">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>

                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>


        <!-- AUP modal -->


        <div class="modal fade smallaup" id="aupmodal" tabindex="-1" role="dialog" aria-labelledby="aupmodal">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Edit AUP</h4>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="aup" class="col-sm-2 control-label">AUP</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="aup">
                                    <input type="hidden" class="form-control" id="itemhidden">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" name="btnupdateaup" class="btn btn-success">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection


@section('customjs')
    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

        <script src="{{ URL::asset('js/editsalesorder.js') }}"></script>

@endsection