@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Sales Orders</h2>
            {!! Breadcrumbs::render('salesorder') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Sales Order</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('nocurrencyrate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    <strong>{{ Session::get('nocurrencyrate') }} {{$errors->first()}}</strong>
                                </div>
                            @endif
                            <div class="col-lg-12">

                                <form class="form-horizontal" method="post" action="{{ route('salesorder.store') }}">
                                    {{ csrf_field() }}

                                    <div class="col-lg-6">

                                        <div class="form-group {{ $errors->has('_customer') ? ' has-error' : '' }}">
                                            <label class="col-sm-4 control-label">Customer</label>

                                            <div class="col-sm-8">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <input type="hidden" name="custidhidden">
                                                        <input type="text" name="_customer" value="{{ old('_customer') }}" readonly class="form-control">
                                                        @if ($errors->has('_customer'))
                                                            <span class="help-block">
                                                                 <strong>{{ $errors->first('_customer') }}</strong>
                                                                </span>
                                                        @endif
                                                    </div>

                                                    <div class="col-sm-2 wd-cust">
                                                        <input class="btn btn-default btn-sm" name="btncustomer" type="button" value="....">

                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('_currency') ? ' has-error' : '' }}">
                                            <label class="col-sm-4 control-label">Currency</label>

                                            <div class="col-sm-8">
                                                <select name="_currency" class="form-control">
                                                    <option></option>


                                                </select>
                                                @if ($errors->has('_currency'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('_currency') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Exchange Rate</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="currencyrate" readonly
                                                       class="form-control">

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Payment Term</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="paymentterm" readonly
                                                       class="form-control">
                                                <input type="hidden" name="_paymentterm">
                                            </div>
                                        </div>

                                   {{-- <div class="form-group {{ $errors->has('_paymentterm') ? ' has-error' : '' }}">
                                            <label class="col-sm-4 control-label">Cancellation Term</label>

                                            <div class="col-sm-8">
                                                <select name="_cancellationterm" class="form-control">
                                                    <option></option>
                                                    @foreach($cancellterm as $cancel)
                                                        <option value="{{ $cancel->id }}">{{ $cancel->description }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('_cancellationterm'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('_cancellationterm') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div> --}}

                                        <div class="form-group {{ $errors->has('pricevalidity') ? ' has-error' : '' }}">
                                            <label class="col-sm-4 control-label">Price Date Validity</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="pricevalidity"
                                                       class="form-control">
                                                @if ($errors->has('pricevalidity'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('pricevalidity') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                                            <label class="col-sm-4 control-label">Notes</label>

                                            <div class="col-sm-8">
                                                <textarea name="notes" value="{{ old('notes') }}" class="form-control" rows="3"></textarea>
                                                @if ($errors->has('notes'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('notes') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        <!-- <div class="form-group {{ $errors->has('_leadtime') ? ' has-error' : '' }}">
                                            <label class="col-sm-4 control-label">Lead Time</label>

                                            <div class="col-sm-8">

                                                <select   name="_leadtime" class="form-control">
                                                    <option></option>
                                                    @foreach($leadtimes as $leadtime)
                                                        <option value="{{ $leadtime->id }}">{{ $leadtime->description }}</option>
                                                    @endforeach
                                                </select>

                                                @if ($errors->has('_leadtime'))
                                                    <span class="help-block">
                                                          <strong>{{ $errors->first('_leadtime') }}</strong>
                                                      </span>
                                                @endif
                                            </div>
                                        </div> -->


                                        <div class="form-group">
                                            <div class="col-sm-offset-4 col-sm-10 mg-top">
                                                <button type="submit" class="btn btn-primary">Save</button>
                                                <a href="{{ route('salesorder') }}" class="btn btn-info">Back</a>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="hr-line-dashed"></div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>


        <!-- Modal -->
        <div class="modal fade" id="customerlistmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Customer List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablecustomerlist">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>

                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>




    </div>


@endsection


@section('customjs')
    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    <script src="{{ URL::asset('js/createsalesorder.js') }}"></script>

@endsection