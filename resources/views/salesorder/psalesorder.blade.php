<!doctype html>
<html lang="en">
<head>
    <script>
        window.print();
        setTimeout(function() {
            window.location.href = "salesorder";
        }, 20); 
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{ asset('css/printstyle.css') }}">
    <title>Salesorder</title>
    <style type="text/css">
        .left{
            margin-left: 17px;
        }
        
        .page-break {
            page-break-after: always;
        }

        .header,
        .footer {
            width: 100%;
            text-align: right;
            position: fixed;
        }

        .header {
            position: fixed;
            top: -50px;
            left: 0px;
            right: 0px;
            height: 50px;
            padding: .5em;
            text-align: center;
        }

        .footer {
            bottom: 5em;
        }

        .pagenum:before {
            content: counter(page);
        }

    </style>


</head>
<body>
<div class="header">
    Page <span class="pagenum"></span>
</div>
<div class="contentbody">


    <table class="noborder">
        <tr>
            <td>
                <img class="left" src="{{ asset('public/img/logo.gif') }}">
            </td>
            <td style="vertical-align: text-top; line-height: .4em;">
                <h3><strong>{{ $company->name }}</strong></h3>
                <p>{{ $company->address }}</p>
                <p>{{ $company->telephone }}</p>

            </td>
        </tr>

    </table>

    <br/>

    <table class="noborder left">
        <tr>
            <td class="printdate">
                {{  $printdate }}
            </td>
        </tr>

    </table>

    <br/>

    <table class="noborder left">
        <tr>
            <td>
                Sales Order No
            </td>
            <td>
                {{ $so->tnumber }}
            </td>
        </tr>
        <tr>
            <td>
                Payment Term
            </td>
            <td>
                {{ $term->description }}
            </td>
        </tr>
        <tr>
            <td>
                Delivery Date
            </td>
            <td>
                {{ $fprintdeliverydate }}
            </td>
        </tr>
    </table>
    <br/>
    <table class="noborder left">
        <tr>
            <td>
                {{ $contact->name }}
            </td>
        </tr>
        <tr>
            <td>
                {{ $customer->name }}
            </td>
        </tr>
        <tr>
            <td>
                {{ $customer->address }}
            </td>
        </tr>

        <tr>
            <td>
                {{ $contact->mobile }} / {{ $contact->landline }}
            </td>
        </tr>

        <tr>
            <td>
                {{ $contact->email }}
            </td>
        </tr>

    </table>

    <br/>




    <table class="quotation_order_item left">
        <tr><td colspan="11"><h2>Order Detail:</h2></td></tr>
        <tbody>
        <tr>
            <th>Lno</th>
            <th>Product</th>
            <th>Unit</th>
            <th>Quantity</th>
            <th>Cost Price</th>
            <th>Mark Up</th>
            <th>Unit Price</th>
            <th>A.U.P</th>
            <th colspan="2">Total Price</th>
            <th>Total A.U.P</th>

        </tr>

        @foreach($sods as $item =>$value)

            <tr>
                <td style="text-align: right;width: 0.2in">{{ ++$item }}</td>
                <td>{{ $value->code }} - {{ $value->name }}</td>
                <td >{{ $value->description }}</td>
                <td >{{ $value->quantity }}</td>
                <td >{{ number_format($value->costprice,2,'.',',') }}</td>
                <td >{{ number_format($value->markup,2,'.',',') }}</td>
                <td >{{ number_format($value->unitprice,2,'.',',') }}</td>
                <td >{{ number_format($value->aup,2,'.',',') }}</td>

                <td colspan="2" style="text-align: right;">{{ number_format($value->totalprice,2,'.',',') }}</td>
                <td style="text-align: right;width:1in">{{ number_format($value->auptotal,2,'.',',') }}</td>
            </tr>

        @endforeach


        </tbody>


        <tr>
            <td colspan="8" style="text-align: right;"><strong>Grand Total</strong></td>
            <td colspan="2" style="text-align: right;width:1in;"><strong>{{ number_format($sumtotalprice->sum(),2,'.',',')  }}</strong></td>
            <td class="change_order_total_col"><strong> {{ number_format($sumtotal->sum(),2,'.',',')  }}</strong></td>
        </tr>
    </table>

    <table class="noborder left">
        <tr><td colspan="7"><h2>Terms & Conditions:</h2></td></tr>
        <tr>
            <td>Payment Term</td>
            @if(!is_null($term))
                <td>{{ $term->description }} </td>
            @else
                <td>&nbsp;</td>
            @endif
        </tr>
        <tr>
            <td> Cancellation Term  </td>
            @if(!is_null($cancelterm))
                <td>{{ $cancelterm->description }} </td>
            @else
                <td>&nbsp;</td>
            @endif

        </tr>
        <tr>
            <td> Price Validity </td>
            <td>{{ $so->pricevalidity }} </td>
        </tr>
        <tr>
            <td>Notice </td>
            <td> {{ $so->notes }} </td>
        </tr>

    </table>

    <br/>
    <br/>




    <table class="noborder left" width="100%" style="margin-top: 1in;">

        <tr>
            <td>
                Prepared By:
                <br/>
                <br/>
                <br/>
                <span style="padding-left: 1in" class="underlinefield">&nbsp;</span>{{ $printdate }}
            </td>
            <td>
                Approved By:
                <br/>
                <br/>
                <br/>
                <span style="padding-left: 1in" class="underlinefield">&nbsp;</span>{{ $sodateapproved }}
            </td>
        </tr>




    </table>

    <div class="footer">

    </div>

</body>
</html>