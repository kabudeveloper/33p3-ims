@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2></h2>

        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>View Bill Of Material</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('duplicate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ Session::get('duplicate') }}
                                </div>
                            @endif
                            <div class="col-lg-7">

                                <form class="form-horizontal" method="POST" id="addvendoritem"
                                      action="{{ route('finishedproduct.bom.update',[$vendoritem->_item,$vendoritem->_subitem]) }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="_method" value="PUT">


                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Product</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="productname" value="{{ $mainitem->code }} - {{ $mainitem->name }} "
                                                   readonly class="form-control">

                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('rmcode') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">RM Code</label>

                                        <div class="col-sm-8">

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text" name="rmcode" value="{{ $subitem->code }}"
                                                           class="form-control" readonly>
                                                    @if ($errors->has('rmcode'))
                                                        <span class="help-block">
                                                                 <strong>{{ $errors->first('rmcode') }}</strong>
                                                            </span>
                                                    @endif
                                                    <input type="hidden" name="_hiddenrmid"
                                                           value="{{ $subitem->id }}">
                                                    <div class="rmcode">

                                                    </div>


                                                </div>

                                                <div class="col-sm-6">

                                                    <button id="vc" type="submit" class="btn btn-info btn-sm"
                                                            data-loading-text="Verifying..." autocomplete="off"
                                                            disabled
                                                    >Verify Code
                                                    </button>


                                                    <button id="fv" type="submit" class="btn btn-success btn-sm" disabled>
                                                        Find RM
                                                    </button>

                                                </div>


                                            </div>


                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('rmname') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">RM Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="rmname" value="{{ $subitem->name  }}"
                                                   class="form-control" readonly>

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('itempacking') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Packing</label>

                                        <div class="col-sm-8">
                                            <select name="packing" class="form-control" readonly>





                                            </select>
                                            @if ($errors->has('itempacking'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('itempacking') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Quantity</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="quantity" value="{{ $vendoritem->quantity }}"
                                                   class="form-control" readonly>
                                            @if ($errors->has('quantity'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('quantity') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <div>
                                                <label>
                                                    @if($vendoritem->directrm)
                                                        <input type="checkbox" value="1" checked name="isactive" disabled>
                                                        Direct RM
                                                    @else
                                                        <input type="checkbox" name="directrm" disabled>  Direct RM

                                                    @endif

                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ URL::to('finishedproduct/'.$vendoritem->_item ).'/edit' }}" class="btn btn-white">Back</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal fade" tabindex="-1" id="findRawmMaterialModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Find Raw Material</h4>
                        </div>
                        <div class="modal-body">

                            <form class="form-inline" id="formvendorsearch">
                                <div class="form-group">
                                    <label for="criteria">Criteria</label>
                                    <select class="form-control" name="criteria" id="criteria">
                                        <option value="vendorcode">Code</option>
                                        <option value="vendorname">Name</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="valuesearch">Value</label>
                                    <input type="email" class="form-control" id="valuesearch" name="valuesearch">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="containssearch" value="1"> Contains
                                    </label>
                                </div>
                                <button type="button" class="btn btn-primary btn-sm" id="searchvendor"><i
                                            class="fa fa-search" aria-hidden="true"></i>
                                    Search
                                </button>
                            </form>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive" id="findrawvendortable">
                                        <table class="table table-bordered table-hover tablemodal">
                                            <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Name</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="findselectvendor" class="btn btn-primary">Select</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


        </div>
    </div>


@endsection




@section('customjs')

    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
        var packing = {!! $itempacking !!};
    </script>

    <script src="{{ URL::asset('js/edit-billofmaterial.js') }}"></script>


@endsection