@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Finisihed Product</h2>
            {!! Breadcrumbs::render('rawmaterial') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Job Order Detail</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form  class="form-horizontal" method="POST" action="{{ route('joborderdetail.update',$jodetail->_item) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">

                                      <div class="form-group {{ $errors->has('tnumber') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">TR #</label>
                                          <div class="col-sm-8">
                                              <input type="text" name="tnumber"  value="{{ $jodetail->tnumber  }}" class="form-control">
                                              @if ($errors->has('tnumber'))
                                                  <span class="help-block">
                                              <strong>{{ $errors->first('tnumber') }}</strong>
                                              </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <div class="row">
                                                  <div class="col-sm-10">
                                                      <input type="text" name="code" value="{{ $item->code  }}"  class="form-control">
                                                      @if ($errors->has('code'))
                                                          <span class="help-block">
                                                  <strong>{{ $errors->first('code') }}</strong>
                                                  </span>
                                                      @endif
                                                  </div>

                                                 {{-- <div class="col-sm-2 wd-cust">
                                                      <input type="button" name="btnjodetailfin"  value="..." class="btn btn-default form-control">
                                                  </div>--}}
                                              </div>


                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_item') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Item Name</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="_item" value="{{ $item->name  }}"  class="form-control">
                                              <input type="hidden" name="itemhidden" value="{{ $jodetail->_item }}">
                                              @if ($errors->has('_item'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_item') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group {{ $errors->has('packing') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Packing</label>

                                          <div class="col-sm-8">
                                              <select name="packing" class="form-control" >
                                                  <option></option>
                                              </select>
                                              @if ($errors->has('packing'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('packing') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

{{--
                                      <div class="form-group {{ $errors->has('unitprice') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Selling Prcie</label>
                                          <div class="col-sm-8">
                                              <input type="text" name="unitprice"  value="{{ $jodetail->unitprice }}" class="form-control">
                                              @if ($errors->has('unitprice'))
                                                  <span class="help-block">
                                              <strong>{{ $errors->first('unitprice') }}</strong>
                                              </span>
                                              @endif
                                          </div>
                                      </div>--}}


                                      <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Quantity</label>
                                          <div class="col-sm-8">
                                              <input type="text" name="quantity"  value="{{ $jodetail->quantity }}" class="form-control">
                                              @if ($errors->has('quantity'))
                                                  <span class="help-block">
                                              <strong>{{ $errors->first('quantity') }}</strong>
                                              </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="hr-line-dashed"></div>
                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <a href="{{ URL::to('joborder/'.$jodetail->tnumber ).'/edit' }}" class="btn btn-white">Cancel</a>
                                              <button type="submit" class="btn btn-primary">Save </button>
                                          </div>
                                      </div>



                                  </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="finishprodlistmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Finished Product List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablejodetail">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>

                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>





    </div>


@endsection



@section('customjs')
   <script type="text/javascript">

    var APP_URL = {!! json_encode(url('/')) !!};
  </script>

   <script src="{{ URL::asset('js/editcreatejoborderdetail.js') }}"></script>


@endsection