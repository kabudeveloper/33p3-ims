@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2></h2>

        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Process</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('duplicate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ Session::get('duplicate') }}
                                </div>
                            @endif
                            <div class="col-lg-7">


                                <form class="form-horizontal" method="post" id="addbillofmat"
                                      action="">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        
                                    </div>


                                    <div class="form-group {{ $errors->has('rmcode') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Process Code</label>
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <input type="text" name="pcode" value="{{$proc->code}}"
                                                           class="form-control" readonly>
                                                    <input type="hidden" name="_hiddenpid">
                                                    @if ($errors->has('rmcode'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('rmcode') }}</strong>
                                                        </span>
                                                    @endif
                                                    <div class="rmcode">

                                                    </div>


                                                </div>

                                                <div class="col-sm-7">

                                                    <button id="vc" type="submit" class="btn btn-info btn-sm"
                                                            data-loading-text="Verifying..." autocomplete="off"

                                                    disabled=>Verify Code
                                                    </button>


                                                    <button id="fv" type="button" class="btn btn-success btn-sm"
                                                            name="findproc" disabled>
                                                        Find Process
                                                    </button>

                                                </div>


                                            </div>


                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('rmname') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Process Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="pname" class="form-control" value="{{$proc->name}}" readonly>

                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('itempacking') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Location</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="location" class="form-control" value="{{$proc->location}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Amount</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="quantity" class="form-control" value="{{$proc->amount}}" readonly>
                                            @if ($errors->has('quantity'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('quantity') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>




                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ URL::to('finishedproduct/'.$proc->item).'/edit'}}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary" disabled>Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

 <!-- Modal -->
        <div class="modal fade" id="processmodallist" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Process List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tableprocesslist">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Process Name</th>
                                    <th>Location</th>
                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>



    </div>


@endsection
<script type="text/javascript">

    var APP_URL = {!! json_encode(url('/')) !!};
</script>


@section('customjs')

    <script src="{{ URL::asset('js/addprocess.js') }}"></script>

@endsection