@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Process</h2>
            {!! Breadcrumbs::render('process') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')

    <div class="wrapper-content">
        <div class="white-bg box-wrapper">
          <div class="ibox-title">
              <h5>Process<small></small></h5>
              <div class="ibox-tools">
                <a class="btn btn-primary btn-rounded btn-sm" href="process/create">Add Process</a>
              </div>
          </div>
          <div class="box-content white-bg">
            <div class="table-responsive">
                <table width="100%" class="table table-bordered table-striped" id="process-list-table">
                    <thead>
                    <tr>
                      <th>Code</th>
                        <th>Process Name</th>
                        <th>Location</th>
                        <th>Action</th>
                    </tr>
                    </thead>


                </table>
            </div>
          </div>
        </div>
    </div>

@endsection



@section('customjs')
  <script src="{{ URL::asset('js/process.js') }}"></script>
@endsection