@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Process</h2>
            {!! Breadcrumbs::render('process') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Process</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('nouom'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    <strong>{{ Session::get('nouom') }} {{$errors->first()}}</strong>
                                </div>
                            @endif
                               <div class="col-lg-6">

                                  <form class="form-horizontal" method="post" action=" {{route('updateprocess1', $process->mpid)}}">
                                      {{ csrf_field() }}
                                      <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code" class="form-control" value="{{$process->code}}">
                                              @if ($errors->has('code'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('code') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Process Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="process_name"  value="{{ $process->process_name }}" class="form-control">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                      <div class="form-group {{ $errors->has('_category') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Category</label>

                                          <div class="col-sm-8">
                                              <input type="hidden" name="_category"> 
                                              <select name="_category1" class="form-control" disabled="disabled">
                                                  


                                              </select>
                                              @if ($errors->has('_category'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_category') }}</strong>
                                                  </span>
                                              @endif
                                           </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('storagelocation') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Location</label>

                                          <div class="col-sm-8">
                                              <select name="_location" class="form-control">
                                                  
                                                  @foreach($locations as $location)
                                                    @if($process->_location == $location->id)
                                                     <option value="{{ $location->id }}" selected>{{ $location->name }}</option>
                                                    @else
                                                      <option value="{{ $location->id }}">{{ $location->name }}</option>
                                                    @endif
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('storagelocation'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_type') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>
                                      
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('process') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection

<script type="text/javascript">

    var APP_URL = {!! json_encode(url('/')) !!};
</script>

@section('customjs')
    <script src="{{ URL::asset('js/editprocess.js') }}"></script>
@endsection