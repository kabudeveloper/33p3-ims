@extends('layouts.layout')     

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Purchase Order Balance</h2>
            {!! Breadcrumbs::render('purchasebalance') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Purchase Order Balance
                            <small></small>
                        </h5>
                        <!-- <div class="ibox-tools">
                            <a class="btn btn-primary btn-sm" href="{{ URL::to('purchasing/create') }}">Create Purchase Order</a>
                        </div> -->
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="purchaseorder-balance-table">
                                <thead>
                                <tr>
                                    <th>PO #</th>
                                    <th>Date</th>
                                    <th>Vendor</th>
                                    <th>Item Name</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection

@section('customjs')

    <script src="{{ URL::asset('js/purchasing.js') }}"></script>


@endsection