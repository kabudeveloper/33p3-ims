@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Purchase Orders</h2>
            {!! Breadcrumbs::render('purchasebalance') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Purchase Order</h5>

                    </div>
                    <div class="box-content white-bg">
                        <div class="row">


                                <div class="col-lg-12">

                                <form  class="form-horizontal" method="post" action="createpo">
                                    {{ csrf_field() }}

                                    <div class="col-lg-6">
                                        <div class="form-group {{ $errors->has('_branch') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Branch</label>

                                            <div class="col-sm-8">
                                                @if ($po->status ==='D')
                                                <select name="_branch" class="form-control">
                                                @elseif ($po->status !=='D')
                                                <select name="_branch" class="form-control" disabled>
                                                @endif
                                                    <option></option>
                                                    @foreach($branches as $branch)
                                                        <option value="{{ $branch->id }}"  {{ ($branch->id == $po->_branch) ? "selected=selected": '' }}>{{ $branch->name }}</option>
                                                    @endforeach

                                                </select>
                                                @if ($errors->has('_branch'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('_branch') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('_vendor') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Vendor</label>

                                            <div class="col-sm-8">

                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <input type="hidden" name="vendoridhidden">
                                                        <input type="hidden" name="tmode">
                                                        <input type="text" name="_vendor" value="{{ $vendorname }}" readonly class="form-control">
                                                        @if ($errors->has('_vendor'))
                                                            <span class="help-block">
                                                  <strong>{{ $errors->first('_vendor') }}</strong>
                                                  </span>
                                                        @endif
                                                    </div>

                                                    <div class="col-sm-2 wd-cust">
                                                        @if($po->status ==='D')
                                                         <input class="btn btn-default btn-sm" name="btnvendor" type="button" value="....">
                                                        @endif
                                                    </div>


                                                </div>


                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('reference') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Reference</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="reference" value="{{ $po->jonumber }}" class="form-control" readonly>
                                                @if ($errors->has('reference'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('reference') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('_paymentterm') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Payment Term</label>

                                            <div class="col-sm-8">
                                                @if ($po->status ==='D')
                                                <select name="_paymentterm" class="form-control">
                                                @elseif ($po->status !=='D')
                                                <select name="_paymentterm" class="form-control" disabled>
                                                @endif
                                                    <option></option>
                                                    @foreach($paymentterm as $payment)
                                                        <option value="{{ $payment->id }}"  {{ ($payment->id == $po->_paymentterm) ? "selected=selected": '' }}>{{ $payment->description }}</option>
                                                    @endforeach

                                                </select>
                                                @if ($errors->has('_paymentterm'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('_paymentterm') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Particulars</label>

                                            <div class="col-sm-8">
                                                @if ($po->status ==='D')
                                                <textarea name="notes"  class="form-control" rows="3"> {{ $po->notes }}</textarea>
                                                @elseif ($po->status !=='D')
                                                <textarea name="notes"  class="form-control" rows="3" readonly> {{ $po->notes }}</textarea>
                                                @endif
                                                
                                                @if ($errors->has('notes'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('notes') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">PO Number</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tnumber" value="{{ $po->tnumber }}" readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">PO Date</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tdate" value="{{ $po->tdate }}"  readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Status</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="status" value="{{ $postatus }}"  readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Delivery Date</label>

                                            <div class="col-sm-8">
                                                @if ($po->status ==='D')
                                                <input type="text" name="deliverydate" value="{{ $po->deliverydate }}" class="form-control">
                                                @elseif ($po->status !=='D')
                                                <input type="text" name="deliverydate" value="{{ $po->deliverydate }}" class="form-control" readonly>
                                                @endif
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <div class="ibox-tools mg-bottom">
                                            <a href="{{ route('purchaseorderdetail.create',['trnid'=>$po->tnumber]) }}" class="btn btn-primary btn-sm disabled">Add New Item</a>
                                        </div>

                                        <div class="table-responsive">
                                            <table width="100%" class="table table-bordered table-striped" id="pobal-detail-table">
                                                <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Packing</th>
                                                    <th>Balance</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div><br />
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-8 mg-top">
                            
                                                <a href="{{ route ('purchasebalance') }}" class="btn btn-danger">Back</a>
                                                <!-- <a >Create PO</a> -->
                                                <button type="submit" class="btn btn-primary"> Create PO</button>
                                        </div>
                                    </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

@endsection


@section('customjs')

    <script src="{{ URL::asset('js/editpurchasebal.js') }}"></script>

@endsection