@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Companies</h2>
            {!! Breadcrumbs::render('company') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Company</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form class="form-horizontal" method="post" action="{{ route('company.store') }}">
                                      {{ csrf_field() }}
                                      <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code"   class="form-control">
                                              @if ($errors->has('code'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('code') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ old('name') }}" class="form-control">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Address</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="address" value="{{ old('address') }}" class="form-control">
                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('address') }}</strong>
                                                  </span>
                                            @endif

                                        </div>
                                    </div>

                                      <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Email</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="email" value="{{ old('email') }}" class="form-control">
                                              @if ($errors->has('email'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('email') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('telephone') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Telephone Number</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="telephone" value="{{ old('telephone') }}" class="form-control">
                                              @if ($errors->has('telephone'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('telephone') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('fax') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Fax Number</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="fax" value="{{ old('fax') }}" class="form-control">
                                              @if ($errors->has('fax'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('fax') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('zipcode') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Zip code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="zipcode"  value="{{ old('zipcode') }}" class="form-control" placeholder="">
                                              @if ($errors->has('zipcode'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('zipcode') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group {{ $errors->has('tin') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Tin</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="tin" value="{{ old('tin') }}" class="form-control">
                                            @if ($errors->has('tin'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('tin') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                      <div class="form-group {{ $errors->has('_currency') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Currency</label>

                                          <div class="col-sm-8">
                                              <select name="_currency" class="form-control">
                                                  <option></option>
                                                  @foreach($currencies as $currency)
                                                      <option value="{{ $currency->id }}">{{ $currency->symbol }} - {{ $currency->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_currency'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_currency') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('glcode') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">GL Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="glcode" value="{{ old('glcode') }}" class="form-control">
                                              @if ($errors->has('glcode'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('glcode') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>




                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('company') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection


@section('customjs')

    <script src="{{ URL::asset('js/company.js') }}"></script>

@endsection