<!doctype html>
<html lang="en">
<head>
     
    <script>
       
        window.print();
        setTimeout(function() {
            window.location.href = "receiving";
        }, 20); 
        
        
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{ asset('css/printstyle.css') }}">
    <title>Receiving Receipt Print</title>
    <style type="text/css">
       /* body,#print {
           -webkit-transform: scale(0.85);  
                 -moz-transform: scale(0.85);  
                  -ms-transform: scale(0.85);  
                   -o-transform: scale(0.85);  
                      transform: scale(0.85);
            margin: 0px 0px 0;  
        }*/
        .headerimage {       
            height: auto;
            margin: 0 0 20px 0;
            margin-left: 17px;
        }
        .left{
            margin-left: 17px;
        }
        div.box1 {
            width: 51%;
            margin-left: 17px;
            display: inline-block;
        }
        div.box3 {
            width: 98%;
            margin-left: 17px;
            display: inline-block;
        }
        .page-break {
            page-break-after: always;
        }

        .header,
        .footer {
            width: 100%;
            text-align: left;
            position: fixed;
        }

        .header {
            position: fixed;
            top: -50px;
            left: 0px;
            right: 0px;
            height: 50px;
            padding: .5em;
            text-align: center;
        }

        .footer {
            bottom: 2em;
        }

        .pagenum:before {
            content: counter(page);
        }

        .no-border{
            border: none;
            margin-left: 17px;
        }

        .align-left{
            text-align: left;
        }

    </style>


</head>
<body>
<div id="print">
<div class="header">
    Page <span class="pagenum"></span>

</div>
<div class="contentbody">
    <div class="headerimage"><img src="{{ asset('public/img/logo.gif') }}"></div>
    
    <table class="left">
        <tr>
            <!-- <td colspan="1" class="no-border">
                <img src="{{ asset('public/img/logo.gif') }}">
            </td> -->
            <td class="no-border"><b style="font-size:16px;">33 Point 3 Export Inc.</b><br />
            Address: M.L. Quezon Street., Casuntingan Mandaue City 6014<br />
            Phone: (032) 344 8831</td>
        </tr>
    </table>

    <div class="left"><br />{{ $printdate }}<br /><br /></div>
    <div style="font-size:20px;margin-left: 17px;"><b><u>RECEIVING RECEIPT</u></b><br /><br /></div>
    <div class="box1">
        <table width="100%">
            <tr>
                <td class="no-border">RR Number: {{ $rcv->tnumber }}</td>
            </tr>
            <tr>
                <td class="no-border">RR Date: {{ $rcvdate }}</td>
            </tr>
            <tr>
                <td class="no-border">PO Number: {{ $rcv->ponumber }}</td>
            </tr>
            <tr>
                <td class="no-border">TRR Number: {{ $rcv->trrnumber }}</td>
            </tr>


        </table>
    </div>
    <div style="height:20px;"></div>
    <div>
        <div class="box1">
            <table width="100%">
                <tr>
                    <td class="no-border">Vendor: {{ $vendorname }}</td>
                </tr>
                <tr>
                    <td class="no-border">Invoice Number: {{ $rcv->invoicenumber }}</td>
                </tr>
                <tr>
                    <td class="no-border">Invoice Date: {{ $invoicedate }}</td>
                </tr>
            </table>
        </div>
    </div>

    <!-- <p align="justify">
        We hereby conformed to purchase from you the following merchandise to be shipped/delivered in good 
        condition and subject to the terms and particulars as stated herein.
    </p> -->

    <div class="left"><h3><b>Delivery Details:</b></h3></div>
    <div class="box3">
        <table>
            <tr>
                <td class="meta-head" style="text-align: center;">LNo</td>
                <td class="meta-head" style="text-align: center;">Item Description</td>
                <td class="meta-head" style="text-align: center;">Quantity</td>
                <td class="meta-head" style="text-align: center;">Packing</td>
                <td class="meta-head" style="text-align: center;">Unit Cost</td>
                <td class="meta-head" style="text-align: center;">Amount</td>
                <!-- <td class="meta-head" style="text-align: center;">RR Price</td> -->
                <!-- <td class="meta-head" style="text-align: center;">Qty Ordered</td> -->
                <!-- <td class="meta-head" style="text-align: center;">Qty Delivered</td> -->
                <!-- <td class="meta-head" style="text-align: center;">Total Price</td> -->
            </tr>
            
            @foreach($rcvdetail as $item =>$value)
                <tr>
                    <td style="text-align: center;">{{ $value->lno }}</td>
                    <td style="width:250px;">{{ $value->item()->first()->code }} - {{ $value->item()->first()->name }}</td>
                    <td style="text-align:center;width:50px;">{{ $value->qtyaccepted }}</td>
                    <td style="text-align:center;width:50px;">{{ $value->packing()->first()->description }}</td>
                    <td style="text-align:center;width:50px;">{{ number_format($value->poprice, 2, '.', ',') }}</td>
                    <td style="text-align:center;width:50px;">
                        <?php
                        $val = number_format($value->poprice, 2, '.', ',') * number_format($value->qtyaccepted, 2, '.', ',');

                        echo number_format($val, 2, '.', ',');

                        ?>
                    </td>
                    <!-- <td style="text-align:center;width:50px;">{{ number_format($value->rrprice, 2, '.', ',') }}</td> -->
                    <!-- <td style="text-align:right;width:50px;">{{ $value->qtyordered }}</td> -->
                    <!-- <td style="text-align:right;width:50px;">{{ $value->qtydelivered }}</td> -->
                    <!-- <td style="text-align:right;width:50px;">{{ number_format(($value->qtyaccepted*$value->rrprice),2,'.',',') }}</td> -->
                </tr> 
            @endforeach
                
            
            <tr>
                <td colspan="5" align="right">Grand Total</td>
                <td style="text-align: right;">{{ number_format($sumtotal->sum(),2,'.',',') }}</td>
            </tr>
        </table>
    </div>
    <div style="height:50px;"></div>
    <table class="left">
        <tr>
            
            <td style="width:300px;" class="no-border ">Prepared By</td>
            <td style="width:300px;" class="no-border ">Verified By</td>
            <td style="width:300px;" class="no-border">Approved By</td>
        </tr>
        <tr>
            <td style="width:300px;" class="no-border">
                _____________________
            </td>
            <td style="width:300px;" class="no-border">
                _____________________
            </td>
            <td style="width:300px;" class="no-border">
                _____________________
            </td>
        </tr>
        <tr>
            <td style="width:300px;" class="no-border">{{ $creatorfname }} {{ $creatorlname }}</td>
            <td style="width:300px;" class="no-border">{{ $approverfname }} {{ $approverlname }}</td>
            <td style="width:300px;" class="no-border">MARIBELLE F. CRUZ</td>
        </tr>

    </table>
    
    <div class="footer left">{{ $rcvstatus }}</div>
    
</div>
</div>


    

</body>
</html>