@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Receiving Transactions</h2>
            {!! Breadcrumbs::render('receiving') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
      <div class="row">
        <div class="col-lg-12">
          <div class="white-bg box-wrapper">
            <div class="box-tool-bar">
              <h5>Edit Receiving Transaction</h5>
            </div>

            <div class="box-content white-bg">
              <div class="row">
                <div class="col-lg-12">
                  <form class="form-horizontal" method="post" action="{{ route('receiving.update',$rr->tnumber) }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="col-lg-6">

                      <div class="form-group {{ $errors->has('ponumber') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">PO Number</label>
                        <div class="col-sm-10">
                          <div class="row">
                            <div class="col-sm-8">
                              <input type="text" name="ponumber" value="{{ $rr->ponumber }}" class="form-control">
                              <input type="hidden" name="tmode" value="{{ $rr->tmode }}">
                              @if ($errors->has('ponumber'))
                              <span class="help-block">
                                <strong>{{ $errors->first('ponumber') }}</strong>
                              </span>
                              @endif
                            </div>
                            <div class="col-sm-2 wd-cust">
                              <input class="btn btn-default btn-sm" name="verify" id="verify" type="button" value="Verify">
                            </div>
                            <div class="col-sm-2 wd-cust">
                              <input class="btn btn-default btn-sm" name="btnvendor" type="button" value="....">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('trrnumber') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">TRR Number</label>
                        <div class="col-sm-8">
                          <input type="text" name="trrnumber" value="{{ $rr->trrnumber }}" class="form-control" readonly>
                          @if ($errors->has('trrnumber'))
                          <span class="help-block">
                            <strong>{{ $errors->first('trrnumber') }}</strong>
                          </span>
                          @endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('_branch') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Branch</label>
                        <div class="col-sm-8">
                          <input type="hidden" name="branchidhidden" value="{{ $rr->_branch }}">
                          <input type="text" name="_branch" value="{{ $branchname }}" class="form-control" readonly>
                          @if ($errors->has('_branch'))
                          <span class="help-block">
                            <strong>{{ $errors->first('_branch') }}</strong>
                          </span>
                          @endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('_vendor') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Vendor</label>
                        <div class="col-sm-8">
                          <input type="hidden" name="vendoridhidden" value="{{ $rr->_vendor }}">
                          <input type="text" name="_vendor" value="{{ $vendorname }}" class="form-control" readonly>
                          @if ($errors->has('_vendor'))
                          <span class="help-block">
                            <strong>{{ $errors->first('_vendor') }}</strong>
                          </span>
                          @endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('invoicenumber') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Invoice No.</label>
                        <div class="col-sm-8">
                          <input type="text" name="invoicenumber" value="{{ $rr->invoicenumber }}" class="form-control">
                          @if ($errors->has('invoicenumber'))
                          <span class="help-block">
                            <strong>{{ $errors->first('invoicenumber') }}</strong>
                          </span>
                          @endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Particulars</label>

                        <div class="col-sm-8">
                          <textarea name="notes" value="{{ old('notes') }}" class="form-control" rows="3">{{ $rr->notes }}</textarea>
                          @if ($errors->has('notes'))
                            <span class="help-block">
                          <strong>{{ $errors->first('notes') }}</strong>
                          </span>
                          @endif
                        </div>
                      </div>

                      

                    </div>

                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">RR Number</label>

                        <div class="col-sm-8">
                            <input type="text" name="tnumber" value="{{ $rr->tnumber }}" readonly class="form-control">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">RR Date</label>

                        <div class="col-sm-8">
                            <input type="text" name="tdate" value="{{ $rr->tdate }}" readonly class="form-control">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Status</label>

                        <div class="col-sm-8">
                            <input type="text" name="status" value="{{ $rrstatus }}" readonly class="form-control">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Invoice Date</label>

                        <div class="col-sm-8">
                            <input type="text" name="deliverydate" value="{{ $rr->invoicedate }}" class="form-control">
                        </div>
                      </div>

                    </div>

                    <div class="col-lg-12">
                      <div class="table-responsive">
                        <table width="100%" class="table table-bordered table-striped" id="tablereceivepodetail">
                          <thead>
                            <tr>
                              <th>LNo</th>
                              <th>Item Description</th>
                              <th>Unit</th>
                              <th>PO Price</th>
                              <th>RR Price</th>
                              <th>Qty Ordered</th>
                              <th>Qty Delivered</th>
                              <th>Qty Accepted</th>
                              <th>Balance</th>
                              <th>Total Price</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($rrdetail as $rrdetails=>$value)
                              <tr>
                                <td>
                                  {{ $value->lno }} 
                                  <input type="checkbox" style="display:none" id="rcvdetail{{$value->lno}}" name="rcvdetail[]" value="{{$value->lno}},{{$value->_item}},{{$value->packing}},{{$value->poprice}},{{$value->rrprice}},{{$value->qtyordered}},{{$value->qtydelivered}},{{$value->qtyaccepted}},{{$value->balance}}" checked>
                                </td>
                                <td>
                                  {{ $value->item()->first()->code }} - {{ $value->item()->first()->name }}
                                  <input type="hidden" id="item{{$value->lno}}" value="{{$value->_item}}">
                                </td>
                                <td>
                                  {{ $value->packing()->first()->description }}
                                  <input type="hidden" id="packing{{$value->lno}}" value="{{$value->packing}}">
                                </td>
                                <td>
                                  {{ number_format($value->poprice, 2, '.', ',')}}
                                  <input type="hidden" id="poprice{{$value->lno}}" value="{{$value->poprice}}">
                                </td>
                                <td class="tdrrprice{{$value->lno}}">
                                  {{ number_format($value->rrprice, 2, '.', ',') }}
                                  <input type="hidden" id="rrprice{{$value->lno}}" value="{{$value->rrprice}}">
                                </td>
                                <td>
                                  {{ $value->qtyordered }}
                                  <input type="hidden" id="qtyordered{{$value->lno}}" value="{{$value->qtyordered}}">
                                </td>
                                <td class="tdqtydel{{$value->lno}}">
                                  {{ $value->qtydelivered }}
                                  <input type="hidden" id="qtydelivered{{$value->lno}}" value="{{$value->qtydelivered}}">
                                </td>
                                <td class="tdqtyacc{{$value->lno}}">
                                  {{ $value->qtyaccepted }}
                                  <input type="hidden" id="qtyaccepted{{$value->lno}}" value="{{$value->qtyaccepted}}">
                                </td>
                                <td class="tdbalance{{$value->lno}}">
                                  {{ $value->balance }}
                                  <input type="hidden" id="balance{{$value->lno}}" value="{{$value->balance}}">
                                </td>
                                <td class="tdtotal{{$value->lno}}">
                                  {{ number_format(($value->qtyaccepted*$value->rrprice),2,'.',',') }}
                                  <input type="hidden" id="totalcost{{$value->lno}}" value="{{ $value->rrprice * $value->qtyaccepted }}">
                                </td>
                                <td>
                                  <a class="btn btn-xs btn-primary" onclick="editLineItem({{$value->lno}})" data-toggle="modal" data-target="#myModal" style="cursor:pointer;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>

                      <!-- <div class="form-group">
                        <div class="col-sm-8 mg-top">
                          <button type="submit" class="btn btn-primary">Save </button>
                        </div>
                      </div> -->
                      <div class="form-group">
                        <div class="col-sm-8 mg-top">
                          <button type="submit" class="btn btn-primary">Save </button>
                            @if($rr->status !=='C')
                              <a href="{{ route('createrr',['tnumber'=>$rr->tnumber]) }}" class="btn btn-primary" name="submit" target="_blank">Print</a>
                              <!-- <button class="btn btn-primary" onclick="window.open()"></button> -->
                             
                            @elseif($rr->status === 'A')
                              <a href="{{ route('createrr',['tnumber'=>$rr->tnumber]) }}" target="_blank" style="display:none;" class="btn btn-primary" name="submit">Print</a>
                              
                            @endif
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-8 mg-top">
                            @if($rr->status ==='D')
                                <input type="submit" class="btn btn-primary" value="Save as Draft">
                                <a href="{{ route('submitrr',['tnumber'=>$rr->tnumber]) }}" class="btn btn-primary" name="submit" >Submit</a>
                                <input type="button" class="btn btn-danger"  name="cancel" value="Cancel">
                             @elseif($rr->status ==='S')
                                <a href="{{ route('approverr',['tnumber'=>$rr->tnumber]) }}" class="btn btn-primary" name="submit" >Approve</a>
                                <a href="{{ route('rollbackrr',['tnumber'=>$rr->tnumber]) }}" class="btn btn-primary" name="submit" >Rollback</a>
                                <input type="button" class="btn btn-danger"  name="cancel" value="Cancel">
                             @elseif($rr->status ==='A')
                                <input type="button" class="btn btn-danger"  name="cancel" value="Cancel">
                                <a href="{{ route('createrr',['tnumber'=>$rr->tnumber]) }}" target="_blank" class="btn btn-primary" name="submit" >Create Receiving Report</a>
                             @endif

                        </div>
                    </div>
                    </div>



                    <div class="hr-line-dashed"></div>

                  </form>
                </div>
              </div>

              

            </div>

            
          </div>

        </div>

      </div>


        <!-- Modal -->
        <div class="modal fade" id="vendorlistmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">PO Lookup</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablereceivinglist">
                                <thead>
                                <tr>
                                    <th>PO Number</th>
                                    <th>JO Number</th>
                                    <th>TRR Number</th>
                                    <th>PO Date</th>
                                    <th>Vendor ID</th>
                                    <th>Vendor</th>
                                    <th>Branch ID</th>
                                    <th>Branch</th>
                                    <th>Particulars</th>
                                    <th>Type</th>
                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Line Item</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <input type="hidden" name="rr" id="rr" value="{{$rr->tnumber}}">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Item Description</label>
                    <div class="col-sm-10">
                      <input type="hidden" id="editlno" value="">
                      <input type="text" class="form-control" id="itemdesc" name="item" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">RR Price</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="editrrprice">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Qty Delivered</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="editqtyreceived" name="delivered">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Qty Accepted</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="editqtyaccepted" name="accepted">
                    </div>
                  </div>
                   </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="saveLineItem" class="btn btn-primary">Save changes</button>
               
              </div>
            </div>
          </div>
        </div>




    </div>


@endsection


@section('customjs')
    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

   

    <script src="{{ URL::asset('js/editreceiving.js') }}"></script>

@endsection