@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Receiving Transactions</h2>
            {!! Breadcrumbs::render('receiving') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
      <div class="row">
        <div class="col-lg-12">
          <div class="white-bg box-wrapper">
            <div class="box-tool-bar">
              <h5>Create New Receiving Transaction</h5>
            </div>

            <div class="box-content white-bg">
              <div class="row">
                <div class="col-lg-12">
                  <form class="form-horizontal" method="post" action="{{ route('receiving.store') }}">
                    {{ csrf_field() }}

                    <div class="col-lg-6">

                      <div class="form-group {{ $errors->has('ponumber') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">PO Number</label>
                        <div class="col-sm-10">
                          <div class="row">
                            <div class="col-sm-8">
                              <input type="text" name="ponumber" value="{{ old('ponumber') }}" class="form-control">
                              <input type="hidden" name="tmode">
                              @if ($errors->has('ponumber'))
                              <span class="help-block">
                                <strong>{{ $errors->first('ponumber') }}</strong>
                              </span>
                              @endif
                            </div>
                            <div class="col-sm-2 wd-cust">
                              <input class="btn btn-default btn-sm" name="verify" id="verify" type="button" value="Verify">
                            </div>
                            <div class="col-sm-2 wd-cust">
                              <input class="btn btn-default btn-sm" name="btnvendor" type="button" value="....">
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('trrnumber') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">TRR Number</label>
                        <div class="col-sm-8">
                          <input type="text" name="trrnumber" value="{{ old('trrnumber') }}" class="form-control" readonly>
                          @if ($errors->has('trrnumber'))
                          <span class="help-block">
                            <strong>{{ $errors->first('trrnumber') }}</strong>
                          </span>
                          @endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('_branch') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Branch</label>
                        <div class="col-sm-8">
                          <input type="hidden" name="branchidhidden">
                          <input type="text" name="_branch" value="{{ old('_branch') }}" class="form-control" readonly>
                          @if ($errors->has('_branch'))
                          <span class="help-block">
                            <strong>{{ $errors->first('_branch') }}</strong>
                          </span>
                          @endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('_vendor') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Vendor</label>
                        <div class="col-sm-8">
                          <input type="hidden" name="vendoridhidden">
                          <input type="text" name="_vendor" value="{{ old('_vendor') }}" class="form-control" readonly>
                          @if ($errors->has('_vendor'))
                          <span class="help-block">
                            <strong>{{ $errors->first('_vendor') }}</strong>
                          </span>
                          @endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('invoicenumber') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Invoice No.</label>
                        <div class="col-sm-8">
                          <input type="text" name="invoicenumber" value="{{ old('invoicenumber') }}" class="form-control">
                          @if ($errors->has('invoicenumber'))
                          <span class="help-block">
                            <strong>{{ $errors->first('invoicenumber') }}</strong>
                          </span>
                          @endif
                        </div>
                      </div>

                      <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Particulars</label>

                        <div class="col-sm-8">
                          <textarea name="notes" value="{{ old('notes') }}" class="form-control" rows="3"></textarea>
                          @if ($errors->has('notes'))
                            <span class="help-block">
                          <strong>{{ $errors->first('notes') }}</strong>
                          </span>
                          @endif
                        </div>
                      </div>

                      

                    </div>

                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="col-sm-4 control-label">RR Number</label>

                        <div class="col-sm-8">
                            <input type="text" name="tnumber"  readonly class="form-control">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">RR Date</label>

                        <div class="col-sm-8">
                            <input type="text" name="tdate"  readonly class="form-control">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Status</label>

                        <div class="col-sm-8">
                            <input type="text" name="status"  readonly class="form-control">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-4 control-label">Invoice Date</label>

                        <div class="col-sm-8">
                            <input type="text" name="deliverydate" class="form-control">
                        </div>
                      </div>

                    </div>

                    <div class="col-lg-12">
                      <div class="table-responsive">
                        <table width="100%" class="table table-bordered table-striped" id="tablereceivepodetail">
                          <thead>
                            <tr>
                              <th>LNo</th>
                              <th>Item Description</th>
                              <th>Unit</th>
                              <th>PO Price</th>
                              <th>RR Price</th>
                              <th>Qty Ordered</th>
                              <th>Qty Delivered</th>
                              <th>Qty Accepted</th>
                              <th>Balance</th>
                              <th>Total Price</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-8 mg-top">
                          <button type="submit" class="btn btn-primary">Save </button>
                        </div>
                      </div>
                    </div>



                    <div class="hr-line-dashed"></div>

                  </form>
                </div>
              </div>

              

            </div>

            
          </div>

        </div>

      </div>


        <!-- Modal -->
        <div class="modal fade" id="vendorlistmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">PO Lookup</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablereceivinglist">
                                <thead>
                                <tr>
                                    <th>PO Number</th>
                                    <th>JO Number</th>
                                    <th>TRR Number</th>
                                    <th>PO Date</th>
                                    <th>Vendor ID</th>
                                    <th>Vendor</th>
                                    <th>Branch ID</th>
                                    <th>Branch</th>
                                    <th>Particulars</th>
                                    <th>Type</th>
                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Line Item</h4>
              </div>
              <div class="modal-body">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Item Description</label>
                    <div class="col-sm-10">
                      <input type="hidden" id="editlno" value="">
                      <input type="text" class="form-control" id="itemdesc" readonly>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">RR Price</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="editrrprice">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Qty Delivered</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="editqtyreceived">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Qty Accepted</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="editqtyaccepted">
                    </div>
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="saveLineItem" class="btn btn-primary">Save changes</button>
              </div>
            </div>
          </div>
        </div>




    </div>


@endsection


@section('customjs')
    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    <script src="{{ URL::asset('js/createreceiving.js') }}"></script>

@endsection