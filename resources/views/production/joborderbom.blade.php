@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Production Status and Control</h2>
            {!! Breadcrumbs::render('production') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="box-tool-bar">
                        <h5>Job Order B.O.M.</h5>
                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table width="100%" class="table table-bordered table-striped" id="prod-jobom">
                                        <thead>
                                        <tr>
                                            <th>LNo</th>
                                            <th>Raw Material</th>
                                            <th>Packing</th>
                                            <th>BOM Qty</th>
                                            <th>PO Qty</th>
                                            <th>Rcvd Qty</th>

                                        </tr>
                                        </thead>
                                        
                                    </table>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-8 mg-top">    
                                        <a href="{{ route('production') }}" class="btn btn-danger" name="submit" >Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection

@section('customjs')

    <script src="{{ URL::asset('js/productionjobom.js') }}"></script>


@endsection