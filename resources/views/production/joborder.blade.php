@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Production Status and Control</h2>
            {!! Breadcrumbs::render('production') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="box-tool-bar">
                        <h5>Job Order Details</h5>
                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            <div class="col-lg-12">
                                <form  class="form-horizontal">
                                    <input type="hidden" name="_method" value="PUT">

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Branch</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="reference" value="{{ $jo->_branch }}" class="form-control" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Customer</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="reference" value="{{ $jo->_customer }}" class="form-control" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Reference</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="reference" value="{{ $jo->reference }}" class="form-control" readonly>
                                            </div>
                                        </div>

                                        <!-- <div class="form-group">
                                            <label class="col-sm-2 control-label">Payment Term</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="reference" value="" class="form-control" readonly>
                                            </div>
                                        </div> -->

                                        <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Note</label>

                                            <div class="col-sm-8">
                                                <textarea name="notes"  class="form-control" rows="3" readonly> {{ $jo->notes }}</textarea>
                                            </div>
                                        </div>




                                    </div>


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">JO Number</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tnumber" value="{{ $jo->tnumber }}" readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">JO Date</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tdate" value="{{ $jo->tdate }}"  readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Status</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="status" value="{{ $jo->status }}"  readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Delivery Date</label>

                                            <div class="col-sm-8">
                                                
                                                <input type="text" name="deliverydate" value="{{ $jo->deliverydate }}" class="form-control" readonly>
                                                
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="box-tool-bar">
                                            <h5>Products Ordered</h5>
                                        </div>
                                        <div class="table-responsive">
                                            <table width="100%" class="table table-bordered table-striped" id="tablereceivepodetail">
                                              <thead>
                                                <tr>
                                                  <th>LNo</th>
                                                  <th>Product</th>
                                                  <th>Packing</th>
                                                  <th>Qty Ordered</th>
                                                  <th>Qty Completed</th>
                                                </tr>
                                              </thead>
                                              <tbody>
                                                <?php $i = 1;?>
                                                @foreach($jodetail as $rrdetails=>$value)
                                                  <tr>
                                                    <td>
                                                      {{ $i }}
                                                    </td>
                                                    <td>
                                                      {{ $value->item()->first()->code }} - {{ $value->item()->first()->name }}
                                                    </td>
                                                    <td>
                                                      {{ $value->packing()->first()->description }}
                                                    </td>
                                                    <td>
                                                      {{ $value->quantity }}
                                                    </td>
                                                    <td class="tdqtydel{{$value->lno}}">
                                                      {{ $value->quantity }} 
                                                    </td>
                                                  </tr>
                                                <?php $i++ ?>
                                                @endforeach
                                              </tbody>
                                            </table>
                                          </div>

                                          <div class="form-group">
                                            <div class="col-sm-8 mg-top">
                                                
                                                <a href="{{ URL::to('production/'.$jo->tnumber.'/joborderbom') }}" class="btn btn-primary" name="submit" >B.O.M.</a>
                                                <a href="{{ route('production') }}" class="btn btn-danger" name="submit" >Back</a>
                                                    

                                            </div>
                                          </div>
                                    </div>
                                    
                                </form>
                            </div>




                        </div>


                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection

@section('customjs')

    <script src="{{ URL::asset('js/production.js') }}"></script>


@endsection