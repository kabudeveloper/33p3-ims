@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Production Status and Control</h2>
            {!! Breadcrumbs::render('production') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Production Status and Control
                            <small></small>
                        </h5>
                        
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="production-table">
                                <thead>
                                <tr>
                                    <th>LNo</th>
                                    <th>JO Date</th>
                                    <th>JO Number</th>
                                    <th>Customer</th>
                                    <th>Status</th>
                                    <th width="35%">Action</th>

                                </tr>
                                </thead>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection

@section('customjs')

    <script src="{{ URL::asset('js/production.js') }}"></script>


@endsection