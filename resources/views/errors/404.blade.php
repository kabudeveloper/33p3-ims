<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>404</title>


    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('public/css/font-awesome.min.css') }}">
</head>
<body class="error">
<div class="container">
    <div class="col-lg-8 col-lg-offset-2 text-center">
        <div class="logo">
            <h1>0ops!</h1>
        </div>
        <p class="lead text-muted">We can't seem to find the page you are looking for.</p>
        <div class="clearfix"></div>
        <div class="col-lg-6 col-lg-offset-3">

        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-lg-6 col-lg-offset-3">

        </div>
    </div><!-- /.col-lg-8 col-offset-2 -->
</div>
</body>
</html>