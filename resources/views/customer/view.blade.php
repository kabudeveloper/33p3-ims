@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Companies</h2>
            {!! Breadcrumbs::render('customer') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>View Customer</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form  class="form-horizontal" method="POST" action="{{ route('customer.update',$company->id) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">
                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code"  value="{{ $company->code }}" class="form-control" readonly>

                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ $company->name }}" class="form-control" readonly>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('billing_address') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Billing Address</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="billing_address" value="{{ $company->billing_address }}" class="form-control" readonly>
                                            @if ($errors->has('billing_address'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('billing_address') }}</strong>
                                                  </span>
                                            @endif

                                        </div>
                                    </div>

                                      <div class="form-group {{ $errors->has('delivery_address') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Delivery Address</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="delivery_address" value="{{ $company->delivery_address }}" class="form-control" readonly>
                                              @if ($errors->has('delivery_address'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('delivery_address') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_paymenttterm') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Payment Term</label>

                                          <div class="col-sm-8">
                                              <select name="_paymentterm" class="form-control" disabled>
                                                  <option></option>
                                                  @foreach($terms as $term)
                                                      <option value="{{ $term->id }}" {{ ($term->id == $company->_paymentterm) ? "selected=selected" : '' }}>{{ $term->description }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_paymentterm'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_paymentterm') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                      <div class="form-group {{ $errors->has('zipcode') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Zip code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="zipcode" value="{{ $company->zipcode }}" class="form-control" readonly>
                                              @if ($errors->has('zipcode'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('zipcode') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('tin') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Tin</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="tin" value="{{ $company->tin }}" class="form-control" readonly>
                                              @if ($errors->has('tin'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('tin') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_currency') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Currency</label>

                                          <div class="col-sm-8">
                                              <select name="_currency" class="form-control" disabled>
                                                  <option></option>
                                                  @foreach($currencies as $currency)
                                                      <option value="{{ $currency->id }}" {{ ($currency->id == $company->_currency) ? "selected=selected" : '' }}>{{ $currency->symbol }} - {{ $currency->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_currency'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_currency') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($company->isactive)
                                                          <input type="checkbox" value="1" checked name="isactive" disabled>  Active
                                                      @else
                                                          <input type="checkbox" name="isactive" disabled>  Active

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>










                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('customer') }}" class="btn btn-white">Back</a>
                                            <!-- <button type="submit" class="btn btn-primary">Save </button> -->
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Contacts
                            <small></small>
                        </h5>
                        <div class="ibox-tools">

                            <!-- <a class="btn btn-primary btn-rounded btn-sm" href="{{ URL::to('customer/'.$company->id.'/contact/create') }}">Add Contacts</a> -->

                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="contacts-table">
                                <thead>
                                <tr>

                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Mobile</th>
                                    <th>Landline</th>
                                    <th>Fax</th>
                                    <th>Email</th>
                                    <th>Primary</th>
                                    <th></th>

                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection




@section('customjs')

    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
        var CUST_ID = '{{ $company->id }}';
    </script>

    <script src="{{ URL::asset('js/customer.js') }}"></script>
    <script src="{{ URL::asset('js/customer-contact.js') }}"></script>


@endsection