@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Location</h2>
            {!! Breadcrumbs::render('location') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Location</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form class="form-horizontal" method="post" action="{{ route('location.store') }}">
                                      {{ csrf_field() }}
                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code"  value="{{ old('code') }}" class="form-control">
                                              @if ($errors->has('code'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('code') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Location</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ old('name') }}" class="form-control">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                      <div class="form-group {{ $errors->has('_branch') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Select Branch</label>

                                          <div class="col-sm-8">
                                              <select name="_branch" class="form-control">
                                                  <option></option>
                                                  @foreach($maincompany as $main)
                                                      <option value="{{ $main->id }}">{{ $main->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_branch'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_branch') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_type') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Location Types</label>

                                          <div class="col-sm-8">
                                              <select name="_type" class="form-control">
                                                  <option></option>
                                                  @foreach($locationtypes as $type)
                                                      <option value="{{ $type->id }}">{{ $type->description }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_type'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_type') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>
                                      <div class="form-group {{ $errors->has('glcode') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">GL Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="glcode"  value="{{ old('glcode') }}" class="form-control">
                                              @if ($errors->has('glcode'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('glcode') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('location') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection


@section('customjs')

    <script src="{{ URL::asset('js/location.js') }}"></script>

@endsection