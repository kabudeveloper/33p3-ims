<!doctype html>
<html lang="en">
<head>
     <script>
        window.print();
        setTimeout(function() {
            window.location.href = "salesorder";
        }, 20); 
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{ asset('css/printstyle.css') }}">
    <title>Temporary Receiving Report</title>
    <style type="text/css">
        .left{
            margin-left: 17px;
        }
        .page-break {
            page-break-after: always;
        }

        .header,
        .footer {
            width: 100%;
            text-align: left;
            position: fixed;
        }

        .header {
            position: fixed;
            top: -100px;
            left: 0px;
            right: 0px;
            height: 100px;
            padding: .5em;
            text-align: center;
        }

        .footer {
            bottom: 2em;
        }

        .pagenum:before {
            content: counter(page);
        }

        .no-border{
            border: none;
        }

        .align-left{
            text-align: left;
        }

    </style>


</head>
<body>
<div class="header">
    Page <span class="pagenum"></span>

</div>
<div class="contentbody">
    <table>
        <tr>
            <td colspan="1" class="no-border">
                <img class="left" src="{{ asset('public/img/logo.gif') }}">
            </td>
            <td class="no-border"><b style="font-size:16px;">33 Point 3 Export Inc.</b><br />
            Address: M.L. Quezon Street., Casuntingan Mandaue City 6014<br />
            Phone: (032) 344 8831</td>
        </tr>
    </table>
    <div class="left"><br />{{ $printdate }}<br /><br /></div>
    <div class="left" style="font-size:20px;"><b><u>TEMPORARY RECEIVING REPORT</u></b><br /><br /></div>
    <div class="box1 left">
        <table width="100%">
            <tr>
                <td class="no-border" style="width:150px;">Supplier's Name: </td>
                <td class="no-border" style="width:150px;">{{ $vendorname }}</td>
                <td class="no-border" style="width:150px;">TRR No.: </td>
                <td class="no-border" style="width:150px;">{{ $vtrr }}</td>
            </tr>
            <tr>
                <td class="no-border">PO No.: </td>
                <td class="no-border">{{ $po->tnumber }}</td>
                <!-- <td class="no-border">Subject to Quality Check: </td>
                <td class="no-border"></td> -->
                <td class="no-border">Due Date: </td>
                <td class="no-border">{{ $podeldate }}</td>
            </tr>
            <!-- <tr>
                <td class="no-border">Due Date: </td>
                <td class="no-border">{{ $podeldate }}</td>
            </tr> -->
        </table>
    </div>

    <div></div><br />
    <div class="box1 left">
        <table>
            <tr>
                <td class="meta-head" style="text-align:center;width:60px;">Code</td>
                <td class="meta-head" style="text-align:center;width:150px;">Item Description</td>
                <td class="meta-head" style="text-align:center;">Ordered Quantity</td>
                <td class="meta-head" style="text-align:center;">Quantity Received</td>
                <td class="meta-head" style="text-align:center;">Quantity Accepted</td>
                <td class="meta-head" style="text-align:center;center;width:60px;">Balance</td>
                <td class="meta-head" style="text-align:center;width:250px;">Disposition</td>
            </tr>
            
                @foreach($podetail as $item =>$value)
                <tr>
                    <td style="text-align:center;height:30px;">{{ $value->item()->first()->code }}</td>
                    <td>{{ $value->item()->first()->name }}</td>
                    <td style="text-align:center;">{{ $value->quantity }}</td>
                    <td style="text-align:right;"></td>
                    <td style="text-align:center;"></td>
                    <td style="text-align:center;">{{ $value->balance }}</td>
                    <td></td>
                </tr> 
                @endforeach
                
            
            
        </table>
    </div>

    <p></p>
    <br />

    <table class="left">
        <tr>
            <td style="width:150px;" class="no-border">Items Received by: </td>
            <td style="width:250px;" class="no-border">________________________________</td>
            <td class="no-border">Date: </td>
            <td style="width:250px;" class="no-border">________________________________</td>
        </tr>
        <tr>
            <td class="no-border"><div style="height:20px;"></div></td>
        </tr>
        <tr>
            <td style="width:150px;" class="no-border">Items Quality Checked by: </td>
            <td style="width:250px;" class="no-border">________________________________</td>
            <td class="no-border">Date: </td>
            <td style="width:250px;" class="no-border">________________________________</td>
        </tr>
        <tr>
            <td class="no-border"><div style="height:20px;"></div></td>
        </tr>
        <tr>
            <td style="width:150px;" class="no-border">Recommended by: </td>
            <td style="width:250px;" class="no-border">________________________________</td>
            <td class="no-border">Date: </td>
            <td style="width:250px;" class="no-border">________________________________</td>
        </tr>
        <tr>
            <td class="no-border"><div style="height:20px;"></div></td>
        </tr>
        <tr>
            <td style="width:150px;" class="no-border">Approved by: </td>
            <td style="width:250px;" class="no-border">________________________________</td>
            <td class="no-border">Date: </td>
            <td style="width:250px;" class="no-border">________________________________</td>
        </tr>
    </table>
    

    <!-- <div class="footer">{{ $postatus }}</div> -->
    
</div>

<!-- <div class="footer">
    <table width="100%">
        <tr>
            <th>Monitoring</th>
            <th>Marketing User</th>
            <th>Owner Name</th>
        </tr>
        <tr>
            <td style="text-align: center;">Prepared By</td>
            <td style="text-align: center;">Reviewed By</td>
            <td style="text-align: center;">Approved By</td>
        </tr>
    </table>
</div> -->

</body>
</html>