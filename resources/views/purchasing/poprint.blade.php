<!doctype html>
<html lang="en">
<head>
     <script>
        window.print();
       
        window.onafterprint = function(e){
            closePrint();
        };
        function closePrint(){
            window.location.href = "purchasing";
        }
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{ asset('css/printstyle.css') }}">
    <title>Purchase Order Print</title>
    <style type="text/css">
        .left{
            margin-left: 17px;
        }
        .page-break {
            page-break-after: always;
        }

        .header,
        .footer {
            width: 100%;
            text-align: left;
            position: fixed;
        }

        .header {
            position: fixed;
            top: -50px;
            left: 0px;
            right: 0px;
            height: 50px;
            padding: .5em;
            text-align: center;
        }

        .footer {
            bottom: 2em;
        }

        .pagenum:before {
            content: counter(page);
        }

        .no-border{
            border: none;
        }

        .align-left{
            text-align: left;
        }

    </style>


</head>
<body>
<div class="header">
    Page <span class="pagenum"></span>

</div>
<div class="contentbody">
    <table>
        <tr>
            <td colspan="1" class="no-border">
                <img class="left" src="{{ asset('public/img/logo.gif') }}">
            </td>
            <td class="no-border"><b style="font-size:16px;">33 Point 3 Export Inc.</b><br />
            Address: M.L. Quezon Street., Casuntingan Mandaue City 6014<br />
            Phone: (032) 344 8831</td>
        </tr>
    </table>
    <div class="left"><br />{{ $printdate }}<br /><br /></div>
    <div class="left" style="font-size:20px;"><b><u>PURCHASE ORDER</u></b><br /><br /></div>
    <div class="left">
        <table width="100%">
             <tr>
                <td class="no-border">Reference Number: {{ $po->jonumber }}</td>
            </tr>
            <tr>
                <td class="no-border">PO Number: {{ $po->tnumber }}</td>
            </tr>
            <tr>
                <td class="no-border"><img src="data:image/png;base64,{{DNS1D::getBarcodePNG($po->tnumber, 'C39')}}" alt="barcode" /></td>
            </tr>
            <tr>
                <td class="no-border">Payment Term: {{ $paymenttermdesc }}</td>
            </tr>
            <tr>
                <td class="no-border">Delivery Date: {{ $podeldate }}</td>
            </tr>


        </table>
    </div>
    <div class="left">
        <p><div>Contact Name: {{ $contactname }}</div>
        <div>Supplier's Name: {{ $vendorname }}</div>
        <div>Supplier's Address: {{ $vendoraddress }}</div>
        <div>Contact Numbers: {{ $contactmobile }}</div>
        <div>Email Address: {{ $contactemail }}</div></p>
    </div>
    <p align="justify" class="left">
        We hereby conformed to purchase from you the following merchandise to be shipped/delivered in good
        condition and at the requested delivery date, subject to the terms and particulars as stated herein.
    </p>

    <div class="left"><h3><b>Order Details:</b></h3></div>
    <div class="left">
        <table width="100%">
            <tr>
                <td class="meta-head" style="text-align: center;">LNo</td>
                <td class="meta-head" style="text-align: center;">Item Description</td>
                <td class="meta-head" style="text-align: center;">Unit</td>
                <td class="meta-head" style="text-align: center;">Quantity</td>
                <td class="meta-head" style="text-align: center;">Unit Price</td>
                <td class="meta-head" style="text-align: center;">Total Amount</td>
            </tr>
            
                @foreach($podetail as $item =>$value)
                <tr>
                    <td style="text-align: center;width:10px;">{{ ++$item }}</td>
                    <td style="width:380px;">{{ $value->item()->first()->code }} - {{ $value->item()->first()->name }}</td>
                    <td style="text-align: center;width:60px;">{{ $value->packing()->first()->description }}</td>
                    <td style="text-align: center;width:60px;">{{ $value->quantity }}</td>
                    <td style="text-align: right;width:60px;">{{ number_format($value->unitprice, 2, '.', ',') }}</td>
                    <td style="text-align: right;width:80px;">{{number_format($value->total_price,2,'.',',')   }}</td>
                </tr> 
                @endforeach
                
            
            <tr>
                <td colspan="5" align="right">Grand Total</td>
                <td style="text-align: right;">{{ number_format($sumtotal->sum(),2,'.',',') }}</td>
            </tr>
        </table>
    </div>

    <div class="left">
        <br />Instructions:
        <ul>
            @foreach($trninstruct as $item =>$value)
                @foreach($poinstruct as $poinstruction)
                    @if($value->_instruction == $poinstruction->id)
                        <li>{{ $poinstruction->instruction }}</li>
                    @endif
                @endforeach
            @endforeach
        </ul>
    </div>

    <p class="left">Respectfully Yours,</p>

    <table class="left" width="100%">
        <tr>
            <td style="width:300px;" class="no-border">Prepared By</td>
            <td style="float: right; margin-right: 83px;" class="no-border">Noted By</td>
            
        </tr>
        <tr>
            <td style="width:300px;" class="no-border">
                <!-- <div style="height:7px;"></div> -->
                ____________
            </td>
            <td style="float: right;margin-right: 40px;" class="no-border left">
                <!-- <div style="height:7px;"></div> -->
                ____________
            </td>
        </tr>
        <tr>
            <!-- <td style="width:300px;" class="no-border">{{ $creatorfname }} {{ $creatorlname }}</td> -->
            <td style="width:300px;" class="no-border">Joelyn Mae Arnaiz</td>
            <!-- <td style="width:300px;" class="no-border">{{ $approverfname }} {{ $approverlname }}</td> -->
            <td style="float: right;margin-right: 4px;" class="no-border">Monitoring Department</td>
        </tr>

    </table>
    <div style="height:20px;"></div>
    <table class="left" width="100%">
        <tr>
            <td style="width:300px;" class="no-border">Received By</td>
            <td style="float: right; margin-right:65px;" class="no-border">Approved By</td>

        </tr>
        <tr>
            <td style="width:300px;" class="no-border">
                <!-- <div style="height:7px;"></div> -->
                ____________
            </td>
            <td style="float: right; margin-right:39px;" class="no-border">
                <!-- <div style="height:7px;"></div> -->
                ____________
            </td>
        </tr>
        <tr>
            <td style="width:300px;" class="no-border">Vendor's Representative</td>
            <td style="float: right; margin-right:42px;" class="no-border">Maribelle F. Cruz</td>
        </tr>
    </table>

    <div class="footer left">{{ $postatus }}</div>
    
</div>

<!-- <div class="footer">
    <table width="100%">
        <tr>
            <th>Monitoring</th>
            <th>Marketing User</th>
            <th>Owner Name</th>
        </tr>
        <tr>
            <td style="text-align: center;">Prepared By</td>
            <td style="text-align: center;">Reviewed By</td>
            <td style="text-align: center;">Approved By</td>
        </tr>
    </table>
</div> -->

</body>
</html>