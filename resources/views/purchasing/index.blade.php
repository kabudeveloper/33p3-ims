@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Purchase Orders</h2>
            {!! Breadcrumbs::render('purchasing') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Purchasing
                            <small></small>
                        </h5>
                        <div class="ibox-tools">
                            <a class="btn btn-primary btn-sm" href="{{ URL::to('purchasing/create') }}">Create Purchase Order</a>
                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="purchaseorder-table">
                                <thead>
                                <tr>
                                    <th>PO #</th>
                                    <th>Date</th>
                                    <th>Vendor</th>
                                    <th>Particulars</th>
                                    <th>Status</th>
                                    <th>Action</th>

                                </tr>
                                </thead>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection

@section('customjs')

    <script src="{{ URL::asset('js/purchasing.js') }}"></script>


@endsection