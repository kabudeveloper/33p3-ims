@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Purchase Orders</h2>
            {!! Breadcrumbs::render('purchasing') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Purchase Order</h5>

                    </div>
                    <div class="box-content white-bg">
                        <div class="row">


                                <div class="col-lg-12">

                                <form  class="form-horizontal" method="POST" action="{{ route('purchasing.update',$po->tnumber) }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="_method" value="PUT">


                                    <div class="col-lg-6">
                                        <div class="form-group {{ $errors->has('_branch') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Branch</label>

                                            <div class="col-sm-8">
                                                @if ($po->status ==='D')
                                                <select name="_branch" class="form-control">
                                                @elseif ($po->status !=='D')
                                                <select name="_branch" class="form-control" disabled>
                                                @endif
                                                    <option></option>
                                                    @foreach($branches as $branch)
                                                        <option value="{{ $branch->id }}"  {{ ($branch->id == $po->_branch) ? "selected=selected": '' }}>{{ $branch->name }}</option>
                                                    @endforeach

                                                </select>
                                                @if ($errors->has('_branch'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('_branch') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('_vendor') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Vendor</label>

                                            <div class="col-sm-8">

                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <input type="hidden" name="vendoridhidden">
                                                        <input type="hidden" name="tmode">
                                                        <input type="text" name="_vendor" value="{{ $vendorname }}" readonly class="form-control">
                                                        @if ($errors->has('_vendor'))
                                                            <span class="help-block">
                                                  <strong>{{ $errors->first('_vendor') }}</strong>
                                                  </span>
                                                        @endif
                                                    </div>

                                                    <div class="col-sm-2 wd-cust">
                                                        @if($po->status ==='D')
                                                         <input class="btn btn-default btn-sm" name="btnvendor" type="button" value="....">
                                                        @endif
                                                    </div>


                                                </div>


                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('reference') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Reference</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="reference" value="{{ $po->jonumber }}" class="form-control" readonly>
                                                @if ($errors->has('reference'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('reference') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('_paymentterm') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Payment Term</label>

                                            <div class="col-sm-8">
                                                @if ($po->status ==='D')
                                                <select name="_paymentterm" class="form-control">
                                                @elseif ($po->status !=='D')
                                                <select name="_paymentterm" class="form-control" disabled>
                                                @endif
                                                    <option></option>
                                                    @foreach($paymentterm as $payment)
                                                        <option value="{{ $payment->id }}"  {{ ($payment->id == $po->_paymentterm) ? "selected=selected": '' }}>{{ $payment->description }}</option>
                                                    @endforeach

                                                </select>
                                                @if ($errors->has('_paymentterm'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('_paymentterm') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Particulars</label>

                                            <div class="col-sm-8">
                                                @if ($po->status ==='D')
                                                <textarea name="notes"  class="form-control" rows="3"> {{ $po->notes }}</textarea>
                                                @elseif ($po->status !=='D')
                                                <textarea name="notes"  class="form-control" rows="3" readonly> {{ $po->notes }}</textarea>
                                                @endif
                                                
                                                @if ($errors->has('notes'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('notes') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>




                                    </div>


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">PO Number</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tnumber" value="{{ $po->tnumber }}" readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">PO Date</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tdate" value="{{ $po->tdate }}"  readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Status</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="status" value="{{ $postatus }}"  readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Delivery Date</label>

                                            <div class="col-sm-8">
                                                @if ($po->status ==='D')
                                                <input type="text" name="deliverydate" value="{{ $po->deliverydate }}" class="form-control">
                                                @elseif ($po->status !=='D')
                                                <input type="text" name="deliverydate" value="{{ $po->deliverydate }}" class="form-control" readonly>
                                                @endif
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-lg-12">
                                        <div class="ibox-tools mg-bottom">
                                            @if ($po->status ==='D')
                                                <a href="{{ route('purchaseorderdetail.create',['trnid'=>$po->tnumber]) }}" class="btn btn-primary btn-sm">Add New Item</a>
                                            @elseif ($po->status !=='D')
                                                <a href="{{ route('purchaseorderdetail.create',['trnid'=>$po->tnumber]) }}" class="btn btn-primary btn-sm disabled">Add New Item</a>
                                            @endif
                                            
                                        </div>

                                        <div class="table-responsive">
                                            <table width="100%" class="table table-bordered table-striped" id="po-detail-table">
                                                <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Packing</th>
                                                    <th>Quantity</th>
                                                    <th>Unit Price</th>
                                                    <th>Total Price</th>
                                                    <th></th>

                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <th colspan="4" style="text-align:right;">Page Total:</th>
                                                    <th colspan="2"></th>
                                                </tr>

                                                </tfoot>
                                            </table>
                                        </div><br />
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="ibox-tools mg-bottom">
                                            @if ($po->status ==='D')
                                                <a href="{{ route('poinstruction.create',['trnid'=>$po->tnumber])}}" class="btn btn-primary btn-sm">Add PO Instruction</a>
                                            @elseif ($po->status !=='D')
                                                <a href="{{ route('poinstruction.create',['trnid'=>$po->tnumber])}}" class="btn btn-primary btn-sm disabled">Add PO Instruction</a>
                                            @endif
                                            
                                        </div>

                                        <div class="table-responsive">
                                            <table width="100%" class="table table-bordered table-striped" id="po-instructions">
                                                <thead>
                                                <tr>
                                                    <th width="4%"></th>
                                                    <th>Instruction</th>

                                                </tr>
                                                </thead>
                                                
                                            </table>
                                        </div>
                                    </div>

                                    <!-- <div class="col-lg-12">
                                        <div class="ibox-tools mg-bottom">
                                            <a href="{{ route('poinstruction.create',['trnid'=>$po->tnumber])}}" class="btn btn-primary btn-sm">Add PO Instruction</a>
                                        </div>

                                        <div class="table-responsive">
                                            <table width="100%" class="table table-bordered table-striped" id="po-instructions">
                                                <thead>
                                                <tr>
                                                    <th width="4%"></th>
                                                    <th>Instruction</th>

                                                </tr>
                                                </thead>
                                                
                                            </table>
                                        </div>
                                    </div> -->


                                    <div class="form-group">
                                        <div class="col-sm-8 mg-top">
                                            @if($po->status !=='C')
                                                <a href="{{ route('printpo',['tnumber'=>$po->tnumber]) }}" class="btn btn-primary" target="_blank" name="submit" >Print</a>

                                                <!-- <input type="button" class="btn btn-info"  name="status" value="Status"> -->
                                            @endif

                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <div class="col-sm-8 mg-top">
                                            <a href="{{ URL::to('joborder/'.$po->tnumber.'/joborderbom') }}" type="button" class="btn btn-primary" >BOM</a>
                                        </div>
                                    </div> -->

                                    <div class="form-group">
                                        <div class="col-sm-8 mg-top">
                                            @if($po->status ==='D')
                                                <input type="submit" class="btn btn-primary" value="Save as Draft">
                                                <a href="{{ route('submitpo',['tnumber'=>$po->tnumber]) }}" class="btn btn-primary" name="submit" >Submit</a>
                                                <input type="button" class="btn btn-danger"  name="cancel" value="Cancel">
                                             @elseif($po->status ==='S')
                                                <a href="{{ route('approvepo',['tnumber'=>$po->tnumber]) }}" class="btn btn-primary" name="submit" >Approve</a>
                                                <a href="{{ route('rollbackpo',['tnumber'=>$po->tnumber]) }}" class="btn btn-primary" name="submit" >Rollback</a>
                                                <input type="button" class="btn btn-danger"  name="cancel" value="Cancel">
                                             @elseif($po->status ==='A')
                                                <input type="button" class="btn btn-danger"  name="cancel" value="Cancel">
                                                <a href="{{ route('createtrr',['tnumber'=>$po->tnumber]) }}" class="btn btn-primary" target="_blank" name="submit" >Create TRR</a>
                                             @endif

                                        </div>
                                    </div>
                                </form>
                            </div>




                        </div>


                    </div>
                </div>

            </div>

        </div>

        <!-- Modal -->
        <div class="modal fade" id="vendorlistmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Customer List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablevendorlist">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="bommodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Bom List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablebomlist">
                                <thead>
                                <tr>
                                    <th>Raw Material</th>
                                    <th>Packing</th>
                                    <th>Quantity</th>
                                    <th>Cost</th>
                                    <th>Total Cost</th>


                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection


@section('customjs')
    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    <script src="{{ URL::asset('js/editcreatepurchaseorder.js') }}"></script>

@endsection