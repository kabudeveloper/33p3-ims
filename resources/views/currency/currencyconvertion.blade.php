@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Currencies</h2>
            {!! Breadcrumbs::render('box') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Conversion</h5>
                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('duplicate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    <strong>{{ Session::get('duplicate') }} {{$errors->first()}}</strong>
                                </div>
                            @endif
                               <div class="col-lg-6">

                                  <form class="form-horizontal" method="post" action="{{ route('currencyconvertion.store') }}">
                                      {{ csrf_field() }}



                                      <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Base</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="name" readonly value="{{ $currency->name }}" class="form-control">
                                              <input type="hidden" name="base" value="{{ $currency->id }}" readonly class="form-control">
                                              @if ($errors->has('name'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group {{ $errors->has('target') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Convert</label>

                                          <div class="col-sm-8">
                                              <select name="target" class="form-control">
                                                  <option></option>
                                                  @foreach($currencies as $c)
                                                      <option value="{{ $c->id }}">{{ $c->name }}</option>
                                                  @endforeach
                                              </select>
                                              @if ($errors->has('target'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('target') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('rate') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Rate</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="rate"  class="form-control">
                                              @if ($errors->has('rate'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('rate') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('currency') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection