@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Currency</h2>
            {!! Breadcrumbs::render('currency') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>View Currency</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('duplicate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    <strong>{{ Session::get('duplicate') }} {{$errors->first()}}</strong>
                                </div>
                            @endif
                               <div class="col-lg-6">

                                  <form  class="form-horizontal" method="POST" action="{{ route('currency.update',$currency->id) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">
                                      <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Name</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="name"  value="{{ $currency->name }}" class="form-control" readonly>
                                              @if ($errors->has('name'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>




                                      <div class="form-group {{ $errors->has('symbol') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Symbol</label>
                                          <div class="col-sm-8">
                                              <input type="text" name="symbol" value="{{ $currency->symbol }}" class="form-control" readonly>
                                              @if ($errors->has('symbol'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('symbol') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>


                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('currency') }}" class="btn btn-white">Back</a>
                                            <!-- <button type="submit" class="btn btn-primary">Save </button> -->
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Conversion<small></small></h5>
                        <div class="ibox-tools">
                            <!-- <a class="btn btn-primary  btn-sm" href="{{ route('createconvertions',$currency->id) }}">Add Conversion</a> -->

                        </div>
                    </div>



                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="currencyconvertion-table">
                                <thead>
                                <tr>

                                    <th>Name</th>
                                    <th>Rate</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>




        <!--modal manual convert-->
        <div class="modal fade smallaup" id="manualconvertmodal" tabindex="-1" role="dialog" aria-labelledby="aupmodal">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title" id="myModalLabel">Manual</h5>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal" method="post" action="{{ route('updatecurr') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="aup" class="col-xs-5 control-label">Base currency</label>
                                <div class="col-xs-7">
                                    <input type="text" readonly name="bcurrency" class="form-control" id="bcurrency">
                                    <input type="hidden" name="bcurrencyhidden" class="form-control" id="bcurrencyhidden">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="aup" class="col-xs-5 control-label">Convert to</label>
                                <div class="col-xs-7">
                                    <input type="hidden" name="manualcurrencyhidden">
                                    <select name="manualcurrency" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="aup" class="col-xs-5 control-label">Rate</label>
                                <div class="col-xs-7">
                                    <input type="text" name="rate" class="form-control" id="rate">

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-5 col-xs-10">
                                    <input type="submit" name="btnupdatecurrency" value="Submit" class="btn btn-success">
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>





    </div>


@endsection



@section('customjs')

    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>
    <script src="{{ URL::asset('js/currencyconvertion.js') }}"></script>

@endsection