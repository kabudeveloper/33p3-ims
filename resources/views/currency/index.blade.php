@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Currency</h2>
            {!! Breadcrumbs::render('currency') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                @if(Session::has('duplicate'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                        <strong>{{ Session::get('duplicate') }} {{$errors->first()}}</strong>
                    </div>
                @endif
                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Currency<small></small></h5>
                        <div class="ibox-tools">

                            {{--<a href="#" data-toggle="dropdown" class="dropdown-toggle">
                                <i class="fa fa-cog"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ URL::to('box/create') }}">Add Box</a>
                                </li>

                            </ul>--}}
                            <a class="btn btn-primary  btn-sm" href="{{ URL::to('currency/create') }}">Add Currency</a>

                        </div>
                    </div>






                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="currency-table">
                                <thead>
                                <tr>

                                    <th>Name</th>
                                    <th>Symbol</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
       <!--modal online convert-->
        <div class="modal fade smallaup" id="convertmodal" tabindex="-1" role="dialog" aria-labelledby="aupmodal">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title" id="myModalLabel">Online</h5>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal" method="post" action="{{ route('currencyconvertion.store') }}">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="aup" class="col-xs-5 control-label">Base currency</label>
                                <div class="col-xs-7">
                                    <input type="text" readonly name="bcurrency" class="form-control" id="bcurrency">
                                    <input type="hidden" name="bcurrencyhidden" class="form-control" id="bcurrencyhidden">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="aup" class="col-xs-5 control-label">Convert to</label>
                                <div class="col-xs-7">
                                    <select name="_currency" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="aup" class="col-xs-5 control-label">Rate</label>
                                <div class="col-xs-7">
                                    <input type="text" name="rate" class="form-control" id="rate">

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-xs-offset-5 col-xs-10">
                                    <button type="submit" name="btnupdateaup" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!--modal manual convert-->
        <div class="modal fade smallaup" id="manualconvertmodal" tabindex="-1" role="dialog" aria-labelledby="aupmodal">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title" id="myModalLabel">Manual</h5>
                    </div>

                    <div class="modal-body">
                        <form class="form-horizontal" method="post" action="{{ route('currencyconvertion.store') }}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="aup" class="col-xs-5 control-label">Base currency</label>
                                <div class="col-xs-7">
                                    <input type="text" readonly name="bcurrency" class="form-control" id="bcurrency">
                                    <input type="hidden" name="bcurrencyhidden" class="form-control" id="bcurrencyhidden">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="aup" class="col-xs-5 control-label">Convert to</label>
                                <div class="col-xs-7">
                                    <select name="manualcurrency" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="aup" class="col-xs-5 control-label">Rate</label>
                                <div class="col-xs-7">
                                    <input type="text" name="rate" class="form-control" id="rate">

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-offset-5 col-xs-10">
                                    <button type="submit" name="btnupdateaup" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


    </div>


@endsection


@section('customjs')

    <script src="{{ URL::asset('js/currency.js') }}"></script>

@endsection