@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Box</h2>
            {!! Breadcrumbs::render('box') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                @if(Session::has('duplicate'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                        <strong>{{ Session::get('duplicate') }} {{$errors->first()}}</strong>
                    </div>
                @endif
                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Currency<small></small></h5>
                        <div class="ibox-tools">



                        </div>
                    </div>






                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="currencyconvertion-table">
                                <thead>
                                <tr>

                                    <th>Name</th>
                                    <th>Symbol</th>
                                    <th>Action</th>
                                </tr>
                                </thead>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>



    </div>


@endsection


@section('customjs')
    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>
    <script src="{{ URL::asset('js/currencyconvertion.js') }}"></script>

@endsection