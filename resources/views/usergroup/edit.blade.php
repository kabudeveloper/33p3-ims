@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Companies</h2>
            {!! Breadcrumbs::render('company') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Group</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form  class="form-horizontal" method="POST" action="{{ route('usergroup.update',$group->id) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">
                                      <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Role</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="description"  value="{{ $group->description }}" class="form-control">
                                              @if ($errors->has('description'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('description') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <button type="submit" class="btn btn-white">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>


                            <div class="col-lg-6">
                                <form class="form-horizontal">
                                    <div class="col-lg-6">
                                        <form class="form-horizontal">
                                            {{ csrf_field() }}

                                                    <div class="mg-bottom" id="tree3" name="selNodes[]"></div>


                                            <div class="form-group ">
                                                <div class="col-sm-10">
                                                    <button type="submit"  id="formrolesbtn" class="btn btn-primary btn-sm">Assign</button>
                                                </div>
                                            </div>


                                        </form>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection



@section('customjs')

    <script src="{{ URL::asset('js/usergroupedit.js') }}"></script>
    <script src="{{ URL::asset('js/utilitytree.js') }}"></script>

@endsection