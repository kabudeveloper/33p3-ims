@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Companies</h2>
            {!! Breadcrumbs::render('company') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')

                            
    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Job Order
                            <small><span class="badge badge-secondary"></span></small>
                        </h5>

                              
                           
                        <div class="ibox-tools">
                            <a class="btn btn-primary btn-sm" href="{{ URL::to('joborder/create') }}">Add Job Order</a>
                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="joborder-table">
                                <thead>
                                <tr>
                                    <th>JO #</th>
                                    <th>JO Date</th>
                                    <th>SO #</th>
                                    <th>Customer</th>
                                    <th>Particulars</th>
                                    <th>Status</th>
                                    <th>Action</th>

                                </tr>
                                </thead>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection

@section('customjs')

    <script src="{{ URL::asset('js/joborder.js') }}"></script>


@endsection