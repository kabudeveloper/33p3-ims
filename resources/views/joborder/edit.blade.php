@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Job Orders</h2>
            {!! Breadcrumbs::render('company') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Job Order</h5>

                    </div>
                    <div class="box-content white-bg">
                        <div class="row">


                            <div class="col-lg-12">

                                <form class="form-horizontal" method="POST"
                                      action="{{ route('joborder.update',$jo->tnumber) }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="_method" value="PUT">


                                    <div class="col-lg-6">
                                        <div class="form-group {{ $errors->has('_branch') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Branch</label>

                                            @if($manual == 1)
                                                <div class="col-sm-8">
                                                    <select name="_branch" disabled="disabled" class="form-control">
                                                        <option></option>
                                                        @foreach($branches as $branch)
                                                            <option value="{{ $branch->id }}" {{ ($branch->id == $jo->_branch) ? "selected=selected": '' }}>{{ $branch->name }}</option>
                                                        @endforeach

                                                    </select>
                                                    @if ($errors->has('_branch'))
                                                        <span class="help-block">
                                                  <strong>{{ $errors->first('_branch') }}</strong>
                                                  </span>
                                                    @endif
                                                </div>
                                            @else
                                                <div class="col-sm-8">
                                                    <select name="_branch" class="form-control">
                                                        <option></option>
                                                        @foreach($branches as $branch)
                                                            <option value="{{ $branch->id }}" {{ ($branch->id == $jo->_branch) ? "selected=selected": '' }}>{{ $branch->name }}</option>
                                                        @endforeach

                                                    </select>
                                                    @if ($errors->has('_branch'))
                                                        <span class="help-block">
                                                  <strong>{{ $errors->first('_branch') }}</strong>
                                                  </span>
                                                    @endif
                                                </div>
                                            @endif




                                        </div>

                                        <div class="form-group {{ $errors->has('_customer') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Customer</label>

                                            <div class="col-sm-8">

                                                <div class="row">


                                                    <div class="col-sm-10">
                                                        <input type="hidden" name="custidhidden" >
                                                        <input type="text" name="_customer" value="{{ $custname }}"
                                                               readonly class="form-control">
                                                        @if ($errors->has('_customer'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('_customer') }}</strong>
                                                             </span>
                                                        @endif
                                                    </div>

                                                    @if($manual != 1)
                                                        <div class="col-sm-2 wd-cust">
                                                            @if($jo->status !=='S' && $jo->status !== 'A')
                                                                <input class="btn btn-default btn-sm" name="btncustomer"
                                                                       type="button" value="....">
                                                            @endif
                                                        </div>

                                                    @endif




                                                </div>


                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('reference') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Reference</label>

                                            <div class="col-sm-8">
                                                @if($manual == 1)
                                                    <input type="text" readonly name="referenceformat" value="{{ $referenceformat }}"   class="form-control">
                                                    <input type="hidden" name="reference" value="{{ $jo->reference }}">
                                                @else
                                                    <input type="text" name="referenceformat" value="{{ $referenceformat }}"   class="form-control">
                                                    <input type="hidden" name="reference" value="{{ $jo->reference }}">
                                                @endif


                                                @if ($errors->has('reference'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('reference') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        {{--<div class="form-group {{ $errors->has('_paymentterm') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Payment Term</label>

                                            <div class="col-sm-8">
                                                <select name="_paymentterm" class="form-control">
                                                    <option></option>


                                                </select>
                                                @if ($errors->has('_paymentterm'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('_paymentterm') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>--}}

                                        <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Particulars</label>

                                            <div class="col-sm-8">
                                                <textarea name="notes" class="form-control"
                                                          rows="3"> {{ $jo->notes }}</textarea>
                                                @if ($errors->has('notes'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('notes') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>


                                    </div>


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">JO Number</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tnumber" value="{{ $jo->tnumber }}" readonly
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">JO Date</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tdate" value="{{ $jo->tdate }}" readonly
                                                       class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Status</label>

                                            <div class="col-sm-8">

                                                <input type="text" name="status" value="{{ $jostatus }}"
                                                       readonly class="form-control">

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Delivery Date</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="deliverydate" value="{{ $jo->deliverydate }}"
                                                       class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="ibox-tools mg-bottom">
                                            <a href="{{ route('joborderdetail.create',['trnid'=>$jo->tnumber]) }}"
                                               class="btn btn-primary btn-sm">Add New</a>
                                        </div>

                                        <div class="table-responsive">
                                            <table width="100%" class="table table-bordered table-striped"
                                                   id="jo-detail-table">
                                                <thead>
                                                <tr>
                                                    <th>Product</th>
                                                    <th>Packing</th>
                                                    <th>Quantity</th>

                                                    <th></th>

                                                </tr>
                                                </thead>

                                            </table>
                                        </div>

                                        @if($jo->status == 'D')
                                            <div class="form-group">
                                                <div class="col-sm-8 mg-top">
                                                    <a href="{{ route('printjo',['tnumber'=>$jo->tnumber]) }}" target="_blank" target="_blank" 
                                                       class="btn btn-primary" name="submit">Print</a>
                                                    <a href="{{ route('printmanagement',['tnumber'=>$jo->tnumber]) }}" target="_blank" 
                                                       class="btn btn-primary" name="submit">Print Mgmt</a>
                                                    <a href="{{ URL::to('joborder/'.$jo->tnumber.'/joborderbom') }}"
                                                       type="button" class="btn btn-primary">BOM</a>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-8">

                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                    <a href="{{ route('submitjoborder',['tnumber'=>$jo->tnumber]) }}"
                                                       class="btn btn-success" name="submit">Submit</a>
                                                    <input type="button" class="btn btn-danger" name="cancel" value="Cancel">
                                                    <a href="{{ route('joborder') }}" class="btn btn-info">Back</a>

                                                </div>
                                            </div>

                                        @elseif( $jo->status == 'S')
                                            <div class="form-group">
                                                <div class="col-sm-8 mg-top">
                                                    <a href="{{ route('printjo',['tnumber'=>$jo->tnumber]) }}" target="_blank"
                                                       class="btn btn-primary" name="submit">Print</a>
                                                    <a href="{{ route('printmanagement',['tnumber'=>$jo->tnumber]) }}" target="_blank"
                                                       class="btn btn-primary" name="submit">Print Mgmt</a>
                                                    <a href="{{ URL::to('joborder/'.$jo->tnumber.'/joborderbom') }}"
                                                       type="button" class="btn btn-primary">BOM</a>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-8">

                                                    <a href="{{ route('approvejoborder',['tnumber'=>$jo->tnumber]) }}"
                                                       class="btn btn-primary" name="submit">Approve</a>
                                                    <a href="{{ route('rollbackjoborder',['tnumber'=>$jo->tnumber]) }}"
                                                       class="btn btn-success" name="submit">Rollback</a>
                                                    <input type="button" class="btn btn-danger" name="cancel" value="Cancel">
                                                    <a href="{{ route('joborder') }}" class="btn btn-info">Back</a>

                                                </div>
                                            </div>

                                        @elseif( $jo->status == 'A')
                                            <div class="form-group">
                                                <div class="col-sm-8 mg-top">
                                                    <a href="{{ route('printjo',['tnumber'=>$jo->tnumber]) }}" target="_blank"
                                                       class="btn btn-primary" name="submit">Print</a>
                                                    <a href="{{ route('printmanagement',['tnumber'=>$jo->tnumber]) }}" target="_blank"
                                                       class="btn btn-primary" name="submit">Print Mgmt</a>
                                                    <a href="{{ URL::to('joborder/'.$jo->tnumber.'/joborderbom') }}"
                                                       type="button" class="btn btn-primary">BOM</a>
                                                    <a href="#"
                                                       type="button" class="btn btn-primary">Status</a>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-8">


                                                    <input type="button" class="btn btn-danger" name="cancel" value="Cancel">
                                                    <a href="{{ route('joborder') }}" class="btn btn-info">Back</a>

                                                </div>
                                            </div>
                                        @endif

                                    </div>


                                </form>
                            </div>


                        </div>


                    </div>
                </div>

            </div>

        </div>

        <!-- Modal -->
        <div class="modal fade" id="customerlistmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Customer List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablecustomerlist">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>

                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="bommodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Bom List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablebomlist">
                                <thead>
                                <tr>
                                    <th>Raw Material</th>
                                    <th>Packing</th>
                                    <th>Quantity</th>
                                    <th>Cost</th>
                                    <th>Total Cost</th>


                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection


@section('customjs')
    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    <script src="{{ URL::asset('js/editcreatejoborder.js') }}"></script>

@endsection