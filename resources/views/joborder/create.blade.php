@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Job Orders</h2>
            {!! Breadcrumbs::render('company') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Job Order</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-12">

                                   @if(Session::has('refdup'))
                                       <div class="alert alert-danger alert-dismissible" role="alert">
                                           <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                           <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                           <strong>{{ Session::get('refdup') }} {{$errors->first()}}</strong>
                                       </div>
                                   @endif


                                  <form class="form-horizontal" method="post" action="{{ route('joborder.store') }}">
                                      {{ csrf_field() }}


                                      <div class="col-lg-6">
                                          <div class="form-group {{ $errors->has('_branch') ? ' has-error' : '' }}">
                                              <label class="col-sm-2 control-label">Branch</label>

                                              <div class="col-sm-8">
                                                  <select name="_branch" class="form-control">
                                                      <option></option>
                                                      @foreach($branches as $branch)
                                                          <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                                                      @endforeach

                                                  </select>
                                                  @if ($errors->has('_branch'))
                                                      <span class="help-block">
                                                  <strong>{{ $errors->first('_branch') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                          </div>

                                          <div class="form-group {{ $errors->has('_customer') ? ' has-error' : '' }}">
                                              <label class="col-sm-2 control-label">Customer</label>



                                              <div class="col-sm-8">
                                                  <div class="row">
                                                      <div class="col-sm-10">
                                                          <input type="hidden" name="custidhidden">
                                                          <input type="text" name="_customer" value="{{ old('_customer') }}" readonly class="form-control">
                                                          @if ($errors->has('_customer'))
                                                              <span class="help-block">
                                                                 <strong>{{ $errors->first('_customer') }}</strong>
                                                                </span>
                                                          @endif
                                                      </div>

                                                      <div class="col-sm-2 wd-cust">
                                                          <input class="btn btn-default btn-sm" name="btncustomer" type="button" value="....">

                                                      </div>

                                                  </div>


                                              </div>


                                          </div>

                                          <div class="form-group {{ $errors->has('reference') ? ' has-error' : '' }}">
                                              <label class="col-sm-2 control-label">Reference</label>

                                              <div class="col-sm-8">
                                                  <input type="text" name="reference" value="{{ old('reference') }}" class="form-control">
                                                  @if ($errors->has('reference'))
                                                      <span class="help-block">
                                                  <strong>{{ $errors->first('reference') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                          </div>
                                          {{-- <div class="form-group {{ $errors->has('reference') ? ' has-error' : '' }}">
                                              <label class="col-sm-2 control-label">S.O Number</label>

                                              <div class="col-sm-8">
                                                  <div class="row">
                                                    <div class="col-sm-10">
                                                      <input type="text" name="_sonumber" class="form-control">
                                                      @if ($errors->has('_sonumber'))
                                                          <span class="help-block">
                                                      <strong>{{ $errors->first('_sonumber') }}</strong>
                                                      </span>
                                                      @endif
                                                    </div>
                                                    <div class="col-sm-2 wd-cust">
                                                      <input class="btn btn-default btn-sm" name="btnsonumber" type="button" value="....">
                                                    </div>
                                                  </div>
                                                  
                                              </div>
                                          </div> --}}
                                          <div class="form-group {{ $errors->has('_paymentterm') ? ' has-error' : '' }}">
                                              <label class="col-sm-2 control-label">Payment Term</label>

                                              <div class="col-sm-8">
                                                  <select name="_paymentterm" class="form-control">
                                                      <option></option>


                                                  </select>
                                                  @if ($errors->has('_paymentterm'))
                                                      <span class="help-block">
                                                  <strong>{{ $errors->first('_paymentterm') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                          </div>

                                          <div class="form-group {{ $errors->has('notes') ? ' has-error' : '' }}">
                                              <label class="col-sm-2 control-label">Particulars</label>

                                              <div class="col-sm-8">
                                                  <textarea name="notes" value="{{ old('notes') }}" class="form-control" rows="3"></textarea>
                                                  @if ($errors->has('notes'))
                                                      <span class="help-block">
                                                  <strong>{{ $errors->first('notes') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                          </div>




                                      </div>


                                      <div class="col-lg-6">
                                          <div class="form-group">
                                              <label class="col-sm-4 control-label">JO Number</label>

                                              <div class="col-sm-8">
                                                  <input type="text" name="tnumber"  readonly class="form-control">
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="col-sm-4 control-label">JO Date</label>

                                              <div class="col-sm-8">
                                                  <input type="text" name="tdate"  readonly class="form-control">
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="col-sm-4 control-label">Status</label>

                                              <div class="col-sm-8">
                                                  <input type="text" name="status"  readonly class="form-control">
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="col-sm-4 control-label">Delivery Date</label>

                                              <div class="col-sm-8">
                                                  <input type="text" name="deliverydate"  class="form-control">
                                              </div>
                                          </div>
                                          <div class="form-group">
                                              <div class="col-sm-offset-4 col-sm-10 mg-top">
                                                  <button type="submit" class="btn btn-primary">Save</button>
                                                  <a href="{{ route('joborder') }}" class="btn btn-info">Back</a>
                                              </div>
                                          </div>

                                      </div>


                                    <div class="hr-line-dashed"></div>

                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>


        <!-- Modal -->
        <div class="modal fade" id="customerlistmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Customer List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablecustomerlist">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>

                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="sonumbermodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">SO Lists</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablesolist">
                                <thead>
                                <tr>
                                    <th>TNumber</th>
                                    <th>Customer</th>
                                    <th>Notes</th>
                                    <th>Price Validity</th>
                                    <th>Currency</th>
                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>




    </div>


@endsection


@section('customjs')
    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    <script src="{{ URL::asset('js/createjoborder.js') }}"></script>

@endsection