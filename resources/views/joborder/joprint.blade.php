<!doctype html>
<html lang="en">
<head>
     <script>
       
        window.print();
        setTimeout(function() {
            window.location.href = "joborder";
        }, 20); 
        
    </script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{ asset('css/printstyle.css') }}">
    <title>JO print</title>
    <style type="text/css">
        .headerimage {       
            height: auto;
            margin: 0 0 20px 0;
            margin-left: 17px;
        }

        h3{
            margin-left: 17px;
        }
        div.box11 {
            width: 40%;
            display: inline-block;
            margin-left: 17px;
        }
        

        div.box22 {
            width: 30%;
            display: inline-block;
            margin-left: 170px;

        }
        .particular{
             margin-left: 17px;
        }

        .itemss {
            border: 1px solid black;
            clear: both;
            margin: 5px 0 0;
            margin-left: 17px;
            width: 94%;
            position: relative;
        }
        .itemss th {
            background: #eee none repeat scroll 0 0;
        }
        .itemss td.blank {
            border: 0 none;
        }
        .itemss td.total-line {
            border-right: 0 none;
            text-align: right;
        }
        .itemss td.total-value {
            border-left: 0 none;
            padding: 10px;
        }

        .page-break {
            page-break-after: always;
        }

        .header,
        .footer {
            width: 100%;
            text-align: right;
            position: fixed;
        }

        .header {
            position: fixed;
            top: -50px;
            left: 0px;
            right: 0px;
            height: 50px;
            padding: .5em;
            text-align: center;
        }

        .footer {
            margin-left: 17px;
            bottom: 2em;
        }

        .pagenum:before {
            content: counter(page);
        }

    </style>


</head>
<body>
<div id="print"> 
<div class="header">
    Page <span class="pagenum"></span>

</div>
<div class="contentbody">
    <div class="headerimage"><img src="{{ asset('public/img/logo.gif') }}"></div>

    <div class="box11">
        <table width="100%">
            <tr>
                <td class="meta-head">Branch</td>
                <td><textarea>{{ $branch->name }}</textarea></td>
            </tr>

            <tr>
                <td class="meta-head">Reference</td>
                <td><textarea>{{ $jo->reference }}</textarea></td>
            </tr>
            <tr>
                <td class="meta-head">Delivery Date</td>
                <td><textarea>{{ $jo->deliverydate }}</textarea></td>
            </tr>


        </table>
    </div>

    <div class="box22">
        <table width="100%">
            <tr>
                <td class="meta-head">JO Number</td>
                <td><textarea>{{ str_pad($jo->tnumber ,5,'0',STR_PAD_LEFT) }}</textarea></td>
            </tr>
            <tr>
                <td class="meta-head">JO Date</td>
                <td><textarea>{{ $jodatetransaction }}</textarea></td>
            </tr>
            <tr>
                <td class="meta-head">Status</td>
                @if($jo->status ==='S')
                    <td><textarea>Submitted ({{ $josubmitted }})</textarea></td>
                @elseif($jo->status === 'A')
                    <td><textarea>Approved ({{ $jodateapproved }})</textarea></td>
                @elseif($jo->status === 'C')
                    <td><textarea>Cancelled ({{ $jocancel }})</textarea></td>
                @elseif($jo->status === 'D')
                    <td><textarea>Draft</textarea></td>


                @endif
            </tr>

        </table>
    </div>


    <!-- /.col-lg-6 -->

    <h3 class="headparticular">Particulars</h3>
    <div class="particular"></div>

    <h3 class="headparticular">Order Details</h3>
    <div>
        <table class="itemss">

            <tr>
                <th width="5">Lno</th>
                <th>Product</th>
                <th width="80">Packing</th>
                <th width="80">Quantity</th>

            </tr>

             @foreach($jod as $item =>$value)

                  <tr>
                      <td style="text-align: right;">{{ ++$item }}</td>
                      <td>{{ $value->item()->first()->name }}</td>
                      <td>{{ $value->packing()->first()->description }}</td>
                      <td style="text-align: right;">{{ $value->quantity }}</td>
                  </tr>

             @endforeach


        </table>
    </div>
</div>

<div class="footer">
    <table width="94%">
        <tr>
            <th style="text-align: center;">Monitoring</th> 
            <th style="text-align: center;">Marketing User</th>
            <th style="text-align: center;">Owner Name</th>
        </tr>
        <tr>
            <td style="text-align: center;">Prepared By</td>
            <td style="text-align: center;">Reviewed By</td>
            <td style="text-align: center;">Approved By</td>
        </tr>
    </table>
</div>
</div>
</body>
</html>