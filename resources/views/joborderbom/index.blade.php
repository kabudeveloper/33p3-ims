@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Bom</h2>
            {!! Breadcrumbs::render('finishedproduct') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="white-bg box-wrapper">

                <div class="ibox-title">
                    <form class="form-inline">
                        <div class="form-group">
                            <label  class="col-sm-4 control-label">JO Number</label>
                            <div class="col-sm-8">
                                <input type="text" readonly class="form-control"  value="{{ $tnumber }}">
                            </div>
                        </div>

                    </form>




                    <div class="ibox-tools">
                        <a class="btn btn-primary btn-sm" href="{{ URL::to('joborder/'.$tnumber.'/joborderbom/create') }}">Add New Item</a>
                    </div>

                </div>

                <div class="box-content white-bg">
                    <div class="table-responsive">
                        <table width="100%" class="table table-bordered table-striped" id="joborder-bom-table">

                            <thead>
                            <tr>
                                <th>Raw Material</th>
                                <th>Packing</th>
                                <th>Quantity</th>
                                <th>Cost</th>
                                <th>Total Cost</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th colspan="4" style="text-align:right;">Page Total:</th>
                                <th colspan="2"></th>
                            </tr>

                            </tfoot>



                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection


@section('customjs')

    <script src="{{ URL::asset('js/joborderbom.js') }}"></script>

@endsection