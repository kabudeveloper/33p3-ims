@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2></h2>
            {!! Breadcrumbs::render('contact') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Item Bom</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if($errors->any())
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ $errors->first() }}
                                </div>
                            @endif
                            <div class="col-lg-7">


                                <form class="form-horizontal" method="post" id="addbillofmat"
                                      action="{{ route('joborder.joborderbom.update',[$jobom->tnumber,$jobom->_item]) }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">JO Number</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="tnumber" value="{{ $jobom->tnumber }}" readonly
                                                   class="form-control">

                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Code</label>
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text" name="code" value="{{ $item->code  }}"
                                                           class="form-control">
                                                    <input type="hidden" value="{{ $jobom->_item }}" name="_hiddenrmid">
                                                    @if ($errors->has('code'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('code') }}</strong>
                                                        </span>
                                                    @endif
                                                    <div class="code">

                                                    </div>


                                                </div>

                                                <div class="col-sm-6">

                                                    <button id="fv" name="findrawmatbtn" type="button" class="btn btn-success btn-sm"
                                                    >
                                                        Item Look Up
                                                    </button>



                                                </div>


                                            </div>


                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('rmname') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="rmname" value="{{ $item->name  }}" class="form-control">

                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('itempacking') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Packing</label>

                                        <div class="col-sm-8">
                                            <select name="packing" class="form-control" >
                                                <option></option>


                                            </select>
                                            @if ($errors->has('itempacking'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('itempacking') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Quantity</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="quantity" value="{{ $jobom->quantity  }}" class="form-control">
                                            @if ($errors->has('quantity'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('quantity') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>




                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <button type="submit" class="btn btn-white">Cancel</button>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>




        <div class="modal fade" id="rawmaterialmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Item List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="rawmateiallist">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>

                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection
<script type="text/javascript">

    var APP_URL = {!! json_encode(url('/')) !!};
</script>


@section('customjs')

    <script src="{{ URL::asset('js/editjoborderbom.js') }}"></script>

@endsection