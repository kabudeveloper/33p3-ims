@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Vendor Item</h2>
            {!! Breadcrumbs::render('vendoritem') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>View Vendor Item</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('duplicate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ Session::get('duplicate') }}
                                </div>
                            @endif
                               <div class="col-lg-7">

                                  <form  class="form-horizontal" method="POST" id="addvendoritem"  action="{{ route('rawmaterial.venditem.update',[$item->id,$vendoritem->_vendor]) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">


                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Item</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="name"  value="{{ $item->name }}" readonly class="form-control" readonly>

                                          </div>
                                      </div>


                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Vendor Code</label>

                                          <div class="col-sm-8">

                                              <div class="row">
                                                  <div class="col-sm-6">
                                                      <input type="text" name="_vendor"  value="{{ $vendoritem->code  }}" class="form-control" readonly>
                                                      <input type="hidden" name="_hiddenvendorid"  value="{{ $vendoritem->_vendor  }}" >
                                                      <div class="_vendor">

                                                      </div>


                                                  </div>

                                                  <div class="col-sm-6">

                                                      <button id="vc" type="submit" class="btn btn-info btn-sm" data-loading-text="Verifying..." autocomplete="off" disabled>Verify Code</button>


                                                      <button id="fv" type="submit" class="btn btn-success btn-sm" disabled>Find Vendor</button>

                                                  </div>


                                              </div>


                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('vendorname') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Vendor Name</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="vendorname"  value="{{ $vendoritem->name  }}" class="form-control" readonly>

                                          </div>
                                      </div>



                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Cost</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="cost"  value="{{ $vendoritem->cost  }}"  class="form-control" readonly>

                                          </div>
                                      </div>




                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($vendoritem->isactive)
                                                          <input type="checkbox" value="1" checked name="isactive" disabled>  Active
                                                      @else
                                                          <input type="checkbox" name="isactive" disabled>  Active

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>





                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ URL::to('rawmaterial/'.$item->id).'/view' }}" class="btn btn-white">Back</a>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

            <div class="modal fade" tabindex="-1" id="findVendorModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Find Vendor</h4>
                        </div>
                        <div class="modal-body">

                            <form class="form-inline" id="formvendorsearch">
                                <div class="form-group">
                                    <label for="criteria">Criteria</label>
                                    <select class="form-control"  name="criteria" id="criteria">
                                        <option value="vendorcode">Code</option>
                                        <option value="vendorname">Name</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="valuesearch" >Value</label>
                                    <input type="email" class="form-control" id="valuesearch" name="valuesearch">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="containssearch" value="1"> Contains
                                    </label>
                                </div>
                                <button type="button" class="btn btn-primary btn-sm" id="searchvendor"><i class="fa fa-search" aria-hidden="true"></i>
                                    Search</button>
                            </form>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive" id="findrawvendortable">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Name</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="findselectvendor" class="btn btn-primary">Select</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->



        </div>
    </div>


@endsection




@section('customjs')

    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    <script src="{{ URL::asset('js/edit-vendor-item.js') }}"></script>


@endsection