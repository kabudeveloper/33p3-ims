@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Purchase Orders</h2>
            {!! Breadcrumbs::render('materialrequest') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>View Material Request</h5>

                    </div>
                    <div class="box-content white-bg">
                        <div class="row">


                                <div class="col-lg-12">

                                <form  class="form-horizontal" method="POST" action="{{ route('purchasing.update',$mr->tnumber) }}">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="_method" value="PUT">


                                    <div class="col-lg-6">
                                        <div class="form-group {{ $errors->has('_branch') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Branch</label>

                                            <div class="col-sm-8">
                                                @if ($mr->status ==='D')
                                                <select name="_branch" class="form-control" disabled>
                                                @elseif ($mr->status !=='D')
                                                <select name="_branch" class="form-control" disabled>
                                                @endif
                                                    <option></option>
                                                    @foreach($branches as $branch)
                                                        <option value="{{ $branch->id }}"  {{ ($branch->id == $jobranch) ? "selected=selected": '' }}>{{ $branch->name }}</option>
                                                    @endforeach

                                                </select>
                                                @if ($errors->has('_branch'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('_branch') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('_location') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">Location</label>

                                            <div class="col-sm-8">
                                                @if ($mr->status ==='D')
                                                <select name="_location" class="form-control">
                                                @elseif ($mr->status !=='D')
                                                <select name="_location" class="form-control" disabled>
                                                @endif
                                                    <option></option>
                                                    @foreach($location as $loc)
                                                        <option value="{{ $loc->id }}"  {{ ($loc->id == $mr->_locationfrom) ? "selected=selected": '' }}>{{ $loc->name }}</option>
                                                    @endforeach

                                                </select>
                                                @if ($errors->has('_location'))
                                                    <span class="help-block">
                                                  <strong>{{ $errors->first('_location') }}</strong>
                                                  </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group {{ $errors->has('jonumber') ? ' has-error' : '' }}">
                                            <label class="col-sm-2 control-label">JO Number</label>

                                            <div class="col-sm-8">

                                                <div class="row">
                                                  <div class="col-sm-12">
                                                    <input type="hidden" name="vendoridhidden">
                                                    <input type="hidden" name="tmode">
                                                    <input type="text" name="jonumber" value="{{ $mr->jonumber }}" readonly class="form-control">
                                                    @if ($errors->has('jonumber'))
                                                      <span class="help-block">
                                                        <strong>{{ $errors->first('jonumber') }}</strong>
                                                      </span>
                                                    @endif
                                                  </div>

                                                </div>


                                            </div>
                                        </div>

                                        




                                    </div>


                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">MR Number</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tnumber" value="{{ $mr->tnumber }}" readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">MR Date</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="tdate" value="{{ $mr->tdate }}"  readonly class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Status</label>

                                            <div class="col-sm-8">
                                                <input type="text" name="status" value="{{ $mrstatus }}"  readonly class="form-control">
                                            </div>
                                        </div>

                                    </div>


                                    <div class="col-lg-12">
                                        <div class="ibox-tools mg-bottom">
                                            @if ($mr->status ==='D')
                                                <a href="{{ route('materialrequestdetail.create',['tnumber'=>$mr->tnumber]) }}" class="btn btn-primary btn-sm">Add New Item</a>
                                            @elseif ($mr->status !=='D')
                                                <a href="{{ route('materialrequestdetail.create',['tnumber'=>$mr->tnumber]) }}" class="btn btn-primary btn-sm disabled">Add New Item</a>
                                            @endif
                                            
                                        </div>

                                        <div class="table-responsive">
                                            <table width="100%" class="table table-bordered table-striped" id="mr-detail-table">
                                                <thead>
                                                <tr>
                                                    <th>LNo</th>
                                                    <th>Raw Material</th>
                                                    <th>Packing</th>
                                                    <th>Quantity</th>
                                                </tr>
                                                </thead>
                                                
                                            </table>
                                        </div><br />
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-8 mg-top">
                                            @if($mr->status ==='D')
                                                <!-- <input type="submit" class="btn btn-primary" value="Save as Draft"> -->
                                                <a href="{{ route('submitmr',['tnumber'=>$mr->tnumber]) }}" class="btn btn-primary" name="submit" >Submit</a>
                                                <!-- <input type="button" class="btn btn-danger"  name="cancel" value="Cancel"> -->
                                             @elseif($mr->status ==='S')
                                                <a href="{{ route('approvemr',['tnumber'=>$mr->tnumber]) }}" class="btn btn-primary" name="submit" >Approve</a>
                                                <a href="{{ route('rollbackmr',['tnumber'=>$mr->tnumber]) }}" class="btn btn-primary" name="submit" >Rollback</a>
                                                <!-- <input type="button" class="btn btn-danger"  name="cancel" value="Cancel"> -->
                                             @elseif($mr->status ==='A')
                                                
                                                
                                             @endif

                                        </div>
                                    </div>
                                </form>
                            </div>




                        </div>


                    </div>
                </div>

            </div>

        </div>

    </div>


@endsection


@section('customjs')
    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    <script src="{{ URL::asset('js/viewmaterialrequest.js') }}"></script>

@endsection