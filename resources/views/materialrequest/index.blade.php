@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Material Request</h2>
            {!! Breadcrumbs::render('materialrequest') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Material Requests
                            <small></small>
                        </h5>
                        <div class="ibox-tools">
                            <a class="btn btn-primary btn-sm" href="{{ URL::to('materialrequest/create') }}">Create New Material Request</a>
                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="mr-table">
                                <thead>
                                <tr>
                                    <th>LNo</th>
                                    <th>MR Date</th>
                                    <th>MR Number</th>
                                    <th>JO Number</th>
                                    <th>Source Location</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection

@section('customjs')

    <script src="{{ URL::asset('js/materialrequest.js') }}"></script>


@endsection