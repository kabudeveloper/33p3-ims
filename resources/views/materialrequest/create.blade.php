@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Material Request</h2>
            {!! Breadcrumbs::render('materialrequest') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Create Material Request</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-12">

                                  <form class="form-horizontal" method="post" action="{{ route('materialrequest.store') }}">
                                      {{ csrf_field() }}


                                      <div class="col-lg-6">
                                          <div class="form-group {{ $errors->has('_branch') ? ' has-error' : '' }}">
                                              <label class="col-sm-2 control-label">Branch</label>

                                              <div class="col-sm-8">
                                                  <select name="_branch" class="form-control" readonly>
                                                      <option></option>
                                                      

                                                  </select>
                                                  @if ($errors->has('_branch'))
                                                      <span class="help-block">
                                                  <strong>{{ $errors->first('_branch') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                          </div>

                                          <div class="form-group {{ $errors->has('_location') ? ' has-error' : '' }}">
                                              <label class="col-sm-2 control-label">Source Location</label>

                                              <div class="col-sm-8">
                                                  <select name="_location" class="form-control">
                                                      <option></option>
                                                      @foreach($location as $loc)
                                                          <option value="{{ $loc->id }}">{{ $loc->name }}</option>
                                                      @endforeach

                                                  </select>
                                                  @if ($errors->has('_location'))
                                                      <span class="help-block">
                                                  <strong>{{ $errors->first('_location') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                          </div>

                                          <div class="form-group {{ $errors->has('jonumber') ? ' has-error' : '' }}">
                                              <label class="col-sm-2 control-label">JO Number</label>



                                              <div class="col-sm-8">
                                                  <div class="row">
                                                      <div class="col-sm-10">
                                                          <input type="hidden" name="branchidhidden">
                                                          <input type="hidden" name="customeridhidden">
                                                          <input type="text" name="jonumber" value="{{ old('jonumber') }}" readonly class="form-control">
                                                          @if ($errors->has('jonumber'))
                                                              <span class="help-block">
                                                                 <strong>{{ $errors->first('jonumber') }}</strong>
                                                                </span>
                                                          @endif
                                                      </div>

                                                      <div class="col-sm-2 wd-cust">
                                                          <input class="btn btn-default btn-sm" name="btnjonumber" type="button" value="....">

                                                      </div>

                                                  </div>


                                              </div>


                                          </div>

                                          <div class="form-group">
                                              <div class="col-sm-8 mg-top">
                                                  <button type="submit" class="btn btn-primary">Save </button>
                                              </div>
                                          </div>

                                      </div>


                                      <div class="col-lg-6">
                                          <div class="form-group">
                                              <label class="col-sm-4 control-label">JO Number</label>

                                              <div class="col-sm-8">
                                                  <input type="text" name="tnumber2" readonly class="form-control">
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="col-sm-4 control-label">JO Date</label>

                                              <div class="col-sm-8">
                                                  <input type="text" name="tdate" readonly class="form-control">
                                              </div>
                                          </div>

                                          <div class="form-group">
                                              <label class="col-sm-4 control-label">Status</label>

                                              <div class="col-sm-8">
                                                  <input type="text" name="status" readonly class="form-control">
                                              </div>
                                          </div>

                                      </div>


                                    <div class="hr-line-dashed"></div>

                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>


        <!-- Modal -->
        <div class="modal fade" id="jonumberlistmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Job Order List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablejolist">
                                <thead>
                                <tr>
                                    <th>JO Number</th>
                                    <th>JO Date</th>
                                    <th>Branch ID</th>
                                    <th>Branch</th>
                                    <th>Customer ID</th>
                                    <th>Customer</th>
                                    <th>Notes</th>
                                    <th>Status</th>
                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>




    </div>


@endsection


@section('customjs')
    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    <script src="{{ URL::asset('js/creatematerialrequest.js') }}"></script>

@endsection