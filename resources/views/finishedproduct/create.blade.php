@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Finished Product</h2>
            {!! Breadcrumbs::render('finishedproduct') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Finsihed Product</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">


                                  <form class="form-horizontal" method="post" accept-charset="UTF-8" enctype="multipart/form-data" action="{{ route('finishedproduct.store') }}">
                                      <div class="col-lg-6">
                                      {{ csrf_field() }}
                                      <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code" class="form-control">
                                              @if ($errors->has('code'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('code') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Item Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ old('name') }}" class="form-control">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                      <div class="form-group {{ $errors->has('_category') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Category</label>

                                          <div class="col-sm-8">
                                              <select name="_category" class="form-control" disabled="disabled">
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_category'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_category') }}</strong>
                                                  </span>
                                              @endif
                                           </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_class') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Select Class</label>

                                          <div class="col-sm-8">
                                              <select name="_class" class="form-control">
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_class'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_class') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_class') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Select SubClass</label>

                                          <div class="col-sm-8">
                                              <select name="_subclass" class="form-control">
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_subclass'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_subclass') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                          <div class="form-group {{ $errors->has('storagelocation') ? ' has-error' : '' }}">
                                              <label class="col-sm-4 control-label">Location</label>

                                              <div class="col-sm-8">
                                                  <select name="storagelocation" class="form-control">
                                                      <option></option>
                                                      @foreach($locations as $location)
                                                          <option value="{{ $location->id }}">{{ $location->name }}</option>
                                                      @endforeach

                                                  </select>
                                                  @if ($errors->has('storagelocation'))
                                                      <span class="help-block">
                                                  <strong>{{ $errors->first('_type') }}</strong>
                                                  </span>
                                                  @endif
                                              </div>
                                          </div>





                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('finishedproduct') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>

                                </div>

                                     <div class="col-lg-6">
                                   <div class="form-group">
                                       <label for="image">Upload Picture</label>
                                       <input type="file" name="image"  id="image">
                                   </div>
                               </div>
                                  </form>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection



@section('customjs')

    <script src="{{ URL::asset('js/createfinished.js') }}"></script>
@endsection