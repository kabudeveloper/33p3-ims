@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Finished Product</h2>
            {!! Breadcrumbs::render('finishedproduct') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Item<small></small></h5>
                        <div class="ibox-tools">


                            <a class="btn btn-primary btn-rounded btn-sm" href="{{ URL::to('finishedproduct/create') }}">Add Finish Product</a>

                        </div>
                    </div>






                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="finishedproduct-table">
                                <thead>
                                <tr>
                                    <th>Item Code</th>
                                    <th>Item Name</th>
                                    <th>Class</th>
                                    <th>SubClass</th>
                                    <th>Action</th>

                                </tr>
                                </thead>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection


@section('customjs')

    <script src="{{ URL::asset('js/finishedproduct.js') }}"></script>

@endsection