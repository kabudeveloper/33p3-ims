<!doctype html>
<html lang="en">
<head>
    <script>
        window.print();
        setTimeout(function() {
            window.location.href = "finishedproduct";
        }, 20); 
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{ asset('css/printstyle.css') }}">
    <title>FinishProduct</title>
    <style type="text/css">
        .left{
            margin-left: 17px;
        }
        
        .page-break {
            page-break-after: always;
        }

        .header,
        .footer {
            width: 100%;
            text-align: right;
            position: fixed;
        }

        .header {
            position: fixed;
            top: -50px;
            left: 0px;
            right: 0px;
            height: 50px;
            padding: .5em;
            text-align: center;
        }

        .footer {
            bottom: 5em;
        }

        .pagenum:before {
            content: counter(page);
        }

    </style>


</head>
<body>
<div class="header">
    Page <span class="pagenum"></span>
</div>
<div class="contentbody">


    <table class="noborder">
        <tr>
            <td>
                <img class="left" src="{{ asset('public/img/logo.gif') }}">
            </td>
            <td style="vertical-align: text-top; line-height: .4em;">
                <h3><strong>{{ $company->name }}</strong></h3>
                <p>{{ $company->address }}</p>
                <p>{{ $company->telephone }}</p>

            </td>
        </tr>

    </table>

    <br/>


   
    <br/>
    
    <table class="quotation_order_item left">
        <tr><td colspan="11" style="font-size: 14px;"><h2>Finish Product Detail:</h2></td></tr>
        <tbody>
        <tr>
            <td style="width: 10%;font-size: 14px;">Date:</td>
            <td>{{  $printdate }}</td> 
        </tr>

        <tr>
            <td colspan="8" style="font-size: 14px;">Item Name: {{$id1->code}} - {{$id1->name}}</td>
            
        </tr>
    
        </tbody>

    </table>

    <table class="quotation_order_item left">
        <tbody>
        <tr><td colspan="11" style="font-size: 14px;"><h2>Costing:</h2></td></tr>   
        <tr>
            <th>RM Cost(PHP)</th>
            <th>Labor Cost(PHP)</th>      
            <th>Overhead Cost(PHP)</th>
            <th>Other Material Cost</th>
            <th>Other Cost</th>
            <th>Mark up</th>
        </tr>
        <tr>
            <td><span>{{ number_format($costing->rmcost,2) }}</span></td>
            <td><span>{{ number_format($costing->lbcost,2) }}</span></td>
            <td><span>{{ number_format($costing->ovcost,2) }}</span></td>
            <td><span>{{ number_format($costing->omcost,2) }}</span></td>
            <td><span>{{ number_format($costing->ocost,2) }}<</span></td>
            <td><span>{{ number_format($costing->markup,2) }}</span></td>
        </tr>
        <tr>
            <td style="font-size: 14px;">Cost Price: </td>
            <td colspan="8">{{ number_format($costprice,2) }}</td>
            
        </tr>
        <tr>
            <td style="font-size: 14px;">Selling Price: </td>
            <td colspan="8"> {{ number_format($sellingprice,2) }} </td>
            
        </tr>
        <tr>
            <td style="font-size: 14px;">Commited USD: </td>
            <td colspan="8"> {{ number_format($costing->commitedprice,2) }} </td>
            
        </tr>
        </tbody>
    </table>

    
    <table class="quotation_order_item left">
        <tr><td colspan="10" style="font-size: 14px;"><h2>Raw Material:</h2></td></tr>
        <tbody>
        <tr>
            <th>Raw Materials</th>
            <th>Packing</th>      
            <th>Quantity</th>
             <th>Cost</th>
            <th>Total Cost</th>
            
        </tr>
      
        @foreach($bom as $b)
        
        <tr>           

            <td>{{ $b->name }}</td>
            <td>{{ $b->description }}</td>
            <td>{{ number_format($b->quantity) }}</td>
            <td>{{ number_format($b->cost,2) }}</td>
            <td>{{ number_format($b->totalcost,2) }}</td>             
                
        </tr>
        @endforeach

        </tbody>

        <tr>
        
            <td colspan="4" style="text-align: right;"><strong>GRAND TOTAL:</strong></td>
            <td class="change_order_total_col"><strong><center> {{number_format($total,2)}}</center></strong></td>
        </tr>
    </table>

    <table class="quotation_order_item left">
        <tr><td colspan="10" style="font-size: 14px;"><h2>Process:</h2></td></tr>
        <tbody>
        <tr>
            <th>Name</th>       
             <th>Amount</th>
           
            
        </tr>
      
        @foreach($processcost as $prcscst)
        
        <tr>           

            <td>{{ $prcscst->process_name }}</td>
        
            <td>{{ number_format($prcscst->amount,2) }}</td>             
                
        </tr>
        @endforeach

        </tbody>

        <tr>
            <td colspan="1" style="text-align: right;"><strong>GRAND TOTAL:</strong></td>
            <td class="change_order_total_col"><strong><center> {{number_format($ptotal,2)}}</center></strong></td>
        </tr>
    </table>






</body>
</html>