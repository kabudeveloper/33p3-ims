@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Finisihed Product</h2>
            {!! Breadcrumbs::render('finishedproduct') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Finished Product</h5>

                    </div>
                            @if(Session::has('duplicate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ Session::get('duplicate') }}
                                </div>
                            @endif
                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form  class="form-horizontal" method="POST" accept-charset="UTF-8" enctype="multipart/form-data" action="{{ route('finishedproduct.update',$company->id) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">
                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code"  value="{{ $company->code }}" class="form-control">

                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Item Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ $company->name }}" class="form-control">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                     <div class="form-group {{ $errors->has('_category') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Category</label>

                                          <div class="col-sm-8">
                                              <select name="_category" class="form-control" disabled>
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_category'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_category') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_class') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Class</label>

                                          <div class="col-sm-8">
                                              <select name="_class" class="form-control">
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_class'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_class') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group {{ $errors->has('_subclass') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">SubClass</label>

                                          <div class="col-sm-8">
                                              <select name="_subclass" class="form-control">
                                                  <option></option>


                                              </select>
                                              @if ($errors->has('_subclass'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_subclass') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('storagelocation') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Location</label>

                                          <div class="col-sm-8">
                                              <select name="storagelocation" class="form-control">
                                                  <option></option>
                                                  @foreach($locations as $location)
                                                      <option value="{{ $location->id }}" {{ ($location->id == $company->storagelocation ) ? 'selected' : '' }}>{{ $location->name }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('storagelocation'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_type') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($company->isacombo)
                                                          <input type="checkbox" value="1" checked name="isacombo">  Combo
                                                      @else
                                                          <input type="checkbox" name="isacombo">  Combo

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>


                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <input type="hidden" id="bomtotalcost" name="bomtotalcost">
                                              <button type="button" class="btn btn-info" id="costing">Costing</button>
                                          </div>
                                      </div>






                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('finishedproduct') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                           <!--  <button type="button" class="btn btn-primary">Print </button> -->
                                           <a href="{{ route('printfinishproduct',['id'=>$company->id]) }}" class="btn btn-primary" target="_blank" name="submit"> Print </a>
                                        </div>
                                    </div>
                                
                                </div>
                                <div class="col-lg-6">
                                    <img src="{{ $imagepath  }} "  class="img-thumbnail">

                                    <div class="form-group">
                                       <label for="image">Upload Picture</label>
                                       <input type="file" name="image"  id="image">
                                   </div>
                                </div>
                              </form> 
                          </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row" id="divotherproduct">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Other Product
                            <small></small>
                        </h5>
                        <div class="ibox-tools">

                            <a class="btn btn-primary btn-rounded btn-sm" id="otherprod" href="{{ URL::to('finishedproduct/'.$company->id.'/otherproduct/create') }}">Add New</a>

                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="finishedproduct-otherproduct-table">
                                <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Packing</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Total Price</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th colspan="4" style="text-align:right;">Page Total:</th>
                                    <th colspan="2"></th>
                                </tr>

                                </tfoot>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Process
                            <small></small>
                        </h5>
                        <div class="ibox-tools">

                            <a class="btn btn-primary btn-rounded btn-sm" href="{{ URL::to('finishedproduct/'.$company->id.'/addprocess') }}">Add New</a>

                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="process-table">

                                <thead>
                                <tr>
                                    <th>Process Name</th>        
                                    <th>Amount</th>                              
                                    <th></th>
                                </tr>
                                </thead>
                                <tfoot>
                                 <tr>
                                      <th colspan="1" style="text-align:right">Page Total:</th>
                                      <th colspan="2"></th>
                                  </tr>
                                </tfoot>

                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Bill Of Material
                            <small></small>
                        </h5>
                        <div class="ibox-tools">

                            <a class="btn btn-primary btn-rounded btn-sm" href="{{ URL::to('finishedproduct/'.$company->id.'/bom/create') }}">Add New</a>

                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="finishedproduct-bom-table">

                                <thead>
                                <tr>
                                    <th>Raw Material</th>
                                    <th>Packing</th>
                                    <th>Quantity</th>
                                    <th>Cost</th>
                                    <th width="2%">USD</th>
                                    <th>Total Cost</th>
                                    <th width="5%">USD</th>
                                    <th>Direct RM</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th colspan="5" style="text-align:right;">Page Total:</th>
                                    <th colspan="4"></th>
                                </tr>

                                </tfoot>


                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>


        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">

                    <div class="ibox-title">
                        <h5>Packing
                            <small></small>
                        </h5>
                        <div class="ibox-tools">

                            <a class="btn btn-primary btn-rounded btn-sm" href="{{ URL::to('finishedproduct/'.$company->id.'/finishedpacking/create') }}">Add New</a>

                        </div>
                    </div>

                    <div class="box-content white-bg">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="finishedproduct-packing-table">
                                <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Packagiing</th>
                                    <th>UM</th>
                                    <th>UM Value</th>
                                    <th>Inner Qty</th>
                                    <th>Level</th>
                                    <th></th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>




        <div class="modal fade" tabindex="-1" id="costingmodal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Costing</h4>
                    </div>
                    <div class="modal-body">

                        <form class="form-horizontal" id="costingform">
                            <div class="form-group">
                                <label for="_item" class="col-sm-2 control-label">Product</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control"  name="mainitem" >
                                    <input type="hidden" class="form-control" name="_item"  >
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rmcost" class="col-sm-4 control-label">RM Cost (PHP)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="rmcost">
                                            <input type="hidden" class="form-control" name="rmcosthidden">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label for="rmcost" class="col-sm-4 control-label">Other Cost (PHP)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="ocost">
                                        </div>
                                  </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="lbcost" class="col-sm-4 control-label">Labor Cost (PHP)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="lbcost">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="markup" class="col-sm-4 control-label">Cost Price (PHP)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="costprice" readonly>
                                        </div>
                                    </div>


                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ovcost" class="col-sm-4 control-label">Overhead Cost (PHP)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="ovcost">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="markup" class="col-sm-4 control-label">Mark up</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="markup">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="omcost" class="col-sm-4 control-label">Other Material Cost (PHP)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="omcost">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="sellingprice" class="col-sm-4 control-label">Selling Price (PHP)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="sellingprice" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="sellingprice" class="col-sm-4 control-label">Commited Price(USD)</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="cprice">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" class="btn btn-primary btn-rounded btn-block compute"><i class="fa fa-check"></i>
                                        Compute</button>
                                </div>
                            </div>
                        </form>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button"  class="btn btn-primary" id="savecosting">Save</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


    </div>


@endsection



@section('customjs')
   <script type="text/javascript">

    var catid = '{{ $company->_category }}';
    var classid = '{{ $company->_class }}';
    var subclassid = '{{ $company->_subclass }}';
    var APP_URL = {!! json_encode(url('/')) !!};
  </script>
    <script src="{{ URL::asset('js/editfinishedproduct.js') }}"></script>
    <script src="{{ URL::asset('js/processcost.js') }}"></script>
    <script src="{{ URL::asset('js/finishedproduct-otherproduct.js') }}"></script>
    <script src="{{ URL::asset('js/finishedproduct-bom.js') }}"></script>
    <script src="{{ URL::asset('js/finishedproduct-packing.js') }}"></script>


@endsection