@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Purcahse Order Instructions</h2>
            {!! Breadcrumbs::render('purchasing') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add PO Instructions</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form class="form-horizontal" method="post" action="{{ route('poinstruction.store') }}">
                                      {{ csrf_field() }}
                                      <div class="form-group {{ $errors->has('instruction') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Instruction</label>

                                          <div class="col-sm-8">
                                            <input type="hidden" name="trn" value="{{$trn}}">
                                              <input type="text" name="instruction" class="form-control">
                                              @if ($errors->has('instruction'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('instruction') }}</strong>
                                                  </span>
                                              @endif

                                          </div>
                                      </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ URL::to('purchasing/'.$trn ).'/edit' }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection
