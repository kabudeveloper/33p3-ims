@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Categories</h2>
            {!! Breadcrumbs::render('category') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Other Product</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('duplicate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ Session::get('duplicate') }}
                                </div>
                            @endif
                               <div class="col-lg-7">

                                  <form  class="form-horizontal" method="POST" id="addvendoritem"  action="{{ route('finishedproduct.otherproduct.update',[$composition->_item,$composition->_subitem]) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">


                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Item</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="name"  value="{{ $composition->name }}" readonly class="form-control">

                                          </div>
                                      </div>


                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Product Code</label>

                                          <div class="col-sm-8">

                                              <div class="row">
                                                  <div class="col-sm-6">
                                                      <input type="text" name="_vendor" value="{{ $composition->code }}"  class="form-control">
                                                      <input type="hidden" name="_hiddenvendorid"  value="{{ $composition->id }}" >
                                                      <div class="_vendor">

                                                      </div>


                                                  </div>

                                                  <div class="col-sm-6">

                                                      <button id="vc" type="submit" class="btn btn-info btn-sm"
                                                              data-loading-text="Verifying..." autocomplete="off"

                                                      >Verify Code
                                                      </button>


                                                      <button id="fv" type="button" class="btn btn-success btn-sm"
                                                      >
                                                          Find Prod
                                                      </button>

                                                  </div>



                                              </div>


                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('vendorname') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Product Name</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="vendorname"  value="{{ $composition->name  }}" class="form-control">

                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('_packaging') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Packaging</label>

                                          <div class="col-sm-8">
                                              <select name="_packaging" class="form-control">
                                                  <option></option>
                                                  @foreach($packaging as $pack)
                                                      <option value="{{ $pack->id }}" {{ ($composition->packing == $pack->id ? "selected":"") }}>{{ $pack->description }}</option>
                                                  @endforeach

                                              </select>
                                              @if ($errors->has('_packaging'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('_packaging') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Quantity</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="quantity" value="{{ $composition->quantity }}" class="form-control">

                                          </div>
                                      </div>






                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ URL::to('finishedproduct/'.$composition->_item).'/edit' }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

            <div class="modal fade" tabindex="-1" id="findVendorModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Find Product</h4>
                        </div>
                        <div class="modal-body">

                            <form class="form-inline" id="formvendorsearch">
                                <div class="form-group">
                                    <label for="criteria">Criteria</label>
                                    <select class="form-control"  name="criteria" id="criteria">
                                        <option value="vendorcode">Code</option>
                                        <option value="vendorname">Name</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="valuesearch" >Value</label>
                                    <input type="email" class="form-control" id="valuesearch" name="valuesearch">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="containssearch" value="1"> Contains
                                    </label>
                                </div>
                                <button type="button" class="btn btn-primary btn-sm" id="searchvendor"><i class="fa fa-search" aria-hidden="true"></i>
                                    Search</button>
                            </form>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive" id="findrawvendortable">
                                        <table class="table table-bordered table-hover tablemodal">
                                            <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Name</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" id="findselectvendor" class="btn btn-primary">Select</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

        </div>


        <div class="modal fade" tabindex="-1" id="costingmodal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Costing</h4>
                    </div>
                    <div class="modal-body">

                        <form class="form-horizontal" id="costingform">
                            <div class="form-group">
                                <label for="_item" class="col-sm-2 control-label">Product</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="subitem" >
                                    <input type="hidden" class="form-control" name="_item"  >
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rmcost" class="col-sm-4 control-label">RM Cost</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="rmcost">
                                            <input type="hidden" class="form-control" name="rmcosthidden">
                                        </div>
                                    </div>
                                </div>

                            </div>



                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="lbcost" class="col-sm-4 control-label">Labor Cost</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="lbcost">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="markup" class="col-sm-4 control-label">Mark up</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="markup">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ovcost" class="col-sm-4 control-label">Overhead Cost</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="ovcost">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="markup" class="col-sm-4 control-label">Cost Price</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="costprice">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="omcost" class="col-sm-4 control-label">Other Material Cost</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="omcost">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="sellingprice" class="col-sm-4 control-label">Selling Price</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="sellingprice">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="button" class="btn btn-primary btn-rounded btn-block compute"><i class="fa fa-check"></i>
                                        Compute</button>
                                </div>
                            </div>
                        </form>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button"  class="btn btn-primary" id="savecosting">Save</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


    </div>


@endsection




@section('customjs')

    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    <script src="{{ URL::asset('js/edit-otherproduct-item.js') }}"></script>


@endsection