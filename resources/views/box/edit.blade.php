@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Boxes</h2>
            {!! Breadcrumbs::render('box') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Box</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                               <div class="col-lg-6">

                                  <form  class="form-horizontal" method="POST" action="{{ route('box.update',$company->id) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">
                                      <div class="form-group">
                                          <label class="col-sm-4 control-label">Code</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="code"  value="{{ $company->code }}" class="form-control">
                                              @if ($errors->has('code'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('code') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>


                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Branch Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ $company->name }}" class="form-control">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>





                                    <div class="form-group {{ $errors->has('height') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Height ( in )</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="height" value="{{ $company->height }}" class="form-control">
                                            @if ($errors->has('height'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('height') }}</strong>
                                                  </span>
                                            @endif

                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('width') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Width ( in )</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="width" value="{{ $company->width }}" class="form-control">
                                            @if ($errors->has('width'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('width') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('length') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Length ( in )</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="length"  value="{{ $company->length }}" class="form-control" placeholder="">
                                            @if ($errors->has('length'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('length') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>


                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($company->isactive)
                                                          <input type="checkbox" value="1" checked name="isactive">  Active
                                                      @else
                                                          <input type="checkbox" name="isactive">  Active

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>



                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ route('box') }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save </button>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection



@section('customjs')

    <script src="{{ URL::asset('js/branchcompany.js') }}"></script>

@endsection