@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Sales Order Details</h2>

        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Edit Sales Order Detail</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('duplicate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    <strong>{{ Session::get('duplicate') }} {{$errors->first()}}</strong>
                                </div>
                            @endif
                            <form class="form-horizontal" method="post" action="{{ route('salesorderdetail.update',$sod->tnumber) }}">
                                <div class="col-lg-6">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="form-group {{ $errors->has('tnumber') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Sales Order No.</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="tnumber" readonly value="{{ $sod->tnumber  }}"
                                                   class="form-control">
                                            @if ($errors->has('tnumber'))
                                                <span class="help-block">
                                              <strong>{{ $errors->first('tnumber') }}</strong>
                                              </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('code') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Code</label>

                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <input type="text" name="code" value="{{ $item->code }}" class="form-control">
                                                    @if ($errors->has('code'))
                                                        <span class="help-block">
                                                  <strong>{{ $errors->first('code') }}</strong>
                                                  </span>
                                                    @endif
                                                </div>


                                            </div>


                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('_item') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Item Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="_item" value="{{ $item->name }}" class="form-control">
                                            <input type="hidden" name="itemhidden" value="{{ $sod->_item }}">
                                            @if ($errors->has('_item'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('_item') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('packing') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Packing</label>

                                        <div class="col-sm-8">
                                            <select name="packing" class="form-control" >
                                                <option></option>
                                            </select>
                                            @if ($errors->has('packing'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('packing') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('quantity') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Quantity</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="quantity" value="{{ $sod->quantity }}"
                                                   class="form-control">
                                            @if ($errors->has('quantity'))
                                                <span class="help-block">
                                              <strong>{{ $errors->first('quantity') }}</strong>
                                              </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ URL::to('salesorder/'.$sod->tnumber).'/edit' }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-lg-6">
                                    <fieldset class="costing">
                                        <legend class="costing">Costing</legend>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="rmcost" class="col-sm-4 control-label">RM Cost</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" readonly name="rmcost">
                                                        <input type="hidden" class="form-control" name="rmcosthidden">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="lbcost" class="col-sm-4 control-label">Labor
                                                        Cost</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" readonly name="lbcost">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="markup" class="col-sm-4 control-label">Cost
                                                        Price</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" readonly name="costprice">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="ovcost" class="col-sm-4 control-label">Overhead
                                                        Cost</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" readonly name="ovcost">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="markup" class="col-sm-4 control-label">Mark up</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" name="markup">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="omcost" class="col-sm-4 control-label">Other Material
                                                        Cost</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" readonly name="omcost">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sellingprice" class="col-sm-4 control-label">Selling
                                                        Price</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" readonly  name="sellingprice">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="clear: both;">
                                                <div class="form-group">
                                                    <label for="finalsellingprice" class="col-sm-4 control-label">Final Selling
                                                        Price</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control"  name="finalsellingprice">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="clientcurrencysellingprice" class="col-sm-4 control-label">Client Currency Selling
                                                        Price</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control"  name="clientcurrencysellingprice">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="finishprodlistmodal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Finished Product List</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <table width="100%" class="table table-bordered table-striped" id="tablejodetail">
                                <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Name</th>

                                </tr>
                                </thead>

                            </table>
                        </div>


                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>


    </div>


@endsection



@section('customjs')

    <script type="text/javascript">

        var APP_URL = {!! json_encode(url('/')) !!};
    </script>

    <script src="{{ URL::asset('js/editsodetail.js') }}"></script>
@endsection