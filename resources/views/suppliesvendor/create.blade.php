@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2></h2>
            {!! Breadcrumbs::render('contact') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>Add Supplies Vendor</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">
                            @if(Session::has('duplicate'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                    {{ Session::get('duplicate') }}
                                </div>
                            @endif
                            <div class="col-lg-7">


                                <form class="form-horizontal" method="post" id="addvendoritem"
                                      action="{{ route('supplies.suppliesvendor.store',$item['id']) }}">
                                    {{ csrf_field() }}

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Item</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name" value="{{ $item->name }}" readonly
                                                   class="form-control">

                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('_vendor') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Vendor Code</label>

                                        <div class="col-sm-8">

                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <input type="text" name="_vendor" value="{{ old('_vendor') }}"
                                                           class="form-control">
                                                    <input type="hidden" name="_hiddenvendorid">
                                                    @if ($errors->has('_vendor'))
                                                        <span class="help-block">
                                                  <strong>{{ $errors->first('_vendor') }}</strong>
                                                  </span>
                                                    @endif
                                                    <div class="_vendor">

                                                    </div>


                                                </div>

                                                <div class="col-sm-6">

                                                    <button id="vc" type="submit" class="btn btn-info btn-sm"
                                                            data-loading-text="Verifying..." autocomplete="off"

                                                    >Verify Code
                                                    </button>


                                                    <button id="fv" type="button" class="btn btn-success btn-sm"
                                                            >
                                                        Find Vendor
                                                    </button>

                                                </div>


                                            </div>


                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('vendorname') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Vendor Name</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="vendorname" class="form-control">

                                        </div>
                                    </div>


                                    <div class="form-group {{ $errors->has('cost') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Cost</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="cost" class="form-control">
                                            @if ($errors->has('cost'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('cost') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <div>
                                                <label>

                                                    <input type="checkbox" value="1" checked name="isactive"> Active


                                                </label>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ URL::to('supplies/'.$item['id']).'/edit' }}" class="btn btn-white">Cancel</a>
                                            <button type="submit" class="btn btn-primary">Save</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="modal fade" tabindex="-1" id="findVendorModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Find Vendor</h4>
                    </div>
                    <div class="modal-body">

                            <form class="form-inline" id="formvendorsearch">
                                <div class="form-group">
                                    <label for="criteria">Criteria</label>
                                    <select class="form-control"  name="criteria" id="criteria">
                                        <option value="vendorcode">Code</option>
                                        <option value="vendorname">Name</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="valuesearch" >Value</label>
                                    <input type="email" class="form-control" id="valuesearch" name="valuesearch">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="containssearch" value="1"> Contains
                                    </label>
                                </div>
                                <button type="button" class="btn btn-primary btn-sm" id="searchvendor"><i class="fa fa-search" aria-hidden="true"></i>
                                    Search</button>
                            </form>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive" id="findrawvendortable">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Name</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="findselectvendor" class="btn btn-primary">Select</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


    </div>


@endsection
<script type="text/javascript">

    var APP_URL = {!! json_encode(url('/')) !!};
</script>


@section('customjs')

    <script src="{{ URL::asset('js/vendor-item.js') }}"></script>

@endsection