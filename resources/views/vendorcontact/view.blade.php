@extends('layouts.layout')

@section('breadcrumbpage')
    <div class="row white-bg page-heading">
        <div class="col-lg-10">
            <h2>List of Vendor Contact</h2>
            {!! Breadcrumbs::render('vendorcontact') !!}
        </div>
        <div class="col-lg-2">

        </div>
    </div>
@endsection

@section('content')


    <div class="wrapper-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="white-bg box-wrapper">
                    <div class="box-tool-bar">
                        <h5>View Vendor Contact</h5>

                    </div>

                    <div class="box-content white-bg">
                        <div class="row">

                               <div class="col-lg-6">

                                  <form  class="form-horizontal" method="POST" action=" {{ URL::to('vendors/'.$id.'/vendconupdate/'.$company->name) }}">
                                      {{ csrf_field() }}

                                      <input type="hidden" name="_method" value="PUT">



                                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label class="col-sm-4 control-label">Contact Person</label>

                                        <div class="col-sm-8">
                                            <input type="text" name="name"  value="{{ $company->name }}" class="form-control" readonly>
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                  <strong>{{ $errors->first('name') }}</strong>
                                                  </span>
                                            @endif
                                        </div>
                                    </div>


                                      <div class="form-group {{ $errors->has('position') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Position</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="position"  value="{{ $company->position }}" class="form-control" readonly>
                                              @if ($errors->has('glcode'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('position') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('mobile') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Mobile</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="mobile"  value="{{ $company->mobile }}" class="form-control" readonly>
                                              @if ($errors->has('mobile'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('mobile') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('landline') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Landline</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="landline" value="{{ $company->landline }}" class="form-control" readonly>
                                              @if ($errors->has('landline'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('landline') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('fax') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Fax</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="fax" value="{{ $company->fax }}" class="form-control" readonly>
                                              @if ($errors->has('fax'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('fax') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>

                                      <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                          <label class="col-sm-4 control-label">Email</label>

                                          <div class="col-sm-8">
                                              <input type="text" name="email" value="{{ $company->email }}" class="form-control" readonly>
                                              @if ($errors->has('email'))
                                                  <span class="help-block">
                                                  <strong>{{ $errors->first('email') }}</strong>
                                                  </span>
                                              @endif
                                          </div>
                                      </div>



                                      <div class="form-group">
                                          <div class="col-sm-6 col-sm-offset-4">
                                              <div>
                                                  <label>
                                                      @if($company->isprimary)
                                                          <input type="checkbox" value="1" checked name="isactive" disabled>  Primary
                                                      @else
                                                          <input type="checkbox" name="isactive" disabled>  Primary

                                                      @endif

                                                  </label>
                                              </div>
                                          </div>
                                      </div>





                                      <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-6 col-sm-offset-4">
                                            <a href="{{ URL::to('vendors/'.$id).'/view' }}" class="btn btn-white">Back</a>
                                        </div>
                                    </div>
                                </form>
                                </div>
                          </div>
                    </div>
                </div>

            </div>

        </div>
    </div>


@endsection




@section('customjs')

    <script src="{{ URL::asset('js/category.js') }}"></script>


@endsection