<?php

use Illuminate\Database\Seeder;

class CreateUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::CREATE([
            'username'=>'john',
            'firstname'=>'john',
            'lastname'=>'john',
            '_group'=>'1',
            'isactive'=>'1',
            'resetpassword'=>'0',
            'willexpire'=>'0',
            'loggedin'=>'true',
            'locked'=>'false',
            'createddatetime'=>'2015-05-26 00:00:00',
            'createdby'=>'1',
            'email'=>'john@gmail.com',
            'password'=>Hash::make('john123')
        ]);
    }
}
